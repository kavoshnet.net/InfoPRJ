﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="sabtsoft1.aspx.cs" Inherits="Info.Admin.sabtsoft1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="container" class="container full-width">
        <!--------------------------------------------------------------------------------------------------------->
        <script type="text/javascript">
            $(document).ready(function () {

                //$(document).on("click", "[id*=lbtdelete]", function () {
                //    $("#dialog").show("explode", 500);
                //    $("#form").hide("puff", 200);
                //    return false;
                //});
                if ($("#<%=messagehf.ClientID %>").val() === "dodelete") {
                    $("#dialog").show("explode", 500);
                    $("#form").hide("puff", 200);
                    $("#<%=messagehf.ClientID %>").val("");
                        return false;
                    }

                $(document).on("click", "[id*=btndelcancel]", function () {
                    $("#form").show("explode", 500);
                    $("#dialog").hide("puff", 200);
                    return false;
                });
            });
        </script>
        <div id="dialog" class="col-md-4 col-md-offset-4" style="display: none">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <div class="panel-title">
                        <p class="text-center">
                            حذف اطلاعات
                        </p>
                    </div>
                </div>
                <div class="panel-body">
                    <p class="text-center">
                        آیا برای حذف اطمینان دارید؟
                    </p>
                    <p class="text-center">
                        <asp:Button ID="btndelok" runat="server" CssClass="btn btn-danger" OnClick="btndelok_Click" Text=" حذف اطلاعات" Width="175px" />
                        <asp:Button ID="btndelcancel" runat="server" CssClass="btn btn-success" Text=" انصراف" Width="175px" />
                    </p>
                </div>
            </div>
        </div>

        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------back and show err message------------------------------------------>
        <script type="text/javascript">
            $(document).ready(function () {
                //back btn
                $(document).on("click", "[id*=btnback]", function () {
                    $("#form1").data("bootstrapValidator").resetForm(true);
                    window.location.replace("sabtsoft.aspx");// = window.location;
                    //window.refresh();
                    //window.reload();
                });

                //errore message
                if ($("#<%=messagehf.ClientID %>").val() == "1") {
                    $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                    $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                    $("#message").fadeIn().delay(3000).fadeOut();
                    $("#<%=messagehf.ClientID %>").val("");
                    setTimeout(function () { document.location = "sabtsoft.aspx"; }, 3000);
                    //window.location.replace("sabtmddjo.aspx");
                }
                //errore message
            });
        </script>
        <!--------------------------------------------------------------------------------------------------------->
        <!---------------------------------------validator--------------------------------------------------------->
        <script type="text/javascript">
            //validator
            $(document).ready(function () {
                $("#form1").bootstrapValidator({
                    message: "این مقدار صحیح نمیباشد",
                    fields: {
                        "<%= ddlcat_id_soft.UniqueID %>": {
                            message: "این مقدار صحیح نیست",
                            validators: {
                                notEmpty: {
                                    message: "این فیلد نمیتواند خالی باشد"
                                }
                            }

                        },

                        "<%= txtname_soft.UniqueID %>": {
                            message: "این مقدار صحیح نیست",
                            validators: {
                                notEmpty: {
                                    message: "این فیلد نمیتواند خالی باشد"
                                }
                            }

                        },
                        <%--                        "<%= txtcontent_news.UniqueID %>": {
                            message: "این مقدار صحیح نیست",
                            validators: {
                                notEmpty: {
                                    message: "این فیلد نمیتواند خالی باشد"
                                },
                                regexp: {
                                    regexp: /^[a-zA-Z0-9ا-ی ؤءأإئ\-]+$/,
                                    message: "تنها مقادیر حرفی ، عددی و خط فاصله صحیح میباشد"
                                }
                            }

                        },--%>

                    }

                });
                //validator
            });
        </script>

        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->

        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <p id="messagetext" class="text-center"><strong>>پیام خطا </strong></p>
                    </div>
                </div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row" id="form">
            <div class="col-lg-12">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            ثبت نرم افزار 
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label2" runat="server" Text="گروه نرم افزار"></asp:Label>
                                    <asp:DropDownList ID="ddlcat_id_soft" runat="server" Width="350px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label9" runat="server" Text="نام نرم افزار"></asp:Label>
                                    <asp:TextBox ID="txtname_soft" runat="server" Width="350px" placeholder="نام نرم افزار" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label1" runat="server" Text="پیوست"></asp:Label>
                                    <asp:FileUpload ID="fuatt_soft" runat="server" Width="350px" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <br />
                                <p class="text-center">
                                    <asp:Button ID="btnsabt" runat="server" CssClass="btn btn-success" OnClick="btnsabt_Click" Text="ثبت نرم افزار" Width="175px" />
                                    <input type="button" id="btnback" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            نرم افزارهای ثبت شده
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <asp:GridView ID="gvrsoft" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvrsoft_RowCommand" AllowPaging="True" OnPageIndexChanging="gvrsoft_PageIndexChanging" OnPageIndexChanged="gvrsoft_PageIndexChanged">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ش" ItemStyle-Width="3%">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="عنوان نرم افزار">
                                        <ItemTemplate><%#SubString(Eval("name_soft").ToString(),20)%></ItemTemplate>
                                        <ItemStyle Width="15%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="گروه">
                                        <ItemTemplate><%#SubString(Eval("tblcategory.name_cat").ToString(),15)%></ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                    <%--                                    <asp:TemplateField HeaderText="جنسیت">
                                        <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="سابقه وام">
                                        <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />--%>
                                    <asp:TemplateField HeaderText="پیوست">
                                        <ItemTemplate><%#Eval("att_soft")!=null?SubString(Eval("att_soft").ToString(),20):"ندارد"%></ItemTemplate>
                                        <ItemStyle Width="15%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="dtesabt_soft" HeaderText="تاریخ">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="timsabt_soft" HeaderText="زمان">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="کاربر">
                                        <ItemTemplate><%#Eval("tbluser.lname_user").ToString()+" " + Eval("tbluser.fname_user").ToString()%></ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="عملیات">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbtedit" CommandArgument='<%#Eval("id_soft")%>' CommandName="doedit" CssClass="btn btn-success btn-xs">ویرایش</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lbtdelete" CommandArgument='<%#Eval("id_soft")%>' CommandName="dodelete" CssClass="btn btn-danger btn-xs">حذف</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lbtact" CommandArgument='<%#Eval("id_soft")%>' CommandName="doact" CssClass="btn btn-warning btn-xs">فعال</asp:LinkButton>
                                        </ItemTemplate>
                                            <ItemStyle Width="15%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
