﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using InfoModel;
using MyObjects;

namespace Info.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
            txtusername_user.Focus();

            //txtusername_user.Text = "admin";
            //txtpass_user.Text = "123456";
            //btnLogin_Click(sender, null);
        }


        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                var query = from p in _edbcontext.tbluser
                            where p.username_user.Equals(txtusername_user.Text) && p.pass_user.Equals(txtpass_user.Text)
                            select p;
                var user = query.FirstOrDefault();
                if (user != null)
                {
                    Session["vorod"] = "ok";
                    Session["id_user"] = user.id_user.ToString();
                    Session["lname_user"] = user.lname_user.ToString();
                    Session["fname_user"] = user.fname_user.ToString();
                    Session["isadmin_user"] = Convert.ToBoolean(user.isadmin_user.ToString()) == true ? "isadmin" : "isuser";
                    Session["isact_user"] = Convert.ToBoolean(user.isact_user.ToString()) == true ? "isact" : "noact";
                    Session["sex_user"] = Convert.ToBoolean(user.sex_user.ToString()) == true ? "آقای" : "خانم";
                    Response.Redirect("index.aspx");
                }
                else
                {
                    showerrore("1", "خطای ورود اطلاعات", "نام کاربری یا کلمه عبور صحیح نیست دوباره تلاش کنید");

                }
            }
            catch (Exception)
            {
                showerrore("1", "خطای ورود اطلاعات", "در ورود کاربر خطایی رخ داده است لطفا دوباره تلاش کنید");
                //showerrore("1",ex.Message,"خطای ورود اطلاعات");
            }
        }

    }
}
