﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfoModel;
using MyObjects;
using System.Web.Configuration;

namespace Info.Admin
{
    public partial class sabtsoft1 : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction
        protected void setgvrsoft()
        {
            gvrsoft.DataSource =
                (from n in _edbcontext.tblsoft
                 orderby n.dtesabt_soft descending, n.timsabt_soft descending
                 select n).ToList();
            gvrsoft.DataBind();
        }
        protected void setddlcat_id_soft()
        {
            ddlcat_id_soft.DataSource = (from c in _edbcontext.tblcategory
                                         where c.group_cat == 2
                                         orderby c.id_cat ascending
                                         select c).ToList();
            ddlcat_id_soft.DataTextField = "name_cat";
            ddlcat_id_soft.DataValueField = "id_cat";
            ddlcat_id_soft.DataBind();
            ddlcat_id_soft.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }


        protected void initialstate()
        {
            setddlcat_id_soft();
            setgvrsoft();
            Session["emal"] = "donew";
        }
        protected void clsfield()
        {
            ddlcat_id_soft.SelectedIndex = -1;
            Session["emal"] = "donew";
        }
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }

        protected string SubString(object Text, object Length)
        {
            string StringText = Text.ToString();
            int StringLength = int.Parse(Length.ToString());
            if (StringText.Length > StringLength)
            {
                return StringText.Substring(0, StringLength) + "...";
            }
            else
            {
                return StringText;
            }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if (!IsPostBack)
                    {
                        initialstate();
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }
        protected void btnsabt_Click(object sender, EventArgs e)
        {
            if (Session["emal"].ToString() == "donew")
            {
                try
                {
                    int id_user = int.Parse(Session["id_user"].ToString());
                    InfoModel.tblsoft _soft = new InfoModel.tblsoft();
                    _soft.user_id_soft = id_user;
                    _soft.cat_id_soft = Convert.ToInt16(ddlcat_id_soft.SelectedValue);
                    _soft.name_soft = txtname_soft.Text;
                    _soft.dtesabt_soft = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                    _soft.timsabt_soft = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                              DateTime.Now.Minute.ToString().PadLeft(2, '0');
                    //file upload
                    if (fuatt_soft.HasFile && fuatt_soft.FileContent.Length > 0)
                    {
                        string fileName = fuatt_soft.FileName;
                        //string fname = Path.GetFileName(file.FileName);
                        fuatt_soft.SaveAs(Server.MapPath(Path.Combine("~/attsoft/", fileName)));
                        _soft.att_soft = fuatt_soft.FileName;
                    }
                    //file upload
                    _soft.isact_soft = true;
                    _edbcontext.tblsoft.Add(_soft);
                    _edbcontext.SaveChanges();
                    showerrore("1", "پیام سیستم", "نرم افزار درسیستم ثبت گردید");
                }
                catch (Exception)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else if (Session["emal"].ToString() == "doedit")
            {
                try
                {
                    int id_user = int.Parse(Session["id_user"].ToString());
                    int id_soft = int.Parse(Session["id_soft"].ToString());
                    var query = from n in _edbcontext.tblsoft
                                where n.id_soft == id_soft
                                select n;
                    var _soft = query.FirstOrDefault();
                    _soft.id_soft = id_soft;
                    _soft.user_id_soft = id_user;
                    _soft.cat_id_soft = Convert.ToInt16(ddlcat_id_soft.SelectedValue);
                    _soft.name_soft = txtname_soft.Text;
                    _soft.dtesabt_soft = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                    _soft.timsabt_soft = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                              DateTime.Now.Minute.ToString().PadLeft(2, '0');
                    //file upload
                    if (fuatt_soft.HasFile && fuatt_soft.FileContent.Length > 0)
                    {
                        string fileName = fuatt_soft.FileName;
                        //string fname = Path.GetFileName(file.FileName);
                        fuatt_soft.SaveAs(Server.MapPath(Path.Combine("~/attsoft/", fileName)));
                        _soft.att_soft = fuatt_soft.FileName;
                    }
                    //file upload
                    _soft.isact_soft = true;
                    _edbcontext.SaveChanges();
                    showerrore("1", "پیام سیستم", "نرم افزار درسیستم ثبت گردید");
                }
                catch (Exception)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }

            }
        }
        protected void btnnew_Click(object sender, EventArgs e)
        {
            clsfield();
        }

        protected void gvrsoft_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "doedit":
                    {
                        Session["id_soft"] = e.CommandArgument.ToString();
                        Session["emal"] = "doedit";

                        int id_soft = int.Parse(Session["id_soft"].ToString());
                        var query = from n in _edbcontext.tblsoft
                                    where n.id_soft == id_soft
                                    select n;
                        var _soft = query.FirstOrDefault();

                        ddlcat_id_soft.SelectedValue = _soft.cat_id_soft.ToString();
                        txtname_soft.Text = _soft.name_soft.ToString();
                        //hfield.Value = "displaysabtvam";
                        break;
                    }
                case "dodelete":
                    {
                        Session["id_soft"] = e.CommandArgument.ToString();
                        Session["emal"] = "dodelete";

                        messagehf.Value = "dodelete";
                        break;
                    }

            }
        }

        protected void gvrsoft_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvrsoft.PageIndex = e.NewPageIndex;
        }
        protected void btndelok_Click(object sender, EventArgs e)
        {
            int id_soft = int.Parse(Session["id_soft"].ToString());
            var query = from n in _edbcontext.tblsoft
                        where n.id_soft == id_soft
                        select n;
            var _soft = query.FirstOrDefault();
            _edbcontext.tblsoft.Remove(_soft);
            _edbcontext.SaveChanges();
            showerrore("1", "پیام سیستم", "نرم افزار از سیستم حذف گردید");

            //Response.Redirect("~/Admin/sabtsoft.aspx");

        }

        protected void gvrsoft_PageIndexChanged(object sender, EventArgs e)
        {
            //gvrsoft.PageIndex = gvrsoft.SelectedIndex+1 + (gvrsoft.PageIndex * gvrsoft.PageSize);
        }

        /*protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("sabtmddjo.aspx");
        }*/

    }
}