﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="sabtnews.aspx.cs" Inherits="Info.sabtnews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <script type="text/javascript">
        $(document).ready(function () {

            //back btn
            $(document).on("click", "[id*=btnback]", function () {
                $("#form1").data("bootstrapValidator").resetForm(true);
                $("#<%=hfield.ClientID %>").val("hidedook");
                setTimeout(function () { document.location = "sabtfaz2.aspx"; }, 3000);
                //window.refresh();
                //window.reload();
            });
            //back sabtvam btn
            $(document).on("click", "[id*=btnbacksabtvam]", function () {
                $("#form1").data("bootstrapValidator").resetForm(true);
                $("#<%=hfield.ClientID %>").val("hidesabtvam");
                setTimeout(function () { document.location = "sabtfaz2.aspx"; }, 3000);
                //window.refresh();
                //window.reload();
            });

            if ($("#<%=hfield.ClientID %>").val() === "displaydook") {
                $("#sabtnokfile").fadeIn(1000);
                $("#tabpanel").fadeOut(0);

            }
            if ($("#<%=hfield.ClientID %>").val() === "hidedook") {
                $("#tabpanel").fadeIn(1000);
                $("#sabtnokfile").fadeOut(0);

            }
            if ($("#<%=hfield.ClientID %>").val() === "displaysabtvam") {
                $("#sabtvam").fadeIn(1000);
                $("#tabpanel").fadeOut(0);
            }
            if ($("#<%=hfield.ClientID %>").val() === "hidesabtvam") {
                $("#tabpanel").fadeIn(1000);
                $("#sabtvam").fadeOut(0);
                setTimeout(function () { document.location = "sabtfaz2.aspx"; }, 3000);

            }
            $("#<%=txtdtesabt_dvam.ClientID %>").val(""); 
            $("#<%=txtdtesabt_vambank.ClientID %>").val(""); 

            //errore message
            if ($("#<%=messagehf.ClientID %>").val() === "1") {
                $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                $("#message").fadeIn().delay(3000).fadeOut();
                $("#<%=messagehf.ClientID %>").val("");
            }
            //errore message
        });
    </script>

    <!--------------------------------------------------------------------------------------------------------->
    <!---------------------------------------validator--------------------------------------------------------->
    <script type="text/javascript">
        //validator
        $(document).ready(function () {
            $("#form1").bootstrapValidator({
                message: "این مقدار صحیح نمیباشد",
                fields: {
                    "<%= txtdesc_faz2.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9ا-ی ؤءأإئ\-]+$/,
                                message: "تنها مقادیر حرفی ، عددی و خط فاصله صحیح میباشد"
                            }
                        }
                    },
                    "<%= ddlper_id_faz2.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },

                    "<%= txtdtesabt_vambank.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))+$/,
                                message: "تاریخ صحیح نمیباشد (--/--/--13)"
                            }
                        }

                    },
                    "<%= txtdtesabt_dvam.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))+$/,
                                message: "تاریخ صحیح نمیباشد (--/--/--13)"
                            }
                        }

                    },
                    "<%= txtmabvam.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            lessThan: {
                                value: 1000000001,
                                inclusive: true,
                                message: "مبلغ وام باید از 1000000000 کمتر باشد"
                            },
                            greaterThan: {
                                value: 4999999,
                                inclusive: false,
                                message: "مبلغ وام باید از 5000000 بیتشر باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    }
                }

            });
            //validator
        });
    </script>
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <div id="container" class="container full-width">
        <div class="row">
            <!--------------------------------------------------Message Box--------------------------------------------->
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <strong>
                            <p id="messagetext" class="text-center">پیام خطا</p>
                        </strong>
                    </div>
                </div>
            </div>
            <!--------------------------------------------------------------------------------------------------------->
            <div class="col-lg-12">
                <asp:HiddenField ID="hfield" runat="server" />
                        <div role="tabpanel" class="tab-pane" id="sabtnokfile" style="display: none">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        بررسی پرونده
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label36" runat="server" Text="علت تائید یا عدم تائید"></asp:Label>
                                            <asp:TextBox ID="txtdesc_faz2" runat="server" Width="300px" CssClass="form-control" placeholder="علت را وارد کنید" Height="100px" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <asp:Label ID="Label37" runat="server" Text="مشاور / مشاور2"></asp:Label>
                                            <asp:DropDownList ID="ddlper_id_faz2" runat="server" Width="175px" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <br />
                                        <p class="text-center">
                                            <asp:Button ID="btnsabt" runat="server" CssClass="btn btn-success" OnClick="btnsabt_Click" Text="ثبت اطلاعات" Width="175px" />
                                            <input type="button" id="btnback" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="sabtvam" style="display:none">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                       تکمیل پرونده وام
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label4" runat="server" Text="تاریخ معرفی"></asp:Label>
                                    <asp:TextBox ID="txtdtesabt_vambank" runat="server" CssClass="form-control" Width="300px" placeholder="تاریخ معرفی را وارد کنید"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label6" runat="server" Text="تاریخ پرداخت وام"></asp:Label>
                                    <asp:TextBox ID="txtdtesabt_dvam" runat="server" CssClass="form-control" Width="300px" placeholder="تاریخ پرداخت را وارد کنید"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label7" runat="server" Text="مبلغ وام"></asp:Label>
                                            <asp:TextBox ID="txtmabvam" runat="server" Width="300px" CssClass="form-control" placeholder="مبلغ وام را وارد کنید"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <br />
                                        <p class="text-center">
                                            <asp:Button ID="btnsabtvam" runat="server" CssClass="btn btn-success" OnClick="btnsabtvam_Click" Text="معرفی" Width="175px" />
                                            <input type="button" id="btnbacksabtvam" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                <div role="tabpanel" id="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#nokfile" aria-controls="nokfile" role="tab" data-toggle="tab" style="background-color: cornflowerblue">پرونده های رسیده</a></li>
                        <li role="presentation"><a href="#okfile" aria-controls="okfile" role="tab" data-toggle="tab" style="background-color: darkcyan">تائید شده</a></li>
                        <li role="presentation"><a href="#emdadi" aria-controls="emdadi" role="tab" data-toggle="tab" style="background-color: darkturquoise">امدادی</a></li>
                        <li role="presentation"><a href="#banki" aria-controls="banki" role="tab" data-toggle="tab" style="background-color: dodgerblue">بانکی</a></li>
                        <li role="presentation"><a href="#mvam" aria-controls="mvam" role="tab" data-toggle="tab" style="background-color: royalblue">در انتظار پرداخت</a></li>
                        <li role="presentation"><a href="#dvam" aria-controls="dvam" role="tab" data-toggle="tab" style="background-color: seagreen">پرداخت شده</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="nokfile">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های تائید نشده
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvnok" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvnok_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_faz1" HeaderText="تاریخ ثبت قطعی" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtok" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dook" CssClass="btn btn-success btn-xs">تائید</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lbtnnok" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="donok" CssClass="btn btn-danger btn-xs">عدم تائید</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="okfile">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های انجام شده
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvok" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_faz2" HeaderText="تاریخ تائید ناظر" />
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="emdadi">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های در انتظار معرفی به واحد وام
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvemdadi" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvemdadi_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_faz5" HeaderText="تاریخ ارجاع" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtmoarefivam_emd" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="domoarefivam_emd" CssClass="btn btn-success btn-xs">معرفی وام</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="banki">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های در انتظار معرفی به بانک
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvbanki" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvemdadi_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_faz5" HeaderText="تاریخ ارجاع" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtmoarefivam_bnk" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="domoarefivam_bnk" CssClass="btn btn-success btn-xs">معرفی وام</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="mvam">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های معرفی شده به وام
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvmvam" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvemdadi_RowCommand" OnRowDataBound="gvemdadi_OnRowDataBound">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_vambank" HeaderText="تاریخ معرفی" />
                                                <asp:TemplateField HeaderText="نوع معرفی">
                                                    <ItemTemplate><%#(Eval("no_act_vam").ToString().Equals("13") || Eval("no_act_vam").ToString().Equals("14")) ?"بانکی":"امدادی"%></ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtdaryaftvam_emd" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dodaryaftvam_emd" CssClass="btn btn-info btn-xs" Width="85">پرداخت امدادی</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lbtdaryaftvam_bnk" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dodaryaftvam_bnk" CssClass="btn btn-primary btn-xs" Width="85">پرداخت بانکی</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="dvam">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های وام گرفته
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvdvam" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_dvam" HeaderText="تاریخ پرداخت وام" />
                                                <asp:TemplateField HeaderText="نوع پرداخت">
                                                    <ItemTemplate><%#(Eval("no_act_vam").ToString().Equals("16") || Eval("no_act_vam").ToString().Equals("17")) ?"بانکی":"امدادی"%></ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
