﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfoModel;
using MyObjects;
using System.Web.Configuration;

namespace Info.Admin
{
    public partial class sabtsoft : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction
        protected void setgvrsoft(long cat_id_soft = -1, string searchname = "")
        {
            if (cat_id_soft == -1)
            {
                gvrsoft.DataSource =
                    (from n in _edbcontext.tblsoft
                     where n.name_soft.Contains(txtsearch.Text)
                     orderby n.dtesabt_soft descending, n.timsabt_soft descending
                     select n).ToList();
                gvrsoft.DataBind();

            }
            else
            {
                gvrsoft.DataSource =
                    (from n in _edbcontext.tblsoft
                     where n.cat_id_soft == cat_id_soft && n.name_soft.Contains(txtsearch.Text)
                     orderby n.dtesabt_soft descending, n.timsabt_soft descending
                     select n).ToList();
                gvrsoft.DataBind();
            }
        }
        protected void setddlcat_id_soft()
        {
            ddlcat_id_soft.DataSource = (from c in _edbcontext.tblcategory
                                         where c.parent_cat == (long)grouplevel.narmafzar
                                         orderby c.id_cat ascending
                                         select c).ToList();
            ddlcat_id_soft.DataTextField = "name_cat";
            ddlcat_id_soft.DataValueField = "id_cat";
            ddlcat_id_soft.DataBind();
            ddlcat_id_soft.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
        }
        protected void setddlcat_id_soft_search()
        {
            ddlcat_id_soft_search.DataSource = (from c in _edbcontext.tblcategory
                                                where c.parent_cat == (long)grouplevel.narmafzar
                                                orderby c.id_cat ascending
                                                select c).ToList();
            ddlcat_id_soft_search.DataTextField = "name_cat";
            ddlcat_id_soft_search.DataValueField = "id_cat";
            ddlcat_id_soft_search.DataBind();
            ddlcat_id_soft_search.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
        }


        protected void initialstate()
        {
            setddlcat_id_soft();
            setddlcat_id_soft_search();
            setgvrsoft();
            Session["emal"] = "donew";
        }
        protected void clsfield()
        {
            ddlcat_id_soft.SelectedIndex = -1;
            Session["emal"] = "donew";
        }
        protected void showerrore(string showmessage, string messagename, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagename;
            messagetexthf.Value = messagetext;
        }


        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok")
                    && (Session["isadmin_user"].ToString() == "isadmin")
                    && (Session["isact_user"].ToString() == "isact"))
                {
                    if (!IsPostBack)
                    {
                        initialstate();
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }

        }
        protected void btnsabt_Click(object sender, EventArgs e)
        {
            if (Session["emal"].ToString() == "donew")
            {
                try
                {
                    long id_user = long.Parse(Session["id_user"].ToString());
                    InfoModel.tblsoft _soft = new InfoModel.tblsoft();
                    _soft.user_id_soft = id_user;
                    _soft.cat_id_soft = Convert.ToInt64(ddlcat_id_soft.SelectedValue);
                    _soft.name_soft = txtname_soft.Text;
                    _soft.dtesabt_soft = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                    _soft.timsabt_soft = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                              DateTime.Now.Minute.ToString().PadLeft(2, '0');
                    //file upload
                    if (fuatt_soft.HasFile && fuatt_soft.FileContent.Length > 0)
                    {
                        string fileName = fuatt_soft.FileName;
                        //string fname = Path.GetFileName(file.FileName);
                        fuatt_soft.SaveAs(Server.MapPath(Path.Combine("~/attsoft/", fileName)));
                        _soft.att_soft = fuatt_soft.FileName;
                    }
                    //file upload
                    _soft.isact_soft = true;
                    _edbcontext.tblsoft.Add(_soft);
                    _edbcontext.SaveChanges();
                    showerrore("1", "پیام سیستم", "نرم افزار درسیستم ثبت گردید");
                }
                catch (Exception)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else if (Session["emal"].ToString() == "doedit")
            {
                try
                {
                    long id_user = long.Parse(Session["id_user"].ToString());
                    long id_soft = long.Parse(Session["id_soft"].ToString());
                    var query = from n in _edbcontext.tblsoft
                                where n.id_soft == id_soft
                                select n;
                    var _soft = query.FirstOrDefault();
                    string _oldfilename = _soft.att_soft;
                    _soft.id_soft = id_soft;
                    _soft.user_id_soft = id_user;
                    _soft.cat_id_soft = Convert.ToInt64(ddlcat_id_soft.SelectedValue);
                    _soft.name_soft = txtname_soft.Text;
                    _soft.dtesabt_soft = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                    _soft.timsabt_soft = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                              DateTime.Now.Minute.ToString().PadLeft(2, '0');
                    //file upload
                    if (fuatt_soft.HasFile && fuatt_soft.FileContent.Length > 0)
                    {
                        string fileName = fuatt_soft.FileName;
                        //string fname = Path.GetFileName(file.FileName);
                        if (_oldfilename != null)
                            System.IO.File.Delete(Server.MapPath(Path.Combine("~/attsoft/", _oldfilename)));
                        fuatt_soft.SaveAs(Server.MapPath(Path.Combine("~/attsoft/", fileName)));
                        _soft.att_soft = fuatt_soft.FileName;
                    }
                    //file upload
                    _soft.isact_soft = true;
                    _edbcontext.SaveChanges();
                    showerrore("1", "پیام سیستم", "نرم افزار درسیستم ثبت گردید");
                }
                catch (Exception)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }

            }
        }
        protected void btnnew_Click(object sender, EventArgs e)
        {
            clsfield();
        }

        protected void gvrsoft_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "doedit":
                    {
                        Session["id_soft"] = e.CommandArgument.ToString();
                        Session["emal"] = "doedit";

                        long id_soft = long.Parse(Session["id_soft"].ToString());
                        var query = from n in _edbcontext.tblsoft
                                    where n.id_soft == id_soft
                                    select n;
                        var _soft = query.FirstOrDefault();

                        ddlcat_id_soft.SelectedValue = _soft.cat_id_soft.ToString();
                        txtname_soft.Text = _soft.name_soft.ToString();
                        messagehf.Value = "dispsabtsoft";
                        break;
                    }
                case "dodelete":
                    {
                        Session["id_soft"] = e.CommandArgument.ToString();
                        Session["emal"] = "dodelete";

                        messagehf.Value = "dodelete";
                        break;
                    }

                case "doact":
                    {
                        Session["id_soft"] = e.CommandArgument.ToString();
                        Session["emal"] = "doact";

                        long id_soft = long.Parse(Session["id_soft"].ToString());
                        var query = from n in _edbcontext.tblsoft
                                    where n.id_soft == id_soft
                                    select n;
                        var _soft = query.FirstOrDefault();

                        _soft.isact_soft = !_soft.isact_soft;
                        _edbcontext.SaveChanges();
                        showerrore("1", "پیام سیستم", "نرم افزار فعال شد");
                        //messagehf.Value = "dispsabtsoft";
                        break;
                    }
            }
        }

        protected void gvrsoft_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvrsoft.PageIndex = e.NewPageIndex;
            initialstate();
            messagehf.Value = "dispsoft";
        }
        protected void btndelok_Click(object sender, EventArgs e)
        {
            long id_soft = long.Parse(Session["id_soft"].ToString());
            var query = from n in _edbcontext.tblsoft
                        where n.id_soft == id_soft
                        select n;
            var _soft = query.FirstOrDefault();
            string _oldfilename = _soft.att_soft;
            _edbcontext.tblsoft.Remove(_soft);
            _edbcontext.SaveChanges();
            if (_oldfilename != null)
                System.IO.File.Delete(Server.MapPath(Path.Combine("~/attsoft/", _oldfilename)));
            showerrore("1", "پیام سیستم", "نرم افزار از سیستم حذف گردید");

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            setgvrsoft(long.Parse(ddlcat_id_soft_search.SelectedValue), txtsearch.Text);
        }

        protected void ddlcat_id_soft_search_SelectedIndexChanged(object sender, EventArgs e)
        {
            long x = long.Parse(ddlcat_id_soft_search.SelectedValue);
            setgvrsoft(x);
            //Session["cat_id_soft"] = ddlcat_id_soft_search.SelectedValue.ToString();
        }
    }
}