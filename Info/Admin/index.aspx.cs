﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Info.Admin
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok")
                    && (Session["isadmin_user"].ToString() == "isadmin")
                    && (Session["isact_user"].ToString() == "isact"))
                {
                    if (!IsPostBack)
                    {
                        //initialstate();
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }

        protected void sabtnews_Click(object sender, EventArgs e)
        {
            Response.Redirect("sabtnews.aspx");
        }

        protected void sabtsoft_Click(object sender, EventArgs e)
        {
            Response.Redirect("sabtsoft.aspx");

        }

        protected void sabtdownload_Click(object sender, EventArgs e)
        {

        }
    }
}