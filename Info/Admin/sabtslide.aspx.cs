﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfoModel;
using MyObjects;
using System.Web.Configuration;

namespace Info.Admin
{
    public partial class sabtslide : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction
        protected void setgvrslide(long cat_id_slide = -1, string searchname = "")
        {
            if (cat_id_slide == -1)
            {
                gvrslide.DataSource =
                    (from n in _edbcontext.tblslide
                     where n.name_slide.Contains(txtsearch.Text)
                     orderby n.dtesabt_slide descending, n.timsabt_slide descending
                     select n).ToList();
                gvrslide.DataBind();

            }
            else
            {
                gvrslide.DataSource =
                    (from n in _edbcontext.tblslide
                     where n.cat_id_slide == cat_id_slide && n.name_slide.Contains(txtsearch.Text)
                     orderby n.dtesabt_slide descending, n.timsabt_slide descending
                     select n).ToList();
                gvrslide.DataBind();
            }
        }
        protected void setddlcat_id_slide()
        {
            ddlcat_id_slide.DataSource = (from c in _edbcontext.tblcategory
                                         where c.parent_cat == (long)grouplevel.payameslaider
                                         orderby c.id_cat ascending
                                         select c).ToList();
            ddlcat_id_slide.DataTextField = "name_cat";
            ddlcat_id_slide.DataValueField = "id_cat";
            ddlcat_id_slide.DataBind();
            ddlcat_id_slide.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
        }
        protected void setddlcat_id_slide_search()
        {
            ddlcat_id_slide_search.DataSource = (from c in _edbcontext.tblcategory
                                                where c.parent_cat == (long)grouplevel.payameslaider
                                                orderby c.id_cat ascending
                                                select c).ToList();
            ddlcat_id_slide_search.DataTextField = "name_cat";
            ddlcat_id_slide_search.DataValueField = "id_cat";
            ddlcat_id_slide_search.DataBind();
            ddlcat_id_slide_search.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
        }


        protected void initialstate()
        {
            setddlcat_id_slide();
            setddlcat_id_slide_search();
            setgvrslide();
            Session["emal"] = "donew";
        }
        protected void clsfield()
        {
            ddlcat_id_slide.SelectedIndex = -1;
            Session["emal"] = "donew";
        }
        protected void showerrore(string showmessage, string messagename, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagename;
            messagetexthf.Value = messagetext;
        }


        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok")
                    && (Session["isadmin_user"].ToString() == "isadmin")
                    && (Session["isact_user"].ToString() == "isact"))
                {
                    if (!IsPostBack)
                    {
                        initialstate();
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }

        }
        protected void btnsabt_Click(object sender, EventArgs e)
        {
            if (Session["emal"].ToString() == "donew")
            {
                try
                {
                    long id_user = long.Parse(Session["id_user"].ToString());
                    InfoModel.tblslide _slide = new InfoModel.tblslide();
                    _slide.user_id_slide = id_user;
                    _slide.cat_id_slide = Convert.ToInt64(ddlcat_id_slide.SelectedValue);
                    _slide.name_slide = txtname_slide.Text;
                    _slide.dtesabt_slide = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                    _slide.timsabt_slide = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                              DateTime.Now.Minute.ToString().PadLeft(2, '0');
                    //file upload
                    if (fuatt_slide.HasFile && fuatt_slide.FileContent.Length > 0)
                    {
                        string fileName = fuatt_slide.FileName;
                        //string fname = Path.GetFileName(file.FileName);
                        fuatt_slide.SaveAs(Server.MapPath(Path.Combine("~/attslide/", fileName)));
                        _slide.att_slide = fuatt_slide.FileName;
                    }
                    //file upload
                    _slide.isact_slide = true;
                    _edbcontext.tblslide.Add(_slide);
                    _edbcontext.SaveChanges();
                    showerrore("1", "پیام سیستم", "نرم افزار درسیستم ثبت گردید");
                }
                catch (Exception)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else if (Session["emal"].ToString() == "doedit")
            {
                try
                {
                    long id_user = long.Parse(Session["id_user"].ToString());
                    long id_slide = long.Parse(Session["id_slide"].ToString());
                    var query = from n in _edbcontext.tblslide
                                where n.id_slide == id_slide
                                select n;
                    var _slide = query.FirstOrDefault();
                    string _oldfilename = _slide.att_slide;
                    _slide.id_slide = id_slide;
                    _slide.user_id_slide = id_user;
                    _slide.cat_id_slide = Convert.ToInt64(ddlcat_id_slide.SelectedValue);
                    _slide.name_slide = txtname_slide.Text;
                    _slide.dtesabt_slide = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                    _slide.timsabt_slide = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                              DateTime.Now.Minute.ToString().PadLeft(2, '0');
                    //file upload
                    if (fuatt_slide.HasFile && fuatt_slide.FileContent.Length > 0)
                    {
                        string fileName = fuatt_slide.FileName;
                        //string fname = Path.GetFileName(file.FileName);
                        if (_oldfilename != null)
                            System.IO.File.Delete(Server.MapPath(Path.Combine("~/attslide/", _oldfilename)));
                        fuatt_slide.SaveAs(Server.MapPath(Path.Combine("~/attslide/", fileName)));
                        _slide.att_slide = fuatt_slide.FileName;
                    }
                    //file upload
                    _slide.isact_slide = true;
                    _edbcontext.SaveChanges();
                    showerrore("1", "پیام سیستم", "نرم افزار درسیستم ثبت گردید");
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }

            }
        }
        protected void btnnew_Click(object sender, EventArgs e)
        {
            clsfield();
        }

        protected void gvrslide_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "doedit":
                    {
                        Session["id_slide"] = e.CommandArgument.ToString();
                        Session["emal"] = "doedit";

                        long id_slide = long.Parse(Session["id_slide"].ToString());
                        var query = from n in _edbcontext.tblslide
                                    where n.id_slide == id_slide
                                    select n;
                        var _slide = query.FirstOrDefault();

                        ddlcat_id_slide.SelectedValue = _slide.cat_id_slide.ToString();
                        txtname_slide.Text = _slide.name_slide.ToString();
                        messagehf.Value = "dispsabtslide";
                        break;
                    }
                case "dodelete":
                    {
                        Session["id_slide"] = e.CommandArgument.ToString();
                        Session["emal"] = "dodelete";

                        messagehf.Value = "dodelete";
                        break;
                    }

                case "doact":
                    {
                        Session["id_slide"] = e.CommandArgument.ToString();
                        Session["emal"] = "doact";

                        long id_slide = long.Parse(Session["id_slide"].ToString());
                        var query = from n in _edbcontext.tblslide
                                    where n.id_slide == id_slide
                                    select n;
                        var _slide = query.FirstOrDefault();

                        _slide.isact_slide = !_slide.isact_slide;
                        _edbcontext.SaveChanges();
                        showerrore("1", "پیام سیستم", "نرم افزار فعال شد");
                        //messagehf.Value = "dispsabtslide";
                        break;
                    }
            }
        }

        protected void gvrslide_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvrslide.PageIndex = e.NewPageIndex;
            initialstate();
            messagehf.Value = "dispslide";
        }
        protected void btndelok_Click(object sender, EventArgs e)
        {
            long id_slide = long.Parse(Session["id_slide"].ToString());
            var query = from n in _edbcontext.tblslide
                        where n.id_slide == id_slide
                        select n;
            var _slide = query.FirstOrDefault();
            string _oldfilename = _slide.att_slide;
            _edbcontext.tblslide.Remove(_slide);
            _edbcontext.SaveChanges();
            if (_oldfilename != null)
                System.IO.File.Delete(Server.MapPath(Path.Combine("~/attslide/", _oldfilename)));
            showerrore("1", "پیام سیستم", "نرم افزار از سیستم حذف گردید");

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            setgvrslide(long.Parse(ddlcat_id_slide_search.SelectedValue), txtsearch.Text);
        }

        protected void ddlcat_id_slide_search_SelectedIndexChanged(object sender, EventArgs e)
        {
            long x = long.Parse(ddlcat_id_slide_search.SelectedValue);
            setgvrslide(x);
            //Session["cat_id_slide"] = ddlcat_id_slide_search.SelectedValue.ToString();
        }
    }
}