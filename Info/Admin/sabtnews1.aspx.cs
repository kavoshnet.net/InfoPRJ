﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;
using InfoModel;
namespace Info
{
    public partial class sabtnews : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction

        protected void setgvnok()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvnok.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.per_id_faz1 == id_per) && (v.no_act_vam == (int)actlevelvam.Infaz2)
                 orderby v.dtesabt_faz1 descending, v.timsabt_faz1 descending
                 select v).ToList();
            gvnok.DataBind();
        }
        protected void setgvok()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvok.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam >= (int)actlevelvam.Infaz3) &&
                 (v.per_id_faz1 == id_per)
                 orderby v.dtesabt_faz2 descending, v.timsabt_faz2 descending
                 select v).ToList();
            gvok.DataBind();
        }

        protected void setgvemdadi()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvemdadi.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam == (int)actlevelvam.MoarefiVam_EmdadNazer) &&
                 (v.per_id_faz5 == id_per)
                 orderby v.dtesabt_faz5 descending, v.timsabt_faz5 descending
                 select v).ToList();
            gvemdadi.DataBind();
        }
        protected void setgvmvam()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvmvam.DataSource =
                (from v in _edbcontext.tblvams
                 where ((v.no_act_vam == (int)actlevelvam.TaedMoarefiVam_EmdadNazer) || (v.no_act_vam == (int)actlevelvam.TaedMoarefiVam_BankNazer)) &&
                 (v.per_id_faz5 == id_per)
                 orderby v.dtesabt_vambank descending, v.timsabt_vambank descending
                 select v).ToList();
            gvmvam.DataBind();
        }
        protected void setgvdvam()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvdvam.DataSource =
                (from v in _edbcontext.tblvams
                 where ((v.no_act_vam == (int)actlevelvam.DaryaftVam_EmdadNazer) || (v.no_act_vam == (int)actlevelvam.DaryaftVam_BankNazer)) &&
                 (v.per_id_faz5 == id_per)
                 orderby v.dtesabt_vambank descending, v.timsabt_vambank descending
                 select v).ToList();
            gvdvam.DataBind();
        }
        protected void setgvbanki()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvbanki.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam == (int)actlevelvam.MoarefiVam_BankNazer) &&
                 (v.per_id_faz5 == id_per)
                 orderby v.dtesabt_faz5 descending, v.timsabt_faz5 descending
                 select v).ToList();
            gvbanki.DataBind();
        }
        protected void setgrids()
        {
            setgvnok();
            setgvok();
            setgvemdadi();
            setgvbanki();
            setgvmvam();
            setgvdvam();
            //gvemdadi.RowDataBound += new GridViewRowEventHandler(gvemdadi_OnRowDataBound);
        }

        protected void setddlper_id_faz2()
        {
            int mkh_per_id = Convert.ToInt16(Session["mhkh_per_id"].ToString());
            ddlper_id_faz2.DataSource = (from p in _edbcontext.tblpers
                                         //تغییرات اعمال شده در تاریخ 1394/03/26
                                         //where (p.mhkh_per_id == mkh_per_id) && ((p.nofa_per_id == (int)nofa.Moshaver2Shahrestan) || (p.nofa_per_id == (int)nofa.Moshaver))
                                         where (p.mhkh_per_id == mkh_per_id) && ((p.nofa_per_id == (int)nofa.Moshaver2Shahrestan) || (p.nofa_per_id == (int)nofa.Moshaver) || (p.nofa_per_id == (int)nofa.Moshaver1Ostan) || (p.nofa_per_id == (int)nofa.Moshaver2Ostan) || (p.nofa_per_id == (int)nofa.Moshaver2Toy))
                                         select new { fulname_per = p.ln_per + " " + p.fn_per, p.id_per }).ToList();
            ddlper_id_faz2.DataTextField = "fulname_per";
            ddlper_id_faz2.DataValueField = "id_per";
            ddlper_id_faz2.DataBind();
            ddlper_id_faz2.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }


        protected void initialstate()
        {
            setddlper_id_faz2();
            setgrids();
        }
        protected void clsfield()
        {
            ddlper_id_faz2.SelectedIndex = -1;
            Session["emal"] = "dook";
            txtdesc_faz2.Text = string.Empty;
        }
        public string getnopardakht(int code)
        {
            if (code == (int)actlevelvam.DaryaftVam_EmdadNazer)
                return "امدادی";
            else if ((code == (int)actlevelvam.DaryaftVam_BankNazer) || (code == (int)actlevelvam.DaryaftVam_BankMoshaver))
                return "بانکی";
            else return "";
        }
        public string getnoerja(int code)
        {
            if (code == (int)actlevelvam.MoarefiVam_EmdadNazer)
                return "امدادی";
            else if ((code == (int)actlevelvam.MoarefiVam_BankNazer) || (code == (int)actlevelvam.MoarefiVam_BankMoshaver))
                return "بانکی";
            else return "";
        }

        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if (!IsPostBack)
                    {
                        initialstate();
                        Session["emal"] = "dook";
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }

        /*protected void btnback_Click(object sender, EventArgs e)
        {
            hfield.Value = "hidedook";
        }*/
        /*protected void btnbacksabtvam_Click(object sender, EventArgs e)
        {
            hfield.Value = "hidesabtvam";
        }*/

        protected void btnsabt_Click(object sender, EventArgs e)
        {
            if (true)
            {
                try
                {
                    if (Session["emal"].ToString().Equals("dook"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz2 = query.FirstOrDefault();
                        _vfaz2.per_id_faz2 = Convert.ToInt16(ddlper_id_faz2.SelectedValue);
                        _vfaz2.desc_faz2 = txtdesc_faz2.Text;
                        _vfaz2.dtesabt_faz2 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz2.timsabt_faz2 = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.Infaz3, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz2.tblmddjo.cp_mddjo.ToString() + "تائید گردید");
                        setgrids();
                    }
                    if (Session["emal"].ToString().Equals("donok"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz2 = query.FirstOrDefault();
                        _vfaz2.per_id_faz2 = Convert.ToInt16(Session["id_per"].ToString());
                        _vfaz2.desc_faz2 = txtdesc_faz2.Text;
                        _vfaz2.dtesabt_faz2 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz2.timsabt_faz2 = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.BackInfaz1, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz2.tblmddjo.cp_mddjo.ToString() + "جهت اصلاح عودت داده شد");
                        setgrids();
                    }
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else
            {
                showerrore("1", "خطای ورود اطلاعات", "کاربر محترم لطفا داده ها را کامل کنید");
            }
            hfield.Value = "hidedook";
        }

        protected void gvnok_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "dook":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "dook";
                        Label36.Visible = false;
                        txtdesc_faz2.Visible = false;
                        txtdesc_faz2.Text = "پرونده تائید شد";
                        Label37.Visible = true;
                        ddlper_id_faz2.Visible = true;
                        btnsabt.Text = "تائید پرونده";
                        hfield.Value = "displaydook";
                        break;
                    }
                case "donok":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "donok";
                        Label36.Visible = true;
                        txtdesc_faz2.Visible = true;
                        txtdesc_faz2.Text = string.Empty;
                        Label36.Text = "علت عدم تائید";
                        Label37.Visible = false;
                        ddlper_id_faz2.Visible = false;
                        btnsabt.Text = "عدم تائید پرونده";
                        hfield.Value = "displaydook";
                        break;
                    }
            }
        }
        protected void gvemdadi_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var result = e.Row.DataItem.GetType().GetProperty("no_act_vam").GetValue(e.Row.DataItem, null);
                int no_act_vam = int.Parse(result.ToString());
                if (no_act_vam == (int)actlevelvam.TaedMoarefiVam_EmdadNazer)
                {
                    LinkButton lbtnbnk = (LinkButton)e.Row.FindControl("lbtdaryaftvam_bnk");
                    lbtnbnk.Visible = false;
                }
                if ((no_act_vam == (int)actlevelvam.TaedMoarefiVam_BankNazer) || (no_act_vam == (int)actlevelvam.TaedMoarefiVam_BankMoshaver))
                {
                    LinkButton lbtnemd = (LinkButton)e.Row.FindControl("lbtdaryaftvam_emd");
                    lbtnemd.Visible = false;
                }
            }
        }
        protected void gvemdadi_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //
            switch (e.CommandName)
            {
                case "domoarefivam_emd":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "domoarefivam_emd";
                        Label4.Visible = true;
                        txtdtesabt_vambank.Visible = true;
                        Label6.Visible = false;
                        txtdtesabt_dvam.Visible = false;
                        Label7.Visible = false;
                        txtmabvam.Visible = false;
                        btnsabtvam.Text = "معرفی";
                        hfield.Value = "displaysabtvam";
                        break;
                    }
                case "dodaryaftvam_emd":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "dodaryaftvam_emd";
                        Label4.Visible = false;
                        txtdtesabt_vambank.Visible = false;
                        Label6.Visible = true;
                        txtdtesabt_dvam.Visible = true;
                        Label7.Visible = true;
                        txtmabvam.Visible = true;
                        btnsabtvam.Text = "پرداخت";
                        hfield.Value = "displaysabtvam";

                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz2 = query.FirstOrDefault();
                        txtmabvam.Text = _vfaz2.mabvam_faz4.ToString();

                        break;
                    }
                case "domoarefivam_bnk":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "domoarefivam_bnk";
                        Label4.Visible = true;
                        txtdtesabt_vambank.Visible = true;
                        Label6.Visible = false;
                        txtdtesabt_dvam.Visible = false;
                        Label7.Visible = false;
                        txtmabvam.Visible = false;
                        btnsabtvam.Text = "معرفی";
                        hfield.Value = "displaysabtvam";
                        break;
                    }
                case "dodaryaftvam_bnk":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "dodaryaftvam_bnk";
                        Label4.Visible = false;
                        txtdtesabt_vambank.Visible = false;
                        Label6.Visible = true;
                        txtdtesabt_dvam.Visible = true;
                        Label7.Visible = true;
                        txtmabvam.Visible = true;
                        btnsabtvam.Text = "پرداخت";
                        hfield.Value = "displaysabtvam";

                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz2 = query.FirstOrDefault();
                        txtmabvam.Text = _vfaz2.mabvam_faz4.ToString();

                        break;
                    }

            }

        }
        protected void btnsabtvam_Click(object sender, EventArgs e)
        {
            if (true)
            {
                try
                {
                    if (Session["emal"].ToString().Equals("domoarefivam_emd"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();
                        _vfaz5.dtesabt_vambank = txtdtesabt_vambank.Text;
                        //_mddjofaz5.dtesabt_faz2 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_vambank = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.TaedMoarefiVam_EmdadNazer, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "برای وام معرفی گردید");
                        setgrids();
                    }
                    if (Session["emal"].ToString().Equals("dodaryaftvam_emd"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();
                        _vfaz5.dtesabt_dvam = txtdtesabt_dvam.Text;
                        //_mddjofaz5.dtesabt_faz2 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_dvam = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _vfaz5.mabvam = Convert.ToInt32(txtmabvam.Text);
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.DaryaftVam_EmdadNazer, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "وام را دریافت کرد");
                        setgrids();
                    }
                    if (Session["emal"].ToString().Equals("domoarefivam_bnk"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();
                        _vfaz5.dtesabt_vambank = txtdtesabt_vambank.Text;
                        //_mddjofaz5.dtesabt_faz2 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_vambank = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.TaedMoarefiVam_BankNazer, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "برای وام معرفی گردید");
                        setgrids();
                    }
                    if (Session["emal"].ToString().Equals("dodaryaftvam_bnk"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();
                        _vfaz5.dtesabt_dvam = txtdtesabt_dvam.Text;
                        //_mddjofaz5.dtesabt_faz2 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_dvam = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _vfaz5.mabvam = Convert.ToInt32(txtmabvam.Text);
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.DaryaftVam_BankNazer, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "وام را دریافت کرد");
                        setgrids();
                    }
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else
            {
                showerrore("1", "خطای ورود اطلاعات", "کاربر محترم لطفا داده ها را کامل کنید");
            }
            //Response.Redirect(Request.RawUrl);
            hfield.Value = "hidesabtvam";

        }
    }
}