﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;
using InfoModel;
namespace Info
{
    public partial class pobank : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction

        protected void setgvmvam()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvmvam.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam == (int)actlevelvam.TaedMoarefiVam_BankMoshaver) &&
                 (v.per_id_faz5 == id_per)
                 orderby v.dtesabt_vambank descending, v.timsabt_vambank descending 
                 select v).ToList();
            gvmvam.DataBind();
        }
        protected void setgvdvam()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvdvam.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam == (int)actlevelvam.DaryaftVam_BankMoshaver) &&
                 (v.per_id_faz5 == id_per)
                 orderby v.dtesabt_vambank descending, v.timsabt_vambank descending 
                 select v).ToList();
            gvdvam.DataBind();
        }
        protected void setgvbanki()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvbanki.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam == (int)actlevelvam.MoarefiVam_BankMoshaver) &&
                 (v.per_id_faz5 == id_per)
                 orderby v.dtesabt_faz5 descending, v.timsabt_faz5 descending 
                 select v).ToList();
            gvbanki.DataBind();
        }
        protected void setgrids()
        {
            setgvbanki();
            setgvmvam();
            setgvdvam();
        }

        protected void initialstate()
        {
            setgrids();
        }
        protected void clsfield()
        {
            //
        }
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {

                    if (!IsPostBack)
                    {
                        initialstate();
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }
        protected void gvbank_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //
            switch (e.CommandName)
            {
                case "domoarefivam_bnk":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "domoarefivam_bnk";
                        Label4.Visible = true;
                        txtdtesabt_vambank.Visible = true;
                        Label6.Visible = false;
                        txtdtesabt_dvam.Visible = false;
                        Label7.Visible = false;
                        txtmabvam.Visible = false;
                        btnsabtvam.Text = "معرفی";
                        hfield.Value = "displaysabtvam";
                        break;
                    }
                case "dodaryaftvam_bnk":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "dodaryaftvam_bnk";
                        Label4.Visible = false;
                        txtdtesabt_vambank.Visible = false;
                        Label6.Visible = true;
                        txtdtesabt_dvam.Visible = true;
                        Label7.Visible = true;
                        txtmabvam.Visible = true;
                        btnsabtvam.Text = "پرداخت";
                        hfield.Value = "displaysabtvam";
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _mddjopobank = query.FirstOrDefault();
                        txtmabvam.Text = _mddjopobank.mabvam_faz4.ToString();

                        break;
                    }

            }

        }

        protected void btnbacksabtvam_Click(object sender, EventArgs e)
        {
            hfield.Value = "hidesabtvam";
        }

        protected void btnsabtvam_Click(object sender, EventArgs e)
        {
            if (true)
            {
                try
                {
                    if (Session["emal"].ToString().Equals("domoarefivam_bnk"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();
                        _vfaz5.dtesabt_vambank = txtdtesabt_vambank.Text;
                        //_mddjofaz5.dtesabt_faz2 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_vambank = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.TaedMoarefiVam_BankMoshaver, id_mddjo);
                        showerrore("1","پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "برای وام معرفی گردید");
                        setgrids();
                    }
                    if (Session["emal"].ToString().Equals("dodaryaftvam_bnk"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();
                        _vfaz5.dtesabt_dvam = txtdtesabt_dvam.Text;
                        //_mddjofaz5.dtesabt_faz2 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_dvam = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _vfaz5.mabvam = Convert.ToInt32(txtmabvam.Text);
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.DaryaftVam_BankMoshaver, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "وام را دریافت کرد");
                        setgrids();
                    }
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else
            {
                showerrore("1", "خطای ورود اطلاعات", "کاربر محترم لطفا داده ها را کامل کنید");
            }
            hfield.Value = "hidesabtvam";
        }
    }
}