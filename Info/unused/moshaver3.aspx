﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="moshaver3.aspx.cs" Inherits="Info.moshaver3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <div id="container" class="container full-width">
        <div class="row">
                <div class="col-md-6 text-center">
                    <asp:Button ID="pobank" runat="server" CssClass="btn btn-primary" Text="پیگیری امور بانکی" Width="175px" OnClick="pobank_Click" />
                </div>

                <div class="col-md-6 text-center">
                    <asp:Button ID="sabtkar" runat="server" CssClass="btn btn-info" Text="کاریابی" Width="175px" OnClick="sabtkar_Click" />
                </div>
        </div>
    </div>
</asp:Content>
