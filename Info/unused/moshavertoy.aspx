﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="moshavertoy.aspx.cs" Inherits="Info.moshavertoy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="container" class="container full-width">
        <div class="row">
            <div class="col-md-2 text-center">
                <asp:Button ID="sabtparvande" runat="server" CssClass="btn btn-warning" Text="ثبت پرونده" Width="175px" OnClick="sabtparvande_Click" />
            </div>
            <div class="col-md-2 text-center">
                <asp:Button ID="sabtsamane" runat="server" CssClass="btn btn-success" Text="ثبت سامانه ها" Width="175px" OnClick="sabtsamane_Click" />
            </div>
            <div class="col-md-2 text-center">
                <asp:Button ID="sabtkar" runat="server" CssClass="btn btn-info" Text="کاریابی" Width="175px" OnClick="sabtkar_Click" />
            </div>
        </div>
    </div>
</asp:Content>
