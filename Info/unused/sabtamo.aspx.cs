﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;
using InfoModel;
namespace Info
{
    public partial class sabtamo : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction

        protected void setgvmddjo()
        {
            //int id_per = Convert.ToInt16(Session["id_per"].ToString());
            //gvmddjo.DataSource =
            //    (from m in _edbcontext.tblmddjoes
            //     where (m.per_id_mddjo == id_per) && (m.no_act_amo == (int)actlevelamo.Tempmddjo)
            //     orderby m.dtesabt_mddjo descending, m.timsabt_mddjo descending
            //     select m).ToList();
            //gvmddjo.DataBind();
            //تغییرات اعمال شده در تاریخ 1394/03/26
            int no_fa = int.Parse(Session["no_nofa"].ToString());
            int mh_kh = int.Parse(Session["mhkh_per_id"].ToString());
            int id_per = int.Parse(Session["id_per"].ToString());
            if ((no_fa == (int)MyObjects.nofa.Moshaver) || (no_fa == (int)MyObjects.nofa.Moshaver2Shahrestan))
            {
                var per_mhkh = (from p in _edbcontext.tblpers
                                where p.mhkh_per_id == mh_kh
                                select p.id_per).ToList();
                if (per_mhkh.Count > 0)
                {
                    gvmddjo.DataSource =
                        (from m in _edbcontext.tblmddjoes
                         where per_mhkh.Contains(m.per_id_mddjo)
                         orderby m.dtesabt_mddjo descending, m.timsabt_mddjo descending
                         select m).ToList();
                    gvmddjo.DataBind();
                }
            }
            else if ((no_fa == (int)MyObjects.nofa.Moshaver1Ostan) || (no_fa == (int)MyObjects.nofa.Moshaver2Ostan))
            {
                gvmddjo.DataSource =
                    (from m in _edbcontext.tblmddjoes
                     where m.per_id_mddjo == id_per
                     orderby m.dtesabt_mddjo descending, m.timsabt_mddjo descending
                     select m).ToList();
                gvmddjo.DataBind();

            }
        }
        protected void setgvtashkil()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvtashkil.DataSource =
                (from a in _edbcontext.tblamoes
                 where (a.no_act_amo == (int)actlevelamo.Tashkil) &&
                 (a.per_id_amo == id_per)
                 orderby a.dtesabttak_amo descending, a.timsabttak_amo descending
                 select a).ToList();
            gvtashkil.DataBind();
        }

        protected void setgvmoarefi()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvmoarefi.DataSource =
                (from a in _edbcontext.tblamoes
                 where (a.no_act_amo == (int)actlevelamo.Moarefi) &&
                 (a.per_id_amo == id_per)
                 orderby a.dtemor_amo descending, a.timmor_amo descending
                 select a).ToList();
            gvmoarefi.DataBind();
        }
        protected void setgvnatije()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvnatije.DataSource =
                (from a in _edbcontext.tblamoes
                 where (a.no_act_amo == (int)actlevelamo.Natije) &&
                 (a.per_id_amo == id_per)
                 orderby a.dtenat_amo descending, a.timnat_amo descending
                 select a).ToList();
            gvnatije.DataBind();
        }
        protected void setgvmonjar()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvmonjar.DataSource =
                (from a in _edbcontext.tblamoes
                 where (a.no_act_amo == (int)actlevelamo.Monjar) &&
                 (a.per_id_amo == id_per)
                 orderby a.dtemon_amo descending, a.timmon_amo descending
                 select a).ToList();
            gvmonjar.DataBind();
        }
        protected void setgrids()
        {
            setgvmddjo();
            setgvtashkil();
            setgvmoarefi();
            setgvnatije();
            setgvmonjar();
            //gvemdadi.RowDataBound += new GridViewRowEventHandler(gvemdadi_OnRowDataBound);
        }

        protected void setddlsarfasl_id()
        {
            ddlsarfasl_id.DataSource = (from s in _edbcontext.tblsaramoes
                                        select s).ToList();
            ddlsarfasl_id.DataTextField = "desc_sarfasl";
            ddlsarfasl_id.DataValueField = "id_sarfasl";
            ddlsarfasl_id.DataBind();
            ddlsarfasl_id.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }

        protected void setddlonvan_id()
        {
            ddlonvan_id.DataSource = (from o in _edbcontext.tblonvamoes
                                      select o).ToList();
            ddlonvan_id.DataTextField = "desc_onv";
            ddlonvan_id.DataValueField = "id_onv";
            ddlonvan_id.DataBind();
            ddlonvan_id.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }
        protected void setddlesh_id()
        {
            ddlesh_id.DataSource = (from e in _edbcontext.tbleshamoes
                                    select e).ToList();
            ddlesh_id.DataTextField = "desc_esh";
            ddlesh_id.DataValueField = "id_esh";
            ddlesh_id.DataBind();
            ddlesh_id.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }

        protected void initialstate()
        {
            setddlsarfasl_id();
            setddlesh_id();
            setddlonvan_id();
            setgrids();
        }
        protected void clsfield()
        {
            /*ddlper_id_faz2.SelectedIndex = -1;
            Session["emal"] = "dook";
            txtdesc_faz2.Text = string.Empty;*/
        }
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if (!IsPostBack)
                    {
                        initialstate();
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }
        protected void btntashkil_Click(object sender, EventArgs e)
        {
            try
            {
                int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                int id_per = int.Parse(Session["id_per"].ToString());
                InfoModel.tblamo _amo = new InfoModel.tblamo();
                _amo.mddjo_id_amo = id_mddjo;
                _amo.per_id_amo = id_per;
                _amo.dtetak_amo = txtdtetak_amo.Text;
                _amo.shtasvib_amo = txtshtasvib_amo.Text;
                _amo.dtesabttak_amo = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                _amo.timsabttak_amo = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                      DateTime.Now.Minute.ToString().PadLeft(2, '0');
                _edbcontext.tblamoes.Add(_amo);
                _edbcontext.SaveChanges();
                var query = from m in _edbcontext.tblmddjoes
                            where m.id_mddjo == id_mddjo
                            select m;
                var _mddjo = query.FirstOrDefault();

                myclass.setactlevelamo(actlevelamo.Tashkil, id_mddjo);
                showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _mddjo.cp_mddjo.ToString() + "تشکیل گردید");
                setgrids();
            }
            catch (Exception ex)
            {
                showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
            }
            hfield.Value = "hidetashkil";
        }
        protected void btnmoarefi_Click(object sender, EventArgs e)
        {
            try
            {
                int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                int id_per = int.Parse(Session["id_per"].ToString());
                var query = from a in _edbcontext.tblamoes
                            where (a.mddjo_id_amo == id_mddjo) && (a.per_id_amo==id_per)
                            select a;
                var _amo = query.FirstOrDefault();
                _amo.sarfasl_id_amo = Convert.ToInt16(ddlsarfasl_id.SelectedValue);
                _amo.onvan_id_amo = Convert.ToInt16(ddlonvan_id.SelectedValue);
                _amo.nam_amo = txtnam_amo.Text;
                _amo.dtestr_amo = txtdtestramo.Text;
                _amo.dtemor_amo = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                _amo.timmor_amo = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                      DateTime.Now.Minute.ToString().PadLeft(2, '0');
                _edbcontext.SaveChanges();
                myclass.setactlevelamo(actlevelamo.Moarefi, id_mddjo);
                showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _amo.tblmddjo.cp_mddjo.ToString() + "معرفی گردید");
                setgrids();
            }
            catch (Exception ex)
            {
                showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
            }
            hfield.Value = "hidemoarefi";
        }
        protected void btnnatije_Click(object sender, EventArgs e)
        {
            try
            {
                int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                int id_per = int.Parse(Session["id_per"].ToString());
                var query = from a in _edbcontext.tblamoes
                            where (a.mddjo_id_amo == id_mddjo) && (a.per_id_amo == id_per)
                            select a;
                var _amo = query.FirstOrDefault();
                _amo.natije_amo = Convert.ToBoolean(ddlnatije_amo.SelectedValue);
                _amo.dtesod_amo = txtdtesod_amo.Text;
                _amo.dtenat_amo = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                _amo.timnat_amo = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                      DateTime.Now.Minute.ToString().PadLeft(2, '0');
                _edbcontext.SaveChanges();
                myclass.setactlevelamo(actlevelamo.Natije, id_mddjo);
                showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _amo.tblmddjo.cp_mddjo.ToString() + "اعلام نتیجه گردید");
                setgrids();
            }
            catch (Exception ex)
            {
                showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
            }
            hfield.Value = "hidenatije";
        }
        protected void btnmonjar_Click(object sender, EventArgs e)
        {
            try
            {
                int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                int id_per = int.Parse(Session["id_per"].ToString());
                var query = from a in _edbcontext.tblamoes
                            where (a.mddjo_id_amo == id_mddjo) && (a.per_id_amo == id_per)
                            select a;
                var _amo = query.FirstOrDefault();
                _amo.esh_id_amo = Convert.ToInt16(ddlesh_id.SelectedValue);
                _amo.dtemon_amo = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                _amo.timmon_amo = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                      DateTime.Now.Minute.ToString().PadLeft(2, '0');
                _edbcontext.SaveChanges();
                myclass.setactlevelamo(actlevelamo.Monjar, id_mddjo);
                showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _amo.tblmddjo.cp_mddjo.ToString() + "تکمیل گردید");
                setgrids();
            }
            catch (Exception ex)
            {
                showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
            }
            hfield.Value = "hidemonjar";
        }

        protected void gvmddjo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "dotashkil":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        hfield.Value = "displaytashkil";
                        break;
                    }
            }
        }
        protected void gvtashkil_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "domoarefi":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        hfield.Value = "displaymoarefi";
                        break;
                    }
            }
        }
        protected void gvmoarefi_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "donatije":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        hfield.Value = "displaynatije";
                        break;
                    }
            }
        }
        protected void gvnatije_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "domonjar":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        hfield.Value = "displaymonjar";
                        break;
                    }
            }
        }
    }
}