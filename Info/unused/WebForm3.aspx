﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="Info.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <link href="style/bootstrap3.3.2/Content/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="style/bootstrap3.3.2/Content/bootstrap.rtl.css" rel="stylesheet" type="text/css" />
    <link href="style/bootstrap3.3.2/Content/bootstrapValidator.css" rel="stylesheet" type="text/css" />
    <!-- Custom class -->
    <link href="style/bootstrap3.3.2/Content/custom.css" rel="stylesheet" />
<!--    <link href="style/bootstrap3.3.2/Content/jquery-ui.css" rel="stylesheet" type="text/css" /> -->
    <!-- javascript -->
    <script src="style/bootstrap3.3.2/Scripts/Respond.js" type="text/javascript"></script>
    <script src="style/bootstrap3.3.2/Scripts/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script src="style/bootstrap3.3.2/Scripts/jquery-2.1.3.js" type="text/javascript"></script>
    <script src="style/bootstrap3.3.2/Scripts/bootstrap.js" type="text/javascript"></script>
    <script src="style/bootstrap3.3.2/Scripts/custom.js" type="text/javascript"></script>
    <script src="style/bootstrap3.3.2/Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="style/bootstrap3.3.2/Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="style/bootstrap3.3.2/Scripts/bootstrapValidator.js" type="text/javascript"></script>
    

<script type="text/javascript">

    $(document).ready(function () {
        $('#form1').bootstrapValidator({
            message: 'این مقدار صحیح نمیباشد',
            fields: {
                firstname: {
                    message: 'این مقدار صحیح نیست',
                    validators: {
                        notEmpty: {
                            message: 'این فیلد نمیتواند خالی باشد'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'بیش از 6 حرف و کمتر از 30 حرف باشد'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: 'تنها مقادیر حرفی عددی نقطه و حط زیر صحیح میباشد'
                        }
                    }
                }
            }
        });

    });
</script>


<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form-group">
        <label class="control-label" for="firstname">Nome:</label>
        <div class="input-group">
            <span class="input-group-addon">$</span>
            <input class="form-control" placeholder="Insira o seu nome próprio" name="firstname" type="text" />
        </div>
    </div>
        
    <div class="form-group">
        <label class="control-label" for="lastname">Apelido:</label>
        <div class="input-group">
            <span class="input-group-addon">€</span>
            <input class="form-control" placeholder="Insira o seu apelido" name="lastname" type="text" />
        </div>
    </div>
    
        <button type="submit" class="btn btn-primary">Submit</button>    </form>
</body>
</html>
