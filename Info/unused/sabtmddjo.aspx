﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="sabtmddjo.aspx.cs" Inherits="Info.sabtmddjo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->

    <script type="text/javascript">
        $(document).ready(function () {
            //get back btn to previous page
            $(document).on("click", "[id*=btngetback]", function () {
                //var sess = '<%=HttpContext.Current.Session["previouspage"].ToString()%>';
                var previouspage = '<%= Session["previouspage"].ToString().ToLower() %>';
                setTimeout(function () { document.location = previouspage }, 200);
            });

            //$('#<%= txtbd_mddjo.ClientID %>').mask("9999/99/99");
            //tab show
            $("#test").click(function () {
                $("#myTab li:eq(1) a").tab("show");
            });

            if ($("#<%=hfield.ClientID %>").val() === "display") {
                $("#tabpanel").fadeOut(1000);
                $("#newmddjo").fadeOut(1000);
                $("#sabtmddjo").fadeIn(1000);

            }
            else if ($("#<%=hfield.ClientID %>").val() === "hide") {
                $("#tabpanel").fadeIn(1000);
                $("#newmddjo").fadeIn(1000);
                $("#sabtmddjo").fadeOut(1000);

            }
            $("#newmddjo").click(function () {
                $("#tabpanel").fadeOut(1000);
                $("#newmddjo").fadeOut(1000);
                $("#sabtmddjo").fadeIn(1000);
            });
            $("#lbtnedit").click(function () {
                $("#tabpanel").fadeOut(1000);
                $("#sabtmddjo").fadeIn(1000);
            });
            //back btn
            $(document).on("click", "[id*=btnback]", function () {
                $("#form1").data("bootstrapValidator").resetForm(true);
                window.location = window.location;
                //window.refresh();
                //window.reload();
            });

            //errore message
            if ($("#<%=messagehf.ClientID %>").val() === "1") {
                $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                $("#message").fadeIn().delay(5000).fadeOut();
                $("#<%=messagehf.ClientID %>").val("");
            }

            //errore message


        });
    </script>

    <!--------------------------------------------------------------------------------------------------------->
    <!---------------------------------------validator--------------------------------------------------------->
    <script type="text/javascript">
        //validator
        $(document).ready(function () {
            $("#form1").bootstrapValidator({
                message: "این مقدار صحیح نمیباشد",
                fields: {
                    "<%= txtfn_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[ا-ی ؤءأإئ]+$/,
                                message: "تنها حروف فارسی مجاز میباشد"
                            }
                        }
                    },
                    "<%= txtln_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[ا-ی ؤءأإئ]+$/,
                                message: "تنها حروف فارسی مجاز میباشد"
                            }
                        }

                    },

                    "<%= txtfan_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[ا-ی ؤءأإئ]+$/,
                                message: "تنها حروف فارسی مجاز میباشد"
                            }
                        }

                    },
                    "<%= txtshm_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 10,
                                max: 10,
                                message: "تعداد ورودی باید بیش از 10 کاراکتر باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    },
                    "<%= txtbd_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))+$/,
                                message: "تاریخ صحیح نمیباشد (--/--/--13)"
                            }
                        }

                    },
                    "<%= ddlmt_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },
                    "<%= txtshh_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 16,
                                max: 16,
                                message: "تعداد ورودی باید 16 کاراکتر باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    },
                    "<%= txttel_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 11,
                                max: 11,
                                message: "تعداد ورودی باید 11  کاراکتر باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    },
                    "<%= txtcel_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 11,
                                max: 11,
                                message: "تعداد ورودی باید 11  کاراکتر باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    },
                    "<%= txtadd_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9ا-ی ؤءأإئ\-]+$/,
                                message: "تنها مقادیر حرفی ، عددی و خط فاصله صحیح میباشد"
                            }
                        }

                    },
                    "<%= ddlnh_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },
                    "<%= ddlsex_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },
                    "<%= ddlsvm_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    }
                }

            });
            //validator
        });
    </script>


    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <div id="container" class="container full-width">
        <div class="row">

            <!--------------------------------------------------Message Box--------------------------------------------->
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <strong>
                            <p id="messagetext" class="text-center">پیام خطا</p>
                        </strong>
                    </div>
                </div>
            </div>
            <!--------------------------------------------------------------------------------------------------------->

            <div class="col-md-12">
                <p class="text-center">
                    <label id="newmddjo" style="cursor: pointer; width: 150px" class="btn btn-success btn-xs">ثبت مددجوی جدید</label>
                    <!--<label id="test" style="cursor: pointer; width: 150px" class="btn btn-success btn-xs">تست</label>-->
                    <label id="btngetback" style="cursor: pointer; width: 150px;" class="btn btn-warning btn-xs">بازگشت به صفحه اصلی</label>
                </p>
                <asp:HiddenField ID="hfield" runat="server" />
                <div class="tab-pane" id="sabtmddjo" style="display: none">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title">
                                اطلاعات مددجو
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label1" runat="server" Text="نام"></asp:Label>
                                        <asp:TextBox ID="txtfn_mddjo" runat="server" Width="175px" CssClass="form-control" placeholder="نام را وارد کنید"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label3" runat="server" Text="نام خانوادگی"></asp:Label>
                                        <asp:TextBox ID="txtln_mddjo" runat="server" Width="175px" CssClass="form-control" placeholder="نام خانوادگی را وارد کنید"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label5" runat="server" Text="نام پدر"></asp:Label>
                                        <asp:TextBox ID="txtfan_mddjo" runat="server" Width="175px" CssClass="form-control" placeholder="نام پدر را وارد کنید"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label9" runat="server" Text="شماره ملی"></asp:Label>
                                        <asp:TextBox ID="txtshm_mddjo" runat="server" Width="175px" CssClass="form-control" placeholder="کد ملی را وارد کنید" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label7" runat="server" Text="تاریخ تولد"></asp:Label>
                                        <asp:TextBox ID="txtbd_mddjo" runat="server" Width="175px" CssClass="form-control" placeholder="لطفا تاریخ تولد را وارد کنید" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label33" runat="server" Text="مدرک تحصیلی"></asp:Label>
                                        <asp:DropDownList ID="ddlmt_mddjo" runat="server" Width="175px" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label6" runat="server" Text="کد مددجویی"></asp:Label>
                                        <asp:TextBox ID="txtshh_mddjo" runat="server" Width="250px" CssClass="form-control" placeholder="کد مددجویی (حمایتی) را وارد کنید" MaxLength="16"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label34" runat="server" Text="تلفن ثابت"></asp:Label>
                                        <asp:TextBox ID="txttel_mddjo" runat="server" Width="250px" CssClass="form-control" placeholder="تلفن را وارد کنید(08122222222)" MaxLength="11"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label35" runat="server" Text="تلفن همراه"></asp:Label>
                                        <asp:TextBox ID="txtcel_mddjo" runat="server" Width="250px" CssClass="form-control" placeholder="موبایل را وارد کنید(09122222222)" MaxLength="11"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label36" runat="server" Text="آدرس"></asp:Label>
                                        <asp:TextBox ID="txtadd_mddjo" runat="server" Width="675px" CssClass="form-control" placeholder="آدرس را وارد کنید"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label37" runat="server" Text="نوع حمایت"></asp:Label>
                                        <asp:DropDownList ID="ddlnh_mddjo" runat="server" Width="250px" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label38" runat="server" Text="جنسیت"></asp:Label>
                                        <asp:DropDownList ID="ddlsex_mddjo" runat="server" Width="175px" CssClass="form-control">
                                            <asp:ListItem Value="True">مرد</asp:ListItem>
                                            <asp:ListItem Value="False">زن</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label4" runat="server" Text="سابقه دریافت وام"></asp:Label>
                                        <asp:DropDownList ID="ddlsvm_mddjo" runat="server" Width="175px" CssClass="form-control">
                                            <asp:ListItem Value="True">دارد</asp:ListItem>
                                            <asp:ListItem Value="False">ندارد</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <br />
                                    <p class="text-center">
                                        <asp:Button ID="btnsabt" runat="server" CssClass="btn btn-success" OnClick="btnsabt_Click" Text="ثبت اطلاعات" Width="175px" />
                                        <input type="button" id="btnback" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" id="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#tempmddjo" aria-controls="tempmddjo" role="tab" data-toggle="tab" style="background-color: blanchedalmond">پرونده های موقت</a></li>
                        <li role="presentation"><a href="#backmddjo" aria-controls="backmddjo" role="tab" data-toggle="tab" style="background-color: cornsilk">پرونده های برگشتی</a></li>
                        <li role="presentation"><a href="#mddjo" aria-controls="mddjo" role="tab" data-toggle="tab" style="background-color: burlywood">پرونده های انجام شده</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tempmddjo">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده موقت
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvtempmddjo" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvmddjo_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ردیف">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="fan_mddjo" HeaderText="نام پدر" />
                                                <asp:BoundField DataField="tblnh.no_nh" HeaderText="نوع حمایت" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="cel_mddjo" HeaderText="موبایل" />
                                                <asp:BoundField DataField="dtesabt_mddjo" HeaderText="تاریخ ثبت" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtnedit" CommandArgument='<%#Eval("id_mddjo")%>' CommandName="doedit" CssClass="btn btn-warning btn-xs">ویرایش</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lbtndelete" CommandArgument='<%#Eval("id_mddjo")%>' CommandName="dodelete" OnClientClick="return confirm('آیا برای حذف اطمینان دارید؟');" CssClass="btn btn-danger btn-xs">حذف</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnfaz1" CommandArgument='<%#Eval("id_mddjo")%>' CommandName="dofaz1" CssClass="btn btn-success btn-xs">ثبت قطعی</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="backmddjo">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده برگشتی
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvbackmddjo" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvmddjo_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ردیف">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:BoundField DataField="tblmddjo.tblnh.no_nh" HeaderText="نوع حمایت" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblmddjo.cel_mddjo" HeaderText="موبایل" />
                                                <asp:BoundField DataField="tblmddjo.dtesabt_mddjo" HeaderText="تاریخ برگشت" />
                                                <asp:BoundField DataField="desc_faz2" HeaderText="علت برگشت" />
                                                <asp:TemplateField HeaderText="برگشت دهنده">
                                                    <ItemTemplate><%#Eval("tblper2.ln_per") + " " + Eval("tblper2.fn_per")%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtnedit" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="doedit" CssClass="btn btn-warning btn-xs">ویرایش</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lbtndelete" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dodelete" OnClientClick="return confirm('آیا برای حذف اطمینان دارید؟');" CssClass="btn btn-danger btn-xs">حذف</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnfaz1" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dofaz1" CssClass="btn btn-success btn-xs">ثبت قطعی پرونده</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="mddjo">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های انجام شده
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvmddjo" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ردیف">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:BoundField DataField="tblmddjo.tblnh.no_nh" HeaderText="نوع حمایت" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblmddjo.cel_mddjo" HeaderText="موبایل" />
                                                <asp:BoundField DataField="dtesabt_faz1" HeaderText="تاریخ ثبت" />
                                                <asp:TemplateField HeaderText="وضعیت">
                                                    <ItemTemplate>
                                                        <itemtemplate><%#Convert.ToInt16(Eval("no_act_vam").ToString()) >=1?"تائید شد":"در انتظار تائید"%></itemtemplate>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
