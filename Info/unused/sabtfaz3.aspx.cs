﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;
using InfoModel;
namespace Info
{
    public partial class sabtfaz3 : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction

        protected void setgvreside()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvreside.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.per_id_faz2 == id_per) && (v.no_act_vam == (int)actlevelvam.Infaz3) && (v.per_id_faz4==null)
                 orderby v.dtesabt_faz2 descending,v.timsabt_faz2 descending 

                 select v).ToList();
            gvreside.DataBind();
        }
        protected void setgvbargashti()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvbargashti.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.per_id_faz2 == id_per) && (v.no_act_vam == (int)actlevelvam.Infaz3) && (v.per_id_faz4 != null)
                 orderby v.dtesabt_faz2 descending, v.timsabt_faz2 descending

                 select v).ToList();
            gvbargashti.DataBind();
        }
        protected void setgvnok()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvnok.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.per_id_faz2 == id_per) && (v.no_act_vam == (int)actlevelvam.Takmil)
                 orderby v.dtesabt_faz3 descending, v.timsabt_faz3 descending 

                 select v).ToList();
            gvnok.DataBind();
        }
        protected void setgvok()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvok.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam >= (int)actlevelvam.Infaz4) &&
                 (v.per_id_faz2 == id_per)
                 orderby v.dtesabt_faz3 descending, v.timsabt_faz3 descending 
                 select v).ToList();
            gvok.DataBind();
        }

        protected void setgrids()
        {
            setgvreside();
            setgvbargashti();
            setgvnok();
            setgvok();
        }

        protected void setddlper_id_faz3()
        {
            int mkh_per_id = Convert.ToInt16(Session["mhkh_per_id"].ToString());
            ddlper_id_faz3.DataSource = (from p in _edbcontext.tblpers
                                         where (p.mhkh_per_id == mkh_per_id) && ((p.nofa_per_id == (int)nofa.KarshenasMasol))
                                         select new { fulname_per = p.ln_per + " " + p.fn_per, p.id_per }).ToList();
            ddlper_id_faz3.DataTextField = "fulname_per";
            ddlper_id_faz3.DataValueField = "id_per";
            ddlper_id_faz3.DataBind();
            ddlper_id_faz3.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }

        protected void initialstate()
        {
            setddlper_id_faz3();
            setgrids();
        }
        protected void clsfield()
        {
            ddlper_id_faz3.SelectedIndex = -1;
            Session["emal"] = "tojihi";
        }
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }


        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if (!IsPostBack)
                    {
                        initialstate();
                        Session["emal"] = "tojihi";
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }

        /*protected void btnbacktojihi_Click(object sender, EventArgs e)
        {
            hfield.Value = "hidedotojihi";
        }*/

        protected void btnsabttojihi_Click(object sender, EventArgs e)
        {
            if (true)
            {
                try
                {
                    int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                    var query = from v in _edbcontext.tblvams
                                where v.mddjo_id == id_mddjo
                                select v;
                    var _vfaz3 = query.FirstOrDefault();
                    _vfaz3.per_id_faz3 = Convert.ToInt16(ddlper_id_faz3.SelectedValue);
                    _vfaz3.desc_faz3 = txtdesc_faz3.Text;
                    _vfaz3.dtesabt_faz3 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                    _vfaz3.timsabt_faz3 = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                              DateTime.Now.Minute.ToString().PadLeft(2, '0');
                    _edbcontext.SaveChanges();
                    myclass.setactlevelvam(actlevelvam.Takmil, id_mddjo);
                    showerrore("1", "پیام سیستم", "طرح توجیهی پرونده با کد پیگیری " + _vfaz3.tblmddjo.cp_mddjo.ToString() + "ثبت گردید");
                    setgrids();
                    txtdesc_faz3.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else
            {
                showerrore("1", "خطای ورود اطلاعات", "کاربر محترم لطفا داده ها را کامل کنید");
            }
            hfield.Value = "hidedotojihi";
        }

        /*protected void btnexit_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("login.aspx");
        }*/
        protected void gvreside_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "dotojihi":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        hfield.Value = "displaydotojihi";
                        break;
                    }
            }
        }
        protected void gvbargashti_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "dotojihi":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        hfield.Value = "displaydotojihi";
                        break;
                    }
            }
        }
        protected void gvnok_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "dotakmil":
                    {
                        int id_mddjo = int.Parse(e.CommandArgument.ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz4 = query.FirstOrDefault();
                        _vfaz4.dtesabt_takmil_faz3 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz4.timsabt_takmil_faz3 = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.Infaz4, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz4.tblmddjo.cp_mddjo.ToString() + "تکمیل شد");
                        setgrids();
                        break;
                    }
            }
        }
    }
}