﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;
using InfoModel;
namespace Info
{
    public partial class sabtfaz4 : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction

        protected void setddlnazer()
        {
            int mkh_per_id = Convert.ToInt16(Session["mhkh_per_id"].ToString());
            ddlnazer.DataSource = (from p in _edbcontext.tblpers
                                   where (p.mhkh_per_id == mkh_per_id) && (p.nofa_per_id == (int)nofa.Nazer)
                                   orderby p.sex_per descending
                                   select new { fulname_per = p.ln_per + " " + p.fn_per, p.id_per }).ToList();
            ddlnazer.DataTextField = "fulname_per";
            ddlnazer.DataValueField = "id_per";
            ddlnazer.DataBind();
            ddlnazer.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }
        protected void setddlmoshaver()
        {
            int mkh_per_id = Convert.ToInt16(Session["mhkh_per_id"].ToString());
            ddlmoshaver.DataSource = (from p in _edbcontext.tblpers
                                      where (p.mhkh_per_id == mkh_per_id) && ((p.nofa_per_id == (int)nofa.Moshaver) || (p.nofa_per_id == (int)nofa.Moshaver2Shahrestan) || (p.nofa_per_id == (int)nofa.Moshaver3Ostan))
                                      orderby p.sex_per descending
                                      select new { fulname_per = p.ln_per + " " + p.fn_per, p.id_per }).ToList();
            ddlmoshaver.DataTextField = "fulname_per";
            ddlmoshaver.DataValueField = "id_per";
            ddlmoshaver.DataBind();
            ddlmoshaver.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }
        protected void setddlnovam()
        {
            ddlnovam.DataSource = _edbcontext.tblnovams.ToList();
            ddlnovam.DataTextField = "desc_novam";
            ddlnovam.DataValueField = "id_novam";
            ddlnovam.DataBind();
            ddlnovam.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }
        protected void setddletebar()
        {
            ddletebar.DataSource = _edbcontext.tbletebars.ToList();
            ddletebar.DataTextField = "desc_etebar";
            ddletebar.DataValueField = "id_etebar";
            ddletebar.DataBind();
            ddletebar.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }
        protected void setddlbank()
        {
            ddlbank.DataSource = _edbcontext.tblbanks.ToList();
            ddlbank.DataTextField = "nam_bank";
            ddlbank.DataValueField = "id_bank";
            ddlbank.DataBind();
            ddlbank.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }
        protected void setddlghtarh()
        {
            ddlghtarh.DataSource = _edbcontext.tblghtarhs.ToList();
            ddlghtarh.DataTextField = "no_ghtarh";
            ddlghtarh.DataValueField = "id_ghtarh";
            ddlghtarh.DataBind();
            ddlghtarh.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }


        protected void setgvnok()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvnok.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.per_id_faz3 == id_per) && (v.no_act_vam == (int)actlevelvam.Infaz4)
                 orderby v.dtesabt_faz3 descending, v.timsabt_faz3 descending 
                 select v).ToList();
            gvnok.DataBind();
        }
        protected void setgvok()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvok.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam == (int)actlevelvam.Etmamfaz4) &&
                 (v.per_id_faz3 == id_per)
                 orderby v.dtesabt_faz4 descending, v.timsabt_faz4 descending 
                 select v).ToList();
            gvok.DataBind();
        }
        protected void setgvnazer()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvnazer.DataSource =
                (from v in _edbcontext.tblvams
                 where ((v.no_act_vam == (int)actlevelvam.MoarefiVam_BankNazer) || (v.no_act_vam == (int)actlevelvam.MoarefiVam_EmdadNazer)) &&
                 (v.per_id_faz4 == id_per)
                 orderby v.dtesabt_faz4 descending, v.timsabt_faz4 descending 
                 select v).ToList();
            gvnazer.DataBind();
        }
        protected void setgvmoshaver()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvmoshaver.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam == (int)actlevelvam.MoarefiVam_BankMoshaver) &&
                 (v.per_id_faz4 == id_per)
                 orderby v.dtesabt_faz4 descending, v.timsabt_faz4 descending 
                 select v).ToList();
            gvmoshaver.DataBind();
        }

        protected void setgrids()
        {
            setgvnok();
            setgvok();
            setgvnazer();
            setgvmoshaver();
        }

        protected void initialstate()
        {
            setgrids();
            setddlnovam();
            setddletebar();
            setddlbank();
            setddlghtarh();
            setddlnazer();
            setddlmoshaver();
        }
        protected void clsfield()
        {
            //Session["emal"] = "dook";
            txtdesc_faz4.Text = string.Empty;
            txtnamtarh_faz4.Text = string.Empty;
            txtshtasvib_faz4.Text = string.Empty;
            txtmabvam_faz4.Text = string.Empty;
            txtshp_mddjo.Text = string.Empty;
            ddlbank.SelectedIndex = -1;
            ddletebar.SelectedIndex = -1;
            ddlghtarh.SelectedIndex = -1;
            ddlmoshaver.SelectedIndex = -1;
            ddlnazer.SelectedIndex = -1;
            ddlnovam.SelectedIndex = -1;
            //messagehf.Value = "";
        }
        private void objectokvisible()
        {
            Label36.Visible = false;
            txtdesc_faz4.Visible = false;
            Label37.Visible = true;
            txtnamtarh_faz4.Visible = true;
            Label38.Visible = true;
            txtshtasvib_faz4.Visible = true;
            Label39.Visible = true;
            txtmabvam_faz4.Visible = true;
            Label4.Visible = true;
            //txtshp_mddjo.Visible = true;
            btnsabt.Text = "تصویب پرونده";

            Label7.Visible = true;
            ddlbank.Visible = true;
            Label9.Visible = true;
            txtshbank.Visible = true;
            Label1.Visible = true;
            ddletebar.Visible = true;
            Label8.Visible = true;
            ddlghtarh.Visible = true;
            Label3.Visible = true;
            ddlnovam.Visible = true;
        }
        private void objectnokvisible()
        {
            Label36.Visible = true;
            txtdesc_faz4.Visible = true;
            Label36.Text = "علت عدم تصویب طرح";
            Label37.Visible = false;
            txtnamtarh_faz4.Visible = false;
            Label38.Visible = false;
            txtshtasvib_faz4.Visible = false;
            Label39.Visible = false;
            txtmabvam_faz4.Visible = false;
            Label4.Visible = false;
            //txtshp_mddjo.Visible = false;
            btnsabt.Text = "عدم تصویب پرونده";

            Label7.Visible = false;
            ddlbank.Visible = false;
            Label9.Visible = false;
            txtshbank.Visible = false;
            Label1.Visible = false;
            ddletebar.Visible = false;
            Label8.Visible = false;
            ddlghtarh.Visible = false;
            Label3.Visible = false;
            ddlnovam.Visible = false;

        }

        protected void objectmoshavervisible()
        {
            Label6.Visible = false;
            ddlnazer.Visible = false;
            Label11.Visible = true;
            ddlmoshaver.Visible = true;
        }
        protected void objectnazervisible()
        {
            Label6.Visible = true;
            ddlnazer.Visible = true;
            Label11.Visible = false;
            ddlmoshaver.Visible = false;

        }

        public string getnoerja(int code)
        {
            if (code == (int)actlevelvam.MoarefiVam_EmdadNazer)
                return "امدادی";
            else if ((code == (int)actlevelvam.MoarefiVam_BankNazer) || (code == (int)actlevelvam.MoarefiVam_BankMoshaver))
                return "بانکی";
            else return "";
        }
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }



        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            txtshp_mddjo.Visible = false;
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if (!IsPostBack)
                    {
                        initialstate();
                        Session["emal"] = "dook";
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }

        /*protected void btnbacksabt_Click(object sender, EventArgs e)
        {
            hfield.Value = "hidesabtfile";
        }*/

        protected void btnsabt_Click(object sender, EventArgs e)
        {
            if (true)
            {
                try
                {
                    if (Session["emal"].ToString().Equals("dook"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz4 = query.FirstOrDefault();
                        _vfaz4.per_id_faz4 = Convert.ToInt16(Session["id_per"].ToString());
                        _vfaz4.desc_faz4 = txtdesc_faz4.Text;
                        _vfaz4.bank_id_faz1 = Convert.ToInt16(ddlbank.SelectedValue);
                        _vfaz4.shbank_faz1 = txtshbank.Text;
                        _vfaz4.etebar_id_faz1 = Convert.ToInt16(ddletebar.SelectedValue);
                        _vfaz4.ghtarh_id_faz1 = Convert.ToInt16(ddlghtarh.SelectedValue);
                        _vfaz4.novam_id_faz1 = Convert.ToInt16(ddlnovam.SelectedValue);
                        _vfaz4.mabvam_faz4 = Convert.ToInt32(txtmabvam_faz4.Text);
                        _vfaz4.namtarh_faz4 = txtnamtarh_faz4.Text;
                        //تغییرات اعمال شده در تاریخ 1394/03/26
                        //_vfaz4.shtasvib_faz4 =  txtshtasvib_faz4.Text;
                        //_vfaz4.tblmddjo.shp_mddjo = txtshp_mddjo.Text;
                        _vfaz4.shtasvib_faz4 = Session["cod_mhkh"].ToString() + txtshtasvib_faz4.Text;
                        if (_vfaz4.mabvam_faz4 < 5000000 || _vfaz4.mabvam_faz4 > 1000000000)
                        {
                            showerrore("1", "خطای ورود اطلاعات", "مبلغ وام در محدوده مجاز نیست");
                            return;
                        }
                        if (_vfaz4.namtarh_faz4.Equals(string.Empty) || _vfaz4.shtasvib_faz4.Equals(string.Empty) || _vfaz4.tblmddjo.shp_mddjo.Equals(string.Empty))
                        {
                            showerrore("1", "خطای ورود اطلاعات", "اطلاعات کامل نیست لطفا تمام فیلدها را تکمیل کنید");
                            return;
                        }


                        _vfaz4.dtesabt_faz4 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz4.timsabt_faz4 = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.Etmamfaz4, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz4.tblmddjo.cp_mddjo.ToString() + "تصویب گردید");
                        setgrids();
                    }
                    if (Session["emal"].ToString().Equals("donok"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz4 = query.FirstOrDefault();
                        _vfaz4.per_id_faz4 = Convert.ToInt16(Session["id_per"].ToString());
                        _vfaz4.desc_faz4 = txtdesc_faz4.Text;
                        _vfaz4.dtesabt_faz3 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz4.timsabt_faz3 = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.Infaz3, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz4.tblmddjo.cp_mddjo.ToString() + "تصویب نگردید");
                        setgrids();
                    }
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else
            {
                showerrore("1", "خطای ورود اطلاعات", "کاربر محترم لطفا داده ها را کامل کنید");
            }
            clsfield();
            hfield.Value = "hidesabtfile";
        }

        protected void btnexit_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("login.aspx");
        }
        protected void gvnok_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "dook":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "dook";
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz4 = query.FirstOrDefault();

                        ddlbank.SelectedValue = _vfaz4.bank_id_faz1.ToString();
                        txtshbank.Text = _vfaz4.shbank_faz1.ToString();
                        ddletebar.SelectedValue = _vfaz4.etebar_id_faz1.ToString();
                        ddlghtarh.SelectedValue = _vfaz4.ghtarh_id_faz1.ToString();
                        ddlnovam.SelectedValue = _vfaz4.novam_id_faz1.ToString();
                        txtmabvam_faz4.Text = _vfaz4.mabvam_faz1.ToString();
                        objectokvisible();
                        hfield.Value = "displaysabtfile";
                        break;
                    }
                case "donok":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "donok";
                        objectnokvisible();
                        hfield.Value = "displaysabtfile";
                        break;
                    }
            }
        }

        protected void gvok_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "doemdadi":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "doemdadi";
                        btnsabtersal.Text = "ارجاع امدادی";
                        objectnazervisible();
                        hfield.Value = "displaysabtersal";
                        clsfield();
                        break;
                    }
                case "dobankinazer":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "dobankinazer";
                        btnsabtersal.Text = "ارجاع بانکی";
                        objectnazervisible();
                        hfield.Value = "displaysabtersal";
                        clsfield();
                        break;
                    }
                case "dobankimoshaver":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "dobankimoshaver";
                        objectmoshavervisible();
                        hfield.Value = "displaysabtersal";
                        clsfield();
                        break;
                    }
            }
        }

        /*protected void btnbacksabtersal_Click(object sender, EventArgs e)
        {
            hfield.Value = "hidesabtersal";
        }*/

        protected void btnsabtersal_Click(object sender, EventArgs e)
        {
            if (true)
            {
                try
                {
                    if (Session["emal"].ToString().Equals("doemdadi"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();

                        _vfaz5.per_id_faz5 = Convert.ToInt16(ddlnazer.SelectedValue);
                        _vfaz5.dtesabt_faz5 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_faz5 = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.MoarefiVam_EmdadNazer, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "به ناظر مربوطه ارجاع گردید");
                        setgrids();
                    }
                    if (Session["emal"].ToString().Equals("dobankinazer"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();

                        _vfaz5.per_id_faz5 = Convert.ToInt16(ddlnazer.SelectedValue);
                        _vfaz5.dtesabt_faz5 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_faz5 = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.MoarefiVam_BankNazer, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "به ناظر مربوطه ارجاع گردید");
                        setgrids();

                    }
                    if (Session["emal"].ToString().Equals("dobankimoshaver"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();

                        _vfaz5.per_id_faz5 = Convert.ToInt16(ddlmoshaver.SelectedValue);
                        _vfaz5.dtesabt_faz5 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_faz5 = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevelvam(actlevelvam.MoarefiVam_BankMoshaver, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "به مشاور مربوطه ارجاع گردید");
                        setgrids();
                    }
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else
            {
                showerrore("1", "خطای ورود اطلاعات", "کاربر محترم لطفا داده ها را کامل کنید");
            }
            clsfield();
            hfield.Value = "hidesabtersal";
        }

    }
}