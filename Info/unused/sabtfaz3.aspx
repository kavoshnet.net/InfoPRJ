﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="sabtfaz3.aspx.cs" Inherits="Info.sabtfaz3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <script type="text/javascript">
        $(document).ready(function () {
            //get back btn to previous page
            $(document).on("click", "[id*=btngetback]", function () {
                //var sess = '<%=HttpContext.Current.Session["previouspage"].ToString()%>';
                var previouspage = '<%= Session["previouspage"].ToString().ToLower() %>';
                setTimeout(function () { document.location = previouspage }, 200);
            });

            //back btnbacktojihi
            $(document).on("click", "[id*=btnbacktojihi]", function () {
                $("#form1").data("bootstrapValidator").resetForm(true);
                $("#<%=hfield.ClientID %>").val("hidedotojihi");
                window.location = window.location;
                //window.refresh();
                //window.reload();
            });


            if ($("#<%=hfield.ClientID %>").val() === "displaydotojihi") {
                $("#sabttojihi").fadeIn(1000);
                $("#tabpanel").fadeOut(0);

            }
            if ($("#<%=hfield.ClientID %>").val() === "hidedotojihi") {
                $("#tabpanel").fadeIn(1000);
                $("#sabttojihi").fadeOut(0);
                $("#myTab li:eq(2) a").tab("show");

            }
            //errore message
            if ($("#<%=messagehf.ClientID %>").val() === "1") {
                $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                $("#message").fadeIn().delay(3000).fadeOut();
                $("#<%=messagehf.ClientID %>").val("");
            }
            //errore message

        });
    </script>
    <!--------------------------------------------------------------------------------------------------------->
    <!---------------------------------------validator--------------------------------------------------------->
    <script type="text/javascript">
        //validator
        $(document).ready(function () {
            $("#form1").bootstrapValidator({
                message: "این مقدار صحیح نمیباشد",
                fields: {
                    "<%= txtdesc_faz3.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9ا-ی ؤءأإئ\-]+$/,
                                message: "تنها مقادیر حرفی ، عددی و خط فاصله صحیح میباشد"
                            }
                        }
                    },
                    "<%= ddlper_id_faz3.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    }

                }

            });
            //validator
        });
    </script>

    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <div id="container" class="container full-width">
        <div class="row">
            <!--------------------------------------------------Message Box--------------------------------------------->
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <strong>
                            <p id="messagetext" class="text-center">پیام خطا</p>
                        </strong>
                    </div>
                </div>
            </div>
            <!--------------------------------------------------------------------------------------------------------->
            <div class="col-lg-12">
                <p class="text-center">
                    <label id="btngetback" style="cursor: pointer; width: 150px;" class="btn btn-warning btn-xs">بازگشت به صفحه اصلی</label>
                </p>
                <asp:HiddenField ID="hfield" runat="server" />
                <div role="tabpanel" class="tab-pane" id="sabttojihi" style="display: none">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="panel-title">
                                ثبت طرح توجیهی
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label36" runat="server" Text="خلاصه طرح توجیهی"></asp:Label>
                                    <asp:TextBox ID="txtdesc_faz3" runat="server" placeholder="خلاصه طرح توجیهی جهت تصویب تایپ شود"
                                        Width="500px" Height="200px" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label37" runat="server" Text="کارشناس مسئول"></asp:Label>
                                    <asp:DropDownList ID="ddlper_id_faz3" runat="server" Width="175px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <br />
                                <p class="text-center">
                                    <asp:Button ID="btnsabttojohi" runat="server" CssClass="btn btn-success" OnClick="btnsabttojihi_Click" Text="ثبت طرح" Width="175px" />
                                    <input type="button" id="btnbacktojihi" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" id="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#reside" aria-controls="reside" role="tab" data-toggle="tab" style="background-color: deepskyblue">پرونده های بدون طرح توجیهی</a></li>
                        <li role="presentation"><a href="#bargashti" aria-controls="bargashti" role="tab" data-toggle="tab" style="background-color: #9ACD32">پرونده های برگشتی</a></li>
                        <li role="presentation"><a href="#nokfile" aria-controls="nokfile" role="tab" data-toggle="tab" style="background-color: lightcoral">پرونده های در انتظار تکمیل</a></li>
                        <li role="presentation"><a href="#okfile" aria-controls="okfile" role="tab" data-toggle="tab" style="background-color: palegoldenrod">پرونده های تکمیل شده</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="reside">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های در انتظار ثبت طرح توجیهی
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvreside" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvreside_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_faz2" HeaderText="تاریخ ثبت ناظر" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtsabttojihi" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dotojihi" CssClass="btn btn-success btn-xs">ثبت طرح </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="bargashti">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های تصویب نشده توسط کارشناس مسئول
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvbargashti" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvbargashti_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_faz3" HeaderText="تاریخ برگشت" />
                                                <asp:TemplateField HeaderText="برگشت دهنده">
                                                    <ItemTemplate><%#Eval("tblper4.ln_per") + " " + Eval("tblper4.fn_per")%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="desc_faz4" HeaderText="علت برگشت" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtsabttojihi" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dotojihi" CssClass="btn btn-success btn-xs">ثبت طرح </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="nokfile">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های در انتظار تکمیل فیزیکی
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvnok" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvnok_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_faz3" HeaderText="تاریخ ثبت طرح" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbttakmil" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dotakmil" CssClass="btn btn-success btn-xs">تکمیل شد</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="okfile">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های تکمیل شده
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvok" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_takmil_faz3" HeaderText="تاریخ تکمیل پرونده" />
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
