﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;
namespace Info
{
    public partial class moshaver : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {

                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
            if (Session["no_nofa"] != null)
            {
                int no_fa = int.Parse(Session["no_nofa"].ToString());
                if (no_fa == (int)nofa.Moshaver1Shahrestan)
                {
                    sabttarh.Visible = false;
                    peygiri.Visible = true;
                    sabtsamane.Visible = false;
                    sabtparvande.Visible = true;
                    sabtamo.Visible = false;
                    sabtkar.Visible = true;
                }
                else if ((no_fa == (int)nofa.Moshaver1Ostan) || (no_fa == (int)nofa.Moshaver2Ostan))
                {
                    sabttarh.Visible = true;
                    peygiri.Visible = false;
                    sabtsamane.Visible = true;
                    sabtparvande.Visible = true;
                    sabtamo.Visible = true;
                    sabtkar.Visible = false;
                }
            }
            else
                Response.Redirect("login.aspx");

        }

        protected void sabtparvande_Click(object sender, EventArgs e)
        {
            Session["previouspage"] = Request.UrlReferrer.ToString();
            Response.Redirect("sabtmddjo.aspx");
        }

        protected void sabttarh_Click(object sender, EventArgs e)
        {
            Session["previouspage"] = Request.UrlReferrer.ToString();
            Response.Redirect("sabtfaz3.aspx");
        }

        protected void peygiri_Click(object sender, EventArgs e)
        {
            Session["previouspage"] = Request.UrlReferrer.ToString();
            Response.Redirect("pobank.aspx");
        }

        protected void sabtsamane_Click(object sender, EventArgs e)
        {
            Session["previouspage"] = Request.UrlReferrer.ToString();
            Response.Redirect("sabtsamane.aspx");
        }
        protected void sabtamo_Click(object sender, EventArgs e)
        {
            Session["previouspage"] = Request.UrlReferrer.ToString();
            Response.Redirect("sabtamo.aspx");
        }
        protected void sabtkar_Click(object sender, EventArgs e)
        {
            Session["previouspage"] = Request.UrlReferrer.ToString();
            Response.Redirect("sabtkar.aspx");
        }
    }
}