﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="sabtsamane.aspx.cs" Inherits="Info.sabtsamane" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <script type="text/javascript">
        $(document).ready(function () {

            //get back btn to previous page
            $(document).on("click", "[id*=btngetback]", function () {
                //var sess = '<%=HttpContext.Current.Session["previouspage"].ToString()%>';
                var previouspage = '<%= Session["previouspage"].ToString().ToLower() %>';
                setTimeout(function () { document.location = previouspage }, 200);
            });
            //back sabtvam btn
            $(document).on("click", "[id*=btnbacksabtsam]", function () {
                $("#form1").data("bootstrapValidator").resetForm(true);
                $("#<%=hfield.ClientID %>").val("hidedook");
                $("#sabtsamane").fadeOut(0);
                $("#tabpanel").fadeIn(1000);
                //setTimeout(function () { document.location = "sabtfaz2.aspx"; }, 3000);
                //window.refresh();
                //window.reload();
            });

            if ($("#<%=hfield.ClientID %>").val() === "displaydook") {
                $("#sabtsamane").fadeIn(1000);
                $("#tabpanel").fadeOut(0);

            }
            if ($("#<%=hfield.ClientID %>").val() === "hidedook") {
                $("#tabpanel").fadeIn(1000);
                $("#sabtsamane").fadeOut(0);

            }
            $("#<%=txtcp_sam.ClientID %>").val("");
            $("#<%=txtdesc_sam.ClientID %>").val("");
            $("#<%=txtdtesabt_sam.ClientID %>").val("");

            //errore message
            if ($("#<%=messagehf.ClientID %>").val() === "1") {
                $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                $("#message").fadeIn().delay(3000).fadeOut();
                $("#<%=messagehf.ClientID %>").val("");
            }
            //errore message
        });
    </script>

    <!--------------------------------------------------------------------------------------------------------->
    <!---------------------------------------validator--------------------------------------------------------->
    <script type="text/javascript">
        //validator
        $(document).ready(function () {
            $("#form1").bootstrapValidator({
                message: "این مقدار صحیح نمیباشد",
                fields: {
                    "<%= ddlnosam_id.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },
                    "<%= txtcp_sam.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 6,
                                max: 13,
                                message: "تعداد ورودی باید بین 6 تا 13 کاراکتر باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    },
                    "<%= txtdesc_sam.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9ا-ی ؤءأإئ\-]+$/,
                                message: "تنها مقادیر حرفی ، عددی و خط فاصله صحیح میباشد"
                            }
                        }

                    },
                    "<%= txtdtesabt_sam.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))+$/,
                                message: "تاریخ صحیح نمیباشد (--/--/--13)"
                            }
                        }

                    }
                }

            });
            //validator
        });
    </script>
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <div id="container" class="container full-width">
        <div class="row">
            <!--------------------------------------------------Message Box--------------------------------------------->
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <strong>
                            <p id="messagetext" class="text-center">پیام خطا</p>
                        </strong>
                    </div>
                </div>
            </div>
            <!--------------------------------------------------------------------------------------------------------->
            <div class="col-lg-12">
                <p class="text-center">
                    <label id="btngetback" style="cursor: pointer; width: 150px;" class="btn btn-warning btn-xs">بازگشت به صفحه اصلی</label>
                </p>
                <asp:HiddenField ID="hfield" runat="server" />
                <div role="tabpanel" class="tab-pane" id="sabtsamane" style="display: none">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="panel-title">
                                ثبت سامانه
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label2" runat="server" Text="کدرهگیری"></asp:Label>
                                    <asp:TextBox ID="txtcp_sam" runat="server" CssClass="form-control" Width="300px" placeholder="کد رهگیری را وارد کنید" MaxLength="13"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label3" runat="server" Text="توضیحات"></asp:Label>
                                    <asp:TextBox ID="txtdesc_sam" runat="server" CssClass="form-control" Width="300px" placeholder="توضیحات را وارد کنید"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label4" runat="server" Text="تاریخ ثبت"></asp:Label>
                                    <asp:TextBox ID="txtdtesabt_sam" runat="server" CssClass="form-control" Width="300px" placeholder="تاریخ ثبت را وارد کنید"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label1" runat="server" Text="نوع سامانه"></asp:Label>
                                    <asp:DropDownList ID="ddlnosam_id" runat="server" Width="175px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <br />
                                <p class="text-center">
                                    <asp:Button ID="btnsabtsam" runat="server" CssClass="btn btn-success" OnClick="btnsabtsam_Click" Text="ثبت" Width="175px" />
                                    <input type="button" id="btnbacksabtsam" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" id="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#nokfile" aria-controls="nokfile" role="tab" data-toggle="tab" style="background-color: cornflowerblue">پرونده های رسیده</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="nokfile">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های ثبت نشده
                                    </div>
                                </div>
                                <div class="panel-body">
                                   <!-- <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <asp:CheckBoxList ID="cblnosam" runat="server" AutoPostBack="True" RepeatColumns="4" RepeatDirection="Horizontal" Width="100%">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div> 
                                    </div> -->
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvnok" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvnok_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtok" CommandArgument='<%#Eval("id_mddjo")%>' CommandName="dook" CssClass="btn btn-success btn-xs">ثبت سامانه</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
