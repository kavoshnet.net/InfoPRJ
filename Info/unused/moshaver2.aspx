﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="moshaver2.aspx.cs" Inherits="Info.moshaver2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <div id="container" class="container full-width">
        <div class="row">
                <div class="col-md-3 text-center">
                    <asp:Button ID="sabttarh" runat="server" CssClass="btn btn-danger" Text="ثبت طرح توجیهی" Width="175px" OnClick="sabttarh_Click" />
                </div>

<%--                <div class="col-md-3 text-center">
                    <asp:Button ID="peygiri" runat="server" CssClass="btn btn-primary" Text="پیگیری امور بانکی" Width="175px" OnClick="peygiri_Click" />
                </div>--%>

                <div class="col-md-3 text-center">
                    <asp:Button ID="sabtsamane" runat="server" CssClass="btn btn-success" Text="ثبت سامانه ها" Width="175px" OnClick="sabtsamane_Click" />
                </div>
                <div class="col-md-3 text-center">
                    <asp:Button ID="sabtamo" runat="server" CssClass="btn btn-default" Text="آموزش" Width="175px" OnClick="sabtamo_Click" />
                </div>
        </div>
    </div>
</asp:Content>
