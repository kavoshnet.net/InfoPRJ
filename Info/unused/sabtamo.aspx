﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="sabtamo.aspx.cs" Inherits="Info.sabtamo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <script type="text/javascript">
        $(document).ready(function () {

            //get back btn to previous page
            $(document).on("click", "[id*=btngetback]", function () {
                //var sess = '<%=HttpContext.Current.Session["previouspage"].ToString()%>';
                var previouspage = '<%= Session["previouspage"].ToString().ToLower() %>';
                setTimeout(function () { document.location = previouspage }, 200);
            });
            //back btn
            $(document).on("click", "[id*=btnback]", function () {
                $("#form1").data("bootstrapValidator").resetForm(true);
                setTimeout(function () { document.location = "sabtamo.aspx"; }, 200);
            });

            if ($("#<%=hfield.ClientID %>").val() === "displaytashkil") {
                $("#tashkil").fadeIn(1000);
                $("#tabpanel").fadeOut(0);

            }
            if ($("#<%=hfield.ClientID %>").val() === "hidetashkil") {
                $("#tabpanel").fadeIn(1000);
                $("#tashkil").fadeOut(0);

            }
            if ($("#<%=hfield.ClientID %>").val() === "displaymoarefi") {
                $("#moarefi").fadeIn(1000);
                $("#tabpanel").fadeOut(0);
            }
            if ($("#<%=hfield.ClientID %>").val() === "hidemoarefi") {
                $("#tabpanel").fadeIn(1000);
                $("#moarefi").fadeOut(0);
            }
            if ($("#<%=hfield.ClientID %>").val() === "displaynatije") {
                $("#natije").fadeIn(1000);
                $("#tabpanel").fadeOut(0);
            }
            if ($("#<%=hfield.ClientID %>").val() === "hidenatije") {
                $("#tabpanel").fadeIn(1000);
                $("#natije").fadeOut(0);
            }
            if ($("#<%=hfield.ClientID %>").val() === "displaymonjar") {
                $("#monjar").fadeIn(1000);
                $("#tabpanel").fadeOut(0);
            }
            if ($("#<%=hfield.ClientID %>").val() === "hidemonjar") {
                $("#tabpanel").fadeIn(1000);
                $("#monjar").fadeOut(0);
            }

            $("#<%=txtdtetak_amo.ClientID %>").val("");
            $("#<%=txtshtasvib_amo.ClientID %>").val("");
            $("#<%=txtnam_amo.ClientID %>").val("");
            $("#<%=txtdtestramo.ClientID %>").val("");
            $("#<%=txtshtasvib_amo.ClientID %>").val("");
            $("#<%=txtshtasvib_amo.ClientID %>").val("");
            $("#<%=txtshtasvib_amo.ClientID %>").val("");
            $("#<%=txtshtasvib_amo.ClientID %>").val("");

            //errore message
            if ($("#<%=messagehf.ClientID %>").val() === "1") {
                $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                $("#message").fadeIn().delay(3000).fadeOut();
                $("#<%=messagehf.ClientID %>").val("");
            }
            //errore message
        });
    </script>

    <!--------------------------------------------------------------------------------------------------------->
    <!---------------------------------------validator--------------------------------------------------------->
    <script type="text/javascript">
        //validator
        $(document).ready(function () {
            $("#form1").bootstrapValidator({
                message: "این مقدار صحیح نمیباشد",
                fields: {

                    "<%= txtdtetak_amo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))+$/,
                                message: "تاریخ صحیح نمیباشد (--/--/--13)"
                            }
                        }
                    },
                    "<%= txtshtasvib_amo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 3,
                                max: 14,
                                message: "تعداد ورودی باید بین 3 تا 14 رقم باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    },

                    "<%= ddlsarfasl_id.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },

                    "<%= ddlonvan_id.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },
                    "<%= txtnam_amo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9ا-ی ؤءأإئ\-]+$/,
                                message: "تنها مقادیر حرفی ، عددی و خط فاصله صحیح میباشد"
                            }
                        }

                    },
                    "<%= txtdtestramo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))+$/,
                                message: "تاریخ صحیح نمیباشد (--/--/--13)"
                            }
                        }

                    },
                    "<%= ddlnatije_amo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },
                    "<%= txtdtesod_amo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))+$/,
                                message: "تاریخ صحیح نمیباشد (--/--/--13)"
                            }
                        }

                    },
                    "<%= ddlesh_id.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    }
                }

            });
            //validator
        });
    </script>
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <div id="container" class="container full-width">
        <div class="row">
            <!--------------------------------------------------Message Box--------------------------------------------->
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <strong>
                            <p id="messagetext" class="text-center">پیام خطا</p>
                        </strong>
                    </div>
                </div>
            </div>
            <!--------------------------------------------------------------------------------------------------------->
            <div class="col-lg-12">
                <asp:HiddenField ID="hfield" runat="server" />
                <p class="text-center">
                    <label id="btngetback" style="cursor: pointer; width: 150px;" class="btn btn-warning btn-xs">بازگشت به صفحه اصلی</label>
                </p>
                <div role="tabpanel" class="tab-pane" id="tashkil" style="display: none">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="panel-title">
                                تشکیل پرونده(فرم تصویب)
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label36" runat="server" Text="تاریخ تشکیل پرونده"></asp:Label>
                                    <asp:TextBox ID="txtdtetak_amo" runat="server" Width="300px" CssClass="form-control" placeholder="تاریخ را وارد کنید"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label8" runat="server" Text="شماره تصویب"></asp:Label>
                                    <asp:TextBox ID="txtshtasvib_amo" runat="server" Width="300px" CssClass="form-control" placeholder="شماره تصویب را وارد کنید" MaxLength="14"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <br />
                                <p class="text-center">
                                    <asp:Button ID="btntashkil" runat="server" CssClass="btn btn-success" OnClick="btntashkil_Click" Text="ثبت اطلاعات" Width="175px" />
                                    <input type="button" id="btnback" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="moarefi" style="display: none">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="panel-title">
                                معرفی و هدایت کارآموزان به مراکز آموزشی
                            </div>
                        </div>
                        <div class="panel-body">
                        <div class="form-group"></div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label4" runat="server" Text="سرفصل آموزشی"></asp:Label>
                                    <asp:DropDownList ID="ddlsarfasl_id" runat="server" Width="175px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label9" runat="server" Text="عنوان دوره آموزشی"></asp:Label>
                                    <asp:DropDownList ID="ddlonvan_id" runat="server" Width="175px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label6" runat="server" Text="نام آموزشگاه"></asp:Label>
                                    <asp:TextBox ID="txtnam_amo" runat="server" CssClass="form-control" Width="300px" placeholder="نام آموزشگاه را وارد کنید"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label7" runat="server" Text="تاریخ شروع دوره"></asp:Label>
                                    <asp:TextBox ID="txtdtestramo" runat="server" Width="300px" CssClass="form-control" placeholder="تاریخ را وارد کنید"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-4">
                                </div>
                            </div>
                                <p class="text-center">
                                    <asp:Button ID="btnmoarefi" runat="server" CssClass="btn btn-success" OnClick="btnmoarefi_Click" Text="معرفی" Width="175px" />
                                    <input type="button" id="btnback" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="natije" style="display: none">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="panel-title">
                                دریافت و ارائه گواهینامه پایان دوره به امداد
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label38" runat="server" Text="نتیجه آزمون"></asp:Label>
                                    <asp:DropDownList ID="ddlnatije_amo" runat="server" Width="175px" CssClass="form-control">
                                        <asp:ListItem Value="">انتخاب شود</asp:ListItem>
                                        <asp:ListItem Value="True">قبول</asp:ListItem>
                                        <asp:ListItem Value="False">مردود</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label1" runat="server" Text="تاریخ صدور گواهینامه"></asp:Label>
                                    <asp:TextBox ID="txtdtesod_amo" runat="server" Width="300px" CssClass="form-control" placeholder="تاریخ را وارد کنید"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <br />
                                <p class="text-center">
                                    <asp:Button ID="btnnatije" runat="server" CssClass="btn btn-success" OnClick="btnnatije_Click" Text="ثبت نتیجه" Width="175px" />
                                    <input type="button" id="btnback" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="monjar" style="display: none">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="panel-title">
                                 پیگیری و منجر شدن آموزش به اشتغال (غیر از افراد شاغل)
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label5" runat="server" Text="منجر به اشتغال"></asp:Label>
                                    <asp:DropDownList ID="ddlesh_id" runat="server" Width="175px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <br />
                                <p class="text-center">
                                    <asp:Button ID="btnmonjar" runat="server" CssClass="btn btn-success" OnClick="btnmonjar_Click" Text="ثبت اطلاعات" Width="175px" />
                                    <input type="button" id="btnback" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" id="tabpanel">
                    <%-- از این قسمت به بعد مانده ایجاد تب ها و قسمت برنامه نویسی و ولیدیتها --%>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#tabmddjo" aria-controls="tabmddjo" role="tab" data-toggle="tab" style="background-color: cornflowerblue">اطلاعات مددجویان</a></li>
                        <li role="presentation"><a href="#tabtashkil" aria-controls="tabtashkil" role="tab" data-toggle="tab" style="background-color: darkcyan">تشکیل شده</a></li>
                        <li role="presentation"><a href="#tabmoarefi" aria-controls="tabmoarefi" role="tab" data-toggle="tab" style="background-color: darkturquoise">معرفی شده</a></li>
                        <li role="presentation"><a href="#tabnatije" aria-controls="tabnatije" role="tab" data-toggle="tab" style="background-color: dodgerblue">آزمون داده</a></li>
                        <li role="presentation"><a href="#tabmonjar" aria-controls="tabmonjar" role="tab" data-toggle="tab" style="background-color: royalblue">تکمیل شده</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tabmddjo">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        در انتظار تشکیل پرونده آموزش
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvmddjo" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvmddjo_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="fan_mddjo" HeaderText="نام پدر" />
                                                <asp:BoundField DataField="tblnh.no_nh" HeaderText="نوع حمایت" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="cel_mddjo" HeaderText="موبایل" />
                                                <asp:BoundField DataField="dtesabt_mddjo" HeaderText="تاریخ ثبت" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbttashkil" CommandArgument='<%#Eval("id_mddjo")%>' CommandName="dotashkil" CssClass="btn btn-success btn-xs">تشکیل پرونده</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="tabtashkil">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های در انتظار معرفی به آموزشگاه
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvtashkil" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvtashkil_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:BoundField DataField="tblmddjo.tblnh.no_nh" HeaderText="نوع حمایت" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblmddjo.cel_mddjo" HeaderText="موبایل" />
                                                <asp:BoundField DataField="dtesabttak_amo" HeaderText="تاریخ تشکیل" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtmoarefi" CommandArgument='<%#Eval("mddjo_id_amo")%>' CommandName="domoarefi" CssClass="btn btn-success btn-xs">معرفی</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="tabmoarefi">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های در انتظار دریافت نتیجه‌آزمون
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvmoarefi" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvmoarefi_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:BoundField DataField="tblmddjo.tblnh.no_nh" HeaderText="نوع حمایت" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="nam_amo" HeaderText="آموزشگاه" />
                                                <asp:BoundField DataField="tblmddjo.cel_mddjo" HeaderText="موبایل" />
                                                <asp:BoundField DataField="dtemor_amo" HeaderText="تاریخ معرفی" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtnatije" CommandArgument='<%#Eval("mddjo_id_amo")%>' CommandName="donatije" CssClass="btn btn-success btn-xs">نتیجه آزمون</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="tabnatije">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های در انتظار اعلام نتیجه نهایی دوره آموزش
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvnatije" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvnatije_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:BoundField DataField="tblmddjo.tblnh.no_nh" HeaderText="نوع حمایت" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="نتیجه">
                                                    <ItemTemplate><%#Eval("natije_amo").ToString()=="True"?"قبول":"مردود"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblmddjo.cel_mddjo" HeaderText="موبایل" />
                                                <asp:BoundField DataField="dtenat_amo" HeaderText="تاریخ نتیجه" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtmonjar" CommandArgument='<%#Eval("mddjo_id_amo")%>' CommandName="domonjar" CssClass="btn btn-success btn-xs">نتیجه نهایی</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="tabmonjar">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های تکمیل شده در مرحله آموزش
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvmonjar" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:BoundField DataField="tblmddjo.tblnh.no_nh" HeaderText="نوع حمایت" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblonvamo.desc_onv" HeaderText="دوره" />
                                                <asp:BoundField DataField="tblmddjo.cel_mddjo" HeaderText="موبایل" />
                                                <asp:BoundField DataField="dtemon_amo" HeaderText="تاریخ نهایی" />
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
