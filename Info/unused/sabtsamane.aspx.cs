﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;
using InfoModel;
namespace Info
{
    public partial class sabtsamane : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction

        /*   protected void setgvnok1()
        {
            var  selected = cblnosam.Items.Cast<ListItem>()
                .Where(li => li.Selected)
                .Select(li=>li.Value)
                .ToList();
            if(selected.Count >0)
            gvnok.DataSource =
                (from s in _edbcontext.tblsams
                 where selected.Contains(s.nosam_id.ToString())
                 select s).ToList();
            else
                gvnok.DataSource =
                    (from m in _edbcontext.tblmddjoes
                     select m).ToList();

            //gvnok.DataBind();
        }*/
        protected void setgvnok()
        {
            int no_fa = int.Parse(Session["no_nofa"].ToString());
            int mh_kh = int.Parse(Session["mhkh_per_id"].ToString());
            int id_per = int.Parse(Session["id_per"].ToString());
            if ((no_fa == (int)MyObjects.nofa.Moshaver) || (no_fa == (int)MyObjects.nofa.Moshaver2Shahrestan))
            {
                var per_mhkh = (from p in _edbcontext.tblpers
                    where p.mhkh_per_id == mh_kh
                    select p.id_per).ToList();
                if (per_mhkh.Count > 0)
                {
                    gvnok.DataSource =
                        (from m in _edbcontext.tblmddjoes
                         where per_mhkh.Contains(m.per_id_mddjo)
                         orderby m.dtesabt_mddjo descending, m.timsabt_mddjo descending
                         select m).ToList();
                    gvnok.DataBind();
                }
            }
            else if ((no_fa == (int)MyObjects.nofa.Moshaver1Ostan) || (no_fa == (int)MyObjects.nofa.Moshaver2Ostan))
            {
                gvnok.DataSource =
                    (from m in _edbcontext.tblmddjoes
                     where m.per_id_mddjo == id_per
                     orderby m.dtesabt_mddjo descending, m.timsabt_mddjo descending
                     select m).ToList();
                gvnok.DataBind();

            }
        }
        protected void setgrids()
        {
            setgvnok();
        }

        protected void setddlnosam_id(int id_mddjo)
        {
            ddlnosam_id.DataSource = (from s in _edbcontext.tblnosams
                                      where !_edbcontext.tblsams.Any(es => (es.mddjo_id == id_mddjo) && (es.nosam_id == s.id_nosam))
                                      select s).ToList();
            ddlnosam_id.DataTextField = "desc_nosam";
            ddlnosam_id.DataValueField = "id_nosam";
            ddlnosam_id.DataBind();
            ddlnosam_id.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }

        /*     protected void setcblnosam()
             {
                 cblnosam.DataSource = (from s in _edbcontext.tblnosams
                                        select s).ToList();
                 cblnosam.DataTextField = "desc_nosam";
                 cblnosam.DataValueField = "id_nosam";
                 cblnosam.DataBind();
                 //cblnosam.Items.Insert(0, new ListItem("انتخاب شود", ""));
             }*/

        protected void initialstate()
        {
            setgrids();
            //setcblnosam();
        }
        protected void clsfield()
        {
            ddlnosam_id.SelectedIndex = -1;
            txtdtesabt_sam.Text = string.Empty;
            Session["emal"] = "dook";
        }
        /*public string getnopardakht(int code)
        {
            if (code == (int)actlevel.DaryaftVam_EmdadNazer)
                return "امدادی";
            else if ((code == (int)actlevel.DaryaftVam_BankNazer) || (code == (int)actlevel.DaryaftVam_BankMoshaver))
                return "بانکی";
            else return "";
        }
        public string getnoerja(int code)
        {
            if (code == (int)actlevel.MoarefiVam_EmdadNazer)
                return "امدادی";
            else if ((code == (int)actlevel.MoarefiVam_BankNazer) || (code == (int)actlevel.MoarefiVam_BankMoshaver))
                return "بانکی";
            else return "";
        }*/

        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    setgvnok();
                    if (!IsPostBack)
                    {
                        initialstate();
                        Session["emal"] = "dook";
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }

        protected void btnsabtsam_Click(object sender, EventArgs e)
        {
            if (true)
            {
                try
                {
                    if (Session["emal"].ToString().Equals("dook"))
                    {
                        InfoModel.tblsam _sam = new InfoModel.tblsam();
                        _sam.mddjo_id = int.Parse(Session["id_mddjo"].ToString());
                        _sam.nosam_id = Convert.ToInt16(ddlnosam_id.SelectedValue);
                        _sam.cp_sam = txtcp_sam.Text;
                        _sam.desc_sam = txtdesc_sam.Text;
                        _sam.dtesabt_sam = txtdtesabt_sam.Text;//BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _sam.timsabt_sam = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _sam.active_sam = 1;

                        _edbcontext.tblsams.Add(_sam);
                        _edbcontext.SaveChanges();

                        setgrids();
                        clsfield();

                        //myclass.setactlevel(actlevel.Infaz3, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _sam.tblmddjo.cp_mddjo.ToString() + "درسامانه گردید");
                        setgrids();
                    }
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else
            {
                showerrore("1", "خطای ورود اطلاعات", "کاربر محترم لطفا داده ها را کامل کنید");
            }
            hfield.Value = "hidedook";
        }

        protected void gvnok_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "dook":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Session["emal"] = "dook";
                        hfield.Value = "displaydook";
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        setddlnosam_id(id_mddjo);

                        break;
                    }
            }
        }
        /*protected void gvemdadi_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var result = e.Row.DataItem.GetType().GetProperty("no_act").GetValue(e.Row.DataItem, null);
                int no_act = int.Parse(result.ToString());
                if (no_act == (int)actlevel.TaedMoarefiVam_EmdadNazer)
                {
                    LinkButton lbtnbnk = (LinkButton)e.Row.FindControl("lbtdaryaftvam_bnk");
                    lbtnbnk.Visible = false;
                }
                if ((no_act == (int)actlevel.TaedMoarefiVam_BankNazer) || (no_act == (int)actlevel.TaedMoarefiVam_BankMoshaver))
                {
                    LinkButton lbtnemd = (LinkButton)e.Row.FindControl("lbtdaryaftvam_emd");
                    lbtnemd.Visible = false;
                }
            }
        }*/
        /*protected void btnsabtsam_Click(object sender, EventArgs e)
        {
            if (true)
            {
                try
                {
                    if (Session["emal"].ToString().Equals("domoarefivam_emd"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from v in _edbcontext.tblvams
                                    where v.mddjo_id == id_mddjo
                                    select v;
                        var _vfaz5 = query.FirstOrDefault();
                        //_mddjofaz5.dtesabt_faz2 = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _vfaz5.timsabt_vambank = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _edbcontext.SaveChanges();
                        myclass.setactlevel(actlevel.TaedMoarefiVam_EmdadNazer, id_mddjo);
                        showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _vfaz5.tblmddjo.cp_mddjo.ToString() + "برای وام معرفی گردید");
                        setgrids();
                    }
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else
            {
                showerrore("1", "خطای ورود اطلاعات", "کاربر محترم لطفا داده ها را کامل کنید");
            }
            //Response.Redirect(Request.RawUrl);
            hfield.Value = "hidesabtvam";

        }*/
    }
}