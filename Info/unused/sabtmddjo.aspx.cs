﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;
using InfoModel;
namespace Info
{
    public partial class sabtmddjo : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction

        protected void setgvtempmddjo()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvtempmddjo.DataSource =
                (from m in _edbcontext.tblmddjoes
                 where (m.per_id_mddjo == id_per)&&(m.no_act_vam==0)
                 orderby m.dtesabt_mddjo descending,m.timsabt_mddjo descending 
                 select m).ToList();
            gvtempmddjo.DataBind();
        }
        protected void setgvbackmddjo()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvbackmddjo.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam == (int)actlevelvam.BackInfaz1)&&
                 (v.tblmddjo.per_id_mddjo == id_per)
                 orderby v.dtesabt_faz2 descending,v.timsabt_faz2 descending 
                 select v).ToList();
            gvbackmddjo.DataBind();
        }
        protected void setgvmddjo()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvmddjo.DataSource =
                (from v in _edbcontext.tblvams
                 where (v.no_act_vam >= (int)actlevelvam.Infaz1) && 
                 (v.tblmddjo.per_id_mddjo == id_per)
                 orderby v.dtesabt_faz1 descending,v.timsabt_faz1 descending 
                 select v).ToList();
            gvmddjo.DataBind();
        }

        protected void setgrids()
        {
            setgvtempmddjo();
            setgvbackmddjo();
            setgvmddjo();
        }

        protected void setddlmt_mddjo()
        {
            ddlmt_mddjo.DataSource = _edbcontext.tbltahs.ToList();
            ddlmt_mddjo.DataTextField = "no_tah";
            ddlmt_mddjo.DataValueField = "id_tah";
            ddlmt_mddjo.DataBind();
            ddlmt_mddjo.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }

        protected void setddlnh_mddjo()
        {
            ddlnh_mddjo.DataSource = _edbcontext.tblnhs.ToList();
            ddlnh_mddjo.DataTextField = "no_nh";
            ddlnh_mddjo.DataValueField = "id_nh";
            ddlnh_mddjo.DataBind();
            ddlnh_mddjo.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }

        protected void initialstate()
        {
            setddlmt_mddjo();
            setddlnh_mddjo();
            setgrids();
        }
        protected void setfield(InfoModel.tblmddjo _mddjo)
        {
            txtfn_mddjo.Text = _mddjo.fn_mddjo;
            txtln_mddjo.Text = _mddjo.ln_mddjo;
            txtfan_mddjo.Text = _mddjo.fan_mddjo;
            txtshm_mddjo.Text = _mddjo.shm_mddjo;
            txtbd_mddjo.Text = _mddjo.bd_mddjo;
            ddlmt_mddjo.SelectedValue = _mddjo.mt_id_mddjo.ToString();
            txttel_mddjo.Text = _mddjo.tel_mddjo;
            txtcel_mddjo.Text = _mddjo.cel_mddjo;
            txtadd_mddjo.Text = _mddjo.add_mddjo;
            txtshh_mddjo.Text = _mddjo.shh_mddjo;
            ddlnh_mddjo.SelectedValue = _mddjo.nh_id_mddjo.ToString();
            ddlsex_mddjo.SelectedValue = _mddjo.sex_mddjo.ToString();
            ddlsvm_mddjo.SelectedValue = _mddjo.svm_mddjo.ToString();
        }
        protected void getfield(InfoModel.tblmddjo _mddjo)
        {
            //            int maxid = myclass.getmaxid();
            _mddjo.shh_mddjo = txtshh_mddjo.Text;
            _mddjo.fn_mddjo = txtfn_mddjo.Text;
            _mddjo.ln_mddjo = txtln_mddjo.Text;
            _mddjo.fan_mddjo = txtfan_mddjo.Text;
            _mddjo.shm_mddjo = txtshm_mddjo.Text;
            _mddjo.cp_mddjo = Session["cod_mhkh"].ToString() + _mddjo.shm_mddjo.PadLeft(10, '0');
            _mddjo.bd_mddjo = txtbd_mddjo.Text;
            _mddjo.mt_id_mddjo = Convert.ToInt16(ddlmt_mddjo.SelectedValue);
            _mddjo.tel_mddjo = txttel_mddjo.Text;
            _mddjo.cel_mddjo = txtcel_mddjo.Text;
            _mddjo.add_mddjo = txtadd_mddjo.Text;
            _mddjo.nh_id_mddjo = Convert.ToInt16(ddlnh_mddjo.SelectedValue);
            _mddjo.sex_mddjo = Convert.ToBoolean(ddlsex_mddjo.SelectedValue);
            _mddjo.svm_mddjo = Convert.ToBoolean(ddlsvm_mddjo.SelectedValue);
            _mddjo.per_id_mddjo = Convert.ToInt16(Session["id_per"].ToString());
            _mddjo.dtesabt_mddjo = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
            _mddjo.timsabt_mddjo = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" + DateTime.Now.Minute.ToString().PadLeft(2, '0');
            _mddjo.no_act_vam = 0;
            _mddjo.no_act_amo = 0;
            _mddjo.no_act_kar = 0;
            //****************************create cp_mddjo*********************************************************
            /*if (_mddjo.cp_mddjo == null)
            {
                string temp = _mddjo.shm_mddjo.Substring(0, 3);
                temp +=
                    (((int)int.Parse(_mddjo.ln_mddjo.Substring(0, 1)) + int.Parse(_mddjo.ln_mddjo.Substring(1, 1)))%100)
                        .ToString()
                        .PadLeft(2, '0');
                temp +=
                    ((int.Parse(_mddjo.fn_mddjo.Substring(0, 1)) + int.Parse(_mddjo.fn_mddjo.Substring(1, 1)))%100)
                        .ToString()
                        .PadLeft(2, '0');
                temp += _mddjo.shm_mddjo.Substring(7, 3);
                Random rnd = new Random();
                temp += rnd.Next(0, 100).ToString().PadLeft(2, '0');
                _mddjo.cp_mddjo = temp;
            }*/
            //****************************create cp_mddjo*********************************************************
        }
        protected void clsfield()
        {
            txtfn_mddjo.Text = string.Empty;
            txtln_mddjo.Text = string.Empty;
            txtfan_mddjo.Text = string.Empty;
            txtshm_mddjo.Text = string.Empty;
            txtbd_mddjo.Text = string.Empty;
            txttel_mddjo.Text = string.Empty;
            txtcel_mddjo.Text = string.Empty;
            txtadd_mddjo.Text = string.Empty;
            txtshh_mddjo.Text = string.Empty;
            ddlmt_mddjo.SelectedIndex = -1;
            ddlnh_mddjo.SelectedIndex = -1;
            ddlsex_mddjo.SelectedIndex = -1;
            ddlsvm_mddjo.SelectedIndex = -1;
            Session["emal"] = "donew";
        }
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if (!IsPostBack)
                    {
                        initialstate();
                        Session["emal"] = "donew";
                        hfield.Value = "hide";
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }
        protected void btnsabt_Click(object sender, EventArgs e)
        {
            if (true)
            {
                try
                {
                    if (Session["emal"].ToString().Equals("donew"))
                    {
                        InfoModel.tblmddjo _mddjo = new InfoModel.tblmddjo();
                        getfield(_mddjo);
                        if (!myclass.validshmeli(txtshm_mddjo.Text))
                        {
                            showerrore("1", "خطای ورود اطلاعات", "شماره ملی مددجو ایراد دارد لطفا دوباره تلاش کنید");
                            return;
                        }
                        _edbcontext.tblmddjoes.Add(_mddjo);
                        _edbcontext.SaveChanges();

                        setgrids();
                        clsfield();
                        showerrore("1", "پیام سیستم", "اطلاعات اولیه مددجو با کد پیگیری " + _mddjo.cp_mddjo.ToString() + "  درسامانه ثبت گردید.");
                        hfield.Value = "hide";

                    }
                    if (Session["emal"].ToString().Equals("doedit"))
                    {
                        int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                        var query = from m in _edbcontext.tblmddjoes
                                    where m.id_mddjo == id_mddjo
                                    select m;
                        var _mddjo = query.First();
                        //myclass.setactlevelvam(actlevelvam.Tempmddjo, id_mddjo);
                        getfield(_mddjo);

                        _edbcontext.SaveChanges();
                        setgrids();
                        clsfield();
                        showerrore("1", "پیام سیستم", "اطلاعات اولیه مددجو با کد پیگیری " + _mddjo.cp_mddjo.ToString() + " در سامانه اصلاح گردید");
                        hfield.Value = "hide";
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        if (ex.InnerException.ToString().Contains("'IX_shm_tblmddjo'"))
                            showerrore("1", "خطای ورود اطلاعات", "کد ملی تکراری است دوباره تلاش کنید");
                        else if (ex.InnerException.ToString().Contains("'IX_cp_tblmddjo'"))
                            showerrore("1", "خطای ورود اطلاعات", "کد پیگیری تکراری است دوباره تلاش کنید");
                        else if (ex.InnerException.ToString().Contains("'IX_shp_tblmddjo'"))
                            showerrore("1", "خطای ورود اطلاعات", "شماره پرونده تکراری است دوباره تلاش کنید");
                        else if (ex.InnerException.ToString().Contains("'IX_shh_tblmddjo'"))
                            showerrore("1", "خطای ورود اطلاعات", "کد مددجویی تکراری است دوباره تلاش کنید");
                        else
                            showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                    }
                    else
                        showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                }
            }
            else
            {
                showerrore("1", "خطای ورود اطلاعات", "کاربر محترم لطفا داده ها را کامل کنید");
            }
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            //Response.Redirect("sabtmddjo.aspx");
            //Page.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
        }
        protected void gvmddjo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "doedit":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        int id_mddjo = int.Parse(e.CommandArgument.ToString());
                        var query = from m in _edbcontext.tblmddjoes
                                    where m.id_mddjo == id_mddjo
                                    select m;
                        var _mddjo = query.First();
                        setfield(_mddjo);
                        Session["emal"] = "doedit";
                        hfield.Value = "display";
                        break;
                    }
                case "dodelete":
                    {
                        int id_mddjo = int.Parse(e.CommandArgument.ToString());
                        var query = from m in _edbcontext.tblmddjoes
                                    where m.id_mddjo == id_mddjo
                                    select m;
                        var _mddjo = query.FirstOrDefault();
                            try
                            {
                                _edbcontext.tblmddjoes.Remove(_mddjo);
                                _edbcontext.SaveChanges();

                                showerrore("1", "پیام سیستم", "اطلاعات مددجو با کد پیگیری " + _mddjo.cp_mddjo +"از سامانه حذف گردید");
                                setgrids();

                            }
                            catch (Exception ex)
                            {
                                showerrore("1", "خطای ورود اطلاعات", "پرونده مددجو در حال بررسی است و قابل حذف نمی باشد");

                            }
                        break;
                    }
                case "dofaz1":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        Response.Redirect("sabtfaz1.aspx");
                        break;
                    }


            }
        }
    }
}