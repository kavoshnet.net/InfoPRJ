﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;
using InfoModel;
namespace Info
{
    public partial class sabtkar : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction

        protected void setgvmddjo()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvmddjo.DataSource =
                (from m in _edbcontext.tblmddjoes
                 where (m.per_id_mddjo == id_per) && (m.no_act_kar == (int)actlevelkar.Tempmddjo)
                 orderby m.dtesabt_mddjo descending, m.timsabt_mddjo descending
                 select m).ToList();
            gvmddjo.DataBind();
        }
        protected void setgvtashkil()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvtashkil.DataSource =
                (from k in _edbcontext.tblkars
                 where (k.no_act_kar == (int)actlevelkar.Tashkil) &&
                 (k.per_id_kar == id_per)
                 orderby k.dtesabttak_kar descending, k.timsabttak_kar descending
                 select k).ToList();
            gvtashkil.DataBind();
        }

        protected void setgvmoarefi()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvmoarefi.DataSource =
                (from k in _edbcontext.tblkars
                 where (k.no_act_kar == (int)actlevelkar.Moarefi) &&
                 (k.per_id_kar == id_per)
                 orderby k.dtesabtmor_kar descending, k.timsabtmor_kar descending
                 select k).ToList();
            gvmoarefi.DataBind();
        }
        protected void setgvnatije()
        {
            int id_per = Convert.ToInt16(Session["id_per"].ToString());
            gvnatije.DataSource =
                (from k in _edbcontext.tblkars
                 where (k.no_act_kar == (int)actlevelkar.Natije) &&
                 (k.per_id_kar == id_per)
                 orderby k.dtesabtest_kar descending, k.timsabtest_kar descending
                 select k).ToList();
            gvnatije.DataBind();
        }
        protected void setgrids()
        {
            setgvmddjo();
            setgvtashkil();
            setgvmoarefi();
            setgvnatije();
            //gvemdadi.RowDataBound += new GridViewRowEventHandler(gvemdadi_OnRowDataBound);
        }
        protected void setddlnokar_id()
        {
            ddlnokar_id.DataSource = (from n in _edbcontext.tblkaryabs
                                      select n).ToList();
            ddlnokar_id.DataTextField = "desc_karyab";
            ddlnokar_id.DataValueField = "id_karyab";
            ddlnokar_id.DataBind();
            ddlnokar_id.Items.Insert(0, new ListItem("انتخاب شود", ""));
        }
        protected void initialstate()
        {
            setddlnokar_id();
            setgrids();
        }
        protected void clsfield()
        {
            /*ddlper_id_faz2.SelectedIndex = -1;
            Session["emal"] = "dook";
            txtdesc_faz2.Text = string.Empty;*/
        }
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if (!IsPostBack)
                    {
                        initialstate();
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }
        protected void btntashkil_Click(object sender, EventArgs e)
        {
            try
            {
                int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                int id_per = int.Parse(Session["id_per"].ToString());
                InfoModel.tblkar _kar = new InfoModel.tblkar();
                _kar.mddjo_id_kar = id_mddjo;
                _kar.per_id_kar = id_per;
                _kar.dtetak_kar = txtdtetak_kar.Text;
                _kar.dtesabttak_kar = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                _kar.timsabttak_kar = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                      DateTime.Now.Minute.ToString().PadLeft(2, '0');
                _edbcontext.tblkars.Add(_kar);
                _edbcontext.SaveChanges();
                var query = from m in _edbcontext.tblmddjoes
                            where m.id_mddjo == id_mddjo
                            select m;
                var _mddjo = query.FirstOrDefault();

                myclass.setactlevelkar(actlevelkar.Tashkil, id_mddjo);
                showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _mddjo.cp_mddjo.ToString() + "تشکیل گردید");
                setgrids();
            }
            catch (Exception ex)
            {
                showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
            }
            hfield.Value = "hidetashkil";
        }
        protected void btnmoarefi_Click(object sender, EventArgs e)
        {
            try
            {
                int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                int id_per = int.Parse(Session["id_per"].ToString());
                var query = from k in _edbcontext.tblkars
                            where (k.mddjo_id_kar == id_mddjo) && (k.per_id_kar == id_per)
                            select k;
                var _kar = query.FirstOrDefault();
                _kar.dtemor_kar = txtdtemorkar.Text;
                _kar.nokar_id_kar = Convert.ToInt16(ddlnokar_id.SelectedValue);
                _kar.namkarfy_kar = txtnamkarfy_kar.Text;
                _kar.dtesabtmor_kar = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                _kar.timsabtmor_kar = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                      DateTime.Now.Minute.ToString().PadLeft(2, '0');
                _edbcontext.SaveChanges();
                myclass.setactlevelkar(actlevelkar.Moarefi, id_mddjo);
                showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _kar.tblmddjo.cp_mddjo.ToString() + "معرفی گردید");
                setgrids();
            }
            catch (Exception ex)
            {
                showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
            }
            hfield.Value = "hidemoarefi";
        }
        protected void btnnatije_Click(object sender, EventArgs e)
        {
            try
            {
                int id_mddjo = int.Parse(Session["id_mddjo"].ToString());
                int id_per = int.Parse(Session["id_per"].ToString());
                var query = from k in _edbcontext.tblkars
                            where (k.mddjo_id_kar == id_mddjo) && (k.per_id_kar == id_per)
                            select k;
                var _kar = query.FirstOrDefault();
                _kar.dteest_kar = txtdteest_kar.Text;
                _kar.namkarg_kar = txtnamkarg_kar.Text;
                _kar.modat_kar = Convert.ToInt32(txtmodat_kar.Text);
                _kar.mab_kar = Convert.ToInt32(txtmab_kar.Text);
                _kar.dtesabtest_kar = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                _kar.timsabtest_kar = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                      DateTime.Now.Minute.ToString().PadLeft(2, '0');
                _edbcontext.SaveChanges();
                myclass.setactlevelkar(actlevelkar.Natije, id_mddjo);
                showerrore("1", "پیام سیستم", "پرونده با کد پیگیری " + _kar.tblmddjo.cp_mddjo.ToString() + "به کار گماری گردید");
                setgrids();
            }
            catch (Exception ex)
            {
                showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
            }
            hfield.Value = "hidenatije";
        }

        protected void gvmddjo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "dotashkil":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        hfield.Value = "displaytashkil";
                        break;
                    }
            }
        }
        protected void gvtashkil_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "domoarefi":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        hfield.Value = "displaymoarefi";
                        break;
                    }
            }
        }
        protected void gvmoarefi_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "donatije":
                    {
                        Session["id_mddjo"] = e.CommandArgument.ToString();
                        hfield.Value = "displaynatije";
                        break;
                    }
            }
        }
    }
}