﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="sabtfaz4.aspx.cs" Inherits="Info.sabtfaz4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <script type="text/javascript">
        $(document).ready(function () {
            //btn btnbacksabtersal
            $("#btnbacksabtersal").click(function () {
                $("#form1").data("bootstrapValidator").resetForm(true);
                $("#<%=hfield.ClientID %>").val("hidesabtersal");
                window.location = window.location;
            });
            //btn btnbacksabt
            $("#btnbacksabt").click(function () {
                $("#form1").data("bootstrapValidator").resetForm(true);
                $("#<%=hfield.ClientID %>").val("hidesabtfile");
                window.location = window.location;
            });

            if ($("#<%=hfield.ClientID %>").val() === "displaysabtersal") {
                $("#sabtersal").fadeIn(1000);
                $("#tabpanel").fadeOut(0);

            }
            if ($("#<%=hfield.ClientID %>").val() === "hidesabtersal") {
                $("#tabpanel").fadeIn(1000);
                $("#sabtersal").fadeOut(0);
                setTimeout(function () { document.location = "sabtfaz4.aspx"; }, 3000);
                $("#myTab li:eq(1) a").tab("show");
            }
            if ($("#<%=hfield.ClientID %>").val() === "displaysabtfile") {
                $("#sabtfile").fadeIn(1000);
                $("#tabpanel").fadeOut(0);
            }
            if ($("#<%=hfield.ClientID %>").val() === "hidesabtfile") {
                $("#tabpanel").fadeIn(1000);
                $("#sabtfile").fadeOut(0);
                setTimeout(function () { document.location = "sabtfaz4.aspx"; }, 3000);
                $("#myTab li:eq(1) a").tab("show");
            }
            //errore message
            if ($("#<%=messagehf.ClientID %>").val() === "1") {
                $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                $("#message").fadeIn().delay(3000).fadeOut();
                $("#<%=messagehf.ClientID %>").val("");

            }
            //errore message
        });
    </script>

    <!--------------------------------------------------------------------------------------------------------->
    <!---------------------------------------validator--------------------------------------------------------->
    <script type="text/javascript">
        //validator
        $(document).ready(function () {
            $("#form1").bootstrapValidator({
                message: "این مقدار صحیح نمیباشد",
                fields: {
                    "<%= ddlnazer.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }
                    },
                    "<%= ddlmoshaver.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },

                    "<%= txtdesc_faz4.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9ا-ی ؤءأإئ\-]+$/,
                                message: "تنها مقادیر حرفی ، عددی و خط فاصله صحیح میباشد"
                            }
                        }

                    },
                    "<%= ddlbank.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },
                    "<%= txtshbank.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[ا-ی ؤءأإئ]+$/,
                                message: "تنها حروف فارسی مجاز میباشد"
                            }
                        }

                    },
                    "<%= ddletebar.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },
                    "<%= ddlghtarh.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }
                        }

                    },
                    "<%= ddlnovam.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            }

                        }

                    },
                    "<%= txtmabvam_faz4.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            lessThan: {
                                value: 1000000001,
                                inclusive: true,
                                message: "مبلغ وام باید از 1000000000 کمتر باشد"
                            },
                            greaterThan: {
                                value: 4999999,
                                inclusive: false,
                                message: "مبلغ وام باید از 5000000 بیتشر باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    },
                    "<%= txtnamtarh_faz4.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^[ا-ی ؤءأإئ]+$/,
                                message: "تنها حروف فارسی مجاز میباشد"
                            }
                        }

                    },
                    "<%= txtshtasvib_faz4.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 6,
                                max: 6,
                                message: "تعداد ورودی باید 6 رقم باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    },
                    "<%= txtshp_mddjo.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 9,
                                max: 9,
                                message: "تعداد ورودی باید 9 رقم باشد"
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: "تنها مقادیر عددی صحیح میباشد"
                            }
                        }

                    }
                }

            });
            //validator
        });
    </script>
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <div id="container" class="container full-width">
        <div class="row">
            <!--------------------------------------------------Message Box--------------------------------------------->
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <strong>
                            <p id="messagetext" class="text-center">پیام خطا</p>
                        </strong>
                    </div>
                </div>
            </div>
            <!--------------------------------------------------------------------------------------------------------->
            <div class="col-lg-12">
                <asp:HiddenField ID="hfield" runat="server" />
                <div role="tabpanel" class="tab-pane" id="sabtersal" style="display: none">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="panel-title">
                                پیگیری پرداخت وام
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label6" runat="server" Text="ناظر"></asp:Label>
                                    <asp:DropDownList ID="ddlnazer" runat="server" Width="220px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label11" runat="server" Text="مشاور پیگیر امور بانکی"></asp:Label>
                                    <asp:DropDownList ID="ddlmoshaver" runat="server" Width="230px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <br />
                                <p class="text-center">
                                    <asp:Button ID="btnsabtersal" runat="server" CssClass="btn btn-success" OnClick="btnsabtersal_Click" Text="تصویب پرونده" Width="175px" />
                                    <input type="button" id="btnbacksabtersal" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="sabtfile" style="display: none">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="panel-title">
                                تصویب / عدم تصویب پرونده
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label ID="Label36" runat="server" Text="علت تائید یا عدم تائید"></asp:Label>
                                    <asp:TextBox ID="txtdesc_faz4" runat="server" Width="300px" CssClass="form-control" placeholder="علت عدم تصویب طرح را وارد کنید" Height="100px" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label7" runat="server" Text="بانک"></asp:Label>
                                    <asp:DropDownList ID="ddlbank" runat="server" Width="220px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label9" runat="server" Text="شعبه بانک"></asp:Label>
                                    <asp:TextBox ID="txtshbank" runat="server" Width="220px" placeholder="شعبه مورد نظر را وارد کنید" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label1" runat="server" Text="محل اعتبار"></asp:Label>
                                    <asp:DropDownList ID="ddletebar" runat="server" Width="230px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label8" runat="server" Text="نوع فعالیت"></asp:Label>
                                    <asp:DropDownList ID="ddlghtarh" runat="server" Width="220px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label3" runat="server" Text="نوع وام"></asp:Label>
                                    <asp:DropDownList ID="ddlnovam" runat="server" Width="220px" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label39" runat="server" Text="مبلغ وام (ریال)"></asp:Label>
                                    <asp:TextBox ID="txtmabvam_faz4" runat="server" Width="250px" CssClass="form-control" placeholder="مبلغ تصویب شده وام را وارد کنید" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label37" runat="server" Text="نام طرح"></asp:Label>
                                    <asp:TextBox ID="txtnamtarh_faz4" runat="server" Width="250px" CssClass="form-control" placeholder="نام طرح را وارد کنید"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label38" runat="server" Text="شماره تصویب"></asp:Label>
                                    <asp:TextBox ID="txtshtasvib_faz4" runat="server" Width="250px" CssClass="form-control" placeholder="شماره تصویب طرح را وارد کنید" MaxLength="5"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <asp:Label ID="Label4" runat="server" Text="شماره پرونده خودکفایی"></asp:Label>
                                    <asp:TextBox ID="txtshp_mddjo" runat="server" Width="250px" CssClass="form-control" placeholder="شماره پرونده خودکفائی را وارد کنید" MaxLength="9"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <br />
                                <p class="text-center">
                                    <asp:Button ID="btnsabt" runat="server" CssClass="btn btn-success" OnClick="btnsabt_Click" Text="تصویب پرونده" Width="175px" />
                                    <input type="button" id="btnbacksabt" value="بازگشت" class="btn btn-warning" style="width: 175px" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" id="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#nokfile" aria-controls="nokfile" role="tab" data-toggle="tab" style="background-color: darkorange">در انتظار تصویب</a></li>
                        <li role="presentation"><a href="#okfile" aria-controls="okfile" role="tab" data-toggle="tab" style="background-color: sandybrown">تصویب شده</a></li>
                        <li role="presentation"><a href="#nazerfile" aria-controls="nazerfile" role="tab" data-toggle="tab" style="background-color: orange">ارجاعی به ناظر</a></li>
                        <li role="presentation"><a href="#moshaverfile" aria-controls="moshaverfile" role="tab" data-toggle="tab" style="background-color: peru">ارجاعی به مشاور</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="nokfile">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های تائید نشده
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvnok" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvnok_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="سابقه وام">
                                                    <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="dtesabt_faz3" HeaderText="تاریخ ثبت طرح" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtnok" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dook" CssClass="btn btn-success btn-xs">تائید</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lbtnnok" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="donok" CssClass="btn btn-danger btn-xs">عدم تائید</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="okfile">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های انجام شده
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvok" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False" OnRowCommand="gvok_RowCommand">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="tblbank.nam_bank" HeaderText="نام بانک" />
                                                <asp:BoundField DataField="shbank_faz1" HeaderText="شعبه" />
                                                <asp:BoundField DataField="mabvam_faz4" HeaderText="مبلغ وام" />
                                                <asp:BoundField DataField="shtasvib_faz4" HeaderText="شماره تصویب" />
                                                <asp:BoundField DataField="dtesabt_faz4" HeaderText="تاریخ تصویب" />
                                                <asp:TemplateField HeaderText="عملیات">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lbtnemdadi" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="doemdadi" CssClass="btn btn-primary btn-xs">امدادی</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lbtnbankinazer" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dobankinazer" CssClass="btn btn-success btn-xs">بانکی(ناظر)</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lbtnbankimoshaver" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dobankimoshaver" CssClass="btn btn-warning btn-xs">بانکی(مشاور)</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="nazerfile">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های ارجاعی به ناظر جهت پیگیری دریافت وام
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvnazer" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="tblbank.nam_bank" HeaderText="نام بانک" />
                                                <asp:BoundField DataField="shbank_faz1" HeaderText="شعبه" />
                                                <asp:BoundField DataField="mabvam_faz4" HeaderText="مبلغ وام" />
                                                <asp:TemplateField HeaderText="نوع ارجاع">
                                                    <ItemTemplate><%#(Eval("no_act_vam").ToString().Equals("10") || Eval("no_act_vam").ToString().Equals("11")) ?"بانکی":"امدادی"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="dtesabt_faz5" HeaderText="تاریخ ارجاع" />
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="moshaverfile">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        پرونده های ارجاعی به مشاور جهت پیگیری پرداخت وام
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvmoshaver" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="#333333" GridLines="None" ShowFooter="True" UseAccessibleHeader="False">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:BoundField DataField="tblmddjo.shm_mddjo" HeaderText="کد ملی" />
                                                <asp:BoundField DataField="tblmddjo.fn_mddjo" HeaderText="نام" />
                                                <asp:BoundField DataField="tblmddjo.ln_mddjo" HeaderText="نام خانوادگی" />
                                                <asp:BoundField DataField="tblmddjo.fan_mddjo" HeaderText="نام پدر" />
                                                <asp:TemplateField HeaderText="جنسیت">
                                                    <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tblnovam.desc_novam" HeaderText="نوع وام" />
                                                <asp:BoundField DataField="tblghtarh.no_ghtarh" HeaderText="قالب طرح" />
                                                <asp:BoundField DataField="tblbank.nam_bank" HeaderText="نام بانک" />
                                                <asp:BoundField DataField="shbank_faz1" HeaderText="شعبه" />
                                                <asp:BoundField DataField="mabvam_faz4" HeaderText="مبلغ وام" />
                                                <asp:TemplateField HeaderText="نوع ارجاع">
                                                    <ItemTemplate><%#(Eval("no_act_vam").ToString().Equals("10") || Eval("no_act_vam").ToString().Equals("11")) ?"بانکی":"امدادی"%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="dtesabt_faz5" HeaderText="تاریخ ارجاع" />
                                            </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
