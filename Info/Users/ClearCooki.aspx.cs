﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Info.Users
{
    public partial class ClearCooki : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Cookies["UserCooki"] != null)
            {
                lblcooki.Text = "Cooki exist.";
                HttpCookie myCookie = new HttpCookie("UserCooki");
                myCookie.Value = String.Empty;
                myCookie.Expires = DateTime.Now.AddDays(-1);
                Response.SetCookie(myCookie);
                Session["vorod"] = null;
                Session["id_user"] = null;
                Session["lname_user"] = null;
                Session["fname_user"] = null;
                Session["isadmin_user"] = null;
                Session["isact_user"] = null;
                Session["sex_user"] = null;
                Session.RemoveAll();

            }
        }
    }
}