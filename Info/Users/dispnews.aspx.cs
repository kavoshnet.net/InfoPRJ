﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfoModel;
using MyObjects;
using System.Web.Services;

namespace Info.Users
{
    public partial class dispnews : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction
        protected void setgvrnews()
        {
            Session["cat_id"] = Request.QueryString["cat_id"].ToString();
            long cat_id = long.Parse(Session["cat_id"].ToString());
            string cat_name = _edbcontext.tblcategory.SingleOrDefault(p => p.id_cat == cat_id).name_cat;
            Session["cat_name"] = cat_name;
            string search = txtsearch.Text;//Session["name_soft"].ToString();

            gvrnews.DataSource =
                (from n in _edbcontext.tblnews
                 where (n.isact_news == true) && (n.cat_id_news == cat_id) && (n.title_news.Contains(search))
                 orderby n.dtesabt_news descending, n.timsabt_news descending
                 select n).ToList();
            gvrnews.DataBind();
        }


        protected void initialstate()
        {
            setgvrnews();
        }
        protected void clsfield()
        {
        }
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                initialstate();
            }
        }

        protected void gvrnews_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //
        }
        protected void DownloadFile(object sender, EventArgs e)
        {
            string filePath = (sender as LinkButton).CommandArgument;
            string orgFileName = Server.MapPath("~/attnews/") + Path.GetFileName(filePath);
            if (System.IO.File.Exists(orgFileName))
            {
                //Response.ContentType = ContentType;
                Response.ContentType = "application/octet-straem";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
                Response.WriteFile(orgFileName);
                Response.End();
            }
            else
                showerrore("dispnews.aspx?cat_id=" + Session["cat_id"].ToString(), "خطا", "فایل از مقصد حذف شده است");

        }
        protected void gvrnews_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvrnews.PageIndex = e.NewPageIndex;
            initialstate();
        }
        protected void btnsearch_Click(object sender, EventArgs e)
        {
            long cat_id = long.Parse(Session["cat_id"].ToString());
            string search = txtsearch.Text;//Session["name_soft"].ToString();
            gvrnews.DataSource =
                (from n in _edbcontext.tblnews
                 where (n.isact_news == true) && (n.cat_id_news == cat_id) && ((n.title_news.Contains(search) || (n.content_news.Contains(search))))
                 orderby n.dtesabt_news descending, n.timsabt_news descending
                 select n).ToList();
            gvrnews.DataBind();

        }

        protected void lnkAccept_Click(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok")
                    && (Session["isadmin_user"].ToString() == "isuser")
                    && (Session["isact_user"].ToString() == "isact"))
                {
                    try
                    {
                        long id_user = long.Parse(Session["id_user"].ToString());
                        long id_news = long.Parse((sender as LinkButton).CommandArgument);
                        var acc = (from sacc in _edbcontext.tblacc
                                   where (sacc.user_id_acc == id_user) && (sacc.news_id_acc == id_news)
                                   select sacc).ToList();
                        if (acc.Count == 0)
                        {
                            InfoModel.tblacc _acc = new InfoModel.tblacc();
                            _acc.news_id_acc = id_news;
                            _acc.user_id_acc = id_user;
                            _acc.dte_acc = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                            _acc.tim_acc = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                      DateTime.Now.Minute.ToString().PadLeft(2, '0');
                            _edbcontext.tblacc.Add(_acc);
                            _edbcontext.SaveChanges();
                            showerrore("1", "پیام سیستم", "موافقت شما ثبت گردید");
                            //Response.Redirect("dispnews.aspx?cat_id=" + Session["cat_id"].ToString());
                        }
                    }
                    catch (Exception)
                    {
                        showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                    }

                }
                else
                {
                    if (Request.Cookies["UserCooki"] != null)
                    {
                        
                        string username_user = Request.Cookies["UserCooki"]["username"];
                        string pass_user = Request.Cookies["UserCooki"]["password"];
                        //Response.Cookies["UserCooki"]["username"] = txtusername_user.Text;
                        //Response.Cookies["UserCooki"]["password"] = txtpass_user.Text;
                        //UserCooki.Expires = DateTime.Now.AddDays(7);
                        //Response.Cookies.Add(UserCooki);
                        LoginMethod(username_user,pass_user);
                    }

                    else
                        showerrore("1", "خطای ورود اطلاعات", "شما وارد سامانه نشده اید لطفا از لینک ورود به سامانه استفاده کنید");
                }
            }
            else
            {
                if (Request.Cookies["UserCooki"] != null)
                {
                    string username_user = Request.Cookies["UserCooki"]["username"];
                    string pass_user = Request.Cookies["UserCooki"]["password"];
                    //Response.Cookies["UserCooki"]["username"] = txtusername_user.Text;
                    //Response.Cookies["UserCooki"]["password"] = txtpass_user.Text;
                    //UserCooki.Expires = DateTime.Now.AddDays(7);
                    //Response.Cookies.Add(UserCooki);
                    LoginMethod(username_user, pass_user);
                }

                else
                    showerrore("1", "خطای ورود اطلاعات", "شما وارد سامانه نشده اید لطفا از لینک ورود به سامانه استفاده کنید");
            }
        }
        [WebMethod(EnableSession = true)]
        public static string LoginMethod(string username, string password)
        {
            try
            {
                InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
                var query = from p in _edbcontext.tbluser
                            where p.username_user.Equals(username) && p.pass_user.Equals(password)
                            select p;
                var user = query.FirstOrDefault();
                if (user != null)
                {
                    if (HttpContext.Current.Request.Cookies["UserCooki"] == null)
                    {
                        HttpCookie UserCooki = new HttpCookie("UserCooki");
                        UserCooki.Values["username"] = username;
                        UserCooki.Values["password"] = password;
                        UserCooki.Expires = DateTime.Now.AddDays(7);
                        HttpContext.Current.Response.Cookies.Add(UserCooki);
                    }
                    HttpContext.Current.Session["vorod"] = "ok";
                    HttpContext.Current.Session["id_user"] = user.id_user.ToString();
                    HttpContext.Current.Session["lname_user"] = user.lname_user.ToString();
                    HttpContext.Current.Session["fname_user"] = user.fname_user.ToString();
                    HttpContext.Current.Session["isadmin_user"] = Convert.ToBoolean(user.isadmin_user.ToString()) == true ? "isadmin" : "isuser";
                    HttpContext.Current.Session["isact_user"] = Convert.ToBoolean(user.isact_user.ToString()) == true ? "isact" : "noact";
                    HttpContext.Current.Session["sex_user"] = Convert.ToBoolean(user.sex_user.ToString()) == true ? "آقای" : "خانم";
                }
                else
                {
                    return "false";

                }
            }
            catch (Exception)
            {
                return "false";
            }
            return "true";
        }

    }

}
