﻿<%@ Page Language="C#" MasterPageFile="~/Users/UsersLoginMaster.master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="Info.Users.Login" Title="صفحه ورود به سامانه" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!---------------------------------------errormesage------------------------------------------------------->
    <!---------------------------------------validator--------------------------------------------------------->
    <script type="text/javascript">
        //validator
        $(document).ready(function () {
            $("#form1").bootstrapValidator({
                message: "این مقدار صحیح نمیباشد",
                fields: {
                    "<%= txtusername_user.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 4,
                                max: 30,
                                message: "تعداد ورودی باید بیش از 4 و کمتر از 30 باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: "تنها مقادیر حرفی ، عددی ، نقطه و خط زیر صحیح میباشد"
                            }
                        }
                    },
                    "<%= txtpass_user.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 4,
                                max: 30,
                                message: "تعداد ورودی باید بیش از 4 و کمتر از 30 باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: "تنها مقادیر حرفی ، عددی ، نقطه و خط زیر صحیح میباشد"
                            }
                        }

                    }
                }
            });
        });
        //validator
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            //errore message
            if ($("#<%=messagehf.ClientID %>").val() == "1") {
                $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                $("#message").fadeIn().delay(3000).fadeOut();
                $("#<%=messagehf.ClientID %>").val("");
            }
        });
        //errore message
    </script>

    <!--------------------------------------------------------------------------------------------------------->

    <div class="container full-width">
        <div class="row">
            <!--------------------------------------------------Message Box--------------------------------------------->
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <p id="messagetext" class="text-center"><strong>پیام خطا</strong></p>
                    </div>
                </div>
            </div>
            <!--------------------------------------------------------------------------------------------------------->

            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            ورود به سامانه
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-6 col-lg-offset-5">
                            <img src="../Images/signin.jpg" alt="" width="20%" height="20%" />
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6 col-lg-offset-5">
                                <asp:Label ID="Label2" runat="server" Text="نام کاربری:"></asp:Label>
                                <asp:TextBox ID="txtusername_user" runat="server" Width="175px" CssClass="form-control" placeholder="نام کاربری را وارد کنید"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6 col-lg-offset-5">
                                <asp:Label ID="Label3" runat="server" Text="کلمه عبور:"></asp:Label>
                                <asp:TextBox ID="txtpass_user" runat="server" Width="175px" CssClass="form-control" placeholder="کلمه عبور را وارد کنید" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-offset-5">
                            <br />
                            <asp:Button ID="btnlogin" runat="server" CssClass="btn btn-default" OnClick="btnLogin_Click" Text="ورود" Width="175px" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
