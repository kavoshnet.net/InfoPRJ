﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfoModel;
using MyObjects;
using System.Web.Services;

namespace Info.Users
{
    public partial class dispnewsimp : System.Web.UI.Page
    {

        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        #region userfunction
        protected void setgvrnews()
        {
            Session["cat_id"] = Request.QueryString["cat_id"].ToString();
            long cat_id = long.Parse(Session["cat_id"].ToString());
            string cat_name = _edbcontext.tblcategory.SingleOrDefault(p => p.id_cat == cat_id).name_cat;
            Session["cat_name"] = cat_name;
            string search = txtsearch.Text;//Session["name_soft"].ToString();

            gvrnews.DataSource =
                (from n in _edbcontext.tblnews
                 where (n.isact_news == true) && (n.cat_id_news == cat_id) && (n.title_news.Contains(search))
                 orderby n.dtesabt_news descending, n.timsabt_news descending
                 select n).ToList();
            gvrnews.DataBind();
        }
        protected void CKEditorConfig()
        {
            //CKEditor1.config.uiColor = "#BFEE62";
            txtcontent_news.config.language = "fa";
            //CKEditor1.config.removePlugins = "link";
            //CKEditor1.config.enterMode = EnterMode.BR;
            //CKEditor1.config.toolbar = new object[]
            //{
            // new object[] { "Source", "-", "Save", "NewPage", "Preview", "-", "Templates" },
            // new object[] { "Link", "Unlink", "Anchor" },
            //};

            //CKEditor1.config.toolbar = new object[]
            //{
            //    new object[]{ "Source", "DocProps", "-", "Save", "NewPage", "Preview", "-", "Templates"},
            //    new object[]{ "Cut","Copy","Paste","PasteText","PasteWord","-","Print","SpellCheck"},
            //    new object[]{"Undo","Redo","-","Find","Replace","-","SelectAll","RemoveFormat"},
            //    new object[]{"Form","Checkbox","Radio","TextField","Textarea","Select","Button","ImageButton","HiddenField"},
            //    "/",
            //    new object[]{"Bold","Italic","Underline","StrikeThrough","-","Subscript","Superscript"},
            //    new object[]{"OrderedList","UnorderedList","-","Outdent","Indent","Blockquote"},
            //    new object[]{"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"},
            //    new object[]{"Link","Unlink","Anchor"},
            //    new object[]{"Image","Flash","Table","Rule","Smiley","SpecialChar","PageBreak"},
            //    "/",
            //    new object[]{"Style","FontFormat","FontName","FontSize"},
            //    new object[]{"TextColor","BGColor"},
            //    new object[]{"FitWindow","ShowBlocks","-","About"}, // No comma for the last row.
            //};

            //CKEditor1.config.toolbar = new object[]
            //{
            //   new object[] { "Cut", "Copy", "Paste", "PasteText", "PasteWord" },
            //   new object[] { "Undo","Redo","-","Bold","Italic","Underline","StrikeThrough" },
            //   "/",
            //   new object[] {"OrderedList","UnorderedList","-","Outdent","Indent" },
            //   new object[] {"Link","Unlink","Anchor" },
            //   "/",
            //   new object[] {"Style" },
            //   new object[] {"Table","Image","Flash","Rule","SpecialChar" },
            //   new object[] {"About" }
            //};

        }



        protected void initialstate()
        {
            setgvrnews();
            CKEditorConfig();
        }
        protected void clsfield()
        {
        }
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Response.Cookies.Remove("UserCooki");
            //if (!IsPostBack)
            {
                initialstate();
            }
        }

        protected void gvrnews_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //
        }
        protected void gvrnews_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvrnews.PageIndex = e.NewPageIndex;
            initialstate();
        }
        protected void btnsearch_Click(object sender, EventArgs e)
        {
            long cat_id = long.Parse(Session["cat_id"].ToString());
            string search = txtsearch.Text;//Session["name_soft"].ToString();
            gvrnews.DataSource =
                (from n in _edbcontext.tblnews
                 where (n.isact_news == true) && (n.cat_id_news == cat_id) && ((n.title_news.Contains(search) || (n.content_news.Contains(search))))
                 orderby n.dtesabt_news descending, n.timsabt_news descending
                 select n).ToList();
            gvrnews.DataBind();

        }

        protected void btnsabt_Click(object sender, EventArgs e)
        {

            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok")
                    //&& (Session["isadmin_user"].ToString() == "isadmin")
                    && (Session["isact_user"].ToString() == "isact"))
                {
                    try
                    {
                        long id_user = long.Parse(Session["id_user"].ToString());
                        InfoModel.tblnews _news = new InfoModel.tblnews();
                        _news.user_id_news = id_user;
                        _news.cat_id_news = (int)MyObjects.grouplevel.payamhamkar;
                        _news.title_news = "پیام همکار : " + Session["lname_user"].ToString() + " " + Session["fname_user"].ToString();
                        _news.content_news = txtcontent_news.Text;
                        _news.dtesabt_news = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now);
                        _news.timsabt_news = DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" +
                                                  DateTime.Now.Minute.ToString().PadLeft(2, '0');
                        _news.att_news = null;
                        _news.isact_news = false;
                        if (txtcontent_news.Text != "")
                        {
                            _edbcontext.tblnews.Add(_news);
                            _edbcontext.SaveChanges();
                            showerrore("1", "پیام سیستم", "خبر درسیستم ثبت گردید");
                        }
                    }
                    catch (Exception)
                    {
                        showerrore("1", "خطای ورود اطلاعات", "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید");
                    }

                }
                else
                {
                    if (Request.Cookies["UserCooki"] != null)
                    {
                        string username_user = Request.Cookies["UserCooki"]["username"];
                        string pass_user = Request.Cookies["UserCooki"]["password"];
                        //Response.Cookies["UserCooki"]["username"] = txtusername_user.Text;
                        //Response.Cookies["UserCooki"]["password"] = txtpass_user.Text;
                        //UserCooki.Expires = DateTime.Now.AddDays(7);
                        //Response.Cookies.Add(UserCooki);
                        LoginMethod(username_user, pass_user);
                    }

                    else
                        showerrore("1", "خطای ورود اطلاعات", "شما وارد سامانه نشده اید لطفا از لینک ورود به سامانه استفاده کنید");
                }
            }
            else
            {
                if (Request.Cookies["UserCooki"] != null)
                {
                    string username_user = Request.Cookies["UserCooki"]["username"];
                    string pass_user = Request.Cookies["UserCooki"]["password"];
                    //Response.Cookies["UserCooki"]["username"] = txtusername_user.Text;
                    //Response.Cookies["UserCooki"]["password"] = txtpass_user.Text;
                    //UserCooki.Expires = DateTime.Now.AddDays(7);
                    //Response.Cookies.Add(UserCooki);
                    LoginMethod(username_user, pass_user);
                }

                else
                    showerrore("1", "خطای ورود اطلاعات", "شما وارد سامانه نشده اید لطفا از لینک ورود به سامانه استفاده کنید");
            }

        }
        [WebMethod(EnableSession = true)]
        public static string LoginMethod(string username, string password)
        {
            try
            {
                InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
                var query = from p in _edbcontext.tbluser
                            where p.username_user.Equals(username) && p.pass_user.Equals(password)
                            select p;
                var user = query.FirstOrDefault();
                if (user != null)
                {
                    if (HttpContext.Current.Request.Cookies["UserCooki"] == null)
                    {
                        HttpCookie UserCooki = new HttpCookie("UserCooki");
                        UserCooki.Values["username"] = username;
                        UserCooki.Values["password"] = password;
                        UserCooki.Expires = DateTime.Now.AddDays(7);
                        HttpContext.Current.Response.Cookies.Add(UserCooki);
                    }
                    HttpContext.Current.Session["vorod"] = "ok";
                    HttpContext.Current.Session["id_user"] = user.id_user.ToString();
                    HttpContext.Current.Session["lname_user"] = user.lname_user.ToString();
                    HttpContext.Current.Session["fname_user"] = user.fname_user.ToString();
                    HttpContext.Current.Session["isadmin_user"] = Convert.ToBoolean(user.isadmin_user.ToString()) == true ? "isadmin" : "isuser";
                    HttpContext.Current.Session["isact_user"] = Convert.ToBoolean(user.isact_user.ToString()) == true ? "isact" : "noact";
                    HttpContext.Current.Session["sex_user"] = Convert.ToBoolean(user.sex_user.ToString()) == true ? "آقای" : "خانم";
                }
                else
                {
                    return "false";

                }
            }
            catch (Exception)
            {
                return "false";
            }
            return "true";
        }

    }
}
