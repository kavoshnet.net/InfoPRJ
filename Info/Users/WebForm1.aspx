﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Users/UserMasterPage.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Info.Users.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            پیام های فوری
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <asp:GridView ID="gvrnews" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="Black" GridLines="Horizontal" UseAccessibleHeader="False"  AllowPaging="True" OnPageIndexChanging="gvrnews_PageIndexChanging" PageSize="5" ShowHeader="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField HeaderText="ش" ItemStyle-Width="3%">
                                        <ItemTemplate>
                                            <%# (Container.DataItemIndex+(gvrnews.PageIndex*gvrnews.PageCount)+1).ToString() %>
                                        </ItemTemplate>
                                        <ItemStyle Width="3%"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="id_news" HeaderText="ردیف" />

                                    <asp:BoundField DataField="title_news" HeaderText="خبر" />
                                    <asp:BoundField DataField="dtesabt_news" HeaderText="تاریخ خبر" />
                                </Columns>
                                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                                <PagerSettings Position="TopAndBottom" />
                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#242121" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
