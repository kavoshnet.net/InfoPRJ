﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;

namespace Info.Users
{
    public partial class UserMasterPage : System.Web.UI.MasterPage
    {
        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        protected void setrptslide()
        {
            //Session["cat_id"] = Request.QueryString["cat_id"].ToString();
            //int cat_id = int.Parse(Session["cat_id"].ToString());
            //string cat_name = _edbcontext.tblcategory.SingleOrDefault(p => p.id_cat == cat_id).name_cat;
            //Session["cat_name"] = cat_name;
            //string search = txtsearch.Text;//Session["name_soft"].ToString();

            rptslide.DataSource =
                (from n in _edbcontext.tblslide
                 where (n.isact_slide == true) 
                 orderby n.dtesabt_slide descending, n.timsabt_slide descending
                 select n).ToList();
            rptslide.DataBind();
        }
        protected void initialstate()
        {
            setrptslide();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                initialstate();
            }
        }
    }
}