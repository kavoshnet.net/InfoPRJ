﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Users/UserMasterPage.Master" AutoEventWireup="true" CodeBehind="dispsoft.aspx.cs" Inherits="Info.Users.dispsoft" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="container" class="container full-width">
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <script type="text/javascript">
            $(document).ready(function () {
                //errore message
                if ($("#<%=messagehf.ClientID %>").val() !== "") {
                    $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                    $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                    $("#message").fadeIn().delay(3000).fadeOut();
                    setTimeout(function () { document.location = $("#<%=messagehf.ClientID %>").val(); }, 3000);
                    //$("#<%=messagehf.ClientID %>").val("");
                    //window.location.replace("sabtmddjo.aspx");
                }
                //errore message
            });
        </script>
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->

        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <p id="messagetext" class="text-center"><strong>>پیام خطا </strong></p>
                    </div>
                </div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                    <div class="form-inline">
                        <div class="form-group">
                            <asp:TextBox ID="txtsearch" runat="server" Width="375px" CssClass="form-control" placeholder="عنوان را برای جستجو وارد کنید"></asp:TextBox>
                            <asp:Button ID="btnsearch" runat="server" CssClass="btn  btn-default" OnClick="btnsearch_Click" Text="جستجو" Width="175px" />
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title text-center">
                                <%= Session["cat_name"].ToString() %>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <asp:GridView ID="gvrsoft" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="Black" GridLines="Horizontal" UseAccessibleHeader="False" OnRowCommand="gvrsoft_RowCommand" AllowPaging="True" OnPageIndexChanging="gvrsoft_PageIndexChanging" PageSize="15" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" BorderStyle="None">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ش" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>

                                            <ItemStyle Width="5%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="عنوان" ItemStyle-Width="80%">
                                            <ItemTemplate>
                                                <%# Eval("name_soft") %>
                                            </ItemTemplate>
                                            <ItemStyle Width="80%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="تاریخ به روز رسانی" ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <%# Eval("dtesabt_soft") %>
                                            </ItemTemplate>

                                            <ItemStyle Width="15%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDownload" Text="دانلود" CommandArgument='<%# Eval("att_soft") %>' runat="server" OnClick="DownloadFile" CssClass="btn btn-xs btn-default" Width="75px"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="30%"></ItemStyle>
                                        </asp:TemplateField>

                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                    <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                                    <PagerSettings Position="TopAndBottom" />
                                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                    <SelectedRowStyle BackColor="#CC3333" ForeColor="White" Font-Bold="True" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#242121" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</asp:Content>
