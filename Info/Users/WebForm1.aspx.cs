﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Info.Users
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        protected void initialstate()
        {
            gvrnews.DataSource =
                (from n in _edbcontext.tblnews
                 where (n.isact_news == true)
                 orderby n.dtesabt_news descending, n.timsabt_news descending
                 select n).ToList();
            gvrnews.DataBind();

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                initialstate();
            }

        }

        protected void gvrnews_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            (sender as GridView).PageIndex = e.NewPageIndex;
            initialstate();
        }
    }
}