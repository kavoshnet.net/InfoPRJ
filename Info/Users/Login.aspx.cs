﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using InfoModel;
using MyObjects;
using System.Web.Security;
using System.Web.Services;

namespace Info.Users
{
    public partial class Login : System.Web.UI.Page
    {
        InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
            txtusername_user.Focus();
            //if (Request.Cookies["UserCooki"] != null)
            //{
            //    Response.Cookies["UserCooki"].Expires = DateTime.Now.AddDays(-1);
            //    Response.Redirect("login.aspx");
            //}
            if (Request.Cookies["UserCooki"] != null)
            {
                string username = Request.Cookies["UserCooki"]["username"];
                string password = Request.Cookies["UserCooki"]["password"];
                if (username != null && password != null)
                {
                    txtusername_user.Text = username;
                    txtpass_user.Text = password;
                    btnLogin_Click(sender, e);
                }
            }

            //txtusername_user.Text = "admin";
            //txtpass_user.Text = "123456";
            //btnLogin_Click(sender, null);
        }


        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                var query = from p in _edbcontext.tbluser
                            where p.username_user.Equals(txtusername_user.Text) && p.pass_user.Equals(txtpass_user.Text)
                            select p;
                var user = query.FirstOrDefault();
                if (user != null)
                {
                    if (Request.Cookies["UserCooki"] == null)
                    {
                        HttpCookie UserCooki = new HttpCookie("UserCooki");
                        UserCooki.Values["username"] = txtusername_user.Text;
                        UserCooki.Values["password"] = txtpass_user.Text;
                        //Response.Cookies["UserCooki"]["username"] = txtusername_user.Text;
                        //Response.Cookies["UserCooki"]["password"] = txtpass_user.Text;
                        UserCooki.Expires = DateTime.Now.AddDays(7);
                        Response.Cookies.Add(UserCooki);
                    }
                    Session["vorod"] = "ok";
                    Session["id_user"] = user.id_user.ToString();
                    Session["lname_user"] = user.lname_user.ToString();
                    Session["fname_user"] = user.fname_user.ToString();
                    Session["isadmin_user"] = Convert.ToBoolean(user.isadmin_user.ToString()) == true ? "isadmin" : "isuser";
                    Session["isact_user"] = Convert.ToBoolean(user.isact_user.ToString()) == true ? "isact" : "noact";
                    Session["sex_user"] = Convert.ToBoolean(user.sex_user.ToString()) == true ? "آقای" : "خانم";
                    if (user.isact_user == true && user.isadmin_user == true)
                        Response.Redirect("~/Admin/index.aspx");
                    if (user.isact_user == true && user.isadmin_user == false)
                        Response.Redirect("~/Users/index.aspx");
                }
                else
                {
                    showerrore("1", "خطای ورود اطلاعات", "نام کاربری یا کلمه عبور صحیح نیست دوباره تلاش کنید");

                }
            }
            catch (Exception)
            {
                showerrore("1", "خطای ورود اطلاعات", "در ورود کاربر خطایی رخ داده است لطفا دوباره تلاش کنید");
                //showerrore("1",ex.Message,"خطای ورود اطلاعات");
            }
        }

    }

}

