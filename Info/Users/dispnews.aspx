﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Users/UserMasterPage.Master" AutoEventWireup="true" CodeBehind="dispnews.aspx.cs" Inherits="Info.Users.dispnews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="container" class="container full-width">
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <script type="text/javascript">
            $(document).ready(function () {
                //errore message
                if ($("#<%=messagehf.ClientID %>").val() != "") {
                    $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                    $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                    $("#message").fadeIn().delay(3000).fadeOut();
                    //setTimeout(function () { document.location = $("#<%=messagehf.ClientID %>").val(); }, 1500);
                    $("#<%=messagehf.ClientID %>").val("");
                    //window.location.reload();
                }
                //errore message
            });
        </script>
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------LOGIN------------------------------------------------------------------------->
        <script type="text/javascript">
            // cal ajax methode for login
            $(document).ready(function () {
                $("#logindialog").dialog(
              {
                  autoOpen: false,
                  resizable: false,
                  modal: true,
                  bgiframe: true,
                  width: 300,
                  height: 250,
                  //title: "تست",
                  open: function (event, ui) {
                      $(".ui-dialog-titlebar-close").hide();
                  },
                  show: {
                      effect: "fade",
                      duration: 500
                  },
                  hide: {
                      effect: "explode",
                      //effect: "fade",
                      duration: 500
                  },
                  //close: function (event, ui) { },
                  buttons: {
                      'ورود': function () {
                          $.ajax({
                              type: 'POST',
                              contentType: "application/json; charset=utf-8",
                              url: 'dispnewsimp.aspx/LoginMethod',
                              data: "{" +
                                  "'username':'" + $('#txtusername').val() + "'," +
                                  "'password':'" + $('#txtpassword').val() + "'" +
                               "}",
                              async: false,
                              success: function (response) {
                                  if (response.d == 'true') {
                                      $('#txtusername').val('');
                                      $('#txtpassword').val('');
                                      //alert("داده ها با موفقیت ثبت شد");
                                      jQuery('#logindialog').dialog('close');
                                      window.location.reload();
                                      return true;
                                  }
                                  else {
                                      alert("خطا در ورود");
                                      return false;
                                  }

                              },
                              error: function ()
                              { alert("خطا در ورود"); return false; }
                          });

                      },
                      'انصراف': function () {
                          jQuery('#logindialog').dialog('close');
                      }
                  }
              });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                var mycooki = $.cookie("UserCooki");
                if (mycooki != null) {
                    $("#<%= lnkbtnlogin.ClientID %>").hide();
                    $("#<%= Label1.ClientID %>").hide();
                }
                $(document).on("click", "[id*=lnkbtnlogin]", function () {
                    $('#txtusername').val('');
                    $('#txtpassword').val('');
                    $('#logindialog').dialog('open'); return false;
                });
            });
        </script>
        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->
        <div id="logindialog" style="display: none">
            <div class="form-group  form-group-sm form-horizontal">
                <div class="row">
                    <div class="col-sm-12">
                        <asp:HiddenField ID="hfid_login" runat="server" ClientIDMode="Static" />
                        <asp:Label ID="Label2" runat="server" Text="نام کاربری"></asp:Label>
                        <asp:TextBox ID="txtusername" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="نام کاربری را وارد کنید" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <asp:Label ID="Label3" runat="server" Text="کلمه عبور"></asp:Label>
                        <asp:TextBox ID="txtpassword" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="کلمه عبور را وارد کنید" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

        <!--------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------------------------------------------------------------------->

        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-lg-12" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <p id="messagetitle" class="text-center">عنوان خطا</p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <p id="messagetext" class="text-center"><strong>>پیام خطا </strong></p>
                    </div>
                </div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row">
            <div class="col-lg-10">
                <div class="form-inline">
                    <div class="form-group">
                        <asp:TextBox ID="txtsearch" runat="server" Width="800px" CssClass="form-control" placeholder="عنوان را برای جستجو وارد کنید"></asp:TextBox>
                        <asp:Button ID="btnsearch" runat="server" CssClass="btn  btn-default" OnClick="btnsearch_Click" Text="جستجو" Width="175px" />
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
            <br />
        </div>
        <div class="row">
            <div class="col-lg-10">
                <asp:Label ID="Label1" class="text-bold text-info" runat="server" Text="همکاران محترم جهت رویت پیامها ابتدا باید وارد سامانه شوید."></asp:Label>
                <asp:LinkButton ID="lnkbtnlogin" class="btn-info btn-xs text-bold text-info" runat="server" Text="ورود به سامانه" ClientIDMode="Static" Width="100"></asp:LinkButton>
            </div>
        </div>
        <div class="clearfix">
            <br />
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            <%= Session["cat_name"] == null ? "" : Session["cat_name"].ToString() %>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <asp:GridView ID="gvrnews" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-responsive" CellPadding="4" Width="100%" ForeColor="Black" GridLines="Horizontal" UseAccessibleHeader="False" OnRowCommand="gvrnews_RowCommand" AllowPaging="True" OnPageIndexChanging="gvrnews_PageIndexChanging" PageSize="5" ShowHeader="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                <Columns>
                                    <%--                                    <asp:TemplateField HeaderText="ش" ItemStyle-Width="3%">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                        <ItemStyle Width="3%"></ItemStyle>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="col-lg-offset-0">
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading text-bold">
                                                            <div class="row">
                                                                <div class="col-lg-10"><%#Eval("title_news").ToString()%></div>
                                                                <div class="col-lg-offset-10"><%#Eval("dtesabt_news").ToString()%></div>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row ">
                                                                <div class="col-lg-12">
                                                                    <p class="text-success"><%#Eval("content_news").ToString()%></p>
                                                                </div>
                                                            </div>
                                                            <div class="row ">
                                                                <div class="col-lg-8">
                                                                    <h6 class="text-danger">پیام از:
                                                                <%# Eval("tbluser.lname_user") + " " + Eval("tbluser.fname_user")%>
                                                                    </h6>
                                                                </div>
                                                                <div class="col-lg-4" style="display: <%#Eval("att_news")!=null?"block":"none"%>">
                                                                    <h6 class="text-danger text-left">پیوست:
                                                                        <%--<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "~/attnews/"+Eval("att_news") %>' Text='<%# Eval("att_news") %>'></asp:HyperLink>--%>
                                                                        <asp:LinkButton ID="lnkDownload" Text='<%#Eval("att_news")%>' CommandArgument='<%#Eval("att_news") %>' runat="server" OnClick="DownloadFile" CssClass="btn btn-xs btn-default" Visible='<%#Eval("att_news") != null %>'> </asp:LinkButton>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <div class="row ">
                                                                <div class="col-lg-12">
                                                                    <p class="text-success">بازدیدکنندگان : <%#(MyObjects.DataCLS.MemberVisibleNews(Convert.ToInt64(Eval("id_news"))))%></p>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12 col-lg-offset-10">
                                                                <h6 class="text-danger">
                                                                    <asp:LinkButton ID="lnkAccept" Text='رویت شد' CommandArgument='<%#Eval("id_news") %>' runat="server" OnClick="lnkAccept_Click" CssClass="btn btn-xs btn-success"> </asp:LinkButton>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="100%"></ItemStyle>
                                    </asp:TemplateField>

                                    <%--                                    <asp:TemplateField HeaderText="جنسیت">
                                        <ItemTemplate><%#Eval("tblmddjo.sex_mddjo").ToString()=="True"?"مرد":"زن"%></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="سابقه وام">
                                        <ItemTemplate><%#Eval("tblmddjo.svm_mddjo").ToString()=="True"?"دارد":"ندارد"%></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="عملیات">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbtsabttojihi" CommandArgument='<%#Eval("mddjo_id")%>' CommandName="dotojihi" CssClass="btn btn-success btn-xs">ثبت طرح </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                                <PagerSettings Position="TopAndBottom" />
                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#242121" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-lg-offset-0">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            <%--<%= Session["cat_name"].ToString() %>--%>
                            <%= "پیوند به سامانه های استانی" %>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <a href="http://ssv.hm" target="_blank">
                                <img class="fasele" src="../images/ssv.png" alt="" /></a>
                            <a class="fasele" href="http://emha.hm" target="_blank">
                                <img class="fasele" src="../images/emha.png" alt="" /></a>
                            <a href="http://ess.hm" target="_blank">
                                <img class="fasele" src="../images/ess.png" alt="" /></a>
                            <a href="ftp://ftp.hm" target="_blank">
                                <img class="fasele" src="../images/ftp.png" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-lg-offset-0">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            <%--<%= Session["cat_name"].ToString() %>--%>
                            <%= "پیامهای همکاران" %>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <a href="dispnewsimp.aspx?cat_id=7" target="_self">
                                <img class="fasele" src="../images/Message.png" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-lg-offset-0">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            <%--<%= Session["cat_name"].ToString() %>--%>
                            <%= "پیوند به سامانه های سازمانی" %>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <a href="http://192.168.99.5:7001/oa/" target="_blank">
                                <img class="fasele" src="../images/oa.png" alt="" /></a>
                            <a href="http://estelam.nocr.org" target="_blank">
                                <img class="fasele" src="../images/estelam.png" alt="" /></a>
                            <a href="https://dcu.nocr.org" target="_blank">
                                <img class="fasele" src="../images/dcu.png" alt="" /></a>
                            <a href="https://abc.nocr.org" target="_blank">
                                <img class="fasele" src="../images/abc.png" alt="" /></a>
                            <a href="https://srt.nocr.org/" target="_blank">
                                <img class="fasele" src="../images/str.png" alt="" /></a>
                            <a href="https://www.ncr.ir/" target="_blank">
                                <img class="fasele" src="../images/ncr.png" alt="" /></a>
                            <a href="https://emsapp.ssd.net/cas/login?service=http%3A%2F%2Femsapp.ssd.net%2Fems-web%2Findex.jsp" target="_blank">
                                <img class="fasele" src="../images/emsapp.png" alt="" /></a>
                            <a href="http://emsapp.iranid.ir:7001/ccos-update/publish.htm" target="_blank">
                                <img class="fasele" src="../images/emsappupdate.png" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
