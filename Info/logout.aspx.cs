﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Info
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["vorod"] = null;
            Session["id_user"] = null;
            Session["lname_user"] = null;
            Session["fname_user"] = null;
            Session["isadmin_user"] = null;
            Session["isact_user"] = null;
            Session["sex_user"] = null;
            Session.RemoveAll();
            Response.Redirect("login.aspx");

        }
    }
}