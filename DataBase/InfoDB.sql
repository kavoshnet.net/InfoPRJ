USE [infoDB]
GO
/****** Object:  Table [dbo].[tblcategory]    Script Date: 06/22/2019 10:47:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblcategory](
	[id_cat] [numeric](18, 0) NOT NULL,
	[group_cat] [numeric](18, 0) NULL,
	[parent_cat] [numeric](18, 0) NULL,
	[name_cat] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblcategory] PRIMARY KEY CLUSTERED 
(
	[id_cat] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(1 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), N'پیامها')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(2 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), N'نرم افزار')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(3 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), N'اطلاعیه')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(4 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), N'اسلاید')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(5 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), N'دستورالعمل')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(6 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'پیام فوری')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(7 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'پیام همکار')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(8 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'نرم افزار عمومی')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(9 AS Numeric(18, 0)), CAST(9 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'درایورها')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(10 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'نرم افزار اداری')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(11 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'کارت هوشمند')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(12 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'آرشیو')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(13 AS Numeric(18, 0)), CAST(13 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'DCU')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(14 AS Numeric(18, 0)), CAST(14 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'ABC')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(15 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'استعلام')
INSERT [dbo].[tblcategory] ([id_cat], [group_cat], [parent_cat], [name_cat]) VALUES (CAST(16 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), N'پیام اسلایدر')
/****** Object:  Table [dbo].[tbluser]    Script Date: 06/22/2019 10:47:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbluser](
	[id_user] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[lname_user] [nvarchar](max) NULL,
	[fname_user] [nvarchar](max) NULL,
	[sex_user] [bit] NULL,
	[username_user] [nvarchar](max) NULL,
	[pass_user] [nvarchar](max) NULL,
	[isadmin_user] [bit] NULL,
	[isact_user] [bit] NULL,
 CONSTRAINT [PK_tbluser] PRIMARY KEY CLUSTERED 
(
	[id_user] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbluser] ON
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(1 AS Numeric(18, 0)), N'مهدی', N'اخلاص', 1, N'admin', N'3871117668', 1, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(2 AS Numeric(18, 0)), N'حسین', N'اسماعیلی', 1, N'admin', N'123456', 1, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(3 AS Numeric(18, 0)), N'جمشید', N'پناهی اسعد', 1, N'admin', N'654321', 1, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(4 AS Numeric(18, 0)), N'مهدی', N'اخلاص', 1, N'user', N'user', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(5 AS Numeric(18, 0)), N'حسین', N'اصلانی', 1, N'7G2J', N'7G2J', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(6 AS Numeric(18, 0)), N'قاسم', N'اعظمی', 1, N'2204', N'2204', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(7 AS Numeric(18, 0)), N'مهدی', N'پارساصفت', 1, N'4214', N'4214', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(8 AS Numeric(18, 0)), N'حسین', N'زندی', 1, N'9KT5', N'9KT5', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(9 AS Numeric(18, 0)), N'عباس', N'افشاری', 1, N'3syn', N'3syn', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(10 AS Numeric(18, 0)), N'شجاع', N'سبحانی', 1, N'1699', N'1699', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(11 AS Numeric(18, 0)), N'رمضان', N'ملکی', 1, N'1712', N'1712', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(12 AS Numeric(18, 0)), N'فرهاد', N'امانی', 1, N'1665', N'1665', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(13 AS Numeric(18, 0)), N'حاتم', N'زنگنه', 1, N'JE80', N'JE80', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(14 AS Numeric(18, 0)), N'مرتضی', N'جعفرپور', 1, N'TI6C', N'TI6C', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(15 AS Numeric(18, 0)), N'محمد', N'بیگدلی', 1, N'LPAY', N'LPAY', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(16 AS Numeric(18, 0)), N'علیرضا', N'صبوری مقدم', 1, N'F9GU', N'F9GU', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(17 AS Numeric(18, 0)), N'احمد', N'بهرامی', 1, N'22P7', N'22P7', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(18 AS Numeric(18, 0)), N'فرامرز', N'زیوری یار', 1, N'M4CJ', N'M4CJ', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(19 AS Numeric(18, 0)), N'مصطفی', N'بهرامی', 1, N'G866', N'G866', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(20 AS Numeric(18, 0)), N'سعید', N'ولی پور', 1, N'1655', N'1655', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(21 AS Numeric(18, 0)), N'اسماعیل', N'زارعی', 1, N'1694', N'1694', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(22 AS Numeric(18, 0)), N'امید', N'جمشیدی', 1, N'AYBP', N'AYBP', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(23 AS Numeric(18, 0)), N'داریوش', N'شاکری', 1, N'1651', N'1651', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(24 AS Numeric(18, 0)), N'علی', N'طیبی', 1, N'MRQP', N'MRQP', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(25 AS Numeric(18, 0)), N'برزو', N'حسنی تبار', 1, N'W54J', N'W54J', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(26 AS Numeric(18, 0)), N'محمود', N'خزلی', 1, N'4537', N'4537', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(27 AS Numeric(18, 0)), N'محمد', N'تیموری', 1, N'FN41', N'FN41', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(28 AS Numeric(18, 0)), N'رضا', N'اسماعیلی', 1, N'1686', N'1686', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(29 AS Numeric(18, 0)), N'حسین', N'محمدی', 1, N'1661', N'1661', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(30 AS Numeric(18, 0)), N'محمود', N'عبدی', 1, N'1670', N'1670', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(31 AS Numeric(18, 0)), N'بابک', N'بختیاری', 1, N'3X89', N'3X89', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(32 AS Numeric(18, 0)), N'محمدرضا', N'منتظرلطف', 1, N'1717', N'1717', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(33 AS Numeric(18, 0)), N'لیلا', N'زمانیان', 1, N'ZOXC', N'ZOXC', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(34 AS Numeric(18, 0)), N'معصومه', N'مهرزاد', 1, N'ND5F', N'ND5F', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(35 AS Numeric(18, 0)), N'اعظم', N'میرابی', 1, N'RNSC', N'RNSC', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(36 AS Numeric(18, 0)), N'روح اله', N'مهدوی تبار', 1, N'OIWN', N'OIWN', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(37 AS Numeric(18, 0)), N'شمس الدین', N'سلگی', 1, N'GQHH', N'GQHH', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(38 AS Numeric(18, 0)), N'مریم', N'نثاری', 1, N'E4TA', N'E4TA', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(39 AS Numeric(18, 0)), N'عباس', N'گلگون رخ', 1, N'4HF4', N'4HF4', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(40 AS Numeric(18, 0)), N'محمد', N'کرمی', 1, N'VKVP', N'VKVP', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(41 AS Numeric(18, 0)), N'سعید', N'زنگنه', 1, N'4GF4', N'4GF4', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(42 AS Numeric(18, 0)), N'زهرا', N'قلی پور', 1, N'BOA6', N'BOA6', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(43 AS Numeric(18, 0)), N'شهریار', N'آبسالان', 1, N'1722', N'1722', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(44 AS Numeric(18, 0)), N'وحید', N'آورزمانی', 1, N'7BW6', N'7BW6', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(45 AS Numeric(18, 0)), N'مظفر', N'محقق', 1, N'Y6ZR', N'Y6ZR', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(46 AS Numeric(18, 0)), N'رضا', N'فیضی', 1, N'6H8S', N'6H8S', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(47 AS Numeric(18, 0)), N'نادر', N'ابراهیمی', 1, N'1674', N'1674', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(48 AS Numeric(18, 0)), N'حسین', N'مهرجو', 1, N'1687', N'1687', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(49 AS Numeric(18, 0)), N'محمد', N'عافیتی طلوع', 1, N'1675', N'1675', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(50 AS Numeric(18, 0)), N'فاطمه', N'الهیاری خندان', 1, N'DALD', N'DALD', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(51 AS Numeric(18, 0)), N'علی', N'طائفی', 1, N'1676', N'1676', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(52 AS Numeric(18, 0)), N'بتول', N'رنجبردار', 1, N'OTXO', N'OTXO', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(53 AS Numeric(18, 0)), N'زهرا', N'فریدپور', 1, N'4212', N'4212', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(54 AS Numeric(18, 0)), N'علیرضا', N'کمکی', 1, N'XXRN', N'XXRN', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(55 AS Numeric(18, 0)), N'محمدرضا', N'صاحبی', 1, N'PPPG', N'PPPG', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(56 AS Numeric(18, 0)), N'محمود', N'صفری', 1, N'9QEE', N'9QEE', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(57 AS Numeric(18, 0)), N'علی', N'خداکرمی بدیع', 1, N'F6EY', N'F6EY', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(58 AS Numeric(18, 0)), N'علی', N'حدادی', 1, N'1700', N'1700', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(59 AS Numeric(18, 0)), N'حجت', N'یوسفی پارسا', 1, N'UBNT', N'UBNT', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(60 AS Numeric(18, 0)), N'طاهره', N'مجیدی', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(61 AS Numeric(18, 0)), N'عباس', N'قیاسی', 1, N'1723', N'1723', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(62 AS Numeric(18, 0)), N'بهرام', N'ساکی', 1, N'RX73', N'RX73', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(63 AS Numeric(18, 0)), N'واحد', N'شریفی', 1, N'1730', N'1730', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(64 AS Numeric(18, 0)), N'الهه', N'نصیری نژاد', 1, N'VJUI', N'VJUI', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(65 AS Numeric(18, 0)), N'مریم', N'یارمحمدی', 1, N'SQJN', N'SQJN', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(66 AS Numeric(18, 0)), N'ناهید', N'محمدبیگی', 1, N'5061', N'5061', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(67 AS Numeric(18, 0)), N'فاطمه', N'مظهری نیا', 1, N'5062', N'5062', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(68 AS Numeric(18, 0)), N'سمیه', N'سلیمانی', 1, N'S8TJ', N'S8TJ', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(69 AS Numeric(18, 0)), N'باقر', N'توسلی', 1, N'1734', N'1734', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(70 AS Numeric(18, 0)), N'ایرج', N'احمدی', 1, N'1726', N'1726', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(71 AS Numeric(18, 0)), N'مرتضی', N'برزگر زندی', 1, N'133K', N'133K', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(72 AS Numeric(18, 0)), N'فخرالسادات', N'صادقی نژاد', 1, N'VDAU', N'VDAU', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(73 AS Numeric(18, 0)), N'حسنعلی', N'ده پهلوان', 1, N'MHZX', N'MHZX', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(74 AS Numeric(18, 0)), N'حسین', N'سوری', 1, N'1684', N'1684', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(75 AS Numeric(18, 0)), N'محمدجواد', N'احسانی', 1, N'IWBS', N'IWBS', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(76 AS Numeric(18, 0)), N'اعظم', N'دباغی', 1, N'1682', N'1682', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(77 AS Numeric(18, 0)), N'زهرا', N'برکم', 1, N'7ZXE', N'7ZXE', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(78 AS Numeric(18, 0)), N'رضوان', N'بیات', 1, N'1732', N'1732', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(79 AS Numeric(18, 0)), N'محسن', N'محرابی', 1, N'9999', N'9999', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(80 AS Numeric(18, 0)), N'محمود', N'رشیدی', 1, N'1702', N'1702', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(81 AS Numeric(18, 0)), N'غلامرضا', N'مقدری', 1, N'3g83', N'3g83', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(82 AS Numeric(18, 0)), N'عباس', N'افشاری', 1, N'3syn', N'3syn', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(83 AS Numeric(18, 0)), N'جواد', N'گروسین', 1, N'46kh', N'46kh', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(84 AS Numeric(18, 0)), N'عباس', N'اسکندری', 1, N'4213', N'4213', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(85 AS Numeric(18, 0)), N'نادر', N'افشاری', 1, N'x8hc', N'x8hc', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(86 AS Numeric(18, 0)), N'لیلا', N'بی نیاز', 1, N'4105', N'4105', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(87 AS Numeric(18, 0)), N'نادر', N'جامه بزرگ', 1, N'1664', N'1664', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(88 AS Numeric(18, 0)), N'طیبه ', N'جهانگیری', 1, N'6514', N'6514', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(89 AS Numeric(18, 0)), N'فاطمه ', N'حبیبی ثمریان', 1, N'1659', N'1659', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(90 AS Numeric(18, 0)), N'ترنم ', N'حمزه ئی', 1, N'4106', N'4106', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(91 AS Numeric(18, 0)), N'سحر', N'رضائی', 1, N'gl6r', N'gl6r', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(92 AS Numeric(18, 0)), N'فاطمه', N'رنجبر', 1, N'qtxp', N'qtxp', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(93 AS Numeric(18, 0)), N'علی', N'زمانی', 1, N'1698', N'1698', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(94 AS Numeric(18, 0)), N'علی اکبر', N'سعیدی کیا', 1, N'1711', N'1711', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(95 AS Numeric(18, 0)), N'مسعود', N'صادقی', 1, N'1707', N'1707', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(96 AS Numeric(18, 0)), N'مجید', N'شوندی', 1, N'1667', N'1667', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(97 AS Numeric(18, 0)), N'فاطمه', N'صفری', 1, N'w44g', N'w44g', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(98 AS Numeric(18, 0)), N'مجید', N'گلزار', 1, N'row1', N'row1', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(99 AS Numeric(18, 0)), N'مریم ', N'غفاریان', 1, N'4109', N'4109', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(100 AS Numeric(18, 0)), N'بهزاد', N'فیض برازنده', 1, N'MT7Z', N'MT7Z', 0, 1)
GO
print 'Processed 100 total records'
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(101 AS Numeric(18, 0)), N'محمود', N'قره باغی برهمن', 1, N'3543', N'3543', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(102 AS Numeric(18, 0)), N'سیدهادی', N'کاظمی', 1, N'kfx7', N'kfx7', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(103 AS Numeric(18, 0)), N'محمدتقی', N'کاظمی', 1, N'1652', N'1652', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(104 AS Numeric(18, 0)), N'ناهید', N'مازوئی', 1, N'1648', N'1648', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(105 AS Numeric(18, 0)), N'محمود', N'محمودی', 1, N'1660', N'1660', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(106 AS Numeric(18, 0)), N'پروانه', N'محمدی سرشت', 1, N'rsgk', N'rsgk', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(107 AS Numeric(18, 0)), N'سیاوش ', N'سماوات', 1, N'ehzy', N'ehzy', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(108 AS Numeric(18, 0)), N'عباس ', N'افشاری', 1, N'3syn', N'3syn', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(109 AS Numeric(18, 0)), N'محسن ', N'امیدی مدد', 1, N'9999', N'9999', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(110 AS Numeric(18, 0)), N'جواد', N'توحیدی', 1, N'9999', N'9999', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(111 AS Numeric(18, 0)), N'منوچهر', N'فرزام ', 1, N'cyt2', N'cyt2', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(112 AS Numeric(18, 0)), N'حسین ', N'اسماعیلی', 1, N'isrj', N'isrj', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(113 AS Numeric(18, 0)), N'کاترین ', N'منوچهریان', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(114 AS Numeric(18, 0)), N'علی اکبر ', N'بختیاریان ', 1, N'1708', N'1708', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(115 AS Numeric(18, 0)), N'علیرضا ', N'نادری فر', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(116 AS Numeric(18, 0)), N'میترا ', N'قراریان ', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(117 AS Numeric(18, 0)), N'مصطفی', N'محتشمی', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(118 AS Numeric(18, 0)), N'محمد حسین ', N'زورمند', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(119 AS Numeric(18, 0)), N'اصغر ', N'حمزه ای', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(120 AS Numeric(18, 0)), N'خسرو ', N'کاکی', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(121 AS Numeric(18, 0)), N'میثم ', N'خزایی', 1, N'm2qp', N'm2qp', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(122 AS Numeric(18, 0)), N'لیلا', N'ظهوری', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(123 AS Numeric(18, 0)), N'عاطفه ', N'عزیزی شاکر', 1, N'2kvy', N'2kvy', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(124 AS Numeric(18, 0)), N'مهدی ', N'علیمرادی اخلاص', 1, N'mvzj', N'mvzj', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(125 AS Numeric(18, 0)), N'جمشید ', N'پناهی اسعد', 1, N'85uj', N'85uj', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(126 AS Numeric(18, 0)), N'مهدی ', N'دریایی', 1, N'1689', N'1689', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(127 AS Numeric(18, 0)), N'کیانوش ', N'اکبری زاد', 1, N'eulp', N'eulp', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(128 AS Numeric(18, 0)), N'محمد ولی', N'مستقیمان', 1, N'1688', N'1688', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(129 AS Numeric(18, 0)), N'یوسف', N'سوری', 1, N'4487', N'4487', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(130 AS Numeric(18, 0)), N'رقیه ', N'وجدانی پاک', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(131 AS Numeric(18, 0)), N'کاربر ', N'انتظامات', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(132 AS Numeric(18, 0)), N'کبری', N'شیری', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(133 AS Numeric(18, 0)), N'سمیرا', N'فرامرزی', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(134 AS Numeric(18, 0)), N'کاربر ', N'اتاق سرور', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(135 AS Numeric(18, 0)), N'علی', N'سیفی', 1, N'113U', N'113U', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(136 AS Numeric(18, 0)), N'کاربر', N'ذیحسابی', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(137 AS Numeric(18, 0)), N'مهدی', N'نهاوندی', 1, N'LLL8', N'LLL8', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(138 AS Numeric(18, 0)), N'محمود', N'قره باغی برهمن', 1, N'3543', N'3543', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(139 AS Numeric(18, 0)), N'مصطفی', N'بختیاری', 1, N'd5po', N'd5po', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(140 AS Numeric(18, 0)), N'عبدالله', N'مقصودی فر', 1, N'wzqc', N'wzqc', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(141 AS Numeric(18, 0)), N'میثم', N'نقوی', 1, N'm4ei', N'm4ei', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(142 AS Numeric(18, 0)), N'داود', N'عبدلی', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(143 AS Numeric(18, 0)), N'سمیرا', N'امامقلی وند', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(144 AS Numeric(18, 0)), N'جواد ', N'عقبی ', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(145 AS Numeric(18, 0)), N'محمد رحیم ', N'فتاحی', 1, N'2byh', N'2byh', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(146 AS Numeric(18, 0)), N'مرضیه', N'نیر پناه', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(147 AS Numeric(18, 0)), N'حسن', N'شیخی', 1, N'F5OO', N'F5OO', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(148 AS Numeric(18, 0)), N'مرضیه ', N'نیر پناه', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(149 AS Numeric(18, 0)), N'فرشته سادات ', N'رسولی فطرت', 1, NULL, NULL, 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(150 AS Numeric(18, 0)), N'علی', N'فریادرس', 1, N'620C', N'620C', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(151 AS Numeric(18, 0)), N'احمد', N'محمدی', 1, N'BYON', N'BYON', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(152 AS Numeric(18, 0)), N'سید روح الله', N'معروفی', 1, N'O6Y6', N'O6Y6', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(153 AS Numeric(18, 0)), N'مریم', N'عبدالعلی بهرامی', 0, N'WGUZ', N'WGUZ', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(154 AS Numeric(18, 0)), N'محسن', N'اکبری', 1, N'SG3P', N'SG3P', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(155 AS Numeric(18, 0)), N'اسماعیل', N'دانشور', 1, N'QWOP', N'QWOP', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(156 AS Numeric(18, 0)), N'ابراهیم', N'مردانی', 1, N'LXX1', N'LXX1', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(157 AS Numeric(18, 0)), N'کمال', N'نیک پور', 1, N'4YFF', N'4YFF', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(158 AS Numeric(18, 0)), N'ابوالفضل', N'انصاری', 1, N'OFIA', N'OFIA', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(159 AS Numeric(18, 0)), N'ياسين', N'معصومي', 1, N'hxz6', N'hxz6', 1, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(160 AS Numeric(18, 0)), N'مجيد', N'عمادي اعظم', 1, N'fxzg', N'fxzg', 0, 1)
INSERT [dbo].[tbluser] ([id_user], [lname_user], [fname_user], [sex_user], [username_user], [pass_user], [isadmin_user], [isact_user]) VALUES (CAST(161 AS Numeric(18, 0)), N'حسن', N'سلیمانی', 1, N'VXKX', N'1368', 0, 1)
SET IDENTITY_INSERT [dbo].[tbluser] OFF
/****** Object:  Table [dbo].[tblsoft]    Script Date: 06/22/2019 10:47:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblsoft](
	[id_soft] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cat_id_soft] [numeric](18, 0) NULL,
	[user_id_soft] [numeric](18, 0) NULL,
	[name_soft] [nvarchar](max) NULL,
	[att_soft] [nvarchar](max) NULL,
	[dtesabt_soft] [nchar](10) NULL,
	[timsabt_soft] [nchar](10) NULL,
	[isact_soft] [bit] NULL,
 CONSTRAINT [PK_tblsoft] PRIMARY KEY CLUSTERED 
(
	[id_soft] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblsoft] ON
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(1 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'WinRar فشرده ساز', N'winrar.5.21.Final.x86.exe', N'1394/08/16', N'08:32     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(2 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'فونت فارسی برای تمام ویندوزها', N'PersianFont.rar', N'1394/08/16', N'08:12     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(3 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'RadminServer نرم افزار اتصال از راه دور', N'RadminServer.rar', N'1394/08/16', N'08:13     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(4 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'printsetup تنظیمات چاپگر سامانه DCU', N'printsetup.exe', N'1394/08/16', N'08:27     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(5 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'DCU-Setup ارتباط با سامانه جمع آوری اطلاعات از طریق کارت هوشمند', N'DCU-Setup-V0.2.exe', N'1394/08/16', N'08:33     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(6 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'ACD See Pro 3.0 ویرایش و مدیریت تصاویر', NULL, N'1394/08/16', N'08:34     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(7 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'Adobe_Reader_10.0 مدیریت فایلهای PDF', N'Adobe_Reader_10.0.rar', N'1394/08/16', N'08:35     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(8 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'Advanced.Renamer.3.13 تغییر نام دسته ای فایلها', N'Advanced.Renamer.3.13.rar', N'1394/08/16', N'08:36     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(9 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'AttMan نرم افزار ثبت ضمائم', N'AttMan(920405).rar', N'1394/08/16', N'08:41     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(10 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'ابزار مورد نیاز برای اتوماسیون', N'Automation.rar', N'1394/08/16', N'08:42     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(11 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'BurnAway نرم افزاری جهت رایت انواع سی دی و دی وی دی', N'BurnAway.rar', N'1394/08/16', N'08:43     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(12 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'bitdefender نسخه به روز آنتی ویروس سازمانی', N'bitdefender.rar', N'1394/08/16', N'08:45     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(13 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'FileZilla for Windows XP مدیریت FTP برای ویندوز ایکس پی', N'FileZilla Client 3.3.2 Final for Windows XP.rar', N'1394/08/16', N'08:47     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(14 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'FileZilla For Windows 7 مدیریت FTP برای ویندوز سون', N'FileZilla.3.9.0.6.Final For Windows 7.rar', N'1394/08/16', N'08:48     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(15 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'PowerISO مدیریت فایلهای ISO', N'PowerISO.5.4.rar', N'1394/08/16', N'08:48     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(16 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'لغت نامه دهخدا', N'dehkhoda.iso', N'1394/08/16', N'08:50     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(17 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'Web 15Sal نرم افزارهای مرتبط با سامانه شناسنامه تحت وب', N'Web 15Sal.rar', N'1394/08/16', N'08:52     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(18 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'Mozilla.Firefox مرورگر موزیلا', N'Mozilla.Firefox.40.0.2.EN.rar', N'1394/08/16', N'08:53     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(19 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'KMPlayer پخش فایلهای ویدئویی', N'KMPlayer 3.8.0.121.rar', N'1394/08/16', N'08:54     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(20 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'kartmelli نرم افزار کارت ملی قدیمی', N'kartmelliold.rar', N'1394/08/16', N'13:44     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(21 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'GetSetFile نرم افزار ارسال دریافت تصاویر در شناسنامه بزرگسال', N'GetSetFileEndVer.rar', N'1394/08/16', N'08:56     ', 1)
INSERT [dbo].[tblsoft] ([id_soft], [cat_id_soft], [user_id_soft], [name_soft], [att_soft], [dtesabt_soft], [timsabt_soft], [isact_soft]) VALUES (CAST(22 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'office2010 نرم افزار افیس اداری نسخه 2010', N'office2010.iso', N'1394/08/16', N'09:24     ', 0)
SET IDENTITY_INSERT [dbo].[tblsoft] OFF
/****** Object:  Table [dbo].[tblslide]    Script Date: 06/22/2019 10:47:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblslide](
	[id_slide] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cat_id_slide] [numeric](18, 0) NULL,
	[user_id_slide] [numeric](18, 0) NULL,
	[name_slide] [nvarchar](max) NULL,
	[att_slide] [nvarchar](max) NULL,
	[dtesabt_slide] [nchar](10) NULL,
	[timsabt_slide] [nchar](10) NULL,
	[isact_slide] [bit] NULL,
 CONSTRAINT [PK_tblslide] PRIMARY KEY CLUSTERED 
(
	[id_slide] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblslide] ON
INSERT [dbo].[tblslide] ([id_slide], [cat_id_slide], [user_id_slide], [name_slide], [att_slide], [dtesabt_slide], [timsabt_slide], [isact_slide]) VALUES (CAST(3 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'احراز هویت ', N'T-Ehraz Hoviat2.jpg', N'1394/08/18', N'11:17     ', 1)
INSERT [dbo].[tblslide] ([id_slide], [cat_id_slide], [user_id_slide], [name_slide], [att_slide], [dtesabt_slide], [timsabt_slide], [isact_slide]) VALUES (CAST(4 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'لزوم استفاده از فایر فاکس', N'Title03.jpg', N'1394/08/18', N'11:17     ', 1)
INSERT [dbo].[tblslide] ([id_slide], [cat_id_slide], [user_id_slide], [name_slide], [att_slide], [dtesabt_slide], [timsabt_slide], [isact_slide]) VALUES (CAST(5 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'تحویل کارت هوشمند', N'TahvilKart.jpg', N'1394/08/18', N'11:17     ', 1)
SET IDENTITY_INSERT [dbo].[tblslide] OFF
/****** Object:  Table [dbo].[tblnews]    Script Date: 06/22/2019 10:47:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblnews](
	[id_news] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cat_id_news] [numeric](18, 0) NULL,
	[user_id_news] [numeric](18, 0) NULL,
	[title_news] [nvarchar](max) NULL,
	[content_news] [nvarchar](max) NULL,
	[att_news] [nvarchar](max) NULL,
	[dtesabt_news] [nchar](10) NULL,
	[timsabt_news] [nchar](10) NULL,
	[isact_news] [bit] NULL,
 CONSTRAINT [PK_tblimmnews] PRIMARY KEY CLUSTERED 
(
	[id_news] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblnews] ON
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(21 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'تغییر در سایز دسته کارتهای هوشمند ملی', N'به اطلاع میرساند نظر به افزایش تعداد کارتهای موجود در دسته های کارت هوشمند ملی ، برخی از دسته ها بصورت مستقل به عنوان باکس به ادارات ارسال میگردد. 
در ضمن برچسب و فهرست دسته در درون آن قرار گرفته و فهرست کارتن نیز حذف خواهد شد.', NULL, N'1394/08/19', N'13:52     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(22 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'تغییر آدرس سامانه های اداره کل', N'از این پس تمامی سامانه های اداره کل با دامنه hm و از مسیر سامانه info.hm قابل بهره برداری میباشند.', NULL, N'1394/08/19', N'13:51     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(23 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'آغاز بهره برداری از سایت info اداره کل', N'<p>
	<span style="font-family:b zar;"><span style="font-size: 16px;"><strong>همکاران گرامی : از این پس سامانه info اداره کل، از آدرس info.hm قابل بهره برداری میباشد.</strong></span></span></p>
', NULL, N'1394/08/19', N'13:46     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(26 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'شیوه نامه اخذ درخواست کارت هوشمند ملی', N'<p>
	<strong><span style="color:#800080;"><span style="font-size: 18px;"><span style="font-family: b lotus;">روسای محترم ادارات، با سلام و احترام شیوه نامه ی استانی اخذ درخواست کارت هوشمند ملی امروز به کارتابل شما ارسال میشود. شایسته است: </span></span></span></strong></p>
<p>
	<strong><span style="color:#800080;"><span style="font-size: 18px;"><span style="font-family: b lotus;">1) به دقت مطالعه و طی برگزاری جلسه ای با تمامی همکاران مورد بررسی و تبادل نظر قرار گیرد.</span></span></span></strong></p>
<p>
	<strong><span style="color:#800080;"><span style="font-size: 18px;"><span style="font-family: b lotus;">2) صفحات مربوط به اداره پست به ایشان ابلاغ گردد.(صفحات مربوط به اداره، به پست و پیشخوان ارائه نشود) </span></span></span></strong></p>
<p>
	<strong><span style="color:#800080;"><span style="font-size: 18px;"><span style="font-family: b lotus;">3) به همکاران پست تاکید شود مراتب مورد مطالعه و نصب العین هرکدام از قسمت ها باشد.</span></span></span></strong></p>
<p>
	<strong><span style="color:#800080;"><span style="font-size: 18px;"><span style="font-family: b lotus;">4) از شیوه نامه به زودی آزمون گرفته خواهد شد.</span></span></span></strong></p>
<p>
	<strong><span style="color:#800080;"><span style="font-size: 18px;"><span style="font-family: b lotus;">5) نظرات اصلاحی خود را به حوزه معاونت فناوری اعلام تا در صورت لزوم در اسرع وقت شیوه نامه بروز رسانی گردد.</span></span></span></strong></p>
<p>
	<strong><span style="color:#800080;"><span style="font-size: 18px;"><span style="font-family: b lotus;">6) صورت جلسه رویت همکاران و متصدیان پست در اسرع وقت به اداره کل ارسال گردد. </span></span></span></strong></p>
', NULL, N'1394/09/04', N'08:06     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(28 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'بهره برداری از سامانه ثبت مدارک امحایی', N'<h3 style="color: blue;">
	<span style="color:#008080;"><span style="font-family: b mitra;"><span style="font-size: 22px;">با توجه به راه اندازی سامانه ثبت مدارک امحایی ، کارتهای ملی امحایی ادارات تنها از طریق این سامانه قابل دریافت و امحاء میباشد.لذا خواهشمند است همکاران مسئول جهت دریافت نام و کلمه عبور با کارشناسان استان هماهنگی لازم را به عمل آورند. در ضمن جهت استفاده از سامانه مذکور میتوانید از لینک ((سایر سامانه ها)) موجود در سایت info اقدام نمایید.</span></span></span></h3>
', NULL, N'1394/09/28', N'11:56     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(29 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'اعلام مشخصات فرمانداران و بخشداران محترمی که رمز عبور کارت هوشمند خود را فراموش کرده اند', N'<p style="text-align: justify;">
	<span style="color:#800080;"><span style="font-family: b traffic;"><span style="font-size: 16px;">با توجه به نزدیکی انتخابات دوره دهم مجلس شورای اسلامی و الزام استفاده مسئولین انتخابات از کارت هوشمند ملی ، خواهشمند است رؤسای ادارت ، مشخصات فرمانداران یا بخشدارانی که کلمه عبور کارت هوشمند خود را فراموش کرده اند ، به همکاران ستادی اعلام نمایند.</span></span></span></p>
', NULL, N'1394/09/17', N'12:55     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(30 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'تغییر بستر FTP', N'<p style="text-align: justify;">
	<span style="font-size:16px;"><span style="font-family: b yekan;"><span style="color:#0000cd;">با توجه به تغییر بستر سامانه FTP</span><span style="color: rgb(255, 160, 122);">،</span> از این پس همکاران می توانند از لینک موجود در همین سامانه و وارد نمودن نام کاربری و رمز عبور اداره خود، به این سامانه دسترسی یابند. در ضمن نام کاربری FTP عمومی، 111 بوده که جایگزین Setup گردیده است. لطفا جهت دریافت کلمه عبور و هر گونه سوال احتمالی، با واحد فناوری اطلاعات استان تماس گرفته شود.</span></span></p>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
', NULL, N'1394/09/28', N'11:54     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(31 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'قطعی اتوماسیون اداری  تحت وب ', N'<p style="text-align: justify;">
	<strong><span style="font-family:b zar;"><span style="font-size: 14px;"><span dir="RTL">به اطلاع می رساند بدلیل ارتقاء نرم افزاری <span style="color:#ff0000;">اتوماسیون اداری تحت وب</span>، این سامانه در تاریخ های<span style="color:#ff0000;"> 6 و 7 دی</span> ماه قابل دسترس نخواهد بود. مقتضی است تمهیدات لازم برای مدیریت مکاتبات اداری در این روزها اندیشیده شود</span></span></span></strong></p>
', NULL, N'1394/10/02', N'09:30     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(32 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'برقراری ارتباط اتوماسیون', N'<p>
	<strong><span style="font-size:14px;"><span style="font-family: b nazanin;">با سلام</span></span></strong></p>
<p>
	<strong><span style="font-size:14px;"><span style="font-family: b nazanin;">&nbsp;دسترسی به سامانه اتوماسیون برقرار گردید و کلیه کاربران امکان ورود و استفاده از این سامانه&nbsp; را دارند. </span></span></strong></p>
<p>
	<strong><span style="font-size:14px;"><span style="font-family: b nazanin;">در صورت بروز مشکل با واحد فناوری اطلاعات اداره کل تماس گرفته شود.</span></span></strong></p>
', NULL, N'1394/10/09', N'09:06     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(33 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'قطعی سامانه', N'<p>
	<span style="color:#0000ff;"><span style="font-size: 20px;"><span style="font-family: b mitra;">با سلام و احترام </span></span></span></p>
<p>
	<span style="color:#0000ff;"><span style="font-size: 20px;"><span style="font-family: b mitra;">به اطلاع میرساند سامانه کارت هوشمند ملی&nbsp; روز چهارشنبه مورخ 94/10/16 از ساعت 18 قطع خواهدبود.</span></span></span><br />
	&nbsp;</p>
', NULL, N'1394/10/16', N'10:37     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(34 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'جزوه آموزشی آزمون مجازی ', N'<p>
	<span style="font-size:18px;"><span style="font-family: b yagut;">با سلام<br />
	جزوه آموزشی کارت هوشمند ملی جهت بهره برداری همکاران در آزمون مجازی&nbsp; پیشرو&nbsp; در سامانه&nbsp; FTP&nbsp; عمومی (111) قرار داده شده است.</span></span><br />
	&nbsp;</p>
', NULL, N'1394/10/17', N'13:16     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(37 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'تنظیمات جدید اتوماسیون اداری تحت وب', N'<p>
	<span style="font-size:14px;"><span style="font-family: b yekan;"><span style="color:#ff0000;">قابل توجه کارشناسان محترم فناوری اطلاعات ادارات</span></span></span></p>
<p>
	<span style="font-size:14px;"><span style="font-family: b yekan;">لطفا به منظور استفاده بهینه از اتوماسیون اداری تحت وب و پیشگیری از بروز مشکلات احتمالی، تنظیمات مندرج در فایل setting for editor.pdf که در سامانه FTP با کاربری (111) قرار داده شده را بر روی کلیه سیستم های اداره اعمال نمایید.</span></span></p>
', NULL, N'1394/10/21', N'11:51     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(38 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'پیشگیری از انسداد کارت هوشمند ملی', N'<p>
	<span style="font-size:14px;"><span style="font-family: b yekan;">به منظور جلوگیری از <span style="color:#ff0000;">انسداد کارت هوشمند ملی </span>در&nbsp; هنگام تحویل کارت، مقتضی است در صورت عدم انطباق اثر انگشت عملیات این فرایند تا <span style="color:#ff0000;">10 بار </span>تکرار و در نهایت <span style="color:#ff0000;">خرابی&nbsp; اثر انگشت </span>اعلام گردد.</span></span></p>
<p>
	<span style="font-size:14px;"><span style="font-family: b yekan;">تکرار بیش از 10 بار فرایند عدم انطباق باعث انسداد کارت شده و امکان تعویض و یا صدور مجدد نیز فراهم نخواهد بود.</span></span></p>
', NULL, N'1394/10/21', N'12:58     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(39 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'حضور بازرسات حراست سازمان در دفاتر کارت هوشمند', N'<p>
	<span style="font-size:14px;"><strong><span style="font-family: b zar;">با سلام</span></strong></span></p>
<p>
	<span style="font-size:14px;"><strong><span style="font-family: b zar;">به اطلاع می رساند در&nbsp; روز <span style="color:#ff0000;">چهارشنبه&nbsp; مورخه 94.10.30</span>&nbsp; ایستگاههای اخذ درخواست کارت هوشمند ملی توسط <span style="color:#ff0000;">بازرسان حراست سازمان</span> مورد بازدید قرار خواهد گرفت. لطفا ضمن آمادگی لازم، احراز هویت در راس دستورات کاری دفاتر قرار گیرد.</span></strong></span></p>
', NULL, N'1394/10/29', N'14:34     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(40 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'نصب فونت های جدید اتوماسیون', N'<p>
	<span style="font-size:16px;"><strong><span style="font-family: b zar;">با سلام </span></strong></span></p>
<p>
	<strong><span style="font-family:b zar;"><span style="font-size:16px;">به منظور استفاده بهینه از امکانات جدید اتوماسیون اداری تحت وب، لطفا کلیه فونت های پیوست در سیستم کاربران نصب گردد.</span></span></strong></p>
', N'Font.zip', N'1394/11/07', N'10:01     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(41 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'جزوه آموزشی سامانه خدمات شهروندی و جمع آوری اطلاعات', N'<p>
	<strong><span style="font-size:18px;"><span style="font-family: b traffic;">جزوه آموزشی <span style="color:#ff0000;">سامانه خدمات شهروندی</span> جهت بهره برداری همکاران در سامانه قرار داده شد.</span></span></strong></p>
', N'راهنماي کارت هوشمند ملي دفاتر ثبت نام  (ccos).pdf', N'1394/11/14', N'09:10     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(42 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'جزوه آموزشی دوره ارتقاء سلامت اداری', N'<p>
	<span style="font-size:22px;"><span style="font-family: b lotus;">جزوه آموزشی دوره ارتقاء سلامت اداری جهت بهره برداری در سامانه قرار گرفت.<span style="color:#ff8c00;">آزمون در تاریخ 1394/11/18 برگزار میگردد.</span></span></span></p>
', N'جزوه سلامت اداری.rar', N'1394/11/15', N'10:50     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(43 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'', N'<p style="text-align: justify;">
	<span style="font-size:18px;"><span style="font-family: b traffic;"><strong>به اطلاع می رساند به دلیل وجود مشکلات فنی <strong>، سامانه های کارت هوشمند ملی و خدمات شهروندی </strong>در&nbsp; روز <span style="color: rgb(255, 0, 0);">پنج شنبه&nbsp; مورخه 1394/11/15</span> از دسترس خارج میباشد.</strong></span></span></p>
<p style="text-align: justify;">
	<span style="font-size:18px;"><span style="font-family: b traffic;"><strong>لذا تمامی نوبتهای مربوط به این روز در <span style="color: rgb(255, 0, 0);">روز شنبه مورخه 1394/11/17 دارای اعتبار بوده</span> و مراجعین محترم میتوانند جهت اخذ درخواست به ایستگاههای کاری مراجعه نمایند.</strong></span></span></p>
', NULL, N'1394/11/15', N'11:48     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(44 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'ممنوع الخدمات', N'<h3 align="center" dir="rtl">
	<span style="font-size:18px;"><span style="font-family: b nazanin;"><span style="color: rgb(178, 34, 34);"><strong>چگونگی &nbsp;پياده سازي فاز اول تکميل اطلاعات رکوردهاي ممنوع الخدمات &quot;م دار&quot;</strong></span></span></span></h3>
<p dir="rtl">
	&nbsp;</p>
<p dir="rtl">
	<span style="font-size:14px;"><span style="font-family: b nazanin;"><strong>با سلام و احترام</strong></span></span></p>
<p dir="rtl">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">در اجراي فعاليت ساماندهي اطلاعات افراد ممنوع الخدمات (برنامه شماره 2-2-8&nbsp;&nbsp; راهبردی سازمان در سال 1394) و پيرو نامه شماره21283/11/2 -24/3/1394 ،به منظور درج نتايج بررسي هاي انجام شده در پايگاه داده، اینک سامانه &quot;تکمیل اطلاعات اسناد ممنوع الخدمات موجود (م دار)&quot; آماده بهره برداری می باشد . رعایت موارد ذیل الزامی است :</span></span></p>
<p dir="rtl">
	&nbsp;</p>
<ol>
	<li dir="rtl" value="NaN">
		<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp;&nbsp;قبل از بهره برداري از سامانه، آماده سازي فهرست ارسال شده قبلي (اشاره شده در نامه فوق)،به همراه ضمائم مورد نياز، ضروري می باشد.</span></span></li>
</ol>
<p dir="rtl">
	&nbsp;</p>
<ol>
	<li dir="rtl" value="NaN">
		<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; اين سامانه صرفا جهت <strong><u>تکميل</u></strong> اطلاعات رکوردهاي داراي حرف &quot;م&quot; مي باشد . یاد آور می شود ضرورت تکمیل اطلاعات مواردی که درحال حاضر رفع ممنوعیت شده اند، به منظور نگهداری و حفظ سوابق آنها در پایگاه داده می باشد.</span></span></li>
</ol>
<p dir="rtl">
	&nbsp;</p>
<ol>
	<li dir="rtl" value="NaN">
		<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; چگونگی <strong><u>رفع</u> يا<u> ابقاي</u></strong> وضعيت ممنوع الخدمات متعاقبا ارسال خواهد شد.</span></span></li>
</ol>
<p dir="rtl">
	&nbsp;</p>
<ol>
	<li dir="rtl" value="NaN">
		<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; امکان تکميل اطلاعات رکوردهاي &quot;م دار&quot; به کاربر داراي نقش &quot;بايگان&quot; داده شده است. بنابر اين تنها کاربر بايگان قادر به ديدن لينک &quot; تکمیل اطلاعات اسناد ممنوع الخدمات موجود (م دار)&quot; در سامانه ارائه خدمات هويتي خواهد بود.</span></span></li>
</ol>
<p dir="rtl" style="margin-right:-2.3pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;"><strong>نحوه تکميل اطلاعات ممنوع الخدمات در سامانه </strong>:</span></span></p>
<p dir="rtl" style="margin-right:-2.3pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">1-&nbsp; کاربر معرفي شده پس از ورود به سامانه ارائه خدمات هويتي با انتخاب گزينه &quot;تکميل اطلاعات اسناد ممنوع الخدمات موجود (م دار) &quot; فهرست رکوردهاي ممنوع الخدمات مربوط به اداره خود را (بر اساس ليست دستي موجود)مشاهده مي نمايد.</span></span></p>
<p dir="rtl" style="margin-right:-2.3pt;">
	&nbsp;</p>
<ol>
	<li dir="rtl" value="NaN">
		<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; کاربر پس از انتخاب هر رکورد، با کليک کردن بر دکمه &quot;تکميل&quot; وارد صفحه تکميل اطلاعات ممنوعيت مي شود.</span></span></li>
</ol>
<p dir="rtl" style="margin-right:29.85pt;">
	&nbsp;</p>
<ol>
	<li dir="rtl" value="NaN">
		<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; در اين صفحه اطلاعات هويتي فرد از پايگاه داده نمايش داده مي شود و کاربر به شرح زير اقدام به تکميل اطلاعات ممنوع الخدمات مي نمايد:</span></span></li>
</ol>
<p dir="rtl" style="margin-right:5.85pt;">
	&nbsp;</p>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; <strong>&middot;</strong> <strong>شماره نامه وتاريخ نامه: </strong>مربوط به نامه ممنوعيت.</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	&nbsp;</p>
<ul>
	<li dir="rtl">
		<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; <strong>مرجع صدور:</strong> بر اساس مرجع صدور قيد شده در نامه ، يکي از موارد مندرج در فهرست انتخاب مي شود.</span></span></li>
</ul>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 1: براي رکوردهايي که علت &quot;م دار &quot; شدن آنها ، مفهوم نبودن رسم الخط در زمان صورتبرداری بوده است ، گزينه &nbsp; &quot;<strong>سازمان&quot;</strong> انتخاب مي شود.</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 2: چنانچه علت ممنوعيت يکي از گزينه هاي مربوط به تابعيت باشد گزينه مرجع صدور &quot;<strong>وزارت</strong> <strong>امور خارجه</strong>&quot; انتخاب مي &nbsp; شود.</span></span></p>
<p dir="rtl" style="margin-right:-1.25pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 3: در صورتي که مرجع صدور، در فهرست موجود نباشد گزينه &quot;<strong>مراجع غير مجاز</strong>&quot; انتخاب مي شود.</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	&nbsp;</p>
<ul>
	<li dir="rtl">
		<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; <strong>علت ممنوعيت :</strong> بر اساس علت قيد شده در نامه انتخاب مي گردد.</span></span></li>
</ul>
<p dir="rtl" style="margin-right:-1.25pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 1: در صورتيکه علت ممنوعيت درج شده در نامه، در فهرست موجود نباشد، گزينه &quot;<strong>ساير</strong>&quot; انتخاب مي شود. با انتخاب &nbsp; گزينه &quot;ساير&quot; کادري باز مي شود و علت ممنوعيت ذکر شده در نامه، در کادر مزبور وارد مي گردد.</span></span></p>
<p dir="rtl" style="margin-right:-1.25pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 2: اگر علتي در نامه مشخص نشده باشد گزينه &quot;<strong>علت نامعلوم</strong>&quot; انتخاب مي شود.</span></span></p>
<p dir="rtl" style="margin-right:-1.25pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 3: در مورد رکوردهاي نامفهوم در زمان صورتبرداري ،گزینه &quot;<strong>نا مفهوم بودن رسم الخط</strong>&quot; انتخاب مي گردد.</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	&nbsp;</p>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; <strong>&middot;</strong> <strong>تصاوير اسکن شده: </strong>در اين قسمت نوع تصوير براساس نامه مربوط از فهرست انتخاب شده و سپس الحاق مي گردد.</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 1: در اين بخش کليه تصاوير الحاق شده به این سند سجلی در ضمن عمليات میدانی آرشيو، (در صورت وجود) نمايش داده مي شود.کاربر مي بایست تصوير (يا تصاوير) مرتبط با علت ممنوعيت را انتخاب نمايد.</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 2: در صورت عدم وجود تصوير( يا تصاوير) مرتبط ،کاربر مي بایست با کليک کردن بر دکمه &quot;<span dir="ltr">Browse</span>&quot; تصوير جديد را به سند الحاق نماید.</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 3: امکان الحاق چند تصوير بصورت همزمان امکان پذير مي باشد.</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 4: براي رکوردهاي &quot;م دار&quot; که علت ممنوعيت آنها ناشي از مفهوم نبودن رسم الخط در هنگام صورتبرداري مي باشد،با انتخاب گزينه &quot;راهنمای صورتبرداري&quot; ،<a href="http://10.1.20.29/info/dcu/mamnoo/peyvaste1.pdf" target="_blank"> <u>پیوست 1</u></a><u> </u>پس از تکمیل، امضا، مهر و اسکن، الحاق گردد .</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 5 : چنانچه برای رکوردی شماره و تاریخ نامه مربوطه موجود بوده لیکن عين نامه در دسترس نباشد،<u><a href="http://10.1.20.29/info/dcu/mamnoo/peyvaste2.pdf" target="_blank"> پیوست 2 </a></u>پس از &nbsp; تکمیل، امضا، مهر و اسکن، الحاق گردد .</span></span></p>
<p dir="rtl" style="margin-right:5.85pt;">
	<span style="font-size:16px;"><span style="font-family: b nazanin;">&nbsp; تبصره 6: تصوير اسکن شده بايد از نوع <span dir="ltr">jpeg </span>&nbsp;با درجه وضوح <span dir="ltr">&nbsp;(resolution) </span>&nbsp;&nbsp;<span dir="ltr">dpi</span> 300 باشد.</span></span></p>
', NULL, N'1394/11/18', N'08:37     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(45 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'پیام خطا در استفاده از توکن تایید مدیر دفتر', N'<p>
	<span style="font-size:18px;"><span style="font-family: b lotus;">با سلام</span></span></p>
<p>
	<span style="font-size:18px;"><span style="font-family: b traffic;">قابل توجه همکاران محترم با توجه به بروزرسانی های اخیر سامانه خدمات شهروندی (<span style="font-family:times new roman,times,serif;">CCOS</span>) <span style="color:#ff0000;">پیام خطای استفاده از توکن مدیر دفتر</span> بازدارنده نبوده و همکاران میتوانند مانند گذشته نسبت به تایید درخواستها،اقدام نمایند همکاران سازمان در تلاش برای رفع این خطا میباشند.</span></span></p>
', NULL, N'1394/11/19', N'11:23     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(46 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'قطعی سامانه ها', N'<p>
	<span style="font-family:b yekan;">با سلام و روز بخیر</span></p>
<p>
	<span style="font-family:b yekan;">به اطلاع می رساند به منظور بازنگری و ساماندهی زیر ساخت مرکز داده سازمان، کلیه سامانه ها ا<span style="color:#ff0000;">ز ساعت 20:00</span> روز چهارشنبه <span style="color:#ff0000;">مورخه 94/11/21</span> لغایت پایان وقت اداری روز <span style="color:#ff0000;">جمعه 94/11/23 قطع</span> می باشد.</span></p>
', NULL, N'1394/11/20', N'11:30     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(47 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'مسدود شدن کد کابری کاربران غیر فعال در حوزه کارت هوشمند ملی', N'<p><span style="font-size:14px"><span style="font-family:b yekan">به اطلاع می رساند از تاریخ 94.11.25 کد کلیه کاربران کارت هوشمند که در این حوزه فعالیتی ندارند غیر فعال می گردد.&nbsp; کاربران جدید می توانند کد کاربری خود را از طریق سامانه CCOS توسط رئیس اداره دریافت نمایند.</span></span></p>
', NULL, N'1394/11/25', N'08:29     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(50 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), N'پیام همکار : مهدی اخلاص', N'<p>خوشبختانه امروز سامانه اصلا قطع نشد سرعتش هم خوب بود</p>
', NULL, N'1394/11/25', N'14:34     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(51 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), N'پیام همکار : مهدی اخلاص', N'<p>بعضی از کدهای کاربری از کار افتادن علتش چیه؟</p>
', NULL, N'1394/11/25', N'14:34     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(52 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), N'پیام همکار : مهدی اخلاص', N'<p>سلام خدمت همکارای عزیز</p>

<p>اگه سرعت سامانه کارت هوشمند کم بود یا به هر دلیلی قطع شد لطفا اطلاع رسانی کنید</p>
', NULL, N'1394/11/25', N'14:36     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(53 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(112 AS Numeric(18, 0)), N'پیام همکار : حسین  اسماعیلی', N'<p>با سلام . کاربرانی که عملا از سامانه خدمات شهروندی استفاده نمیکردند از 24/11/94 غیر فعال شدند.</p>
', NULL, N'1394/11/25', N'14:47     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(54 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p>باسلام وخسته نباشيد</p>

<p>آيا هنگام تحويل کارت ملي به متقاضيان رسيد تحويل چاپ شود يا خير</p>
', NULL, N'1394/11/26', N'07:48     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(55 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p>باسلام وخسته نباشيد</p>

<p>آيا هنگام تحويل کارت ملي به متقاضيان رسيد تحويل چاپ شود يا خير</p>
', NULL, N'1394/11/26', N'07:48     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(56 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'تکمیل چک لیست قطعی سامانه کارت هوشمند', N'<p><span style="font-size:14px"><span style="font-family:b yekan">قابل توجه روسای محترم و کارشناسان گرامی فناوری اطلاعات ادارات</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">با سلام و روز بخیر</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">لطفا فایل پیوست که حاوی&nbsp; <span style="color:rgb(0, 128, 0)">&quot;چک لیست قطعی سامانه کارت هوشمند ملی &quot;</span> می باشد در اختیار <span style="color:rgb(0, 128, 0)">همکاران فعال در&nbsp; ایستگاههای کارت هوشمند</span><span style="color:rgb(0, 128, 0)"> </span>در اداره <span style="color:rgb(0, 128, 0)">ثبت احوال و پست </span>قرار گیرد. سپس همکاران می بایست بمدت <span style="color:rgb(0, 128, 0)">دو هفته </span>از این تاریخ در <span style="color:rgb(0, 128, 0)">هنگام قطعی سامانه کارت هوشمند </span>نسبت به ثبت اطلاعات درخواستی اقدام و در نهایت <span style="color:rgb(0, 128, 0)">فرم های تکمیل شده به اداره کل ارسال</span> گردد.</span></span></p>
', N'CheckList.pdf', N'1394/11/26', N'11:59     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(57 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:14px">سلام </span></p>

<p><span style="font-size:14px">بعد از ظهرها معمولا اداره پست قطعی داره .. به سازمان هم میگیم می فرمایند قطعی از طرف ما نیست به نظر شما مشکل از کجاست ؟ از خود پستی هاست؟</span></p>

<p>&nbsp;</p>
', NULL, N'1394/11/27', N'13:41     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(58 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:16px">بعضی همکاران بعنوان جایگزین کد کاربری براشون تعریف شده اگه غیر فعال نشوند ممنون میشیم</span></p>

<p>&nbsp;
<p>&nbsp;</p>
</p>
', NULL, N'1394/11/27', N'13:43     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(59 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<p>با سلام.سیستم بسیار کند میباشد.قطع وصلی مکرر دارد.</p>
', NULL, N'1394/11/28', N'09:45     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(60 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<p>با سلام.سیستم بسیار کند میباشد.قطع وصلی مکرر دارد.</p>
', NULL, N'1394/11/28', N'09:45     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(61 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'پیام همکار : مهدی اخلاص', N'<p>با سلام خدمت همکاران</p>

<p>علت قطعی از طرف سازمان پی گیری شد تا دقایقی دیگر سامانه وصل خواهد شد.</p>
', NULL, N'1394/11/28', N'09:49     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(63 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p>باسلام</p>

<p>&nbsp;در صورت کندي سيستم ( در حد قطعي) در چک ليست قطعي سامانه منظور شود</p>
', NULL, N'1394/11/28', N'12:29     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(65 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'پیام همکار : مهدی اخلاص', N'<p>در جواب آقای برازنده:</p>

<p>بله بهتر است در صورت کندي سيستم ( در حد قطعي) در چک ليست قطعي سامانه منظور شود.</p>
', NULL, N'1394/11/28', N'12:31     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(66 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p>باسلام</p>

<p>آيا اطلاع رساني از دريافت کارت هوشمند ملي ( آماده تحويل ) براي متقاضيان بوسيله پيامک ازطريق استان مقدور است .</p>
', NULL, N'1394/12/01', N'13:14     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(67 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'پیام همکار : مهدی اخلاص', N'<p>در حال حاضر اطلاع رسانی جهت کارتهای هوشمند ملی آماده تحویل تنها از طریق سازمان ثبت احوال کل کشور امکانپذیر است.</p>
', NULL, N'1394/12/01', N'13:23     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(68 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p>آیا</p>
', NULL, N'1394/12/04', N'14:02     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(69 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p>آیا</p>
', NULL, N'1394/12/04', N'14:02     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(70 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p>باسلام</p>

<p>در حال حاضر تنها وسيله کنترل افراد ممنوع الخدمات چک کردن مشخصات آنان در سيستم جمع آوري اطلاعات واستعلام الکترونيکي ميباشد. آيا امکان عدم ثبت نام&nbsp; اوليه کارت هوشمند ملي براي اين افراد براي جلوگيري از سوءاستفاده هاي احتمالي در سيستم وجود ندارد.</p>
', NULL, N'1394/12/05', N'08:51     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(71 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p>باسلام مجدد وخسته نباشيد</p>

<p>چند ماهييست که پاکت هاي ارسالي (حاوي کارت هوشمند ملي صادره شده )از سوي سازمان با کيفيت بسيار پايين دريافت ميشود( اکثر پاکت ها هنگام وصول پاره شده ) بطوريکه امکان مفقود شدن کارتها در ادارات پست بسيار زياد است خواهشمند است مراتب به سازمان اعلام تا نسبت به ارسال پاکت ها در وضعيت مطلوب تري اقدام شود.</p>
', NULL, N'1394/12/05', N'08:58     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(72 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:18px"><span style="font-family:b mitra"><span style="color:rgb(0, 0, 128)">سلام علیـــــــــکم</span></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b mitra"><span style="color:rgb(0, 0, 128)">1- پیامهای فروم دیر به دیر نمایش داده میشه </span></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b mitra"><span style="color:rgb(0, 0, 128)">2- هویتی رئیس اداره از دیروز مشکل داره ازدواج های خاص رو ثبت نمی کنه لطفا بررسی بشه </span></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b mitra"><span style="color:rgb(0, 0, 128)">3- سازمان میخواد انتخاب تصویر بصورت فایل رو برداره تکلیف فقدان و تعویضی های هوشمند چی میشه ؟ با توجه به اینکه تو اداره عکاسی نداریم </span></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b mitra"><span style="color:rgb(0, 0, 128)">4- پرداخت تعرفه اینترنتی شده دیگه آخر ماه فیش بانکی نداریم با کارکرد بفرستیم . </span></span></span></p>
', NULL, N'1394/12/11', N'08:52     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(73 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'به روز رسانی نرم افزار ارسال ضمائم', N'<p><span style="font-size:20px"><span style="font-family:b nazanin">با توجه به ارائه نسخه جدید نرم افزار ارسال ضمائم خواهشمند است نسبت به بروز رسانی آن اقدام نمایند.</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b nazanin">ضمنا نسخه جدید نرم افزار در <a href="ftp://ftp.hm">ftp 111</a>&nbsp; و همچنین در ضمیمه جهت دانلود قرار داده شده است.</span></span></p>
', N'new version 941012.rar', N'1394/12/12', N'11:47     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(74 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'پیام همکار : قاسم اعظمی', N'<p>با سلام</p>

<p>همکار محترم سیستم صدور شناسنامه بزرگسال کند میباشدلطفاً بررسی بفرمائید.</p>
', NULL, N'1394/12/13', N'08:51     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(75 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'پیام همکار : قاسم اعظمی', N'<p>با سلام</p>

<p>همکار محترم سیستم صدور شناسنامه بزرگسال کند میباشدلطفاً بررسی بفرمائید.</p>
', NULL, N'1394/12/13', N'08:51     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(76 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'تکمیل چک لیست قطعی کارت هوشمند ملی ', N'<p><span style="color:#800000"><span style="font-size:14px"><span style="font-family:b yekan">قابل توجه روسای محترم و کارشناسان گرامی فناوری اطلاعات ادارات</span></span></span></p>

<p><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">با سلام و روز بخیر</span></span></span></p>

<p><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">لطفا فایل پیوست که حاوی&nbsp;</span></span></span><span style="color:#008000"><span style="font-size:14px"><span style="font-family:b yekan"> &quot;چک لیست قطعی سامانه کارت هوشمند ملی &quot;</span></span></span><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan"> می باشد در اختیار </span></span></span><span style="color:#008000"><span style="font-size:14px"><span style="font-family:b yekan">همکاران فعال در&nbsp; ایستگاههای کارت هوشمند </span></span></span><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">در </span></span></span><span style="color:#B22222"><span style="font-size:14px"><span style="font-family:b yekan">اداره ثبت احوال</span></span></span><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan"> و</span></span></span><span style="color:#B22222"><span style="font-size:14px"><span style="font-family:b yekan"> پست </span></span></span><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">قرار گیرد. سپس همکاران می بایست&nbsp; </span></span></span><span style="color:#B22222"><span style="font-size:14px"><span style="font-family:b yekan">روزانه </span></span></span><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">در هنگام قطعی سامانه کارت هوشمند نسبت به </span></span></span><span style="color:#008000"><span style="font-size:14px"><span style="font-family:b yekan">ثبت اطلاعات درخواستی</span></span></span><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan"> اقدام و </span></span></span><span style="color:#B22222"><span style="font-size:14px"><span style="font-family:b yekan">در پایان هر هفته</span></span></span><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan"> فرم های تکمیل شده را&nbsp;&nbsp;</span></span></span><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan"> به واحد فناوری اطلاعات اداره کل</span></span></span><span style="color:#006400"><span style="font-size:14px"><span style="font-family:b yekan"> ارسال نمایند</span></span></span><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">.</span></span></span></p>
', N'چک لیست قطعی سامانه کارت هوشمند.xlsx', N'1394/12/15', N'13:42     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(77 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'نظارت و کنترل کیفی', N'<p><span style="font-size:18px"><strong><span style="font-family:b traffic">قابل توجه روسای محترم ادارات</span></strong></span></p>

<p><span style="font-size:18px"><span style="font-family:b traffic">با عنایت به بازگشت برخی از مجلدات خواهشمد است ادارات زیر تا پایان وقت اداری 1394/12/16 نسبت به بازبینی مجدد این اسناد در سامانه کنترل کیفی اقدام نمایند.</span></span></p>

<p>1<span style="font-size:18px"><span style="font-family:b traffic">-اسدآباد مجلد 613</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b traffic">2-همدان مجلدهای 1229 و 2374</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b traffic">3-بهار مجلد 1058</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b traffic">4-فامنین مجلد 408</span></span></p>
', NULL, N'1394/12/16', N'10:08     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(78 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p><span style="color:#800000">باسلام</span></p>

<p><span style="color:#006400"><strong>خواهشمند است در صورت امکان باتوجه به ضرورت سوال هاي مطرح شده از سوي همکاران نسبت به پاسخ آنها در همان روز اقدام گردد </strong></span></p>
', NULL, N'1394/12/16', N'10:13     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(81 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="color:#000080"><span style="font-size:14px">نکته : </span></span></p>

<p><span style="font-size:18px"><span style="font-family:b nazanin"><span style="color:rgb(0, 0, 128)">فرمت تاریخ سیستم mm/dd/yyyy به این صورت است . اگر این قالب را تغییر دهید ممکن است برخی قابلیت های ccos که نیاز به ورود تاریخ دارد غیر قابل استفاده گردد. یا اینکه مجبور به وارد کردن تاریخ با قالب جدید باشید ( بازیابی کد رهگیری و خدمات پس از تحویل )</span></span></span></p>
', NULL, N'1394/12/17', N'12:28     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(82 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(29 AS Numeric(18, 0)), N'پیام همکار : حسین محمدی', N'<p>با سلام درخصوص اخذ هزینه تفاوت در خواست کارت هوشمند ملی ناشی از فقدان کارت ملی کاغذی اظهار نظر و یک پارچه سازی گردد</p>
', NULL, N'1394/12/17', N'14:53     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(83 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:16px"><span style="color:rgb(0, 0, 128)">سلام </span></span></p>

<p><span style="font-size:16px"><span style="color:rgb(0, 0, 128)">اغلب بانکها متقاضیانی را که کارت ملی ندارند برای گرفتن سری سریال کارت به ثبت احوال دلالت می دهند .&nbsp; با توجه به مقررات جاری که بایستی اصل کارت ملی را از متقاضی دریافت کنند و محدودیت ثبت احوال در ارائه سری سریال شناسنامه و کارت لطفا ترتیبی اتخاذ گردد از طریق مکاتبه با سرپرستی بانکها از اعزام بی دلیل افراد به ادارات ممانعت نمایند.</span></span></p>
', NULL, N'1394/12/20', N'10:23     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(84 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:12px">سلام لطفا&nbsp; اس ام اس بیشتر بزنن کارتها داره جمع میشه سرهم </span></p>
', NULL, N'1394/12/20', N'12:34     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(85 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p>باسلام</p>

<p><span style="font-size:16px"><span style="color:rgb(128, 0, 0)">خواهشمند است در صورت امکان حالت انتظار( استن باي) سيستم کارت هوشمند ملي را در افزايش داده بطوريکه بعد از گذشت کمتر از يک دقيقه عدم فعاليت بايد از سيستم خارج ودوباره وارد سيستم شد</span>.</span></p>
', NULL, N'1394/12/22', N'10:23     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(86 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p><span style="color:#00FF00">باسلام</span></p>

<p><strong><span style="color:#006400"><span style="font-size:16px">سال نو بر تمامي همکاران مبارک باشد سالي توام با سربلندي وشاد کامي را براي تمامي همکاران عزيز از ايزد منان خواستارم</span></span></strong>.</p>
', NULL, N'1394/12/24', N'10:53     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(87 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'استفاده از اتوماسیون اداری تحت وب  در نمایندگی ها', N'<p><span style="font-family:b yekan">با سلام و روز بخیر</span></p>

<p><span style="font-family:b yekan">با توجه فعال شدن بستر<span style="color:#006400"> اتوماسیون اداری تحت وب&nbsp;</span> در<span style="color:#006400"> نمایندگی های ثبت احوال</span> شهرستانها، مسئولین محترم این نمایندگی ها از ابتدای سال آتی می توانند&nbsp; نسبت به <span style="color:#006400">ثبت نامه های وارده و صادره&nbsp; </span>و همچنین ارجاع نامه های خود&nbsp; از طریق این بستر اقدام نمایند. </span></p>

<p><span style="font-family:b yekan">لطفاً در صورت هر گونه سوال احتمالی با واحد فناوری اطلاعات تماس حاصل فرمائید.</span></p>

<p><span style="font-family:b yekan">نمایندگی ها شامل: <span style="color:#0000FF">1- فیروزان 2- دمق&nbsp; 3-قروه درجزین&nbsp; 4-شیرین سو&nbsp; 5- گل تپه&nbsp;&nbsp; 6-صالح آباد&nbsp; 7-لالجین</span></span></p>
', NULL, N'1394/12/27', N'10:11     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(88 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'پیام همکار : محمدرضا صاحبی', N'<p>سال نو مبارک . سالی توام با سلامتی و موفقیت برای تمامی همکاران محترم آرزومندم.</p>
', NULL, N'1394/12/29', N'10:41     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(89 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(29 AS Numeric(18, 0)), N'پیام همکار : حسین محمدی', N'<p>سلام ، سال 1395 را به تمامی همکاران تبریک عرض نموده و امیدوار سالی پر از موفقیت و پیشرفت باشد. انشاءالله</p>
', NULL, N'1395/01/07', N'07:16     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(90 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'پیام همکار : حسین  اسماعیلی', N'<p><span style="color:#008000"><span style="font-size:18px"><span style="font-family:b nazanin"><strong>&nbsp;در شکفتن نوروز برای تمامی همکاران و دوستان عزیزم سالی سرشار از شادی و سرسبزی، اندیشه پویا، سلامتی و برخورداری از نعمت های الهی را آرزومندم. </strong></span></span></span></p>
', NULL, N'1395/01/09', N'08:34     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(91 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'گزارش فعالیت همکاران', N'<p><span style="font-size:14px"><span style="font-family:b titr">از آنجائیکه فعالیت همکاران در سامانه&nbsp; info مورد توجه و نظر مدیریت محترم بوده و مقرر است مورد تقدیر ایشان قرار گیرند،لذا آخرین وضعیت فعالیت همکاران به شرح ذیل اعلام میگردد.</span></span></p>

<p dir="RTL">&nbsp;</p>

<p dir="RTL"><span style="font-family:b titr">گزارش پیامهای ارسالی توسط کاربران</span></p>

<p dir="RTL">&nbsp;</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" dir="ltr" style="height:441px; width:381px">
	<tbody>
		<tr>
			<td style="background-color: rgb(238, 238, 238);">
			<p dir="RTL" style="text-align:center"><span style="font-family:b titr"><strong>تعداد پیام ارسال شده</strong></span></p>
			</td>
			<td style="background-color: rgb(238, 238, 238);">
			<p dir="RTL" style="text-align:center"><span style="font-family:b titr"><strong>نام خانوادگی کاربر</strong></span></p>
			</td>
			<td style="background-color: rgb(238, 238, 238);">
			<p dir="RTL" style="text-align:center"><span style="font-family:b titr"><strong>نام کاربر</strong></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">9</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">فیض برازنده</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">بهزاد</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">8</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">برزگر زندی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">مرتضی</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong>6</strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">اخلاص</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">مهدی</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">2</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">اعظمی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">قاسم</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">2</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">محمدی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">حسین</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">2</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">طائفی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">علی</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">2</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">اسماعیلی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">حسین</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">1</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">صاحبی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">محمدرضا</span></span></p>
			</td>
		</tr>
	</tbody>
</table>

<div style="clear:both;">&nbsp;</div>

<p dir="RTL">&nbsp;</p>

<p dir="RTL"><span style="font-family:b titr">گزارش تعداد پیامهای رویت شده</span></p>

<table align="center" border="1" cellpadding="0" cellspacing="0" dir="ltr" style="height:1031px; width:395px">
	<tbody>
		<tr>
			<td style="background-color: rgb(238, 238, 238);">
			<p dir="RTL" style="text-align:center"><span style="font-family:b titr"><strong>تعداد پیامهای رویت شده</strong></span></p>
			</td>
			<td style="background-color: rgb(238, 238, 238);">
			<p dir="RTL" style="text-align:center"><span style="font-family:b titr"><strong>نام خانوادگی</strong></span></p>
			</td>
			<td style="background-color: rgb(238, 238, 238);">
			<p dir="RTL" style="text-align:center"><span style="font-family:b titr"><strong>نام کاربر</strong></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">23</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">بختیاری</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">بابک</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">8</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">طائفی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">علی</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">8</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">صبوری مقدم</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">علیرضا</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">7</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">فیض برازنده</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">بهزاد</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">6</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">بیگدلی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">محمد</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">5</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">اصلانی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">حسین</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">4</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">اعظمی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">قاسم</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">4</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">خزلی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">محمود</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">3</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">عبدی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">محمود</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">3</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">تیموری</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">محمد</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">3</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">فریادرس</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">علی</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">2</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">صاحبی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">محمدرضا</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">2</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">محمدی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">احمد</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">2</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">برزگر زندی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">مرتضی</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">1</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">زیوری یار</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">فرامرز</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">1</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">محمدی</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">حسین</span></span></p>
			</td>
		</tr>
		<tr>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus"><strong><span dir="LTR">1</span></strong></span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">گلگون رخ</span></span></p>
			</td>
			<td>
			<p dir="RTL" style="text-align:center"><span style="font-size:16px"><span style="font-family:b lotus">عباس</span></span></p>
			</td>
		</tr>
	</tbody>
</table>

<div style="clear:both;">&nbsp;</div>

<p dir="RTL">&nbsp;</p>
', NULL, N'1395/01/09', N'10:13     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(92 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>باسلام سال نورابه همه همکاران محترم تبریک می گویم</p>
', NULL, N'1395/01/10', N'13:07     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(93 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>باسلام وخسته نباشید به همه همکاران زحمت کش درقسمت کارت هوشمند مخصوصا ازآقایان ستاد اخلاصی وپناهی که همیشه مارا یاری میکنند حداقل روزانه چندین بار ازطریق تماس تلفنی کمکمان میکنند من خودم شخضآ تشکر میکنم</p>
', NULL, N'1395/01/11', N'09:57     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(96 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="color:#000080"><span style="font-size:16px">با سلام و عرض تبریک به مناسبت آغاز سال جدید ...&nbsp; سامانه 3S یه خورده کند شده یه آمار بخواهی بگیری کلی زمان میگیره تازه پیغام میده مدت زمان پاسخگوئی معتبر نمی باشد. </span></span></p>
', NULL, N'1395/01/16', N'09:20     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(97 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'پیام همکار : بهزاد فیض برازنده', N'<p><span style="font-size:22px">باسلام وروز بخیر</span></p>

<p><span style="font-size:14px">&nbsp;</span><span style="font-size:16px">با توجه به اینکه زمان صدور کارت هوشمند ملی حدودا&quot; دو ماه&nbsp;میباشد لذا خواهشمند است جهت جلوگیری از رفت وآمد بیهوده ومکرر متقاضیان اطلاع رسانی شود ( زمان اعلام شده به متقاضیان از سوی دفاتر پیشخوان 15 الی یک ماهه میباشد)</span></p>
', NULL, N'1395/01/16', N'10:18     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(98 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'پیام همکار : حسین اصلانی', N'<p><span style="font-size:20px"><span style="font-family:b tir">باسلام و عرض خسته نباشید خدمت تمامی همکاران زخمت کش ثبت احوال .</span></span></p>

<p><span style="font-size:20px">سال نو مبارک .</span></p>

<p><span style="font-size:24px"><span style="color:rgb(0, 255, 0)"><strong>اللهم عجل لولیک الفرج (انشاا...)</strong></span></span></p>
', NULL, N'1395/01/16', N'12:46     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(99 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'پیام همکار : حسین اصلانی', N'<p><span style="font-size:20px"><span style="font-family:b tir">باسلام و عرض خسته نباشید خدمت تمامی همکاران زخمت کش ثبت احوال .</span></span></p>

<p><span style="font-size:20px">سال نو مبارک .</span></p>

<p><span style="font-size:24px"><span style="color:rgb(0, 255, 0)"><strong>اللهم عجل لولیک الفرج (انشاا...)</strong></span></span></p>
', NULL, N'1395/01/16', N'12:47     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(100 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'پیام همکار : حسین اصلانی', N'<p><span style="color:#FF0000"><span style="font-size:36px">سال نو مبارک</span></span></p>
', NULL, N'1395/01/16', N'12:58     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(101 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'پیام همکار : حسین اصلانی', N'<p><span style="color:#FF0000"><span style="font-size:36px">سال نو مبارک</span></span></p>
', NULL, N'1395/01/16', N'12:58     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(102 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h1>سلام ضمن عرض تبریک سال نووبهار شگفت انگیزبه همه همکاران عزیزامیدوارم که سال خوب وسرشار ازسلامتی برای شما وخانواده های محترمتان باشد</h1>
', NULL, N'1395/01/17', N'09:37     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(103 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h1>سلام ضمن عرض تبریک سال نووبهار شگفت انگیزبه همه همکاران عزیزامیدوارم که سال خوب وسرشار ازسلامتی برای شما وخانواده های محترمتان باشدعلی طائفی</h1>
', NULL, N'1395/01/17', N'09:38     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(104 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h2>سلام ضمن عرض خسته نباشید چنانچه ممکن است مجددا درصفحه پیش ثبت نام تاکید گردد&nbsp; جهت روئیت کلیه &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; دفاترپیشخوان وکافینتهاکه با&nbsp; شناسنامه فاقدعکس ثبت نام ننمایند 1395/01/17علی طائفی</h2>
', NULL, N'1395/01/17', N'09:52     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(105 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'پیام همکار : علی طیبی', N'<p><span style="font-size:18px"><span style="font-family:b yagut">سلام سال نو مبارک، انشاءالله سالی پر از موفقیت و سرشار از خیر و برکت داشته باشید.</span></span></p>

<p>&nbsp;</p>
', NULL, N'1395/01/17', N'10:12     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(106 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:16px"><span style="color:rgb(0, 0, 128)">با سلام لطفا پیشنهاد گردد در سامانه 3S&nbsp; مانند سرستون های اثر انگشت ، تصویر چهره و اسکن مدارک گزینه ای هم دال بر پرداخت هزینه الکترونیکی تعبیه گردد .</span></span></p>
', NULL, N'1395/01/18', N'07:54     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(107 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h2>سلام ضمن آرزوی سلامتی وخوشبختی بهارشگفت انگیزرابه کلیه همکاران عزیزتبرک عرض منمایم 1396/01/18علی طائفی</h2>
', NULL, N'1395/01/18', N'14:08     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(108 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:14px"><span style="color:rgb(0, 0, 128)">نکته : </span></span></p>

<p><span style="font-size:14px"><span style="color:rgb(0, 0, 128)">با توجه به پرداخت اینترنتی هزینه کارت هوشمند ملی بعضی دفاتر اقدام به اخذ رمز اینترنتی کارت بانکی از متقاضیان می نمایند . با توجه به خطرات احتمالی بایستی دفاتر از این اقدام منع شوند .</span></span></p>
', NULL, N'1395/01/21', N'12:55     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(109 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:14px"><span style="color:rgb(0, 0, 128)">واژه &quot; المثنی&quot; پشت کارتهای هوشمند ملی صادره ناشی از فقدان&nbsp; درج نمی گردد . لطفا پیگیری شود آیا ایراد سیستمی است یا نیازی به آن نبوده و قرار نیست زده شود؟</span></span></p>
', NULL, N'1395/01/21', N'13:47     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(110 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'ارسال چک لیست قطعی سامانه کارت هوشمند ملی', N'<p dir="RTL"><span style="font-size:14px"><span style="font-family:b yekan"><span style="color:rgb(0, 100, 0)">قابل توجه کارشناسان محترم فناوری اطلاعات ادارات</span></span></span></p>

<p dir="RTL"><span style="color:#000000"><span style="font-size:14px"><span style="font-family:b yekan">با سلام و روز بخیر</span></span></span></p>

<p dir="RTL"><span style="font-size:14px"><span style="font-family:b yekan">پیرو اطلاعیه مورخه 1394/12/15 لطفا <span style="color:rgb(178, 34, 34)">چک لیست قطعی دو هفته اخیر</span>سامانه کارت هوشمند ملی در<span style="color:rgb(165, 42, 42)"> اداره ثبت احوال و اداره &nbsp;پست </span>&nbsp;را به واحد فناوری اطلاعات اداره کل ارسال نمایید.</span></span></p>

<p dir="RTL"><span style="font-family:b yekan"><span style="color:#000033"><span style="font-size:14px">محل ارسال : پوشه&nbsp; لیست قطعی کارت هوشمند در &nbsp;&nbsp;<span dir="LTR">ftp(111)-&gt;IT</span></span></span></span></p>
', NULL, N'1395/01/22', N'08:23     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(112 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'پیام همکار : احمد محمدی', N'<p>gggdgfdg</p>
', NULL, N'1395/01/22', N'13:34     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(113 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'پیام همکار : محمدرضا صاحبی', N'<p>با سلام . آیا رویه رفع ممنوعیت از خدمات تغییر یافته است؟جهت اعمال تغییرات رفع خدشه سیستم خطای <span style="color:#FF0000">&laquo;ارائه خدمت به صاحب سند ممکن نمي باشد!&raquo;</span> می دهد در صورتیکه قبلا این اخطار نمایش داده نمی شد. با تشکر.</p>
', NULL, N'1395/01/23', N'07:53     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(114 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'پیام همکار : علی فریادرس', N'<p>با سلام</p>

<p>خواهشمند است باتوخه به افزایش تعداد کارتهای موجود در اداره اطلاع رسانی بیشتری درخصوص تحویل کاتهای آماده تحویل صورت گیرد.</p>
', NULL, N'1395/01/24', N'12:49     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(115 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'پیام همکار : علی فریادرس', N'<p>با سلام</p>

<p>خواهشمند است باتوخه به افزایش تعداد کارتهای موجود در اداره اطلاع رسانی بیشتری درخصوص تحویل کاتهای آماده تحویل صورت گیرد.</p>
', NULL, N'1395/01/24', N'12:50     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(116 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'اخلال در سامانه ABC', N'<p><span style="font-size:14px"><span style="font-family:b yekan">قابل توجه کلیه همکاران محترم</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">با سلام و روز بخیر </span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">به اطلاع می رساند <span style="color:#800000">سامانه ABC </span>از ساعت <span style="color:#B22222">12:00 </span>امروز (1395.01.25)با <span style="color:#B22222">اخلال </span>مواجه خواهد بود. لذا لازم است تمهیدات لازم جهت استفاده از این سامانه تا قبل از ساعت 12 اندیشیده شود.</span></span></p>
', NULL, N'1395/01/25', N'10:27     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(117 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="color:#000080"><span style="font-size:14px">DHCP SERVER امروز مشکل نداره ؟ </span></span></p>
', NULL, N'1395/01/26', N'08:20     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(118 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h2>باسلام لطفا در سرستون صفحه پیش ثبت نام جهت اطلاع وآگاهی کلیه عموم ودفاتر پیشخوان کافینت هااعلام گردد باشناسنامه بدون عکس ویاکپی یاهرمدرک دیگری پیش ثبت نام ننمایند این موارد قبلاتکرارومشکلاتی بوجود آورده استباتشکرعلی طائفی1395/01/28</h2>
', NULL, N'1395/01/28', N'08:47     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(119 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h2>باسلام لطفا در سرستون صفحه پیش ثبت نام جهت اطلاع وآگاهی کلیه عموم ودفاتر پیشخوان کافینت هااعلام گردد باشناسنامه بدون عکس ویاکپی یاهرمدرک دیگری پیش ثبت نام ننمایند این موارد قبلاتکرارومشکلاتی بوجود آورده است باتشکرعلی طائفی1395/01/28</h2>
', NULL, N'1395/01/28', N'08:48     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(120 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'پیام همکار : محمد تیموری', N'<h1>همکار گرامی جناب آقای تیموری درگذشت پدر گرامیتان رابه شما وخانواده محترم تسلیت عرض می نمائیم.</h1>

<p>&nbsp;</p>

<p><span style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="font-size:20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; همکاران اداره ثبت احوال قلقلرود</span></p>
', NULL, N'1395/01/30', N'08:35     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(121 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'پیام همکار : محمد تیموری', N'<h1>همکار گرامی جناب آقای تیموری درگذشت پدر گرامیتان رابه شما وخانواده محترم تسلیت عرض می نمائیم.</h1>

<p>&nbsp;</p>

<p><span style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="font-size:20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; همکاران اداره ثبت احوال قلقلرود</span></p>
', NULL, N'1395/01/30', N'08:35     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(122 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'پیام همکار : احمد محمدی', N'<p><span style="font-size:22px">همکار گرامی جناب آقای محمد تیموری درگذشت پدرگرامیتان رابه شما وخانواده محترم&nbsp; تسلیت عرض می نمائیم.</span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="font-size:20px">ازطرف همکاران </span></p>
', NULL, N'1395/01/30', N'14:46     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(123 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'پیام همکار : احمد محمدی', N'<p><span style="font-size:22px">همکار گرامی جناب آقای محمد تیموری درگذشت پدرگرامیتان رابه شما وخانواده محترم&nbsp; تسلیت عرض می نمائیم.</span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="font-size:20px">ازطرف همکاران اداره ثبت احوال قلقلرود</span></p>
', NULL, N'1395/01/30', N'14:47     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(124 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'پیام همکار : محمد تیموری', N'<p><span style="font-size:26px">همکار گرامی جناب آقای محمد تیموری درگذشت پدرگرامیتان راتسلیت عرض مینمائیم&nbsp; </span></p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="font-size:26px">ازطرف همکاران اداره ثبت احوال قلقلرود</span></p>
', NULL, N'1395/01/30', N'14:56     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(125 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'پیام همکار : محمد تیموری', N'<p><span style="font-size:26px"><span style="font-family:georgia,serif"><strong><sub>همکار گرامی جناب آقای تیموری</sub></strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:georgia,serif"><strong>درگذشت پدر گرامیتان رابه شما تسلیت عرض می نمائیم </strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:georgia,serif"><strong>&nbsp; ازطرف همکاران اداره ثبت احوال قلقلرود</strong></span></span></p>
', NULL, N'1395/01/31', N'08:03     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(126 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'پیام همکار : محمد تیموری', N'<p><span style="font-size:26px"><span style="font-family:georgia,serif"><strong><sub>همکار گرامی جناب آقای تیموری</sub></strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:georgia,serif"><strong>درگذشت پدر گرامیتان رابه شما تسلیت عرض می نمائیم </strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:georgia,serif"><strong>&nbsp; ازطرف همکاران اداره ثبت احوال قلقلرود</strong></span></span></p>
', NULL, N'1395/01/31', N'08:03     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(127 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'پیام همکار : محمد تیموری', N'<p><span style="font-size:26px"><span style="font-family:georgia,serif"><strong><sub>همکار گرامی جناب آقای تیموری</sub></strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:georgia,serif"><strong>درگذشت پدر گرامیتان رابه شما تسلیت عرض می نمائیم </strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:georgia,serif"><strong>&nbsp; ازطرف همکاران اداره ثبت احوال قلقلرود</strong></span></span></p>
', NULL, N'1395/01/31', N'08:04     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(128 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'تغییر کاربر دبیرخانه استان', N'<p><span style="font-size:16px"><span style="font-family:b yekan">قابل توجه&nbsp; کلیه همکاران محترم&nbsp; </span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan">با سلام و روز بخیر<br />
با توجه به<span style="color:rgb(178, 34, 34)"> انتصاب آقای </span><span style="color:#006400">حامد خلیلی</span><span style="color:rgb(178, 34, 34)"> به سمت مسئول دبیر خانه استان</span>، لطفا از این پس کلیه مکاتبات ارسالی از طریق اتوماسیون اداری تحت وب به دبیرخانه استان، بجای خانم رضائی <span style="color:rgb(178, 34, 34)">به آقای </span><span style="color:#006400">حامد خلیلی</span><span style="color:rgb(178, 34, 34)"> ارجاع</span> داده شود.</span></span></p>
', NULL, N'1395/01/31', N'11:54     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(129 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'پیام همکار : علی فریادرس', N'<p>با سلام</p>

<p>خواهشمند است در صورت امکان&nbsp; باتوجه به کندی وقطعی مکرر در سامانه کارت هوشمند ملی در روز های اخیر اقدامی نسبت به رفع آن صورت گیرد</p>
', NULL, N'1395/02/01', N'09:58     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(130 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:14px"><span style="color:rgb(0, 0, 128)">سازمان کارتهای هوشمند رو به ترتیب تاریخ ثبت نام، صادر نمیکنه یه کارت 20 روزه میاد یه کارت 4 ماهه . از جایی که به مردم تاریخ دو ماهه میگیم دو تا مشکل داریم 1- پاسخگویی به مردم که کارت شون هنوز نیومده&nbsp; 2- چینش کارتها در مخزن </span></span></p>
', NULL, N'1395/02/04', N'11:00     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(131 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'پیام همکار : قاسم اعظمی', N'<p><span style="font-family:b yagut"><span style="font-size:16px">همکارگرامی جناب آقای تیموری </span></span></p>

<p><span style="font-family:b yagut"><span style="font-size:16px">درگذشت پدر گرامی حضرتعالی را به شما و بازماندگان تسلیت عرض مینمایم</span></span></p>

<p><span style="font-family:b yagut"><span style="font-size:16px">قاسم اعظمی</span></span></p>
', NULL, N'1395/02/05', N'08:43     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(132 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:14px"><span style="color:rgb(0, 0, 128)">درجواب جناب آقای صاحبی </span></span></p>

<p><span style="font-size:14px"><span style="color:rgb(0, 0, 128)">با سلام درج و رفع ممنوع الخدمات از طریق یوزر رئیس امکان پذیر است.</span></span></p>
', NULL, N'1395/02/05', N'11:47     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(133 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h2>همکارگرامی جناب آقای تیموری درگذشت پدرگرامیتان رابه شما وخانواده محترم تسلیت عرض مینمائیم ازطرف همکاران اداره ثبت احوال اسدآباد&nbsp;</h2>

<p>1395/02/06</p>

<p>پیام از علی طائفی</p>
', NULL, N'1395/02/06', N'12:14     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(134 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(25 AS Numeric(18, 0)), N'پیام همکار : برزو حسنی تبار', N'<p>لطفا ترتیبی اتخاذ گردد که در سامانه ای بی سی زمانی که یک خانمی که قصد ازدواج دارند اما شناسنامه اش زیر 15 سال است و نامه از محضر جهت تعویض دارند در هنگام ثبت درخواست ، گزینه دفترخانه را انتخاب نموده در لحظه واکشی عکس به حالت گزینه حکم رشد تغییر نکند وهمچنین در مواردی که به هر دلیل درخواست برگشت داده شده ،جهت رفع نقص اقدام می شود در موارد زیر به حالت اصالتا بر نگردد. مانند&nbsp; زمانی که ولی یاقیم یا گزینه سایر راانتخاب نموده ، در هنگام برگشتی جهت اصلاح به اصالتا تغییر نکند وثابت بماند .ممنون.حسنی تبار</p>
', NULL, N'1395/02/06', N'13:40     ', 0)
GO
print 'Processed 100 total records'
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(135 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(25 AS Numeric(18, 0)), N'پیام همکار : برزو حسنی تبار', N'<p>لطفا ترتیبی اتخاذ گردد که در سامانه ای بی سی زمانی که یک خانمی که قصد ازدواج دارند اما شناسنامه اش زیر 15 سال است و نامه از محضر جهت تعویض دارند در هنگام ثبت درخواست ، گزینه دفترخانه را انتخاب نموده در لحظه واکشی عکس به حالت گزینه حکم رشد تغییر نکند وهمچنین در مواردی که به هر دلیل درخواست برگشت داده شده ،جهت رفع نقص اقدام می شود در موارد زیر به حالت اصالتا بر نگردد. مانند&nbsp; زمانی که ولی یاقیم یا گزینه سایر راانتخاب نموده ، در هنگام برگشتی جهت اصلاح به اصالتا تغییر نکند وثابت بماند .ممنون.حسنی تبار</p>
', NULL, N'1395/02/06', N'13:41     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(136 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h2>ضمن سلام وخسته نباشید لطفا برسی واعلام گرددچرا درسیستم پیش ثبت نام متولدین فروردین 1380 که پانزده سال تمام شدندسیستم اجازه ثبت نام نمدهد</h2>

<p>1395/02/07</p>

<p>پیام از علی طائفی</p>

<p>&nbsp;</p>
', NULL, N'1395/02/07', N'09:12     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(137 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'پیام همکار : احمد محمدی', N'<p><span style="font-size:20px">باسلام وخسته نباشید:تقریبا 14روزی است که هیچ کارت هوشمندی&nbsp; دریافت نشده لطفاپیگیر باشید چرا کارت ملی صادر نمی شود.باتشکراحمد محمدی</span></p>
', NULL, N'1395/02/09', N'08:44     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(138 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>همکار گرامی جناب آقای تیموری درگذشت پدر گرامیتان رابه شما تسلیت عرض می نمائیم ازطرف همکاران اداره ثبت احوال فامنین سیفی</p>
', NULL, N'1395/02/09', N'08:47     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(139 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>همکار گرامی جناب آقای تیموری درگذشت پدر گرامیتان رابه شما تسلیت عرض می نمائیم ازطرف همکاران اداره ثبت احوال فامنین سیفی</p>
', NULL, N'1395/02/09', N'08:47     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(140 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h2>ضمن سلام وخسته نباشیدپیشنهاد میگردد چنانچه ممکن است عکس کارتهای هوشمند ملی رنگی بجای سیاه سفید جایگزین گردد چراکه اکثرعموم مردم ازعکس سیاه سفید راضی نیستند</h2>

<p>1395/02/09</p>

<p>پیام ازعلی طائفی</p>
', NULL, N'1395/02/09', N'10:59     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(141 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h2>ضمن سلام وخسته نباشیدپیشنهاد میگردد چنانچه ممکن است عکس کارتهای هوشمند ملی رنگی بجای سیاه سفید جایگزین گردد چراکه اکثرعموم مردم ازعکس سیاه سفید راضی نیستند</h2>

<p>1395/02/09</p>

<p>پیام ازعلی طائفی</p>
', NULL, N'1395/02/09', N'11:04     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(142 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'پیام همکار : علی طیبی', N'<p><span style="font-size:20px">با سلام و خسته نباشید خدمت همکاران خواهشمند است&nbsp;در صورت امکان نسبت به سرویس و یاتعویض چاپگرهای شناسنامه های بزرگسال اقدام شود و چاپگرهایی با کیفیت بالاتر تهیه گردد چون شناسنامه های چاپ شده با این چاپگرها کیفیت مطلوب و مناسبی را ندارند، هم کم رنگ چاپ میشود و هم اینکه برای چاپ تحولات همسر باید برای هر مورد تنظیمات رو تغییر بدیم تا اسم همسر چاپ شود.ضمنا جوهر این چاپگرها دارای کیفیت خیلی پایین تر نسبت به جوهرهای سال پیش می باشند و با داشتن جوهردر کارتریج دستگاه پیام تعویض کارتریج جوهر را می دهد که باعث اصراف زیاد جوهر می شود. با تشکر از زحمات &nbsp;</span></p>
', NULL, N'1395/02/11', N'10:13     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(143 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'پیام همکار : احمد محمدی', N'<p><span style="font-size:20px">باعرض سلام وخسته نباشید:تقریبا 20 روزی هست که کارت ملی هوشمندصادر و دریافت نمی شود لطفا پیگیر باشد که چرا صدور کارتها به تاخیر افتاده . باتشکر </span></p>
', NULL, N'1395/02/11', N'14:01     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(144 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="color:#000080"><span style="font-size:14px">کارت ملی ها رو چرا سازمان دیر میزنه؟ 3 ماهه کارت نیومده متقاضی از راه دور میاد و دست خالی بر می گرده .</span></span></p>
', NULL, N'1395/02/19', N'08:59     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(145 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'پیام همکار : علی طائفی', N'<h2>با سلام</h2>

<h2>خواهشمنداست به سازمان اعلام گرددچنانچه کارتهای هوشمند ملی طی دو ماهه صادرنمی گرددنوبت تحویل کارت به ارباب رجوع 3ماهه اعلام شودچون تاکنون بعدازگذشت 70روزهیچ کارتی صادروارسال نشده</h2>

<p>1395/02/20</p>

<p>پیام از علی طائفی</p>
', NULL, N'1395/02/20', N'08:44     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(146 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'قطعی سامانه کارت هوشمند ملی', N'<p><span style="color:#FF0000"><span style="font-size:16px"><span style="font-family:b yekan">قابل توجه&nbsp; کلیه همکاران محترم&nbsp; </span></span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan">با سلام<br />
با استحضار میرساند امروز مورخ 1395/02/26 سامنه کارت هوشمند ملی از ساعت 16:00 تا پایان همانروز قطع میباشد.</span></span></p>
', NULL, N'1395/02/26', N'11:07     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(147 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'قطعی تحویل کارت هوشمند ملی', N'<p><span style="font-family:b yekan">قابل توجه کلیه همکاران گرامی </span></p>

<p><span style="font-family:b yekan">با سلام و روز بخیر</span></p>

<p><span style="font-family:b yekan">به اطلاع می رساند از ساعت <span style="color:#FF0000">16:00</span> امروز (<span style="color:#FF0000">95.02.26</span>) سامانه<span style="color:#FF0000"> تحویل کارت هوشمند</span> ملی <span style="color:#FF0000">قطع&nbsp; </span>و امکان تحویل کارت هوشمند ملی وجود نخواهد شد. لذا لازم تمهیدات لازم در این خصوص اندیشیده شود.</span></p>

<p>&nbsp;</p>
', NULL, N'1395/02/26', N'11:48     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(148 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'ارسال پیامک تحویل کارت به متقاضیان', N'<p><span style="font-family:b yekan">قابل توجه کلیه روسای محترم و مسئولین محترم تحویل&nbsp; کارت هوشمند ملی&nbsp; ادارات </span></p>

<p><span style="font-family:b yekan">با سلام و روز بخیر</span></p>

<p><span style="font-family:b yekan">با توجه به <span style="color:#FF0000">ارسال پیامک سازمان به متقاضیان کارت هوشمند ملی</span>&nbsp; که کارت آنها<span style="color:#FF0000"> آماده تحویل</span> می باشد، کلیه همکاران واحد کارت هوشمند ملی ادارات&nbsp; از امروز (95.03.10) آمادگی لازم را جهت پاسخگویی به مراجعین داشته باشند.</span></p>
', NULL, N'1395/03/10', N'09:20     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(149 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'پیام همکار : قاسم اعظمی', N'<p>با سلام</p>

<p>حلول ماه مبارک رمضان مبارک باد<span style="font-family:b yagut"><span style="font-size:16px">.</span></span></p>
', NULL, N'1395/03/20', N'08:13     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(153 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="font-size:20px"><span style="color:rgb(0, 0, 128)">جناب آقای ایرج احمدی مصیبت وارده ( درگذشت والده همسر محترم ) را به جنابعالی و خانواده مغزا تسلیت عرض نموده از درگاه ایزد منان برای آن مرحومه رحمت و مغفرت و برای بازماندگان صبر جمیل خواستاریم</span></span></p>
', NULL, N'1395/03/27', N'12:35     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(154 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'عدم منع درخواست کارت کاغذی از معلولین', N'<p>قابل توجه کلیه همکاران محترم در حوزه کارت هوشمند ملی<br />
با سلام و روز بخیر</p>

<p>با عنایت به اینکه در برخی موارد امکان اخذ تصویر چهره&nbsp; بصورت زنده از معلولین وجود ندارد، لذا همکاران می بایست نسبت به اخذ درخواست کارت ملی کاغذی از آنان اقدام نمایند.</p>
', NULL, N'1395/03/29', N'14:02     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(155 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>جنبابآقای ایرج احمدی مصیبت وارده رابه جنابعالی وخانواده مغزا تسلیت عرص نموده از درگاه ایزد منان برای آنمرحومه رحمت ومغفرت وبرای بازماندگان صبر جمیل خواستاریم&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ازطرف سیفی ثبت احوال فامنین</p>
', NULL, N'1395/04/03', N'11:44     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(156 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>جنبابآقای ایرج احمدی مصیبت وارده رابه جنابعالی وخانواده مغزا تسلیت عرص نموده از درگاه ایزد منان برای آنمرحومه رحمت ومغفرت وبرای بازماندگان صبر جمیل خواستاریم&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ازطرف سیفی ثبت احوال فامنین</p>
', NULL, N'1395/04/03', N'11:44     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(157 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>جنبابآقای ایرج احمدی مصیبت وارده رابه جنابعالی وخانواده مغزا تسلیت عرص نموده از درگاه ایزد منان برای آنمرحومه رحمت ومغفرت وبرای بازماندگان صبر جمیل خواستاریم&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ازطرف سیفی ثبت احوال فامنین</p>
', NULL, N'1395/04/03', N'11:45     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(158 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'پیام همکار : حسین اصلانی', N'<p><strong><span style="font-family:b tir">با آرزوی قبولی طاعات و عبادات همکاران محترم در ماه مبارک رمضان و لیالی قدر .</span></strong></p>

<p><strong>التماس دعا </strong></p>

<p><span style="font-family:b tir"><strong><span style="background-color:#00FF00">اللهم عجل الولیک الفرج ( ان شا ا... )</span></strong></span></p>
', NULL, N'1395/04/03', N'12:39     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(159 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'پیام همکار : قاسم اعظمی', N'<p><span style="font-size:24px">با سلام خدمت همه همکاران محترم </span></p>

<p><span style="font-size:24px">ضمن قبولی طاعات وعبادات در این ماه پربرکت پیشاپیش عید سعید فطررا تبریک وتهنیت عرض مینمایم. </span></p>

<p>&nbsp;</p>
', NULL, N'1395/04/14', N'08:49     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(160 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'پیام همکار : قاسم اعظمی', N'<p><span style="font-size:24px">با سلام خدمت همه همکاران محترم </span></p>

<p><span style="font-size:24px">ضمن قبولی طاعات وعبادات در این ماه پربرکت پیشاپیش عید سعید فطررا تبریک وتهنیت عرض مینمایم. </span></p>

<p>&nbsp;</p>
', NULL, N'1395/04/14', N'08:50     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(161 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'تغییرات سامانه استعلام', N'<p><span style="font-family:b yekan">قابل توجه کلیه همکاران گرامی </span></p>

<p><span style="font-size:20px"><span style="font-family:b yekan">به اطلاع می رساند قابلیت نمایش عکس در سامانه استعلام برای تمامی همکاران فعال میباشد.</span></span></p>
', NULL, N'1395/04/26', N'11:19     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(162 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>ازتاریخ 1/3/1395کارتهای هوشمند ملی که از تهران صادر میشود توسط اداره پست فامنین به اداره ثبت احوال فامنین فرستاده میشود دراداره فامنین تحویل آقایان قاسم اغظمی نادر جامه بزرگ میشود اینجانب علی سیفی درمورد بعنوان تحویل گیرنده هیچ گونه مسولیتی ندارم عهدار امضاء کننده گان هستند</p>
', NULL, N'1395/04/26', N'12:04     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(163 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>ازتاریخ 1/3/1395کارتهای هوشمند ملی که از تهران صادر میشود توسط اداره پست فامنین به اداره ثبت احوال فامنین فرستاده میشود دراداره فامنین تحویل آقایان قاسم اغظمی نادر جامه بزرگ میشود اینجانب علی سیفی درمورد بعنوان تحویل گیرنده هیچ گونه مسولیتی ندارم عهدار امضاء کننده گان هستند</p>
', NULL, N'1395/04/26', N'12:05     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(164 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'پیام همکار : احمد محمدی', N'<p><span style="font-size:18px">باسلام و خسته نباشید خدمت همکاران فعال زحمت کش&nbsp; . باتوجه به این که اخیرا&nbsp; قطعی های&nbsp; مکرری در سیستم </span><span style="font-size:18px">کارت هوشمند به وجود آمده و شهروندان پس از انتظارات طولانی وعدم انجام کار مورد نظر باعث نارضایتی آنان می شود:&nbsp; لطفا درصورت اطلاع از قطعی سیستم به همکاران اطلاع دهید&nbsp; که به متقاضان&nbsp; اطلاع رسانی نمایند ودر صورت امکان درروزهایی که سامانه مشکل دارد نوبت دهی انجام نگیرد </span><span style="font-size:18px">باتشکر </span></p>
', NULL, N'1395/04/29', N'11:50     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(165 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'لزوم استفاده از کارتخوان(کارتخوان - اسکنر تک انگشت) جهت ورود به سامانه های ثبت احوال', N'<p><span style="font-size:18px"><span style="font-family:b titr">با سلام خدمت همکاران گرامی</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b lotus">ازآنجایی که مقرر است از تاریخ 1395/05/05 ورود به سامانه ها صرفا از طریق کارت هوشمند و با استفاده از کارت خوان صورت پذیرد. لذا خواهشمند است موارد ذیل مد نظر قرار گیرد.</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b lotus">1)&nbsp; تمامی همکاران نسبت به همراه داشتن کارت هوشمند ملی خود اقدام نمایند.</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b lotus">2)&nbsp; کارت خوان های مورد نیاز از طریق اداره درخواست و در اسرع وقت از معاونت توسعه و مدیریت نیروی انسانی پی گیری گردد.</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b lotus">3)&nbsp; در اسرع وقت نسبت به نصب برنامه مورد نیاز اقدام گردد.در صورت بروز مشکل با همکاران واحد فناوری اطلاعات هماهنگ شود.</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b lotus">مراتب بروز مشکلات بعدی در خصوص عدم آمادگی اداره متوجه رئیس اداره می باشد.</span></span></p>
', NULL, N'1395/04/31', N'08:09     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(166 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'نصب، راه اندازی و استفاده از اسکنر تک انگشتی جهت ورود سامانه ', N'<p><span style="color:#B22222"><span style="font-size:16px"><span style="font-family:b yekan">قابل توجه مسئولین محترم فناوری اطلاعات ادارات </span></span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">با سلام و روز بخیر </span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">پیرو اطلاعیه قبلی در خصوص<span style="color:#008000"> لزوم استفاده از اسکنر تک انگشتی</span> به منظور ورود به سامانه های سازمان، مقتضیست مسئولین محترم فناوری اطلاعات ادارات بلافاصله پس از وصول دستگاهها با <span style="color:#008000">مراجعه به لینک زیر و مطالعه راهنما و دریافت نرم افزار</span> های مورد نیاز نسبت به راه اندازی دستگاهها اقدام نمایند.</span></span></p>

<p><strong><span style="font-size:14px"><span style="font-family:b yekan"><span style="color:rgb(0, 128, 0)">لینک راهنما: </span></span></span></strong><a href="http://10.1.20.29/info/Dcu/help_print.htm" target="_blank"><span style="color:#0000FF">http://10.1.20.29/info/Dcu/help_print.htm</span></a></p>
', NULL, N'1395/04/31', N'12:12     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(167 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'پیام همکار : مرتضی برزگر زندی', N'<p><span style="color:#000080"><span style="font-size:16px">با عرض سلام </span></span></p>

<p><span style="color:#000080"><span style="font-size:16px">با عنایت به قطعی سامانه پشتیبانی هوشمند لطفا تدبیر و چاره ای برای درخواستهایی که نیاز به ویرایش دارند اندیشیده شود. </span></span></p>
', NULL, N'1395/05/02', N'19:40     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(168 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(101 AS Numeric(18, 0)), N'پیام همکار : محمود قره باغی برهمن', N'<p>جناب اقای فرهاد امانی همکار محترم</p>

<p>باابراز همدردی مصیبت وارده را تسلیت عرض نموده مارا در غم خود شریک بدانید برای ان مرحومه مغفوره علو درجات وبرای بازماندگان صبر جمیل از خداوند متعال خواستاریم</p>
', NULL, N'1395/05/17', N'10:40     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(169 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'نحوه درخواست افزودن ایستگاه جدید کارت هوشمند ملی', N'<p><span style="font-size:14px"><span style="font-family:b yekan"><span style="color:#008000">قابل توجه مسئولین محترم فناوری اطلاعات ادارات </span></span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">با سلام و روز بخیر </span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">به اطلاع می رساند از این پس به منظور <span style="color:#800000">افزودن و یا تغییر ایستگاه کاری کارت هوشمند</span> در اداره و یا دفاتر پستی، <span style="color:#A52A2A">درخواست </span>می بایست <span style="color:#A52A2A">در قالب فرم پیوست</span> صورت پذیرد.</span></span></p>
', N'Form.rar', N'1395/05/18', N'11:02     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(170 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), N'پیام همکار : فرهاد امانی', N'<h1 style="text-align:justify"><span style="font-family:b titr"><span style="font-size:22px">باسلام وعرض ارادت خدمت کلیه همکاران محترم&nbsp;</span></span></h1>

<h1 style="text-align:justify"><span style="font-family:b titr"><span style="font-size:22px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size:18px">بدینوسیله از حضورکلیه سرورانی که درمراسم تشییع ، تدفین ومجلس ترحیم والده اینجانب شرکت وصمیمانه ابراز همدردی نموده تا سنگینی این غم بر بازماندگان قابل تحمل ترباشد ، کمال تشکر وامتنان را دارم و برای ایشان از پروردگار متعال&nbsp; صحت و سلامتی و موفقیت روزافزون رامسئلت مینمایم . ضمنا مراسم شب هفت آن مرحومه روزپنجشنبه مورخ 95/5/21 از ساعت 16/30 الی 18 درمسجد چهارباب الحوائج (کوی چمن) واقع در همدان بلوار کاشانی برگزار میگردد.</span><span style="font-size:22px">&nbsp;&nbsp;&nbsp; </span></span></h1>

<h1 style="text-align:justify"><span style="font-family:b titr"><span style="font-size:22px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; فرهاد امانی</span></span></h1>
', NULL, N'1395/05/19', N'08:40     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(171 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), N'پیام همکار : فرهاد امانی', N'<h1 style="text-align:justify"><span style="font-family:b titr"><span style="font-size:22px">باسلام وعرض ارادت خدمت کلیه همکاران محترم&nbsp;</span></span></h1>

<h1 style="text-align:justify"><span style="font-family:b titr"><span style="font-size:22px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size:18px">بدینوسیله از حضورکلیه سرورانی که درمراسم تشییع ، تدفین ومجلس ترحیم والده اینجانب شرکت وصمیمانه ابراز همدردی نموده تا سنگینی این غم بر بازماندگان قابل تحمل ترباشد ، کمال تشکر وامتنان را دارم و برای ایشان از پروردگار متعال&nbsp; صحت و سلامتی و موفقیت روزافزون رامسئلت مینمایم . ضمنا مراسم شب هفت آن مرحومه روزپنجشنبه مورخ 95/5/21 از ساعت 16/30 الی 18 درمسجد چهارباب الحوائج (کوی چمن) واقع در همدان بلوار کاشانی برگزار میگردد.</span><span style="font-size:22px">&nbsp;&nbsp;&nbsp; </span></span></h1>

<h1 style="text-align:justify"><span style="font-family:b titr"><span style="font-size:22px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; فرهاد امانی</span></span></h1>
', NULL, N'1395/05/19', N'08:40     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(172 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), N'پیام همکار : فرهاد امانی', N'<h1 style="text-align:justify"><span style="font-family:b titr"><span style="font-size:22px">باسلام وعرض ارادت خدمت کلیه همکاران محترم&nbsp;</span></span></h1>

<h1 style="text-align:justify"><span style="font-family:b titr"><span style="font-size:22px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size:18px">بدینوسیله از حضورکلیه سرورانی که درمراسم تشییع ، تدفین ومجلس ترحیم والده اینجانب شرکت وصمیمانه ابراز همدردی نموده تا سنگینی این غم بر بازماندگان قابل تحمل ترباشد ، کمال تشکر وامتنان را دارم و برای ایشان از پروردگار متعال&nbsp; صحت و سلامتی و موفقیت روزافزون رامسئلت مینمایم . ضمنا مراسم شب هفت آن مرحومه روزپنجشنبه مورخ 95/5/21 از ساعت 16/30 الی 18 درمسجد چهارباب الحوائج (کوی چمن) واقع در همدان بلوار کاشانی برگزار میگردد.</span><span style="font-size:22px">&nbsp;&nbsp;&nbsp; </span></span></h1>

<h1 style="text-align:justify"><span style="font-family:b titr"><span style="font-size:22px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; فرهاد امانی</span></span></h1>
', NULL, N'1395/05/19', N'08:40     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(173 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>باسلام همکار محترم جناب آقای فرهاد امانی بانهایت تائسف وتاثر درگذشت مادر گرامیتان را تسلیت عرص نموده واز خداوند متعال برای آن مرحومه طلب مغفرت وبرای بازماندگان صبرجمیل خواستاریم&nbsp; ازطرف رئیس ثبت احوال فامنین آقای جامه بزرگ وکارکنان علی سیفی</p>
', NULL, N'1395/05/20', N'11:48     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(174 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'لزوم استفاده از اسکنر تک انگشتی جهت ورود به سامانه های ثبت احوال', N'<p><span style="color:#B22222"><span style="font-size:16px"><span style="font-family:b yekan">قابل توجه همکاران محترم</span></span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">درود و روز بخیر</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">پیرو اطلاعیه قبلی مورخ 1395/04/31در خصوص<span style="color:#008000"> لزوم استفاده از اسکنر تک انگشتی</span> به منظور ورود به سامانه های سازمان، مقتضیست همکاران محترم ادارات از این پس جهت استفاده از سامانه جمع آوری اطلاعات (DCU)از آدرس </span></span><a href="https://dcutest.nocr.org"><strong><span style="font-size:16px"><span style="font-family:courier new,courier,monospace">https://dcutest.nocr.org</span></span></strong></a><span style="font-size:14px"><span style="font-family:b yekan">&nbsp; و کارت هوشمند برای ورود به سامانه استفاده نمایند.</span></span></p>
', NULL, N'1395/05/30', N'11:45     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(175 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'عملکرد تیر ماه  کارت هوشمند ملی ادارات ', N'<p><span style="font-family:b yekan"><span style="color:#008000">قابل توجه روسا و مسئولین محترم فناوری اطلاعات ادارات </span></span></p>

<p><span style="font-family:b yekan">با سلام و روز بخیر </span></p>

<p><span style="font-family:b yekan">عملکرد تیر ماه کارت هوشمند ملی ادارات تابعه استان در قالب فایل پیوست جهت بهره برداری ارسال می گردد.</span></p>
', N'Darkhast.pdf', N'1395/05/30', N'11:55     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(176 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'لزوم پی گیری استفاده از کارت ملی هوشمند جهت ورود به سامانه های ثبت احوال', N'<p><span style="font-family:b titr"><span style="font-size:14px"><strong><span style="color:rgb(178, 34, 34)">قابل توجه رؤسا و مسئولین محترم فناوری اطلاعات ادارات </span></strong></span></span></p>

<p><span style="font-family:b titr"><span style="font-size:14px">درود و روز بخیر</span></span></p>

<p style="text-align:justify"><span style="font-family:b homa"><span style="font-size:16px">&nbsp;&nbsp;</span></span><strong><span style="font-family:b nazanin"><span style="font-size:16px">&nbsp; پیرو اطلاعیه قبلی به تاریخ 1395/05/30 و دو بار تماس تلفنی کارشناسان فن آوری اداره کل، در خصوص<span style="color:rgb(0, 128, 0)"> لزوم استفاده از اسکنر تک انگشتی</span> و همچنین استفاده از نرم افزار <span style="color:#008000">NOCRAuthenticate Application</span> (که قبلا برروی سیستم ها نصب شده)،&nbsp; به منظور ورود به سامانه های سازمان، مسئولین محترم فن آوری اطلاعات و رؤسای ادارات تنها تا پایان وقت اداری امروز 1395/06/02 فرصت دارند نسبت به رفع مشکلاتی از قبیل فراموشی کلمه عبور، عدم کارکرد صحیح کارت ملی هوشمند، فقدان کارت ملی هوشمند، عدم قبول اثر انگشت و یا هرگونه مشکلات احتمالی دیگراقدام و با واحد <span style="color:rgb(178, 34, 34)">فناوری اطلاعات</span> مکاتبه نمایند.در غیر اینصورت مسئولیت بروز هرگونه مشکلات بر عهده رئیس اداره خواهد بود.</span></span></strong></p>
', NULL, N'1395/06/02', N'10:42     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(177 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'تغییر نسخه نرم افزار کارت هوشمند', N'<p><span style="color:#006400"><span style="font-family:b yekan"><span style="font-size:16px">قابل توجه کلیه همکاران محترم فعال در حوزه کارت ملی هوشمند </span></span></span></p>

<p><span style="font-family:b yekan"><span style="font-size:16px">با عرض سلام و روز بخیر </span></span></p>

<p><span style="font-family:b yekan"><span style="font-size:16px">به اطلاع می رساند <span style="color:#B22222">نسخه جدید سامانه CCOS </span>از روز<span style="color:#B22222"> پنج شنبه </span>مورخه<span style="color:#B22222"> 1395/06/04</span> بارگذاری خواهد شد. لذا&nbsp; مقتضی است موضوع به دفاتر پست و پیشخوان اطلاع رسانی&nbsp; گردد. ضمناً در صورت مواجهه با هر گونه خطای احتمالی، هماهنگی لازم با واحد فناوری اطلاعات اداره کل صورت پذیرد.</span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan"><strong>لازم به ذکر است که <span style="color:rgb(178, 34, 34)">سامانه CCOS</span> از بعدازظهر روز چهارشنبه تاریخ 1395/06/03 از دسترس خارج میباشد</strong></span></span></p>

<p>&nbsp;</p>
', NULL, N'1395/06/03', N'11:11     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(178 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'پیام همکار : حسنعلی ده پهلوان', N'<p>با سلام همکار محترم جناب آقای فرهاد امانی از اینکه متوجه درگذشت والده مکرمه نشدیم عذر خواهی نموده وضمن عرض تسلیت&nbsp; ، برای آن مرحومه از خداوند متعال علو درجات وبرای جنابعالی وسایر بازماندگان صبر وشکیبائی خواستاریم .</p>

<p>اداره ثبت احوال تویسرکان ده پهلوان واحسانی</p>
', NULL, N'1395/06/09', N'09:23     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(179 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'پیام همکار : حسنعلی ده پهلوان', N'<p>با سلام همکار محترم جناب آقای فرهاد امانی از اینکه متوجه درگذشت والده مکرمه نشدیم عذر خواهی نموده وضمن عرض تسلیت&nbsp; ، برای آن مرحومه از خداوند متعال علو درجات وبرای جنابعالی وسایر بازماندگان صبر وشکیبائی خواستاریم .</p>

<p>اداره ثبت احوال تویسرکان ده پهلوان واحسانی</p>
', NULL, N'1395/06/09', N'09:23     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(180 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'پیام همکار : قاسم اعظمی', N'<p>بنام خدا</p>

<p>با سلام خدمت همکاران محترم حوزه فناوری اطلاعات با توجه به اینکه درخواست تعویض بالای 15سال از طریق سامانه صدور شناسنامه بزرگسال امکان پذیر است اما برای افراد زیر 15سال باید دستی درخواست نمود خواهشمند است دراین رابطه بمنظور یکسان سازی و سهولت انجام کار (تکریم ارباب رجوع) اقدام مقتضی بعمل آید. با تشکر</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>

<p>&nbsp; قاسم اعظمی&nbsp;</p>
', NULL, N'1395/06/13', N'11:45     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(181 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'پیام همکار : قاسم اعظمی', N'<p>بنام خدا</p>

<p>با سلام خدمت همکاران محترم حوزه فناوری اطلاعات با توجه به اینکه درخواست تعویض بالای 15سال از طریق سامانه صدور شناسنامه بزرگسال امکان پذیر است اما برای افراد زیر 15سال باید دستی درخواست نمود خواهشمند است دراین رابطه بمنظور یکسان سازی و سهولت انجام کار (تکریم ارباب رجوع) اقدام مقتضی بعمل آید. با تشکر</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>

<p>&nbsp; قاسم اعظمی&nbsp;</p>
', NULL, N'1395/06/13', N'11:45     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(182 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'تغییر کاربر دبیرخانه استان ', N'<p><span style="font-size:16px"><span style="font-family:b yekan">قابل توجه&nbsp; کلیه همکاران محترم&nbsp; </span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan">با سلام و روز بخیر<br />
با توجه به<span style="color:rgb(178, 34, 34)"> انتصاب آقای </span><span style="color:#006400">برزو حسنی تبار</span><span style="color:rgb(178, 34, 34)"> به سمت مسئول دبیر خانه استان</span>، لطفا از این پس کلیه مکاتبات ارسالی از طریق اتوماسیون اداری تحت وب به دبیرخانه استان، بجای آقای خلیلی <span style="color:rgb(178, 34, 34)">به آقای </span><span style="color:#006400">برزو حسنی</span><span style="color:rgb(178, 34, 34)"> ارجاع</span> داده شود.</span></span></p>
', NULL, N'1395/06/15', N'07:48     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(183 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'پیام همکار : احمد محمدی', N'<p><span style="font-size:24px">باعرض سلام وخسته نباشید:از زمانی که در سامانه کارت هوشمند تغییراتی حاصل شده است&nbsp; درقسمت اسکن اثر انگشت کیفیت برنامه پایین آمده ودستگاه مثل سابق اثر انگشتها را به خوبی اخذ نمی نماید لطفا برسی نموده تمهیدات و اقدامات لازم را مبذول فرمایید . </span><span style="font-size:18px">باتشکر احمد محمدی </span></p>
', NULL, N'1395/06/15', N'13:44     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(184 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'پیام همکار : احمد محمدی', N'<p><span style="font-size:24px">همکار گرامی:جناب آقای محمود خزلی درگذشت پدر گرامیتان را به جنابعالی وخانواده محترمتان تسلیت عرض نموده وا خداوند منان برای آن مرحوم طلب غفران ورحمت الهی وبرای شما صبرجزیل خواستاریم .</span></p>

<p><span style="font-size:24px">ازطرف کلیه همکاران اداره ثبت احوال قلقلرود</span></p>
', NULL, N'1395/06/29', N'08:09     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(185 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'پیام همکار : احمد محمدی', N'<p><span style="font-size:24px">همکار گرامی:جناب آقای محمود خزلی درگذشت پدر گرامیتان را به جنابعالی وخانواده محترمتان تسلیت عرض نموده وا خداوند منان برای آن مرحوم طلب غفران ورحمت الهی وبرای شما صبرجزیل خواستاریم .</span></p>

<p><span style="font-size:24px">ازطرف کلیه همکاران اداره ثبت احوال قلقلرود</span></p>
', NULL, N'1395/06/29', N'08:09     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(186 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'پیام همکار : حسین اصلانی', N'<p><span style="color:#FFA500"><strong><span style="font-family:b tir"><span style="font-size:26px">باسلام</span></span></strong></span></p>

<p><span style="font-family:b titr">ایام سوگورای&nbsp; <span style="color:rgb(255, 0, 0)">آقا اباعبدا... الحسین (ع) &nbsp; </span>تسلیت باد</span></p>

<p><span style="font-family:b tir"><span style="background-color:#006400">التماس دعا</span></span></p>
', NULL, N'1395/07/18', N'09:58     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(187 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'ارسال اطلاعات و گزارشات', N'<p><span style="color:#800000"><span style="font-family:b yekan">قابل توجه ناظرین محترم کارت هوشمند ملی و مسئولین&nbsp; محترم فناوری اطلاعات&nbsp; ادارات </span></span></p>

<p><span style="color:#000000"><span style="font-family:b yekan">با عرض سلام و روز بخیر </span></span></p>

<p><span style="font-family:b yekan">پیرو جلسه&nbsp; مورخه 1395/07/18 ناظرین کارت هوشمند ملی ، لطفا <span style="color:#800000">ظرف هفته جاری</span>، اطلاعات ذیل به اداره کل ارسال گردد.<br />
<br />
<span style="color:#000033">1-تکمیل و ارسال پرسشنامه ارزیابی عملکرد شش ماهه نخست سال 95 ایستگاههای کارت هوشمند ملی در ادارات پست و دفاتر پیشخوان.</span></span></p>

<p><span style="color:#000033"><span style="font-family:b yekan">2-آخرین وضعیت کاربران ایستگاههای کارت هوشمند ملی در ادارات، دفاتر پیشخوان و پست.</span></span></p>

<p><span style="color:#000033"><span style="font-family:b yekan">3-ارسال کد شناسه ایستگاههای کارت هوشمند ملی در ادارات، دفاتر پیشخوان و پست.</span></span></p>
', NULL, N'1395/07/24', N'08:12     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(188 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'روشن نمودن کلیه کامپیوترهای اداره ', N'<p><span style="font-size:16px"><span style="font-family:b yekan"><span style="color:rgb(255, 0, 0)">اقدام فوری </span></span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan"><span style="color:#B22222">قابل توجه مسئولین محترم فناوری اطلاعات</span></span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan">با سلام و روز بخیر </span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan">به منظور دریافت آخرین آپدیت آنتی ویروس ، لطفا کلیه کامپیوترهای اداره روشن گردند.</span></span></p>

<p>&nbsp;</p>
', NULL, N'1395/08/01', N'11:05     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(189 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'پیام همکار : مهدی پارساصفت', N'<p dir="RTL"><span style="font-family:b titr"><span style="font-size:24px"><strong>همکار گرامی جناب آقای نادر ابراهیمی</strong></span></span></p>

<p dir="RTL">&nbsp;</p>

<p dir="RTL"><span style="font-size:24px"><strong>&nbsp;&nbsp;<span style="font-family:b titr">&nbsp;</span></strong></span><span style="font-family:b titr"><span style="font-size:22px"><strong> </strong><span dir="RTL">با نهایت تاسف و تاثر مصیبت وارده را بشما و خانواده محترم صمیمانه تسلیت عرض نموده علو درجات برای آن مرحوم وصبر و اجر جمیل برای سایر بازماندگان از خداوند متعال خواهانیم.</span></span></span></p>

<p dir="RTL">&nbsp;</p>

<p dir="RTL" style="text-align: left;"><span style="font-family:b titr"><span style="font-size:20px">رئیس و کارکنان اداره ثبت احوال بهار</span></span></p>
', NULL, N'1395/08/25', N'12:25     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(190 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'پیام همکار : مهدی پارساصفت', N'<p dir="RTL"><span style="font-family:b titr"><span style="font-size:24px"><strong>همکار گرامی جناب آقای نادر ابراهیمی</strong></span></span></p>

<p dir="RTL">&nbsp;</p>

<p dir="RTL"><span style="font-size:24px"><strong>&nbsp;&nbsp;<span style="font-family:b titr">&nbsp;</span></strong></span><span style="font-family:b titr"><span style="font-size:22px"><strong> </strong><span dir="RTL">با نهایت تاسف و تاثر مصیبت وارده را بشما و خانواده محترم صمیمانه تسلیت عرض نموده علو درجات برای آن مرحوم وصبر و اجر جمیل برای سایر بازماندگان از خداوند متعال خواهانیم.</span></span></span></p>

<p dir="RTL">&nbsp;</p>

<p dir="RTL" style="text-align: left;"><span style="font-family:b titr"><span style="font-size:20px">رئیس و کارکنان اداره ثبت احوال بهار</span></span></p>
', NULL, N'1395/08/25', N'12:25     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(191 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'کارتهای هوشمند رسیده غیر قابل وصول', N'<p><span style="color:#FF0000"><span style="font-family:b yekan"><span style="font-size:16px">قابل توجه کلیه همکاران محترم فعال در حوزه کارت ملی هوشمند </span></span> </span></p>

<p>با عرض سلام</p>

<p><span style="font-size:16px"><span style="font-family:comic sans ms,cursive">به اطلاع همکاران گرامی میرساند طبق اعلامیه سازمان ثبت احوال کل کشور برخی از کارتهای هوشمند رسیده به ادارات تابعه تا اطلاع ثانوی قابل وصول نمی باشند، لذا خواهشمند است این دسته از کارتها را به صورت جداگانه نگهداری کرده تا در زمان مناسب نسبت به اعلام وصول آنها اقدام گردد.</span></span></p>

<p><span style="font-size:16px"><span style="font-family:comic sans ms,cursive">با تشکر</span></span></p>
', NULL, N'1395/09/16', N'08:59     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(192 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>باسلام همکار محترم جناب آقایی اخلاصی واسماعیلی شاره دسته های 314و485و45و587و106قابل اعلام وصول شدن نیستن لطفآبرسی بفرمائید</p>
', NULL, N'1395/09/20', N'12:19     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(193 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>باسلام آقایی اسماعیلی کارتهای رسیده که اعلام وصول نمیشند بامتقا ضیان دوچار موشکل هستیم لطفآبرسی بفرمائید</p>
', NULL, N'1395/09/22', N'13:31     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(194 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'پیام همکار : علی سیفی', N'<p>باسلام آقایی اسماعیلی کارتهای رسیده که اعلام وصول نمیشند بامتقا ضیان دوچار موشکل هستیم لطفآبرسی بفرمائید</p>
', NULL, N'1395/09/22', N'13:32     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(195 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'تعیین تکلیف کارتهای آماده تحویل', N'<p><br />
با سلام و احترام خدمت همکاران محترم، ضمن سپاس از تلاش مجدانه همکاران در پیشبرد مطلوب طرح کارت هوشمند ملی :</p>

<p>لازم است کارت های آماده تحویل انباشت شده در ادارات مخصوصا کارتهای قدیمی تر با کمک سامانه &quot;&nbsp; 3S&nbsp; &quot; ، فرمهای درخواست و فایل تحویلی از حراست و حتی بکارگیری&nbsp; دفاتر پستی و پیشخوان ، در اسرع وقت اطلاع رسانی و توزیع گردد.</p>

<p>درصد کارتهای انباشت شده از اولویت های ارزیابی پایان سال ادارات میباشد. اسماعیلی</p>
', NULL, N'1395/11/07', N'11:28     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(196 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'آمادگی تحویل کارت', N'<p><br />
با سلام و احترام</p>

<p>با توجه به انباشت کارت آماده تحویل در ادارات، سازمان روز گذشته (به تاریخ 1395/11/16 ) اقدام به ارسال پیامک برای شهروندان نموده است.</p>

<p>ادارات آمادگی لازم برای حجم بیشتری از مراجعات برای تحویل کارت داشته باشند.</p>
', NULL, N'1395/11/17', N'08:05     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(197 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'پیام همکار : غلامرضا مقدری', N'<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; باکمال تاسف باخبرشدیم همکارمان <u>هاشم عاشوری </u> دارفانی راوداع گفت،این مصیبت رابه خانواده آن عزیزوهمکاران محترم <em><u> اداره</u> <u>ثبت احوال ملایر</u></em>تسلیت عرض می نماییم وازخداوندمتعال برای آن مرحوم طلب مغفرت وبرای بازماندگان صبرجمیل مسئلت می نمائیم.</h2>

<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> </strong></h2>
', NULL, N'1395/11/25', N'10:16     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(198 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'پیام همکار : غلامرضا مقدری', N'<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; باکمال تاسف باخبرشدیم همکارمان <u>هاشم عاشوری </u> دارفانی راوداع گفت،این مصیبت رابه خانواده آن عزیزوهمکاران محترم <em><u> اداره</u> <u>ثبت احوال ملایر</u></em>تسلیت عرض می نماییم وازخداوندمتعال برای آن مرحوم طلب مغفرت وبرای بازماندگان صبرجمیل مسئلت می نمائیم.</h2>

<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> </strong></h2>
', NULL, N'1395/11/25', N'10:16     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(199 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'نحوه درج پیوست نامه در اتوماسیون اداری', N'<p><span style="font-size:14px"><span style="color:#006400"><span style="font-family:b titr"><strong>قابل توجه کلیه همکاران محترم </strong></span></span><br />
<span style="font-family:b yekan">با سلام و روز بخیر<br />
به منظور سهولت در <span style="color:#FF0000">درج پیوست به نامه ها</span> در اتوماسیون اداری تحت وب،&nbsp; از این پس از گزینه <span style="color:#FF0000">&quot; پیوست -&gt; پرونده &quot;</span>&nbsp; استفاده گردد.</span></span></p>

<p><img alt="" src="http://info.hm/attnews/Attachment.jpg" style="height:189px; width:800px" /></p>
', NULL, N'1395/11/25', N'13:51     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(200 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'بررسی کارتابل هیاتهای حل اختلاف و کمیسیون تشخیص سن', N'<p><span style="font-size:18px"><strong><span style="font-family:b lotus">با سلام و احترام</span></strong></span></p>

<p><strong><span style="font-size:18px"><span style="font-family:b lotus">با توجه به <span style="color:#FF0000">عملیاتی شدن اخذ درخواست هیاتهای حل اختلاف و کمیسیون تشخیص سن</span> به صورت الکترونیکی لازم است که همکاران مسئول در این زمینه نسبت به بررسی روزانه کارتابل خود اقدام نمایند ضمنا مراتب به صورت&nbsp;روزانه از طریق کاربر استان پیگیری و در بازرسی های استانی لحاظ میگردد.</span></span></strong></p>
', NULL, N'1395/12/02', N'09:57     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(201 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'نصب نرم افزار VoIP', N'<p><span style="color:#B22222"><span style="font-size:16px"><span style="font-family:b yekan">قابل توجه کلیه مسئولین فناوری اطلاعات ادارات </span></span></span><br />
<span style="color:#000000"><span style="font-size:16px"><span style="font-family:b yekan">با سلام و روز بخیر<br />
با توجه به راه اندازی سرویس VoIP در اداره کل ثبت احوال همدان و نیاز به برقراری ارتباط با ادارات تابعه لطفا اقدامات ذیل انجام و نتیجه به واحد فناوری اطلاعات گزارش گردد.</span></span></span></p>

<p><span style="color:#000000"><span style="font-size:16px"><span style="font-family:b yekan">1- </span></span></span><span style="color:#006400"><span style="font-size:16px"><span style="font-family:b yekan">نصب </span></span></span><span style="color:#000000"><span style="font-size:16px"><span style="font-family:b yekan">2 نرم افزار قرار داده شده در </span></span></span><span style="color:#006400"><span style="font-size:16px"><span style="font-family:b yekan">فایل پیوست</span></span></span><span style="color:#000000"><span style="font-size:16px"><span style="font-family:b yekan"> </span></span></span><span style="color:#000000"><span style="font-size:16px"><span style="font-family:b yekan">در </span></span></span><span style="color:#006400"><span style="font-size:16px"><span style="font-family:b yekan">کامپیوتر رئیس اداره و مسئول فناوری اطلاعات </span></span></span><span style="color:#000000"><span style="font-size:16px"><span style="font-family:b yekan">اداره </span></span></span></p>

<p><span style="color:#000000"><span style="font-size:16px"><span style="font-family:b yekan">2-</span></span></span><span style="color:#006400"><span style="font-size:16px"><span style="font-family:b yekan">استخراج آدرس MAC</span></span></span><span style="color:#000000"><span style="font-size:16px"><span style="font-family:b yekan"> این دو سیستم، ذخیره در قالب </span></span></span><span style="color:#006400"><span style="font-size:16px"><span style="font-family:b yekan">فایل TXT</span></span></span><span style="color:#000000"><span style="font-size:16px"><span style="font-family:b yekan"> و قراردادن با نام اداره در پوشه IT</span></span></span><br />
<span style="color:#2F4F4F"><span style="font-size:16px"><span style="font-family:b yekan">نحوه استخراج آدرس MAC : استفاده از دستور GETMAC در محیط CMD </span></span></span></p>
', N'VoIP.zip', N'1395/12/18', N'07:57     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(202 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'راهنمای استفاده از سامانه پالایش اسناد وفات', N'<p><span style="color:#FF0000"><span style="font-size:16px"><span style="font-family:b titr"><strong>با سلام و احترام</strong></span></span></span></p>

<p><span style="font-size:18px"><span style="font-family:comic sans ms,cursive"><strong>با توجه به عملیاتی شدن پالایش اسناد وفات باقیمانده، دستورالعمل و راهنمای استفاده از سامانه جهت بهره برداری ارسال میگردد.</strong></span></span></p>

<p><a href="http://10.1.20.29/info/death/rahnamaye_fot.htm"><strong>راهنمای کاربری سامانه پالایش اسناد وفات</strong></a></p>

<p><a href="http://10.1.20.29/deathrep/Help.htm"><strong>راهنمای گزارش گیری سامانه پالایش اسناد وفات</strong></a></p>
', NULL, N'1395/12/23', N'11:13     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(203 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'آپدیت آنتی ویروس ', N'<p><span style="font-size:16px"><span style="font-family:b yekan"><span style="color:#006400">قابل توجه مسئولین فناوری اطلاعات کلیه ادارات </span><br />
با سلام روز بخیر .</span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan">لطفا به منظور <span style="color:#B22222">دریافت آخرین آپدیت آنتی ویروس</span>، <span style="color:#B22222">کلیه سیستم ها</span>ی کامپیوتری اداره <span style="color:#B22222">روشن </span>گردند.</span></span><br />
&nbsp;</p>
', NULL, N'1395/12/28', N'08:42     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(204 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'به روز رسانی جدید سامانه ارائه خدمات هویتی', N'<p><span style="color:#FF0000"><strong>با سلام و احترام</strong></span></p>

<p><span style="color:#0000FF"><strong>به اطلاع میرساند، در نسخه جدید سامانه ارائه خدمات هویتی که در پایان ساعت اداری امروز نصب و &nbsp;از فردا قابل بهره برداری می باشد،&nbsp;ورود سری و سریال گواهی ولادت اجباری خواهد بود.</strong></span></p>
', NULL, N'1396/01/15', N'11:37     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(205 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'آدرس اتوماسیون اداری تحت وب', N'<p><span style="font-size:14px"><span style="font-family:b yekan">قابل توجه کلیه همکاران محترم </span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">با سلام و روز بخیر </span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">از این پس برای اتصال به<span style="color:#A52A2A"> اتوماسیون اداری تحت وب</span> از آدرس ذیل استفاده گردد.</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan"><span style="color:#0000CD">Http://auto.hm:7001/oa</span></span></span></p>

<p><span style="font-family:b yekan">در صورت عدم امکان اسکن نامه ها تنظیمات مرورگر با استفاده از ابزار پایین صفحه اتوماسیون که به شکل چرخ دنده است مجددا نصب گردد.</span></p>
', NULL, N'1396/02/23', N'14:07     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(206 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'پیام همکار : علی طیبی', N'<p><span style="font-size:26px"><span style="font-family:b nazanin"><span style="color:rgb(0, 128, 0)">&laquo;رمضان&raquo;</span> ماهی ست که</span></span></p>

<p><span style="font-size:26px"><span style="font-family:b nazanin">ابتدایش &laquo;رحمت&raquo; </span></span></p>

<p><span style="font-size:26px"><span style="font-family:b nazanin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; میانه اش &laquo;<span style="color:rgb(0, 128, 0)">مغفرت&raquo;</span></span></span></p>

<p><span style="font-size:26px"><span style="font-family:b nazanin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; و پایانش <span style="color:rgb(0, 128, 0)">&laquo;اجابت&raquo;</span> است</span></span></p>

<p><span style="font-size:26px"><span style="font-family:b nazanin">پیشاپیش ماه رمضان بر همه <span style="color:rgb(0, 128, 0)">همکاران عزیز</span> مبارک</span></span></p>
', NULL, N'1396/03/04', N'10:30     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(207 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'اقدامات پیشگیرانه در برابر باج افزار wannacrypt', N'<p><span style="font-size:24px"><span style="font-family:b titr">قابل توجه مسئولین محترم فن آوری اطلاعات ادارات</span></span></p>

<p>با سلام و روز بخیر</p>

<p style="text-align: justify;">به اطلاع می رساند با توجه به حمله گسترده <span style="color:#FF0000"><span style="font-size:18px">باج افزار wannacrypt</span></span> به سیستم های رایانه ای در سراسر جهان لازم است هرچه سریعتر نسبت به نصب وصله های ویندوز بر روی تمامی سیستمهای رایانه ای ادارات اقدام گردد، همچنین به کاربران توصیه میگردد <span style="font-size:18px"><span style="color:#FF0000">هرگز از حافظه های قابل حمل مانند فلش دیسک،هارد دیسک اکسترنال،دی وی دی و غیره استفاده نگردد</span></span>.</p>

<p style="text-align: justify;">در ضمن وصله های به روز رسانی ویندوز برای ویندوزهای اکس پی و سون در فایل پیوست موجود میباشد. در صورت بروز هرگونه مشکل در نصب وصله ها با واحد فن آوری اطلاعات اداره کل تماس حاصل فرمائید</p>
', N'path.rar', N'1396/03/06', N'07:43     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(208 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'عدم درج مطلب در وب سایت اینترنتی ', N'<p><span style="font-size:16px"><span style="font-family:b yekan">قابل توجه مسئولین فناوری اطلاعات ادارات </span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan">با سلام و روز بخیر<br />
لطفا از ساعت <span style="color:#B22222">12</span> لغایت <span style="color:#B22222">16:00</span> امروز (<span style="color:#B22222"> شنبه 96.03.06</span> ) <span style="color:#B22222">مطلب جدید</span> در وب سایت اینترنتی درج <span style="color:#B22222">نگردد</span>.</span></span><br />
&nbsp;</p>
', NULL, N'1396/03/06', N'11:24     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(209 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'اعلام وصول کارت های ملی هوشمند', N'<p><span style="font-size:14px"><span style="font-family:tahoma,geneva,sans-serif"><span style="color:#000080">قابل توجه مسئولین محترم کارت هوشمند ملی ادارات </span><br />
با سلام و روز بخیر </span></span></p>

<p><span style="font-size:14px"><span style="font-family:tahoma,geneva,sans-serif">با عنایت به رفع نقص تحویل کارت های هوشمند ملی،<span style="color:#008080"> لطفا کارت های&nbsp; وصولی، اعلام وصول گردند.</span></span></span></p>
', NULL, N'1396/03/16', N'07:53     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(210 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'شروع فاز 2 تعیین تکلیف اسناد وفات', N'<p><span style="font-size:24px"><span style="font-family:b titr">قابل توجه روسای محترم ادارات</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b yagut">با سلام و روز بخیر</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b yagut">به اطلاع می رساند با توجه به تعریف کدهای کاربری (اعلام شده از سوی ادارات با عنوان کاربر) سطح 2 در سامانه تعیین تکلیف اسناد وفات لازم است هرچه سریعتر نسبت به تعیین وضعیت&nbsp;اسناد وفات باقیمانده اقدام نمایند.</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b yagut">در ضمن همکاران محترم میتوانند از لینک&nbsp;<a href="http://deathverif.nocr.org"><span style="color:#FF0000">http://deathverif.nocr.org</span></a>&nbsp;جهت ورود به سامانه استفاده کنند.</span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yagut">جهت راهنمایی بیشتر از لینکهای زیر استفاده کنید</span></span></p>

<p><a href="http://10.1.20.29/info/death/rahnamaye_fot.htm"><strong>راهنمای کاربری سامانه پالایش اسناد وفات</strong></a></p>

<p><a href="http://10.1.20.29/deathrep/Help.htm"><strong>راهنمای گزارش گیری سامانه پالایش اسناد وفات</strong></a></p>
', NULL, N'1396/03/16', N'11:06     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(211 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'متن پیامک نظر سنجی سازمان از ارباب رجوع', N'<p><span style="font-size:14px"><span style="font-family:b yekan"><span style="color:#800080">قابل توجه مسئولین فناوری اطلاعات ادارات </span></span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">با سلام و روز بخیر </span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">پیرو ارسال نامه نظر سنجی سازمان، به اطلاع می رساند روش ارسال پیامک به دریافت کنندگان خدمت، به صورت زیر می باشد:</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">1- در ابتدا پیامکی با محتوای ذیل به ارباب رجوع ارسال می گردد.</span></span></p>

<p><span style="color:#008000"><span style="font-size:14px"><span style="font-family:b yekan">نظر شما در خصوص خدمات ثبت احوال چیست ؟</span></span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan"><span style="color:#008000">1-راضی&nbsp;&nbsp;&nbsp;&nbsp; 2-متوسط&nbsp;&nbsp;&nbsp; 3- عدم رضایت</span></span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan"><span style="color:#B22222">2-درصورت انتخاب گزینه 3 توسط ارباب رجوع، </span>پیامکی با محتوای ذیل مجددا به وی ارسال می گردد:</span></span></p>

<p><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">علت عدم رضایت :</span></span></span></p>

<p><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">کد 11: تاخیر در انجام کار</span></span></span></p>

<p><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">کد 12: برخورد نامناسب </span></span></span></p>

<p><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">کد 13: عدم رضایت از نظم و انضباط اداری </span></span></span></p>

<p><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">کد 14: پاسخگو نبودن </span></span></span></p>

<p><span style="color:#000080"><span style="font-size:14px"><span style="font-family:b yekan">کد 15: عدم راهنمایی درست </span></span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan"><span style="color:#000080">کد 16: سایر موارد.</span></span></span></p>
', NULL, N'1396/04/26', N'13:16     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(212 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'به روز رسانی سامانه صدور شناسنامه بزرگسال', N'<p><span style="font-size:22px"><span style="font-family:b titr">قابل توجه روسای محترم ادارات</span></span></p>

<p><span style="font-size:22px"><span style="font-family:b titr">با سلام و روز بخیر</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b traffic">به اطلاع می رساند با توجه به بروزرسانی سامانه صدور شناسنامه بزرگسال (ABC) هرچه سریعتر نسبت به <span style="color:#FF0000">ارتقاء نسخه جدید نرم افزار فایر فاکس</span> از طریق لینک زیر اقدام نمایند.</span></span></p>
', N'Mozilla.Firefox.v52.0.2.x86.exe', N'1396/05/04', N'09:23     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(213 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'راهنمای رفع خطای پرینترهای چاپ شناسنامه بزرگسال جدید', N'<p><span style="font-size:18px"><span style="font-family:b yekan">قابل توجه مسئولین محترم فناوری اطلاعات ادارات<br />
&nbsp;به منظور <span style="color:#FF0000">رفع خطای پرینترها </span>و همچنین کاهش مصرف جوهر، لطفاً از <span style="color:#FF0000">فایل راهنمای پیوست</span>، استفاده گردد.</span></span></p>
', N'Printer Errors.pdf', N'1396/05/09', N'09:08     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(214 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'شروع فاز 2 تعیین تکلیف اسناد وفات', N'<p><span style="font-family:b titr"><strong><span style="font-size:20px">قابل توجه روسای محترم ادارات</span></strong></span></p>

<p><span style="font-family:b titr"><strong><span style="font-size:20px">با سلام و روز بخیر</span></strong></span></p>

<p><span style="font-size:20px"><span style="font-family:b yagut">به اطلاع می رساند با توجه به تعریف کدهای کاربری (اعلام شده از سوی ادارات) <span style="color:#FF0000">در فاز 2 سامانه تعیین تکلیف اسناد وفات</span> لازم است هرچه سریعتر نسبت به تعیین وضعیت&nbsp;اسناد وفات باقیمانده اقدام نمایند.</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b yagut">در ضمن همکاران محترم میتوانند از لینک <a href="http://deathverif.nocr.org:8080/deathshow/login.aspx">http://deathverif.nocr.org:8080/deathshow/login.aspx</a>&nbsp;جهت ورود به سامانه استفاده کنند.</span></span></p>
', NULL, N'1396/05/09', N'13:01     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(215 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'راهنمای انتخاب گزینه های طرح تطبیق اسناد وفات فاز 2', N'<p><strong>قابل توجه روسای محترم ادارات</strong></p>

<h2 style="font-style:italic;"><span style="color:#FF0000"><strong>مهم و فوری</strong></span></h2>

<p><strong>با سلام و روز بخیر</strong></p>

<p>به اطلاع می رساند با توجه به بررسی های انجام شده در نتایج اجرای فاز 2 طرح تطبیق اسناد وفات ، متاسفانه در برخی از ادارات گزینه های نابجا برای تعیین تکلیف اسناد انتخاب گردیده است.<br />
به منظور جلوگیری از این موضوع ، این اداره کل اقدام به تهیه جدول راهنمایی به منظور انتخاب گزینه صحیح نموده&nbsp; که&nbsp; به پیوست ارسال میگردد.</p>
', N'فوت فاز 2.pdf', N'1396/05/19', N'12:51     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(216 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'تایید مدیر درخواستهای کارت هوشمند ملی ', N'<p><br />
<span style="font-size:14px"><span style="font-family:b yekan">با سلام و روز بخیر </span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">به اطلاع می رساند کلیه <span style="color:#B22222">درخواست های درانتظار تایید مدیر</span> <span style="color:#B22222">دفتر </span>که با گذشت بیش از یکماه <span style="color:#B22222">از کارتابل خارج گردیده اند</span>، مجددا <span style="color:#B22222">بارگذاری </span>شده اند. مقتضی است نسبت به تایید این&nbsp; درخواست ها اقدام عاجل صورت پذیرد.</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">واحد فناوری اطلاعات اداره کل </span></span></p>
', NULL, N'1396/06/20', N'11:12     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(217 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'تحویل کارت هوشمند به شهروندان', N'<p><span style="font-size:18px"><span style="font-family:b titr"><strong>قابل توجه روسای محترم ادارات</strong></span></span></p>

<h2><span style="color:#FF0000"><span style="font-size:36px"><span style="font-family:b traffic"><strong>مهم و فوری</strong></span></span></span></h2>

<p><span style="font-size:24px"><strong>با سلام و روز بخیر</strong></span></p>

<p><span style="font-size:18px">با توجه به ارسال مجدد پیامک برای کارتهای آماده تحویل ادارات،شایسته است ادارات آمادگی لازم برای پاسخگویی به شهروندان را داشته باشند.</span></p>
', NULL, N'1396/06/25', N'09:17     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(218 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'پیام همکار : قاسم اعظمی', N'<p><span style="background-color:#00FF00">باسلام </span></p>

<p><span style="background-color:#00FF00">باآرزوی سلامتی آقا امام زمان (عج) و مقام معظم رهبری</span></p>

<p><span style="background-color:#00FF00">یاد وخاطره شهدای کربلا</span>, شهدای هشت سال دفاع مقدس وشهدای مدافعین حرم حضرت زینب (س)</p>

<p>احتراماً با توجه به اینکه حوزه های علمیه از مراکز آموزشی و پرورشی میباشد بامراجعه طلاب علوم دینی جهت عکسدارنمودن شناسنامه با دردست داشتن معرفینامه عکسدارازحوزه ای که درآن به تحصیل اشتغال دارند ادارات ثبت احوال گواهی حوزه را منتفی و متقاضی(طلاب) را به نیروی انتظامی جهت تائید عکس راهنمائی نموده لطفاً بررسی نمائید. &nbsp;&nbsp; و من ا... التوفیق</p>
', NULL, N'1396/06/27', N'08:32     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(219 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'پیام تشکر و خداحافظی', N'<p dir="LTR" style="text-align:center"><span style="font-size:24px"><span style="font-family:b titr"><strong>&laquo;</strong><strong><span dir="RTL">بسمه تعالی</span></strong><strong>&raquo;</strong></span></span></p>

<p dir="LTR" style="text-align:center"><span style="font-size:24px"><span style="font-family:b titr"><strong><span dir="RTL">من لم یشکرالمخلوق لم یشکر الخالق</span></strong></span></span></p>

<p dir="LTR" style="text-align:right"><span style="font-size:20px"><span style="font-family:b titr"><strong><span dir="RTL">با عرض سلام و ارادت خدمت کلیه همکاران گرامی</span></strong></span></span></p>

<p dir="LTR" style="direction: rtl; text-align: justify;"><span style="font-size:20px"><span style="font-family:b nazanin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><span dir="RTL">احتراما، باعنایت به الطاف خاصه الهی نظر به اینکه درشرف بازنشستگی میباشم بدینوسیله از کلیه سرورانم که به نحوی افتخارهمکاری را درطی دوران خدمت داشته و با استفاده از تجربیات ارزشمندشان این مسیر را با موفقیت پشت سر گذاشته ام، ضمن خداحافظی طلب حلالیت را از محضر ایشان استدعا دارم . امیدوارم در مراحل زندگی همواره موفق وسربلند باشید.</span>&nbsp;</strong></span></span></p>

<p dir="LTR" style="direction: rtl; text-align: justify;">&nbsp;</p>

<p dir="LTR" style="direction: rtl; text-align: center;"><span style="font-size:20px"><span style="font-family:b zar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><span dir="RTL">ارادتمند شما </span></strong>&nbsp;</span></span></p>

<p dir="LTR" style="direction: rtl; text-align: center;"><span style="font-size:20px"><span style="font-family:b zar">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong><span dir="RTL">فرهاد امانی</span></strong></span></span></p>

<p dir="RTL" style="direction: rtl; text-align: center;">&nbsp;</p>
', NULL, N'1396/07/01', N'11:56     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(220 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'آپدیت آنتی ویروس سیستم ها', N'<h1><span style="color:#FF0000"><strong><span style="font-family:b zar">اقدام فوری </span></strong></span></h1>

<p><span style="font-size:18px"><span style="font-family:b yekan"><span style="color:#800000">قابل توجه مسئولین فناوری اطلاعات ادارات </span><br />
با سلام و روز بخیر </span></span></p>

<p><span style="font-size:18px"><span style="font-family:b yekan">به منظور <span style="color:#B22222">دریافت آخرین آپدیت</span> آنتی ویروس، لطفا <span style="color:#B22222">کلیه سیستم های</span> اداره <span style="color:#B22222">روشن </span>گردند.</span></span></p>
', NULL, N'1396/07/12', N'10:14     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(221 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'تعیین تکلیف اسناد وفات فاز 2', N'<p><br />
<span style="font-size:24px"><span style="font-family:b titr">قابل توجه روسای محترم ادارات</span></span></p>

<p><br />
<span style="font-size:20px"><span style="color:#FF0000"><span style="font-family:b titr">مهم و فوری</span></span></span></p>

<p><br />
<span style="font-size:20px"><strong><span style="font-family:b zar">با سلام و روز بخیر</span></strong></span></p>

<p style="text-align: justify;"><span style="font-family:b yagut"><strong><span style="font-size:18px">به اطلاع می رساند با توجه به بررسی های انجام شده در نتایج اجرای فاز 2 طرح تطبیق اسناد وفات ،&nbsp;تعدادی از اسناد مجددا&quot; به کارتابل ادارات عودت داده شده است،&nbsp;جهت تعیین تکلیف این اسناد همانند جدول راهنمای قبل اقدام گردد و در صورتی که هیچ گزینه مناسبی جهت تعیین تکلیف سند وجود نداشت از <span style="color:#FF8C00">گزینه فاقد سندثبت کل وقایع</span> استفاده شود.</span></strong></span></p>
', NULL, N'1396/08/06', N'08:17     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(222 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'آمادگی در پاسخگویی به مراجعات مردمی جهت دریافت کارت هوشمند', N'<p><span style="color:#FF0000"><span style="font-size:22px"><span style="font-family:b titr">قابل توجه روسای محترم ادارات</span></span></span></p>

<p><br />
<span style="font-size:22px"><span style="font-family:b nazanin"><strong>با سلام و روز بخیر</strong></span></span></p>

<p><span style="font-family:b yekan"><span style="font-size:22px"><strong>به اطلاع می رساند با توجه ارسال پیامک برای متقاضیانی که کارت&nbsp;هوشمند ملی آنها صادر شده و هنوز اقدامی برای دریافت آن نکرده اند لذا روسای محترم ادارات با برنامه ریزی مناسب آمادگی پاسخگویی برای مراجعات مردمی را داشته باشند.</strong></span></span></p>
', NULL, N'1396/08/21', N'13:17     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(223 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'بروز رسانی آنتی ویروس سیستم ها', N'<h1><span style="color:#FF0000"><strong><span style="font-family:b zar">اقدام فوری </span></strong></span></h1>

<p><span style="font-size:18px"><span style="font-family:b yekan"><span style="color:#800000">قابل توجه مسئولین فناوری اطلاعات ادارات </span><br />
با سلام و روز بخیر </span></span></p>

<p><span style="font-size:18px"><span style="font-family:b yekan">لطفا <span style="color:#FF0000">کلیه سیستم های </span>اداره <span style="color:#FF0000">روشن&nbsp; </span>و گزینه <span style="color:#FF0000">Update Now</span>&nbsp; آنتی ویروس زده شود تا&nbsp;<span style="color:#FF0000"> آخرین آپدیت</span> آنتی ویروس دریافت گردد.</span></span><br />
<span style="font-size:18px"><span style="font-family:b yekan"><span style="color:#FF0000">مسئولیت عواقب عدم بروز بودن سیستم ها بعهده مسئول فناوری اطلاعات اداره&nbsp; خواهد بود. </span></span></span></p>
', NULL, N'1396/09/04', N'10:20     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(224 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'اقدامات پیشگیرانه در برابر باج افزار wannacrypt', N'<p><span style="font-size:24px"><span style="font-family:b titr">قابل توجه مسئولین محترم فن آوری اطلاعات ادارات</span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yagut">با سلام و روز بخیر</span></span></p>

<p><span style="font-family:b yagut"><span style="font-size:18px">به اطلاع می رساند با توجه به حمله گسترده باج افزار <span style="color:#FF0000">wannacrypt </span>به سیستم های رایانه ای در سراسر جهان لازم است هرچه سریعتر نسبت به نصب وصله های ویندوز بر روی تمامی سیستمهای رایانه ای ادارات اقدام گردد، همچنین به کاربران توصیه میگردد هرگز از حافظه های قابل حمل مانند فلش دیسک،هارد دیسک اکسترنال،دی وی دی و غیره استفاده نگردد.</span></span></p>

<p><span style="font-family:b yagut"><span style="font-size:18px">در ضمن وصله های به روز رسانی ویندوز برای <span style="color:#FF0000">ویندوزهای اکس پی و سون</span> در فایل پیوست موجود میباشد. در صورت بروز هرگونه مشکل در نصب وصله ها با واحد فن آوری اطلاعات اداره کل تماس حاصل فرمائید</span></span></p>
', N'path.rar', N'1396/09/09', N'11:23     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(225 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'اعلام شماره تلفن خط adsl', N'<p><span style="font-size:18px"><span style="font-family:b yekan"><strong>قابل توجه مسئولین محترم فناوری اطلاعات ادارات </strong><br />
با سلام و روز بخیر<br />
&nbsp;به منظور بهبود وضعیت خط اینترنت ادارات لطفا <span style="color:rgb(255, 0, 0)">شماره خط تلفن ADSL</span> اداره <span style="color:rgb(255, 0, 0)">بصورت تلفنی</span> به بنده اطلاع داده شود.</span><br />
<span style="font-family:b yekan">با سپاس-پناهی</span></span></p>
', NULL, N'1396/09/25', N'12:25     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(226 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), N'پیام همکار : نادر ابراهیمی', N'<p>با سلام واحترام</p>

<h1>سوم دی روز ملی ثبت احوال به&nbsp; مدیر کل محترم جناب علی پور و جناب فرزام - جناب توحیدی- معاونین محترم اداره کل وتمامی همکاران محترم در سطح استان تبریک عرض نموده وبخاطر کادو این روز فرخنده&nbsp; اینجانب به نمایندگی از همکاران اسد آبادی از مدیر کل محترم&nbsp; جناب عل پور تشکر می کنیم. از ایزد منان آرزوی توفیق وسلامتی برای همگان را دارم. ایام بکام. خدا نگهدار. نادراهیمی -اسدآباد96/10/7</h1>
', NULL, N'1396/10/07', N'08:50     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(227 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'تعیین تکلیف اسناد وفات فاز 3', N'<p><span style="font-size:22px"><span style="font-family:b titr">قابل توجه روسای محترم ادارات</span></span></p>

<p><br />
<span style="color:#FF0000"><span style="font-size:28px"><span style="font-family:b yagut">مهم و فوری</span></span></span></p>

<p><br />
<span style="font-size:22px"><span style="font-family:b nazanin"><strong>با سلام و روز بخیر</strong></span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin"><strong>به اطلاع می رساند فاز 3&nbsp; اسناد وفات جهت تعیین تکلیف طبق دستور العملی که به&nbsp;شماره&nbsp;6727/2/36 مورخ 1396/09/08 &nbsp;به آن اداره ابلاغ گردیده است، از طریق این <a href="http://deathverif.nocr.org/phase2"><span style="color:#FF0000"><span style="background-color:#FFFF00">لینک</span></span></a> قابل اجرا میباشد.</strong></span></span></p>

<p>&nbsp;</p>

<p><a href="http://deathverif.nocr.org/phase2"><span style="color:#FF0000"><span style="font-size:22px"><span style="font-family:b nazanin"><strong>لینک ورود به سامانه</strong></span></span></span></a></p>

<p><a href="http://10.1.20.29/info/tools/phase3.htm"><span style="color:#FF0000"><span style="font-size:22px"><span style="font-family:b nazanin"><strong>نحوه استفاده از سامانه</strong></span></span></span></a></p>
', NULL, N'1396/10/16', N'08:50     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(228 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'قطعی اتوماسیون', N'<p><span style="font-size:26px"><span style="font-family:b titr">قابل توجه همکاران محترم&nbsp;</span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin"><strong>با سلام و روز بخیر</strong></span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin"><strong>بدلیل پاره ای تغییرات سامانه اتوماسیون در روز <span style="color:#FF0000">دوشنبه مورخ 1396/11/2 از ساعت 14 بعد ازظهر به مدت 3 ساعت</span> قطع میباشد.</strong></span></span></p>
', NULL, N'1396/11/01', N'10:33     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(229 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'آدرس جدید اتوماسیون برید', N'<p><span style="color:#FF0000"><strong><span style="font-size:18px">قابل توجه همکاران محترم</span></strong></span></p>

<p><span style="font-family:b titr"><span style="font-size:18px"><strong>با سلام و احترام</strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b lotus"><strong>ضمن عرض تبریک به مناسبت فرا رسیدن سال نو به همه همکاران عزیز آدرس جدید سامانه اتوماسیون اداری <a href="http://auto.hm">auto.hm</a> میباشد در ضمن همکاران محترم توجه داشته باشند که پس از اولین ورود به سامانه کلمه عبور خود را تغییر دهند.</strong><br />
در ضمن نحوه تغییر&nbsp;کلمه عبور در پیوست ضمیمه میباشد.</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b lotus">لینک نرم افزار اسکنر برای کاربران دبیرخانه دانلود از <a href="http://auto.hm/fonix/Mailbox/PargarClient-2.0.0.zip">اینجا</a></span></span></p>

<p><span style="font-size:18px">نسخه های 32 بیتی و 64 بیتی مرورگر کروم نیز در ftp شماره 111 پوشه اخلاص قرار داده شد. دانلود از <a href="ftp://ftp.hm/%C7%CE%E1%C7%D5/">اینجا</a></span></p>
', N'1.JPG', N'1396/11/10', N'09:34     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(230 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'آمادگی در پاسخگویی به مراجعات مردمی جهت دریافت کارت هوشمند', N'<p><span style="font-size:22px"><span style="font-family:b titr">قابل توجه روسای محترم ادارات</span></span></p>

<p><br />
<span style="font-size:18px"><span style="font-family:b lotus"><strong>با سلام و روز بخیر</strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b lotus"><strong>به اطلاع می رساند با توجه <span style="color:#FF0000">ارسال پیامک برای متقاضیانی که کارت&nbsp;هوشمند ملی آنها صادر شده</span> و هنوز اقدامی برای دریافت آن نکرده اند لذا روسای محترم ادارات با برنامه ریزی مناسب آمادگی پاسخگویی برای مراجعات مردمی را داشته باشند.</strong></span></span></p>
', NULL, N'1396/11/25', N'08:40     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(231 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'اتوماسیون', N'<p><span style="color:#FF0000"><strong><span style="font-size:18px">قابل توجه همکاران محترم</span></strong></span></p>

<p><span style="font-family:b titr"><span style="font-size:18px"><strong>با سلام و احترام</strong></span></span></p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="font-size:18px"><span style="font-family:b lotus"><strong>با توجه به ارتقاء سامانه اتوماسیون اداری برید این سامانه از ساعت 9:45 به مدت 1 ساعت از دسترس خارج میشود</strong></span></span>.</p>
', NULL, N'1397/02/11', N'09:36     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(232 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'روشن نمودن کلیه سیستم های اداره ', N'<p><span style="font-size:22px"><span style="color:#FF0000"><span style="font-family:b titr">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;اقدام فوری&nbsp;</span></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b titr">قابل توجه مسئولین محترم فناوری اطلاعات ادارات</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b titr">با سلام و روز بخیر&nbsp;</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b titr">به منظور <span style="color:#FF0000">دریافت </span>آخرین<span style="color:#FF0000"> آپدیت آنتی ویروس</span>، لطفا کلیه سیستم های ادارات<span style="color:#FF0000"> فورا روشن گردند</span>.عواقب ناشی از آلوده شدن سیستم ها به عهده مسئولین فناوری اطلاعات ادارات خواهد بود.</span></span></p>

<p>&nbsp;</p>

<p><span style="font-size:18px"><span style="font-family:b titr">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; با سپاس</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b titr">واحد فناوری اطلاعات اداره کل</span></span></p>
', NULL, N'1397/03/09', N'08:22     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(233 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(101 AS Numeric(18, 0)), N'پیام همکار : محمود قره باغی برهمن', N'<p><span style="font-size:20px"><strong>با سلام وخسته نباشید </strong></span></p>

<p><span style="font-size:20px"><strong>چند ماهی است که پیامهای همکاران بروز رسانی&nbsp; وبار گزاری نمیشود در صورت امکان بررسی شود </strong></span></p>

<p><span style="font-size:20px"><strong>با تشکر</strong></span></p>
', NULL, N'1397/04/06', N'21:06     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(234 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'حذف فایلهای موجود در FTP', N'<p><span style="color:#B22222"><span style="font-size:14px"><span style="font-family:b titr">قابل توجه مسئولین فناوری اطلاعات و همکاران محترم ادارات </span></span></span><br />
<span style="font-size:16px"><span style="font-family:b yekan">با سلام و وقت بخیر </span></span></p>

<p><span style="font-size:16px"><span style="font-family:b yekan">از آنجاییکه <span style="color:rgb(178, 34, 34)">سامانه FTP </span>به منظور<span style="color:rgb(0, 100, 0)"> تبادل اطلاعات</span> بین همکاران ایجاد گردیده است و <span style="color:rgb(178, 34, 34)">نه بایگانی</span> و آرشیو اطلاعات، لذا مقتضی است همکاران محترم تا <span style="color:rgb(255, 0, 0)">تاریخ 1397.04.10</span> نسبت به <span style="color:rgb(178, 34, 34)">پاکسازی اطلاعات خود در FTP</span>اداره اقدام نمایند. شایان ذکر است در تاریخ <span style="color:rgb(178, 34, 34)">1397.04.11 </span>کلیه اطلاعات از سامانه<span style="color:rgb(178, 34, 34)"> FTPپاک خواهد شد.</span></span></span></p>
', NULL, N'1397/04/07', N'20:52     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(235 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'نصب CCOS جدید', N'<p><span style="font-size:16px"><span style="font-family:b titr"><strong><span style="color:#800000">قابل توجه مسئولین فناوری اطلاعات ادارات </span></strong></span></span></p>

<p><strong><span style="font-family:b yekan">با سلام و&nbsp; روز بخیر </span></strong></p>

<p><strong><span style="font-family:b yekan">لطفا به منظور بهره مندی از نرم افزار CCOS با<span style="color:#B22222"> آپدیت جدید</span>، اقدامات ذیل انجام گردد.</span></strong></p>

<p><strong><span style="font-family:b yekan"><span style="color:#B22222">1-</span>حذف کامل CCOS&nbsp; قدیم از کنترل پنل </span></strong></p>

<p><strong><span style="font-family:b yekan"><span style="color:#800000">2- </span>حذف پوشه <span style="color:#B22222">2.0</span> در مسیر زیر</span></strong></p>

<p><strong><span style="font-family:b yekan"><span style="color:rgb(0, 100, 0)">C:\Users\</span><span style="color:#FF0000">usersystemshoma</span><span style="color:rgb(0, 100, 0)">\AppData\Local\Apps</span></span></strong></p>

<p><strong><span style="font-family:b yekan"><span style="color:#800000">3-</span>نصب ccos&nbsp; از مسیر زیر<br />
<span style="color:rgb(0, 100, 0)">http://ccos.ssd.net/ccos-update-2/publish.htm</span></span></strong></p>

<p><span style="font-family:b titr"><span style="color:rgb(255, 0, 0)"><strong>توجه : </strong></span><span style="color:#000000"><strong>منظور از </strong></span><span style="color:#FF0000"><strong>usersystemshoma </strong></span><span style="color:#000000"><strong>همان</strong></span><span style="color:#FF0000"><strong> نام کاربری</strong></span><span style="color:#000000"><strong> است که در سیستم شما استفاده شده است.</strong></span></span></p>

<p><strong><span style="font-family:b yekan">لطفا واحد IT اداره کل را از نتیجه اقدامات مطلع نمایید.</span></strong></p>
', NULL, N'1397/04/30', N'18:00     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(236 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'ارائه آمار فاز 4 اسناد وفات', N'<p><span style="font-size:16px"><strong><span style="font-family:b nazanin">با سلام</span></strong></span></p>

<p><span style="font-size:16px"><strong><span style="font-family:b nazanin">در اجرای فعالیت شماره 4-4-2 طرح تحرک گزارش فعالیت ادارات استان به شرح ذیل اعلام می گردد.</span></strong></span></p>

<table border="2" cellpadding="0" cellspacing="0" style="width:470px">
	<tbody>
		<tr>
			<td colspan="7" dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">گزارش فاز چهار ،تعيين تکليف اسناد وفات زنده در پايگاه&nbsp;</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">استان</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">کد اداره</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">اداره</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">تعداد کل</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1397/05/15</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1397/05/16</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">پیشرفت</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">897</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">شراءوپيشخوار</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">890</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">كبودرآهنگ</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">894</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">896</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">قلقل رود</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">339</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">887</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">بهار</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">598</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">888</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">تويسركان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">349</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">893</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">فامنين</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">12</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">886</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">اسدآباد</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">513</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">786</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">92/15%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">892</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">نهاوند</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">3</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">168</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">16/17%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">889</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">رزن</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1494</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">65</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">116</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">7/76%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">891</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">ملاير</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">4</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">21</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1/46%</span></strong></span></td>
		</tr>
	</tbody>
</table>
', N'systems_specifications.xlsx', N'1397/05/09', N'21:31     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(237 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(159 AS Numeric(18, 0)), N'دریافت آخرین آپدیت آنتی ویروس', N'<p style="text-align:center"><span style="font-size:28px"><span style="font-family:b nazanin"><span style="background-color:#FF0000">&nbsp;اقدام فوری&nbsp;</span></span></span></p>

<p style="text-align:center"><span style="font-size:22px"><span style="color:#0000CD"><span style="font-family:b nazanin">قابل توجه مسئولین محترم فناوری اطلاعات ادارات</span></span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin">با سلام و روز بخیر&nbsp;</span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin">به منظور&nbsp;دریافت&nbsp;آخرین&nbsp;آپدیت آنتی ویروس، لطفا <span style="color:#FF0000">کلیه سیستم های ادارات</span>&nbsp;فورا روشن گردند.&nbsp;عواقب ناشی از آلوده شدن سیستم ها به عهده مسئولین فناوری اطلاعات ادارات خواهد بود.</span></span></p>

<p style="margin-right: 360px; text-align: center;"><span style="font-size:22px"><span style="color:#FF0000"><span style="font-family:b nazanin">با سپاس</span></span></span></p>

<p style="margin-right: 360px; text-align: center;"><span style="font-size:22px"><span style="color:#FF0000"><span style="font-family:b nazanin">واحد فناوری اطلاعات اداره کل</span></span></span></p>
', NULL, N'1397/05/14', N'20:02     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(238 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'ارائه گزارش فاز 4 اسناد وفات', N'<p style="text-align:center"><span style="font-size:18px"><span style="font-family:b titr">قابل توجه رؤسای محترم ادارات</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>با سلام و احترام</strong></span></span></p>

<p style="margin-right:40px"><span style="font-size:18px"><span style="font-family:b nazanin"><strong>در اجرای فعالیت شماره 4-4-2 طرح تحرک، گزارش فعالیت ادارات استان به شرح ذیل اعلام می گردد.</strong></span></span></p>

<table border="2" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="7" dir="RTL" style="text-align:center"><span style="font-size:18px"><span style="font-family:b nazanin"><strong>گزارش فاز چهار ،تعيين تکليف اسناد وفات زنده در پايگاه&nbsp;</strong></span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:18px"><span style="font-family:b nazanin"><strong>استان</strong></span></span></td>
			<td dir="RTL" style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:18px"><span style="font-family:b nazanin"><strong>کد اداره</strong></span></span></td>
			<td dir="RTL" style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:18px"><span style="font-family:b nazanin"><strong>اداره</strong></span></span></td>
			<td dir="RTL" style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:18px"><span style="font-family:b nazanin"><strong>تعداد کل</strong></span></span></td>
			<td dir="RTL" style="background-color:rgb(204, 204, 204); text-align:center">
			<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>انجام شده در</strong></span></span></p>

			<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>1397/05/15</strong></span></span></p>
			</td>
			<td dir="RTL" style="background-color:rgb(204, 204, 204); text-align:center">
			<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>انجام شده در</strong></span></span></p>

			<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>1397/05/16</strong></span></span></p>
			</td>
			<td dir="RTL" style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:18px"><span style="font-family:b nazanin"><strong>پیشرفت</strong></span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>897</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>شراءوپيشخوار</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>249</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>249</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>249</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>100/00%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>890</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>كبودرآهنگ</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>485</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>485</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>485</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>100/00%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>894</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>1959</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>1959</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>1959</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>100/00%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>896</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>قلقل رود</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>391</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>339</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>391</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>100/00%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>887</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>بهار</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>708</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>598</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>708</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>100/00%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>888</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>تويسركان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>615</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>349</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>615</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>100/00%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>893</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>فامنين</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>261</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>12</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>261</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>100/00%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>886</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>اسدآباد</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>853</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>513</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>786</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>92/15%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>892</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>نهاوند</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>1039</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>3</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>168</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>16/17%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>889</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>رزن</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>1494</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>65</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>116</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>7/76%</strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>همدان</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>891</strong></span></span></p>
			</td>
			<td dir="RTL" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>ملاير</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>1439</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>4</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>21</strong></span></span></p>
			</td>
			<td dir="LTR" style="text-align:center">
			<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>1/46%</strong></span></span></p>
			</td>
		</tr>
	</tbody>
</table>
', NULL, N'1397/05/16', N'18:50     ', 0)
GO
print 'Processed 200 total records'
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(239 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'گزارش عملکرد تعیین تکلیف اسناد وفات فاز 4', N'<p><span style="font-size:18px"><span style="font-family:b titr">قابل توجه رؤسای محترم ادارات</span></span></p>

<p><span style="font-size:16px"><strong><span style="font-family:b nazanin">با سلام و احترام</span></strong></span></p>

<p><span style="font-size:16px"><strong><span style="font-family:b nazanin">در اجرای فعالیت شماره 4-4-2 طرح تحرک، گزارش فعالیت ادارات استان به شرح ذیل اعلام می گردد.</span></strong></span></p>

<table border="2" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="9" dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">گزارش فاز چهار ،تعيين تکليف اسناد وفات</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">استان</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">کد اداره</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">اداره</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">تعداد کل</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1397/05/15</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1397/05/16</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1397/05/17</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1397/05/18</span></strong></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">پیشرفت</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">897</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">شراءوپيشخوار</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">890</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">كبودرآهنگ</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">894</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">896</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">قلقل رود</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">339</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">887</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">بهار</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">598</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">888</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">تويسركان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">349</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">893</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">فامنين</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">12</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">886</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">اسدآباد</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">513</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">786</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">816</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">842</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">98/71%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">892</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">نهاوند</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">3</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">168</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">349</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">562</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">54/09%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">891</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">ملاير</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">4</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">21</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">274</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">678</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">47/12%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">889</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">رزن</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1494</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">65</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">116</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">216</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">348</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">23/29%</span></strong></span></td>
		</tr>
	</tbody>
</table>
', NULL, N'1397/05/18', N'18:35     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(242 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'گزارش عملکرد تعیین تکلیف اسناد فوات فاز 4', N'<p><span style="font-size:18px"><span style="font-family:b titr">قابل توجه رؤسای محترم ادارات</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>با سلام و احترام</strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>در اجرای فعالیت شماره 4-4-2 طرح تحرک، گزارش فعالیت ادارات استان به شرح ذیل اعلام می گردد.</strong></span></span></p>

<table border="2" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="10" dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">گزارش فاز چهار ،تعيين تکليف اسناد وفات زنده در پايگاه&nbsp;</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">استان</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">کد اداره</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">اداره</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">تعداد کل</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">1397/05/15</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">1397/05/16</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">1397/05/17</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">1397/05/18</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">1397/05/20</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:14px"><span style="font-family:b titr">پیشرفت</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">897</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">شراءوپيشخوار</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">890</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">كبودرآهنگ</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">894</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">896</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">قلقل رود</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">339</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">887</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">بهار</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">598</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">888</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">تويسركان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">349</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">893</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">فامنين</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">12</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">886</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">اسدآباد</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">513</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">786</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">816</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">842</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">892</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">نهاوند</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">1039</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">3</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">168</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">349</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">562</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">68/14%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">891</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">ملاير</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">1439</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">4</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">21</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">274</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">678</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">849</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">59/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">889</span></span></strong></td>
			<td dir="RTL" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">رزن</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">1494</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">65</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">116</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">216</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">348</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">506</span></span></strong></td>
			<td dir="LTR" style="text-align:center"><strong><span style="font-size:16px"><span style="font-family:b nazanin">33/87%</span></span></strong></td>
		</tr>
	</tbody>
</table>
', NULL, N'1397/05/20', N'17:02     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(244 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'تغیرات در آدرس FTP سازمان', N'<p><span style="font-size:16px"><span style="font-family:b titr">قابل توجه مسئولین محترم فن آوری</span></span></p>

<p><strong><span style="font-size:16px"><span style="font-family:b nazanin">با توجه به تغییر آدرس FTP سازمان از archiveftp2.nocr.org به archiveftp.nocr.org لازم است در سامانه هایی که از این بستر برای ارسال و دریافت اطلاعات استفاده میکنندنیز تغییر یابد.</span></span></strong></p>

<p><strong><span style="font-size:16px"><span style="font-family:b nazanin">1- سامانه ارسال ضمائم(ATTMAN):</span></span></strong></p>

<p><strong><span style="font-size:16px"><span style="font-family:b nazanin">از منوی تنظیمات --&gt; تعریف ادارات --&gt; آدرس سرور به روز رسانی شود</span></span></strong></p>

<p>&nbsp;</p>

<p><img alt="" src="http://info.hm/attnews/1.jpg" style="height:600px; width:800px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="" src="http://info.hm/attnews/2.jpg" style="height:600px; width:800px" /></p>
', NULL, N'1397/05/20', N'18:40     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(246 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'گزارش عملکرد تعیین تکلیف اسناد فوات فاز 4', N'<p><span style="font-size:16px"><span style="font-family:b titr">قابل توجه رؤسای محترم ادارات</span></span></p>

<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>با سلام و احترام</strong></span></span></p>

<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>در اجرای فعالیت شماره 4-4-2 طرح تحرک، گزارش فعالیت ادارات استان به شرح ذیل اعلام می گردد.</strong></span></span></p>

<table border="2" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="11" dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">گزارش فاز چهار ،تعيين تکليف اسناد وفات زنده در پايگاه&nbsp;</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">استان</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">کد اداره</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">اداره</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">تعداد کل</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">1397/05/15</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">1397/05/16</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">1397/05/17</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">1397/05/18</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">1397/05/20</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">1397/05/21</span></span></td>
			<td dir="RTL" style="background-color:rgb(221, 221, 221); text-align:center"><span style="font-size:16px"><span style="font-family:b titr">پیشرفت</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">897</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">شراءوپيشخوار</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">890</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">كبودرآهنگ</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">894</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">896</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">قلقل رود</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">339</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">887</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">بهار</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">598</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">888</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">تويسركان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">349</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">893</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">فامنين</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">12</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">886</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">اسدآباد</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">513</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">786</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">816</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">842</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">892</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">نهاوند</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">3</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">168</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">349</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">562</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">885</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">85/18%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">891</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">ملاير</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">4</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">21</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">274</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">678</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">849</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">938</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">65/18%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">889</span></strong></span></td>
			<td dir="RTL" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">رزن</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1494</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">65</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">116</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">216</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">348</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">506</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">663</span></strong></span></td>
			<td dir="LTR" style="text-align:center"><span style="font-size:16px"><strong><span style="font-family:b nazanin">44/38%</span></strong></span></td>
		</tr>
	</tbody>
</table>
', NULL, N'1397/05/21', N'16:33     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(247 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'گزارش عملکرد تعیین تکلیف اسناد فوات فاز 4', N'<p><span style="font-size:18px"><span style="font-family:b titr">قابل توجه رؤسای محترم ادارات</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>با سلام و احترام</strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>در اجرای فعالیت شماره 4-4-2 طرح تحرک، گزارش فعالیت ادارات استان به شرح ذیل اعلام می گردد.</strong></span></span></p>

<table border="2" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="12" dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">گزارش فاز چهار ،تعيين تکليف اسناد وفات زنده در پايگاه&nbsp;</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">استان</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">کد اداره</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">اداره</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">تعداد کل</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/15</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/16</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/17</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/18</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/20</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/21</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/22</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">پیشرفت</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">897</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">شراءوپيشخوار</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">890</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">كبودرآهنگ</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">894</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">896</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">قلقل رود</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">339</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">887</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">بهار</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">598</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">888</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">تويسركان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">349</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">893</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">فامنين</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">12</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">886</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">اسدآباد</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">513</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">786</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">816</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">842</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">892</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">نهاوند</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1039</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">3</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">168</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">349</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">562</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">885</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1039</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">891</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">ملاير</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1439</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">4</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">21</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">274</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">678</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">849</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">938</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1276</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">88/67%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">889</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">رزن</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1494</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">65</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">116</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">216</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">348</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">506</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">663</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">830</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">55/56%</span></span></strong></td>
		</tr>
	</tbody>
</table>
', NULL, N'1397/05/22', N'17:17     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(249 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'گزارش عملکرد تعیین تکلیف اسناد فوات فاز 4', N'<p><span style="font-size:18px"><span style="font-family:b titr">قابل توجه رؤسای محترم ادارات</span></span></p>

<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>با سلام و احترام</strong></span></span></p>

<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>در اجرای فعالیت شماره 4-4-2 طرح تحرک، گزارش فعالیت ادارات استان به شرح ذیل اعلام می گردد.</strong></span></span></p>

<table border="2" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="13" dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">گزارش فاز چهار ،تعيين تکليف اسناد وفات زنده در پايگاه&nbsp;</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">استان</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">کد اداره</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">اداره</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">تعداد کل</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/15</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/16</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/17</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/18</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/20</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/21</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/22</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/23</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">پیشرفت</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">897</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">شراءوپيشخوار</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">249</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">890</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">كبودرآهنگ</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">485</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">894</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1959</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">896</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">قلقل رود</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">339</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">391</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">887</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">بهار</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">598</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">888</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">تويسركان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">349</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">615</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">893</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">فامنين</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">12</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">261</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">886</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">اسدآباد</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">513</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">786</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">816</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">842</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">853</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">892</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">نهاوند</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1039</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">3</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">168</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">349</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">562</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">708</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">885</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1039</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1039</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">891</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">ملاير</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1439</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">4</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">21</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">274</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">678</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">849</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">938</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1276</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1439</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">100/00%</span></span></strong></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">همدان</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">889</span></span></strong></td>
			<td dir="RTL" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">رزن</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">1494</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">65</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">116</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">216</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">348</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">506</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">663</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">830</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">995</span></span></strong></td>
			<td dir="LTR" style="text-align: center;"><strong><span style="font-size:14px"><span style="font-family:b nazanin">66/60%</span></span></strong></td>
		</tr>
	</tbody>
</table>
', NULL, N'1397/05/23', N'18:30     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(250 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'گزارش عملکرد تعیین تکلیف اسناد فوات فاز 4', N'<p><span style="font-size:18px"><span style="font-family:b titr">قابل توجه رؤسای محترم ادارات</span></span></p>

<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>با سلام و احترام</strong></span></span></p>

<p><span style="font-size:16px"><span style="font-family:b nazanin"><strong>در اجرای فعالیت شماره 4-4-2 طرح تحرک، گزارش فعالیت ادارات استان به شرح ذیل اعلام می گردد.</strong></span></span></p>

<table border="2" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="10" dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">گزارش فاز چهار ،تعيين تکليف اسناد وفات زنده در پايگاه&nbsp;</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">استان</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">کد اداره</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">اداره</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">تعداد کل</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/20</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/21</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/22</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/23</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/24</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">پیشرفت</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">897</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">شراءوپيشخوار</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">890</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">كبودرآهنگ</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">894</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">896</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">قلقل رود</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">887</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">بهار</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">888</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">تويسركان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">893</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">فامنين</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">886</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">اسدآباد</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">892</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">نهاوند</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">885</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">891</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">ملاير</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">849</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">938</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1276</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">889</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">رزن</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1494</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">506</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">663</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">830</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">995</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1217</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">81/46%</span></strong></span></td>
		</tr>
	</tbody>
</table>
', NULL, N'1397/05/24', N'17:55     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(251 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'گزارش عملکرد تعیین تکلیف اسناد فوات فاز 4', N'<p><span style="font-size:18px"><span style="font-family:b titr">قابل توجه رؤسای محترم ادارات</span></span></p>

<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>با سلام و احترام</strong></span></span></p>

<p><span style="font-size:18px"><span style="font-family:b nazanin"><strong>در اجرای فعالیت شماره 4-4-2 طرح تحرک، گزارش فعالیت ادارات استان به شرح ذیل اعلام می گردد.</strong></span></span></p>

<table border="2" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="11" dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">گزارش فاز چهار ،تعيين تکليف اسناد وفات زنده در پايگاه&nbsp;</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">استان</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">کد اداره</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">اداره</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">تعداد کل</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/20</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/21</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/22</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/23</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/24</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">1397/05/25</span></span></td>
			<td dir="RTL" style="text-align: center; background-color: rgb(221, 221, 221);"><span style="font-size:14px"><span style="font-family:b titr">پیشرفت</span></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">897</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">شراءوپيشخوار</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">249</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">890</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">كبودرآهنگ</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">485</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">894</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1959</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">896</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">قلقل رود</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">391</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">887</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">بهار</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">888</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">تويسركان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">615</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">893</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">فامنين</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">261</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">886</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">اسدآباد</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">853</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">892</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">نهاوند</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">708</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">885</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1039</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">891</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">ملاير</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">849</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">938</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1276</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1439</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">100/00%</span></strong></span></td>
		</tr>
		<tr>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">همدان</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">889</span></strong></span></td>
			<td dir="RTL" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">رزن</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1494</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">506</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">663</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">830</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">995</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1217</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">1368</span></strong></span></td>
			<td dir="LTR" style="text-align: center;"><span style="font-size:16px"><strong><span style="font-family:b nazanin">91/57%</span></strong></span></td>
		</tr>
	</tbody>
</table>
', NULL, N'1397/05/25', N'17:28     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(252 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'تقدیر و تشکر اتمام فاز 4 اسناد وفات', N'<p><span style="font-size:18px"><span style="font-family:b titr">رؤسا و همکارات محترم ادارات تابعه</span></span></p>

<p>&nbsp;</p>

<p><strong><span style="font-size:18px"><span style="font-family:b nazanin">با سلام و احترام</span></span></strong></p>

<p>&nbsp;</p>

<p><strong><span style="font-size:18px"><span style="font-family:b nazanin">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; لازم می دانیم از زحمات رؤسای محترم ادارات و همکاران گرامی مرتبط، علی الخصوص ادارات همدان، کبودرآهنگ، قهاوند در راستای اجرای به موقع فعالیت 4-4-2 طرح تحرک سازمان با عنوان فاز 4 اسناد وفات تشکر و قدردانی نمائیم.</span></span></strong></p>

<p style="margin-right:560px"><strong><span style="font-size:18px"><span style="font-family:b nazanin">حوزه معاونت&nbsp;فناوری اطلاعات و آمار جمعیتی</span></span></strong></p>
', NULL, N'1397/05/25', N'18:37     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(254 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(159 AS Numeric(18, 0)), N'لزوم تغییر Computer name های سیستم های مورد استفاده', N'<p style="text-align:center"><span style="color:#FF0000"><span style="font-size:22px"><span style="font-family:b nazanin"><span style="background-color:#FFFF00">قابل توجه مسئولین محترم فناوری اطلاعات ادارات</span></span></span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin">با توجه به لزوم یکپارچگی و نظم برای مدیریت بهتر سیستم ها، لطفا هر چه سریعتر&nbsp;نسبت به تغییر Computer name سیستم های ادارات با فرمت زیر&nbsp;اقدام نمائید :</span></span></p>

<p dir="ltr"><span style="color:#FFFFFF"><span style="font-size:22px"><span style="font-family:b nazanin"><span style="background-color:#0000CD">Computer name = شماره(در صورت نیاز)-نام خانوادگی کاربر-کد سه حرفی معرف شهرستان</span></span></span></span></p>

<table border="1" cellpadding="1" cellspacing="1" style="width:100%">
	<tbody>
		<tr>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">نام شهرستان&nbsp;&nbsp;</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">همدان</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">نهاوند</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">ملایر</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">کبودراهنگ</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">قهاوند</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">قلقلرود</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">فامنین</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">رزن</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">تویسرکان</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">بهار</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">اسدآباد</span></span></td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">کد سه حرفی معرف شهرستان</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">HMD</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">NHV</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">MLR</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">KBD</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">GHV</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">GHL</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">FMN</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">RZN</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">TSK</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">BHR</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">ASD</span></span></td>
		</tr>
	</tbody>
</table>

<p><span style="font-size:22px"><span style="font-family:b nazanin">به عنوان مثال:</span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin">Computer name برای سیستم آقای عمادی در شهرستان کبودراهنگ باید به KBD-EMADI تغییر یابد.</span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin">Computer name برای سیستم دوم آقای برزگر در شهرستان ملایر باید به MLR-BARZEGAR-2 تغییر یابد.&nbsp;</span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin">این تنظیمات برای نمایندگی ها&nbsp; نیز میبایست انجام شود.</span></span></p>

<p dir="ltr"><span style="color:#FFFFFF"><span style="font-size:22px"><span style="font-family:b nazanin"><span style="background-color:#0000CD">Computer name = شماره(در صورت نیاز)-نام خانوادگی کاربر-کد سه حرفی معرف نمایندگی-کد سه حرفی معرف شهرستان</span></span></span></span></p>

<table border="1" cellpadding="1" cellspacing="1" style="width:100%">
	<tbody>
		<tr>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">نام نمایندگی</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">بیمارستان آتیه</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">باغ بهشت</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">جوکار</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">دمق</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">سامن</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">شیرین سو</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">گل تپه</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">صالح آباد</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">بیمارستان فاطمیه</span></span></td>
			<td style="background-color:rgb(204, 204, 204); text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">قروه درجزین</span></span></td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">کد سه حرفی معرف نمایندگی</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">ATY</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">BGH</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">JKR</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">DMG</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">SMN</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">SHS</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">GLT</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">SLA</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">FTM</span></span></td>
			<td style="text-align:center"><span style="font-size:22px"><span style="font-family:b nazanin">GHV</span></span></td>
		</tr>
	</tbody>
</table>

<p><span style="font-size:22px"><span style="font-family:b nazanin">به عنوان مثال:</span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin">Computer name برای سیستم آقای طیبی&nbsp;در نمایندگی شیرین سو درشهرستان کبودراهنگ باید به KBD-SHS-TAYEBI&nbsp;تغییر یابد.</span></span></p>

<p><span style="font-size:22px"><span style="font-family:b nazanin">بدیهی است در صورت جابجایی کاربران، Computer name سیستم مورد استفاده میبایست متناسب با کاربر در حال استفاده از آن&nbsp;به روز رسانی شود.</span></span></p>

<p style="margin-right:840px"><span style="color:#FF0000"><span style="font-size:24px"><span style="font-family:b nazanin">با تشکر</span></span></span></p>

<p style="margin-right:760px"><span style="color:#FF0000"><span style="font-size:24px"><span style="font-family:b nazanin">حوزه معاونت فناوری اطلاعات</span></span></span></p>
', NULL, N'1397/06/03', N'13:28     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(255 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), N'پیام همکار : نادر ابراهیمی', N'<p>سلام .&nbsp; همکاران عزیز&nbsp; ومحترم سلام .حالتون چطوره. امیدوارم که همیشه خنده بر لبانتون نقش ببندد.وغم واندوه از تون دور بجوید.</p>

<p>من کشته ی عشقم. خبرم هیچ مپرسید</p>

<p>گم شد اثر من. اثرم هیچ مپرسید.</p>

<p>گفتند که: چونی ؟ نتوانم که بگویم.</p>

<p>امروز که با درد سرم هیچ مپرسید.</p>

<p>وقتی که نبینم رخش احوال توان گفت</p>

<p>این دم که درو می نگرم هیج مپرسید.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; نادر ابراهیمی : ثبت احوال شهرستان اسدآباد</p>
', NULL, N'1397/07/12', N'10:03     ', 0)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(256 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(159 AS Numeric(18, 0)), N'لزوم تخصیص دستگاه های اسکنر اثر انگشت(تک انگشتی) و کارت خوان به همکاران و تکمیل و ارجاع لیست مربوطه', N'<p style="text-align:center"><span style="color:#FF0000"><span style="font-size:24px"><span style="font-family:b nazanin"><strong><span style="background-color:#FFD700">قابل توجه مسئولین محترم فناوری اطلاعات ادارات</span></strong></span></span></span></p>

<p style="text-align: center;"><font color="#ff0000" face="b nazanin"><span style="font-size:24px"><strong>فوری&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;فوری&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; فوری</strong></span></font></p>

<p><span style="font-family:b titr"><span style="font-size:20px">با توجه به<span style="color:#FF0000"> غیر فعال شدن</span>&nbsp;امکان استفاده از سامانه های DCU&nbsp; و استعلام با نام کاربری و رمز عبور، و<span style="color:#FF0000"> لزوم ورود به سامانه ها صرفا از طریق کارت هوشمند ملی و اثر انگشت همکاران&nbsp;</span>ضرورت دارد تا پایان زمان اداری امروز دوشنبه 28 آبان ماه، دستگاه های اسکنرتک انگشتی دارای کارت خوان به همکاران اختصاص یافته و لیست پیوست تکمیل و برای اینجانب ارسال گردد.</span></span></p>

<p><span style="font-family:b titr"><span style="font-size:20px">مسئولین محترم فناوری اطلاعات میباست درایوهای این دستگاه ها را نصب نموده و آن ها را جهت استفاده آماده سازی کنند.</span></span></p>

<p><span style="font-family:b titr"><span style="font-size:20px">نحوه ی نصب درایور این دستگاه ها در پیام جداگانه ای در همین سایت اعلام خواهد شد.</span></span></p>

<p>&nbsp;</p>

<p style="margin-right:680px"><span style="color:#FF0000"><span style="font-size:20px">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;با تشکر</span></span></p>

<p style="margin-right:640px"><span style="color:#FF0000"><span style="font-size:20px">حوزه معاونت فناوری اطلاعات</span></span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>
', N'FingerAndCardList.xlsx', N'1397/08/28', N'09:29     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(257 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(159 AS Numeric(18, 0)), N'راهنما و فایل های مورد نیاز برای نصب برنامه ورود به سامانه ها با استفاده از اثر انگشت و کارت هوشمند.', N'<p style="margin-right:320px"><span style="font-size:22px"><span style="font-family:b nazanin"><span style="color:#FF0000"><strong><span style="background-color:#FFFF00">&nbsp; &nbsp; &nbsp;قابل توجه مسئولین محترم فناوری اطلاعات</span></strong></span></span></span></p>

<p><span style="font-size:20px"><span style="font-family:b nazanin">فایل های مورد نیاز برای نصب درایورهای دستگاه های اثرانگشت و کارت خوان و پیش نیازهای نصب آن ها به صورت کامل در پیوست&nbsp;قابل دریافت و استفاده است.</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b nazanin">لطفا ابتدا&nbsp; فایل help.pdf را دقیق مطاله کنید و طبق آن مراحل نصب را انجام دهید.</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b nazanin">با توجه به اینکه امکان&nbsp;دسترسی به سامانه ها <span style="color:#FF0000"><strong><u>فقط از این طریق</u></strong> </span>ممکن است. لطفا هر چه سریعتر تمامی دستگاه ها را آماده بهره برداری کنید.</span></span></p>

<p><span style="font-size:20px"><span style="font-family:b nazanin">در صورت مشکل در دانلود فایل میتوانید از ftp اداره کل در پوشه معصومی پوشه ی&nbsp;NocrAuthentication Requirment&nbsp;را دریافت کنید.</span></span></p>
', N'NocrAuthentication Requirment.exe', N'1397/08/28', N'13:14     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(258 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'نحوه ورود به سامانه استعلام ', N'<p><span style="font-family:b titr"><span style="color:#330000"><span style="background-color:#FFD700">قابل&nbsp; توجه کلیه همکارانی که </span></span><span style="color:#FF0000"><span style="background-color:#FFD700">کد کاربری استعلام </span></span><span style="color:#330000"><span style="background-color:#FFD700">آنها </span></span><span style="color:#FF0000"><span style="background-color:#FFD700">غیر فعال</span></span><span style="background-color:#FFD700"> </span><span style="color:#330000"><span style="background-color:#FFD700">شده است.</span></span></span></p>

<p><span style="font-family:b titr">با سلام و وقت بخیر </span></p>

<p><span style="font-family:b titr">همکاران محترمی که کد استعلام آنها با کد DCU متفاوت بود و غیر فعال گردیده بود. برای استفاده از سامانه استعلام، کافیست<span style="color:#0000CD"> نام کاربری سامانه DCU </span>خود را به صورت <span style="color:#0000CD">یکسان </span><span style="color:#000000">بعنوان </span><span style="color:#0000CD">نام کاربری و رمز عبور در استعلام </span>استفاده نموده و پس از ورود <span style="color:#0000CD">رمز </span>خود را <span style="color:#0000CD">تغییر </span><span style="color:#000000">دهند</span>.</span></p>
', NULL, N'1397/09/11', N'12:55     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(259 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'بروز رسانی نرم افزار CCOS', N'<p><span style="font-size:16px"><span style="font-family:b titr"><strong><span style="color:#800000">قابل توجه مسئولین فناوری اطلاعات ادارات&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp;&nbsp;&nbsp; فوری</span></strong></span></span></p>

<p><strong><span style="font-family:b yekan">با سلام و&nbsp; روز بخیر </span></strong></p>

<p><strong><span style="font-family:b yekan">لطفا به منظور بهره مندی از نرم افزار CCOS با<span style="color:#B22222"> آپدیت جدید</span>&nbsp;&nbsp;&nbsp; <span style="color:#B22222">2.8.2.22</span>، اقدامات ذیل انجام گردد.</span></strong></p>

<p><strong><span style="font-family:b yekan"><span style="color:#B22222">1-</span>حذف کامل CCOS&nbsp; قدیم از کنترل پنل </span></strong></p>

<p><strong><span style="font-family:b yekan"><span style="color:#800000">2- </span>حذف پوشه <span style="color:#B22222">2.0</span> در مسیر زیر</span></strong></p>

<p><strong><span style="font-family:b yekan"><span style="color:rgb(0, 100, 0)">C:\Users\</span><span style="color:#FF0000">namekarbarishoma</span><span style="color:rgb(0, 100, 0)">\AppData\Local\Apps</span></span></strong></p>

<p><strong><span style="font-family:b yekan"><span style="color:#800000">3-</span>نصب ccos&nbsp; از مسیر زیر<br />
<span style="color:rgb(0, 100, 0)">http://ccos.ssd.net/ccos-update-2</span></span></strong></p>

<p><span style="font-family:b titr"><span style="color:rgb(255, 0, 0)"><strong>توجه : </strong></span><span style="color:#000000"><strong>منظور از </strong></span></span><strong><span style="font-family:b yekan"><span style="color:#FF0000">namekarbarishoma</span></span></strong><span style="font-family:b titr"><span style="color:#000000"><strong>همان</strong></span><span style="color:#FF0000"><strong> نام کاربری</strong></span><span style="color:#000000"><strong> است که در سیستم شما استفاده شده است.</strong></span></span></p>

<p><strong><span style="font-family:b yekan">لطفا واحد IT اداره کل را از نتیجه اقدامات مطلع نمایید.</span></strong></p>
', N'راهنمای نصب وتغییرات نسخه2.8.2.22 (1).pdf', N'1397/09/17', N'08:41     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(260 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'آپدیت تمامی CCOS های نسخه 7 به 22', N'<p><span style="color:#B22222"><span style="font-family:b titr"><span style="font-size:14px">قابل توجه مسئولین محترم فناوری اطلاعات ادارات&nbsp;</span></span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">با سلام و وقت بخیر&nbsp;</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">از آنجایی که نسخه های <span style="color:#FF0000">7</span> نرم افزار <span style="color:#FF0000">CCOS </span>به صورت اتوماتیک به نسخه بالاتر <span style="color:#FF0000">آپدیت نمی شود</span>. مقتضی است کلیه نسخه های نرم افزار <span style="color:#FF0000">CCOS نسخه 7 </span>در تمامی ایستگاههای کاری اداره، دفاتر پست و پیشخوان <span style="color:#FF0000">به نسخه 22 آپدیت گردد. </span>لازم به ذکر است در روزهای آتی نرم افزار نسخه 7 &nbsp;غیر فعال خواهد شد.</span></span></p>
', NULL, N'1397/09/25', N'11:15     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(261 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'آپدیت نسخه 24 نرم افزار CCOS', N'<p><span style="color:#006400"><span style="font-family:b titr">قابل توجه مسئولین فناوری اطلاعات&nbsp;</span></span></p>

<p><span style="color:#000000"><span style="font-family:b titr">با سلام و وقت بخیر&nbsp;</span></span></p>

<p><span style="font-family:b titr"><span style="color:#FF0000">آپدیت جدید </span><span style="color:#006400"><strong>CCOS</strong> </span><span style="color:#FF0000">با نسخه 24</span> از امروز در دسترس خواهد بود.</span></p>

<p><span style="color:#008000"><span style="font-family:b titr">مسیر دریافت آپدیت :&nbsp; </span><span style="font-size:14px"><strong><span style="font-family:times new roman,times,serif">http://ccos.ssd.net/ccos-update-2</span></strong></span></span>&nbsp;&nbsp; <span style="font-family:b titr">فقط از مرورگر <span style="color:#008000"><strong>Internet explorer</strong> </span>استفاده شود.</span></p>

<p><span style="font-family:b titr">لذا با توجه به <span style="color:#FF0000">حجم بالای این آپدیت</span> لطفا به <span style="color:#FF0000">کلیه دفاتر پستی و پیشخوان</span> دستور فرمائید ظرف <span style="color:#FF0000">بعد از ظهر امروز چهارشنبه (97.09.28) </span>نسبت به آپدیت نرم افزار کارت هوشمند خود<span style="color:#FF0000"> بصورت تک تک</span> اقدام نمایند.</span></p>

<p><span style="color:#FF0000"><span style="font-family:b titr">**** &nbsp;توجه &nbsp;**** </span></span><span style="color:#0000CD"><span style="font-family:b titr">فردا &nbsp;امکان اخذ درخواست وجود ندارد.</span></span></p>
', NULL, N'1397/09/28', N'11:08     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(262 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'مشکل ثبت اسناد وفات', N'<p><span style="font-size:16px"><span style="font-family:b titr"><strong>قابل توجه مسئولین محترم فناوری اطلاعات و روسای ادارات</strong></span></span></p>

<p><span style="font-size:16px"><strong><span style="font-family:b yagut">با توجه به مشکل پیش آمده در ثبت اسناد وفات در قسمت <span style="color:#FF0000">محل وفات که در پرینت سند به صورت نامعلوم چاپ میشود</span><br />
خواهشمند است تا اطلاع ثانوی<span style="color:#FF0000"> قبل از تائید نهایی سند</span>&nbsp;ابتدا از قسمت <span style="color:#FF0000">تصحیح اطلاعات نسبت به تهیه پرینت&nbsp;اقدام</span> و سپس نسبت به تائید نهایی سند اقدام نمایند.</span></strong></span></p>
', NULL, N'1397/10/08', N'10:35     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(263 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'الزام استفاده از کارت هوشمند جهت ورود به سامانه ها', N'<p><span style="color:#B22222"><span style="font-family:b titr">قابل توجه کلیه همکاران گرامی&nbsp;</span></span></p>

<p><span style="font-family:b titr">با سلام و وقت بخیر&nbsp;</span></p>

<p><span style="font-family:b titr">به اطلاع می رساند از <span style="color:#B22222">روز شنبه</span> مورخه <span style="color:#A52A2A">1397/10/15 </span>ورود به <span style="color:#A52A2A">سامانه DCU</span> سازمان <span style="color:#B22222">تنها از طریق کارت هوشمند</span> ملی امکان پذیر خواهد&nbsp;بود. لذا همکاران می بایست همواره کارت هوشمند خود را همراه داشته باشند.</span></p>

<p><span style="font-family:b titr">مقتضی است همکاران تا پایان این هفته نسبت به رفع مشکل احتمالی کارت هوشمند ملی خود اقدام نمایند.</span></p>
', NULL, N'1397/10/09', N'12:52     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(264 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'پیام همکار : معصومه مهرزاد', N'<p><img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAloAAAE9CAIAAABcIFvCAAAgAElEQVR4nOydd1wUufvHY8fe7+y9K2I78bACiqhnb6iAh3g21OPAhtg5FXsBC2I5u2JBEAsqKIIoCkqXJqj0XpeFbfn98fzMN87sLtgoa95/8FpmMsmTzEw+KU8yCDMYDAaD8dODytsABoPBYDDKHyaHDAaDwWAwOWQwGAwGg8khg8FgMBiYySGDwWAwGJjJIYPBYDAYmMkhg8FgMBiYySGDwWAwGJjJIYPBYDAYmMkhg8FgMBiYySGDwWAwGJjJIYPBYDAYmMkhg8FgMBiYySGDwWAwGJjJIYPBYDAYmMkhg8FgMBiYySGDwWAwGJjJIYPBYDAYmMkhg8FgfHdkMplMJqN/8M+S40pCMsoSJocMBoPxnSGCJ5VKpVIp5yw5SOsiRyMZZQ+TQwaDwfhRKOkdljIwo8xgcshgMBjfASUipyhkVlYWxjgkJOTcuXPFxcVMC8sXJocMBoPxHSAil5KSIpVKX79+vW7dOhA8uYSEhPTv33/SpEndu3dHCNnY2HxRchkZGd9qcUnIZLLi4uIfnUrFgckhg8FgfAdACxMSEnr16jV+/PgBAwYghExNTeUGk0qlBgYGiGLTpk2lT+Xp06fNmjUzNjYWCASY6oCKxeLDhw+HhYV9qeV835/8/Px//vnnS6Oq1DA5ZDAYjG+FCJKxsTEtcn/99ZfckEVFRVpaWgih2bNnr169WltbOy0tDcsbWeUjFotnzJgB8YeHh9OnXFxcEEKdO3cuvYwRlx/yA2MsEommTJnSr1+/oqKiUsajAjA5ZDAYjG+FdKq2bt1avXr1QYMGrVu3bsyYMcnJyXIDY4yTkpLOnTsHKkgfV54KxlgkEs2aNQshpK2tTfcOZTLZsGHDEEJVqlR58uTJFxkPzq4Qj1gsnjt3bo0aNV6+fFn6SFQAJocMBoPxHSAdrPv378fGxmKMJRIJlidyRHhoSuNWSi7My8tbv349aB491Hn8+PH27dsbGBh8qVcO6RcWFRUZGhoihP77778vikEFYHLIYDAY/6OU3qGcU4rEjGgkHQlniSEngHIl4yxk5F+SkJCQl5enJAa5xsO/xcXFc+bMQQgdOXKklDGoEkwOGQwG4/+hhw1pFPXn6KuIqsFv8pf+oaRfyBFUufrKUU2ObZzLlWeTmEouF4lEM2fOrF69+tmzZyEYfwMB2s4v7YBWfJgcMhgMxv8g1X1RUZG7uzuslOCLE5GT2NhYWDLIET+ORnIipyHBRCJRSkoKGWKVm6jc48XFxe7u7pmZmYou5JOZmenu7i4SieBfoVA4bdq0xo0b+/j4KLITfy6BfLGs7DA5ZDAYjP9BlCA6OrpXr16RkZFKAjs4OBgbG4OAYZ6K0MokEAiEQqGiFEFaXr9+ra2tvWzZMnNz81J2IoG4uLiePXuCNymJTTn37t3r379/YWEhxrioqGj69Om1a9f29vYmAfhaTnj69Om1a9dKTKLSweSQwWAw/h9aAI4dO/bbb7/l5+djBc4vGGMNDY0pU6bQl2OMnzx54u7uLhaLyfHw8PC//vpLkZcpSfT06dMIoerVq3fp0kWupNHmPXjw4I8//rh37x7G2NHRceDAgdnZ2ZgauVWe05UrV4LlAoFg4sSJ7du3DwgIIEkoGtpNS0tbsGBBgwYNTpw4oTz+ygiTQwaD8XOhaOQTfxoAhHHLsWPHjhw5kh+GEBER0aBBg/nz59MHw8PD1dTUqlSpAsvq4ao1a9a0bdtWkaMpwdnZGVYTGhoayk0U/i0sLLS0tEQItW3bNiIiAmM8fvz4YcOG0VN6dB75P6RSabt27aytrbOzs4cOHdqlS5eYmBhO+fBLSSwW6+npVa1adc2aNUVFRSUqbqWDySGDwfiJoKf0CBxHmMOHD/v5+Y0YMYLT8+OIxL179+iF9nB8wYIFIGktW7aEecfi4mJNTc0+ffpw4iH/ent7e3h4eHp6GhoatmzZUltbOzAwkJMiuUQoFE6ZMqVhw4YnTpyAfdpkMpmWltYff/xRmszC8by8vJYtWxoZGY0fP75nz565ublyL4EjkZGRnp6enp6eCxYs6N27N1n4r2gotfLC5JDBYPxcyPVzIb8/fPjQrFkzZ2fnfv36GRsbY3kKERYWlpSUdPv2bYTQwoUL8SctzMvL69SpE8hh/fr1ExISMMaJiYkIoUGDBhG5JfE8fvx49OjRZAub1q1bh4aG0kaSRMmPGTNmdOvWLSUlhWRHKBR279593rx5mOoaJiUl0Vkml2dlZQUGBsbGxrZr1w4S7dOnz/Lly8meOPRCjrS0NCsrq19//ZVY+OzZM9q873pbyh8mhwwG4yeF078BGYB9zi5cuNC5c+ctW7bQISFwQUGBjo6Or6+vj48PQmjJkiUkgEQicXR0tLa2rlevHtnhLDo6GiE0cOBAGTWYiTFes2ZNlSpViNLUq1fP09MTfz62yenYPXv2rFq1ag0aNDh69GhhYSEcTExMbNmy5dq1a+EqoVAI6+jNzMyOHTtGhBMC29rampmZvX79umHDhvRmcps3b8afy+GzZ886dOhAh9m2bRtdUEwOGQwGo9KTmJgYGhoK1frRo0eHDBkCk3AY47Nnz9asWXPr1q21a9e+cOECuUT2yWPz5s2brVu3FgqFIIcgErQ2PHv2rHbt2hs3boR/3717hxDS1NSUUVN3a9euBY3R1NTU1tYeM2aMu7s7SYiG9MOkUqmOjg4RJ9BOjPGTJ0+qV69OLv/3339btmw5ZswYelNTsuNMz549L1686OTkBGd79OhRr149hNDcuXOJbZAF0MsWLVro6Ohoa2tbWlrSKigrnf9q5YLJIYPB+Ok4ceIE6Edqamrnzp0RQoMHD4ZFeOfPn2/QoMHWrVsRQmvWrMEYZ2Vl0YOTgwcP/vPPP/GnfqSfnx859fDhQ1Csdu3affz4EQ4mJycjhHR1dUmw69evgxRdvXqVLNIAOAOk5K9YLDYzMyNa2K9fv9TUVLgEzNixYwfGODw8fMiQIRkZGWFhYc2aNdu9ezcd+enTp5s0aZKXl7d//36E0PLly7Oysnx8fDp37rxr1y5MDfn27t0bIWRtbR0dHf2dirwSwOSQwWD8XLx582bhwoWwk9mjR49AYCZOnAhicPLkST09PVjzMHTo0Dt37sydO5d8IAKcP11dXTHGR44cad++PfFDcXd3V1NTQwhNmjTp/v37+JO6QO/w4MGDECw6Orply5bdunWjnTmxvLFHoot+fn56enpEC7t37/7mzRtyybVr1xBCXbp0CQkJWbZs2caNG69cudK8eXMjIyM6ZoFA0KNHj/Hjx2OM//33X4SQnZ0dxjgiIqJbt270bt2LFi1CCB07duy7lXglgckhg8H4ubCxsVm6dCn8zsvLMzc3HzhwIOnkzZw508DAwNPTE7SnSpUqLVq0gI6jh4dH7dq1ST/y8OHDx48fh6tev35dtWpVTU1NX19fOELEzN/fX01NjTihjB8/vk6dOsHBwXQYWgvj4uJcXFzIl5WePXtWp04dUEFdXd2TJ08Stxe4ytvbu1q1agihpk2b1q9fv0uXLuDXCo48QG5u7rRp0xBCjx8/xhjv3r0bIdSoUSN1dfWmTZvq6OiQkc9bt24hhNavX09y8R1LvoLD5JDBYPxESCSSvn37Hjp0iD5IvvkuEAg0NDROnjyZkpLSuHFjUMTdu3eLxeLjx4/Dv7dv34bAsEIfLh80aJC6ujqsrMDUYnaMsbOz89y5c0FBDxw4gBACEeX4tQqFwjlz5mzatAk+3rRnzx6McWFhYceOHfv06XPx4sX09HTaZnKtQCAYPHgw7fNSt27dO3fukJAvXrzo378/QsjAwABM8vLyqlGjBgn/4MEDCPnu3btffvlFXV0dhnBVb3ZQOUwOGQzGT0R6enrt2rVPnTqF5XV9du/eXaNGDVjPZ2FhASOfsbGxY8eOrVWrlqGh4cWLFzHPJfWvv/5q2rRpVFQUpib/yK4uycnJEGFQUBBCyMTEBK7ifJuCyC2waNEijPGiRYv69+/P+SYif37Rx8enR48ecOHw4cMfPXpEwtvZ2dWsWfO3337bsGEDmAFJGxkZQQeR+ItijHV0dBo1avT27VuIWSKRsN4hg8FgqCaRkZG1a9eGVXocjh49WrVq1XXr1sG/IpEoNDS0sLCwuLj4+fPnxPWUAFJx48YNhNCVK1fw54OfnFHQtLS0bt266ejo8HcuhTDm5uYIoU6dOhkaGvbr1y84ODgpKal9+/Yw+lqiLKWnp4eGhoLB9PHQ0NA3b97wEy0qKgoJCXn37h05Ym1tXbNmTeKw+hPC5JDBYPxERERENGnSBLxGY2NjExISEhISbt26pa+vD/Nz4GKjSH446/EfP37cuHFjGNjkQC9FSE5OHjhw4KBBgzir4+k4s7KyDhw48OrVK4wxDLru2bOnf//+P2KRH38U1NbWlv60088Jk0MGg/ETkZub2759exhXrP0J+Ldt27bgsalIgehRSozxpUuXGjZsuHPnTviXXMUZSn3+/HmPHj309PTIB5jkRss31cjIaOvWrXLj/BZkn+/xXVBQYGJi0rx5c9gN/GeGySGDwfi5gFV3HDp37vzixQtMecHI1S3QvJycHBMTk549ex49epQfhkQiEAg2bdrUo0ePf/75Bz5wITdO/ElKOSqlrq7u5uaGv7cc0vj4+PTt23fw4MHg6SrXwp8HJocMBuPnQiKR7Nq1q0WLFo0aNWrcuHHLli1XrlwJs2iyz+FcCAeLi4ttbGysrKzi4+PhOEc7ye+EhIQ9e/YEBQXRl3Oi5RwkvyMiImrVqgUfXcLf28mTaPDly5fJAOkPUtxKBJNDBoPxM5KdnR0eHh4REQGfCQTkfv8BoBUrJyenNEl8nbrAVV5eXlpaWjC+yigbmBwyGIyfC7kqRQ9yyuRtyFk2PSeSyu3btydOnEgWRDLKACaHDAbjJ4J0+zi9QPoghPzw4UNUVFRycvLcuXPLbMcyYsB///03a9asskmUATA5ZDAYPx0yeWDKWUYikejr69esWVNTUxMh1KxZs8TExDIzDGN8/PhxsmCfUTYwOWQwGD8d/NlBzhHYqpvQv3//Us4XfhfbMMarV6+eP39+2aTIAJgcMhgMxv8ANQoICOjSpYuGhoaDg4OFhYWXl1eZpQ4GDBs2jA2WljFMDhkMBuP/oTuICQkJycnJ9KmyMQBGa62srGxtbcsgRQaBySGDwWD8P/xVFvwfZWAAxvjGjRvkQxOMsoHJIYPBYPwPek0939emzGw4ceLE06dPyyxFBmZyyGAwGDQletn86NQxxhKJ5PLly2Q7G0bZwOSQwWAwKgogh2Kx+N69e/ABRUaZweSQwWAwKgpkT5y8vDy2JU0Zw+SQwWAwKhCccdpytORng8khg8FgVBToHeMYZQyTQwaDwWAwmBwyGAwGg8HkkMFgMBgMzOSQwWAwGAzM5JDBYDAYDMzkkMFgMBgMzOSQwWAwGAzM5JDBYDAYDMzkkMFgMBgMzOSQwWAwGAzM5JDBYJQS8pEjtoUYQyVhcshgMEpFGX8Cl8EoY5gcMhiMUkF/Jp7BUD2YHDIYjNIilUoxGyxlqChMDiso7MufFZyfShJEIpFYLI6Pj2ePJUOFYXJYIYDvX5Ma9saNGzExMfgnq3MrF+SWcShvu74/8fHxL1++PHPmzNOnTzGbQWSoLkwOyx9OTbp3795169aJRCJW6VRkQA5VfvAwNjbW29t727ZtJiYm+fn5+NN4KYOhejA5LH9oLbS2tjYyMiooKMCsGV6BAS2E37Gxsenp6QUFBYmJiVi1pDExMTEoKOjAgQOjR49OSEjAvGEMBkOVYHJYUZBKpdbW1oaGhrm5ufhTrcpa4hUTogfBwcEPHjxISUk5cuRISkpK+Vr13REKhXZ2dsOGDQsNDcXUMAaTQ4ZKwuSw7KCrkpycnMLCQjgulUoTExOXLl3auXPnrKwszPqFP5hvL16I4c2bN97e3jk5OcbGxlevXsWq1TXEGG/cuHHw4MFhYWHwL8mdimWTwQCYHJYdZKoJY/znn3+amppKJBKM8YoVK9q0adOxY8datWr9999/5WqjiiORSL7XviqZmZkRERHJycljx47dtWsXHKzsOkHsz87OnjBhgpaW1ocPH8rXJAajzGByWEbQXcPnz58jhLp06YIxdnNzQwg1atRowIABCKFTp06Vt6WqDN2/+XbpSk1NHTVq1Jo1a+Bf4mj6jdGWOzExMUOGDNHT08vMzMSfN+MYDBWGyWGZAnVldHT0gAEDli5devToUS0trRYtWmzZsiUwMHD37t1CoVAF6tMKi1QqFYvF3yWqiIiIoUOHrl69Gv4FIVQB2XB3d9fQ0Fi9ejUUFL2epLxNYzB+LEwOywi6rty9e/eVK1dMTU0RQg4ODgkJCTk5OeVr3k/C8+fPR48eHRcXh79tYNPDw6N///70GKlq+JjY2Ni0bNmSjNgTja/s+WIwSgOTw7Lm+vXrCKE6deoghBBC9+/fp89WnHqnlHNsFVwG6FxIpdIpU6YghM6cOfNF13KKYv/+/W3atLl06ZKiSyoRJHfFxcUrVqwYOHAgrLXnhCkP0xhlB3OSApgclhHkObOyskIIzZ07d/ny5fPnz6+wu16VUudkMhk4BGGMi4qKKtrrRPdvZDKZkZFR06ZNwT1EuamyT6vs6WAikcjCwqJZs2YPHjwgwX6o/T8Ucn+jo6N1dXUNDQ3T0tLIqXI1jcEoB5gclhFQv3z8+FFfX9/AwABUUCQS4QrcwSqNSSTMq1evAgMDf7BFXwzdsRMIBO/evXv16tWBAwfevHlT4oWghcSRJC4uTl9ff9SoUeHh4Zjqcf7wPPxgMjIyZsyYsW3bNnKkYj6NjB/K6dOn//333598Mywmh2VHamrqunXrrly5wjn+fd0dvxe5ubmlfzdcXV337NkDm+lUKGg53L59+9ChQ+3t7RFCWlpaJfYO6WufPXvWpUsXExMTgUBAAqiAFmKMHz586OHhAb/pXVjL1ypGWfLhw4emTZsihB4+fFjetpQnTA7LjvDw8ODgYPjNGayXSCQVymEhPT3d09OzlE6Yjo6O1tbWqampP9qqr4DU7O/fv69evTpCCBa0kNURSi4kv11dXQcOHKiSDiYCgSApKQl+011hxk9FQEBAnTp1OnbsmJGRUd62lCdMDrmU6CUIxwsLC8mcmZKoyN8PHz58/PgRfz40Cn/FYrGvry8ZOP1Gy+H3N1Zq169f//fffzHPz56v4ps2bfr7779hY7kSE71z546FhQXdiYR47O3td+/e/aU208Yo2kqbWJ6cnKypqWloaJiUlAQbqtGB5fZrIYCbm5uent7z5885EcLfzMxMegOz0lvOecwKCgq+5ZaR7NO2kfihR8u5d/D31q1b0dHR+Js/7cu5nJ9BzinaEvyFpScXOu9SqRTezdu3b8fHx39pPHL/LY2F/CwrqkmUlA+dlkgkMjMze/ToEebd3PPnz2/cuLG4uFhWUsusNHdWIpF4e3s/efJEeTCVh8khF/rxgk4b5yzGOC0tzc3NTSgUlj7Op0+fkk2QOW/Co0ePduzYoejd+CLLv7FOIejo6Kxbtw5/WoIt44Exzs7ONjU1XblyJXzooMTUhUKhjo4OQgi6WSRwfHx8mzZtEEKvXr3CpXt7ZZ/rn5KFcbTBOTk5dOOXhPf39ydqR4D48/Lyjh07Fhsby0mUXGtjY9OsWbPXr1/jz58cJZaTy0nguLg4d3f3b5FD/g0ikXt6er59+xaCFRUVcbKwePFiHx8fzJOBr0sdY5ydnU12H1RSIPyS/I42nDlzZv78+efOnatRo8aiRYu+IqqQkBAyKk4XWokGcEabFZWA8ueWHHR2dkYI9ezZs6ioiD6enZ2trq6OEHJ3d8dKX5kvrRO+VwVSSWFyKAd4JvLz8318fDiaB6euXbtmZWVVyngwxqmpqZs2bSJR0S8MxtjGxmbFihWcS77O7C99+hXF07t37wMHDtBH4Ad58fz9/fX19bdu3UrClKgEKSkpHTp0qF27dkhICB3t8+fPa9as2atXL7Jfa2ksJFUJX605IWWfT/LRagRHzM3NoVqhgWjDwsJI10rGG0tMS0tr3bo1QujatWtYae3PT50OdujQIbq0vxQZD1ImYrHY2NgYvp2JMbawsJg/f35eXh78m5ubq6WlFRAQgL+83pRrBsb4v//+CwoK4hzkW0sKSiaT5eXl3b9//3sZkJaW1qFDB4QQCAbZJ6GUSKVSW1vb6tWrDx06dMWKFYsXL/bz81OUF74BdC5KfB8VPbHk7+7duxFCy5cv5zwzERERDRs2bNWqFd3CVhR/icaT14ENlTM55EJqz7t3727evFnuY2Rqalqa14xc6+vrO2/ePKxgkOTQoUMXL17EpXh/Spliamrqt8wBiEQifX19GKIRCATv37+HrT6JYVevXlVXVz979iz8SzwwS4w5JCTkzp07+PM6USKReHl5eXt7kyyUpgTk1rN8GzgSRaoVciQjI2PAgAEkdZrY2Nhz585x8kgsxxjn5ubOmjVr7Nix5INcX9Q7lEgk4GA8btw4e3v7ErNcIjLKGxaSePPmTb9+/aDS9Pf3h6Wuz549g/AxMTE1a9aE1sk31oOkYBcuXAjdzcTERBhCV2SkQCAAw65du1a9enVbW9tvNACykJiY2L9///HjxwcHB1tYWMTFxX1R1pKSkm7cuBEUFLRt2zaE0OjRo2GnuhKfcEWCJDewQCCAUVwlSoYxFgqFLi4uZNtYuuX36tUrcHtR8r7A8aKiohLn9UkMTA5/RkpssmGMV61apUjzunfv/s8//5QmCfh78eJFExMTftLp6ekmJiZWVlYwGPK9cHJyAjH7OkQi0bhx4zw9PTHGwcHBd+/eJafEYrGNjc3EiRNpnyC+wJcGfv1SmnjoUsUYR0ZGYowfPXrk4OCgSCBJhBwthNRDQkIQQi9fvuSndenSJRgx5phHh5FIJKTl8UUlkJCQ8OTJk/z8fIlE0qBBg2PHjinJb4mkpKRs3rz5/fv3nDK8cuWKmpoaWOjv79+pU6fRo0eTpa5v377t2rUr6WGU0nIldiYkJLRo0QJ6h6dOnZK7moXc9+Dg4JMnT2KM9fX1EUKWlpbfbgD8TUpKoqcMS581+pk8ePDg1q1bOSM6JcYQFRWFMX7+/LmdnZ2S/eLDwsLgjivRQvoU+c0xg/yWu3wZzvr6+r5790652bQcfuOTUKlRHTnkV6aK6lYZ1Xz++PHj2bNn6dkvEr5Xr14WFhZy4+natSu8vUpeEvqqv//+e+LEifRZoVB46tQpbW3tOXPmwNQU51pcuteYU8VjjCUSiZ2dHZkuUmKVIiQSybhx46Dteffu3RcvXsDx9+/fGxkZLV26ND09XYk9gJKOGrGZ8y+5kHOEA8l1dHR0q1atNm7cOGTIEIQQDPopsodjVUREBIyC+vn5Va1aFaYtOcycOXPt2rW08bRtuHSVCKco4uLiUlNTY2NjNTU1QQxyc3MbNmx4/PhxYq2iyVrODxKtq6trhw4dxowZA3UifX//+++/+vXrk/v1/v174s+FMQ4ODp4wYQI9Giz7vKrlJMQxj3OnMMYxMTFVqlR5+/atWCw+cOAA/T1kTuQYY2dnZ1dXV/gxa9YsUmVzci23POnyF4vFigpf0R3hlyQdQ2Rk5ObNm8ngBw1nkIBzNikpqWPHjhYWFqNHj0YIQYNSrm2urq7Ozs6KsqakBPjWYoz9/f0VeQxJpdIlS5aAtxT9uGLefVFe7D8JKiWH+PPap8QbfPny5ebNm2/YsAFWFHAG/bp27QpyyKdbt24wdyjXI4BjD8bY1NS0TZs2MOhRWFhoamraqFGjIUOGlPj9ihIfTTpAQUFBfn5+amqqgYEBzMPRcN40JXEWFRVNmDABNuvauXMnNHifPn2qr69POjHKaysoFv46DY4BpDIiJU/fO/z5G8u3fNmyZTBF1KRJk969e0MvR5E9dCVy9+7ds2fPgvNhQEBA1apV6ekugoaGhrm5OccM0uTnZ0pRvQyZysjIWLp0ac2aNevUqaOpqXn69GkwID8/v2HDhvAkcPLILys6R/Cvg4NDlSpV2rRpQ1aM0VedP3++QYMGHL9Z8pAHBQVNmzYNRiboKh52F+LcGvpajjHk37CwsN9//10gELx9+3b+/Pn0TeSzePHiL3Vl5D918fHxT548KdHHu8R4gMjISENDw4kTJ8KQPh/O88lh48aNCKEePXq0bNmyU6dOMGUrt6m0ZMkS2NioqKjo7du3MB9Bxyz3PZXx9BJjnJOTs3TpUkWbHn/8+HHEiBHkw9Syz9U0KiqqoKCA/8jB76Kiou87alXxUSk5JPdSJBIRfwFFLXd7e/tJkya5uLjQkkY/DV26dCH+MhKJhOxfhTHu37//nj17lEROm4QxXrVqFUKoW7du+vr6mpqaf/755/79+6FLqggyY6Ekfprg4OAdO3akpaW9fft27ty5cqUIfpQ4PZCenj5+/Pjw8HCZTLZ06dLMzMxDhw6NHTuWjH2RgpLbBYSE3r9/L3eIhq4i6azRr7fc2o1fTVy6dElLSysyMvLp06eK3CPpihtjnJWVdf369SFDhpANdMLCwho2bBgXF8dP8bfffuPIIT9OcjAuLg7a4HIJCQlZsGCBra2to6Pjv//+S48iisXiVq1a3bx5E1O35tatW7A0npMQp9idnZ3Hjx9/+/btxMREcoq26vr16+3ataNbe3QBvnnz5q+//oKQJM7AwEBSGiTkrVu3LCwsSDx08dIpBgYG/v333xjjR48eQcycqhZjLBQK09LSZDLZ3LlzYchd9kmDb9686erqmpqampeX9/btW0UiJ5PJUlNTw8PDQ0ND7e3tybh9KSEmFRcXv3nzBpa43Lhxw8zMDD7lQXpacl89qVSanZ0t9+zt27c1NTUDAgJevdyUsd4AACAASURBVHpFuob8VxjyHhQUVFBQsGLFisGDB/fo0UNTU9Pc3BzGZuVeRVLnlKeHh4eOjo4iL3dPT8+mTZsSg+l7d/nyZXt7e3qrDfpsQkLCo0ePKuZi4h+H6sghpm7nqlWrhgwZAt0auQ/WhQsXHB0dORUcCZmRkSGTyfr06UN2roqIiOjRowc06MRi8aRJk8DfDFLMycnJzc3lP/TkyNy5c9EnzMzMFAUjnD59um/fvrdv3y4xyzKZ7MCBAwsWLOjSpcuGDRswxg8fPjQ2NlYUHqaR5L5phNTUVH19/YiICHhvp06dCt7eZmZm+/btI/6fci0nB/ft2+fr60ufkkqlsNqPU9qFhYVQ4Bjj5OTkIUOGmJiYkK8LwfHU1FR4b+FfUinQTQqZUnmGkmnbtm2vXr1gaBSOe3l5TZ06Ve6GA4MGDVq5ciX+vOeUmZlZWFhIzICQT58+Xb9+Pb9HDuTk5Kxfvx6WJ9KlQbKmp6dHj2FmZ2d37Nixc+fOxCMUiIuLIw73GOOkpKTVq1fTn+eVUW1/qMhOnjy5dOlSuijoIrp27drBgwcx9TxcuXLF0tISmpIksEgk0tXVRQjt3buXBC4sLIyNjaUlFmMcEBAAKn7u3Dm6JUFz//798PDw/Pz8qVOnkmlLqVR6/fr18ePHt2rVSl1dvWfPnrt37+aXpEQi2b9//8SJE7t27Qpv05EjR+SWuXIgX/Hx8c2bNx8wYIC+vn7Dhg0NDQ3p9TZynyWM8d69ewcMGMB5tslZ0grHit+R3NzcKVOmxMfH+/n5eXp6vn//3sbGBiHUv39/TslnZWWR9R60VSKRCJoUGOOtW7d26NBB0Rt9//79unXrcpYFFxYWbt68ecaMGdBghWs3bdo0Y8YMCDl37twmTZo4ODiUWJIqhurIIXnynj59Cq/KjRs3sDy9SUpK6t69+5o1a2AMgfNKFxYWuri4FBQUTJgwAZp4GOMtW7bAB3tzc3MLCgqWL19Oq8Lt27c5tRL5/ebNmwkTJpiamsK3FGbNmkW728l9W1JSUn799VeE0KpVq0rMdWJiYuPGjfv370/cBS9fvrxs2TK5xnh5eT1+/BiXJIcpKSnjxo179+5dUVHR4MGDNTU1HR0dHRwcOnbsiBCCNoGiGOC4WCyeNm0aGQqDgxEREeRaOuPPnj0jniwWFhYIoWbNmsHgD4RJT0+/desW9BXIhfQPuZnlcO/eve7du48bN44zq3r37l3YcAAQCoVkaHHYsGFbtmyh0youLnZ2dqYXhBQUFCxevLhWrVr05B/HmAsXLqirq1+9epUeeiK6FR0d/ffff9N6vGLFCniAoT6CYAKBwN3dncQgk8lu3boFDT66SGWf2nMuLi4YYwcHB/h8B8eq/Px8S0vLJk2agAxjjLOysiZOnIgQIpsIkksSExNbtGiBEBo7diypUgMCAsikMpCfn3/+/Hmo0Pft2wdFB4hEIthlQiQSzZkz5/Xr1wkJCVOnTiXikZOT4+/vX1xcHB8f37t37/79+/P7JQkJCX/88QeiMDY2Jk8FCQY9IU4WOMDxlJSU5s2bN2zYcNmyZbAPbYlERkaqqakhhJQvjOE/mQSBQPDx48fp06dnZmbKZLKCgoL9+/cbGBhs3ryZ3h4IYyyRSFxcXMg4J30qICCAfHjEysqqc+fOilJ88OCBmpoaGdnKy8sbM2bMkCFDli5dSg9m+Pr6VqlSBT72cvr06d9++239+vV8JabLViVRKTmEv+CohhAik9Wc+hfmnBBCM2fO5FyLMX7y5AmMIZiYmMA7KRaLJ0yYgBBSU1Pz8/PLyMgg2oMx/vjx48aNGzmzjxCVo6Nj9erVtbW1ExMTxWKxj4+PkrF4YqG1tTWYZ21tDadSU1Pl+qxjjENDQ2fMmEH7lW3atOno0aP486oZY3z58uUZM2bQH3PIyMggKyjokCkpKZMnT05LS0tISJg0aRLGuKCg4N69e4sWLdq9ezfJglgsTk9Ph/pRKBQeOHCAvLp5eXk6OjpQSiTyvXv3QuubTlEqlW7YsAEqgqioqFq1aiGE2rRpQ5rJGONLly7BQhTOUBIf/HmNkJCQcP369RcvXmzZsqV58+bERZYO+ezZs1jKlenly5dEIWbNmuXm5kZf8urVq927d9PFtXXr1tatW588eZJz68mP5ORkDQ0NuKGQCw4hISH+/v7k323btlWtWhUh1KFDB9Jcwxg/f/4cGg3wb1JSkrGxMT0IRhfCjRs3wBPE09MT/GjoLCcmJmppaSGEYDgBsLKy6tSp0/Xr1zHvfRGJRDdu3KhWrZq2tjY850VFRU5OTpyekKWl5Y4dO+DfpUuXwtuXk5Ozbt26wYMHjxw5Mjo6WiQSTZs2LSMj4+HDh2SBPH3LLC0tZ8+eTW8MC4L38eNHKEYNDY0///yzR48ey5cvJx5wpFhSU1PJUwdHUlJSyLyap6cn3RzMyMgYPnw47VRc4jzCX3/9BbcSFsYQHyW5jyK/JENCQkJCQp4+fTp16lSpVBoTE7Njx469e/dyGtMQPiwsbOvWrXQrkH7qiH5bWVl169aNvr90eT5+/BghZGNjk5eXl5KSsnz58nbt2pEuNQk/c+ZMyNcvv/wCG3nz7X/37h0M8vOzqTKomhxKpVJDQ0M1NbVffvkF+gGcO3fu3DnSuoTBH06Y1atX+/r6pqSkuLq6kqsiIiI0NDRq1Kjx6tUrkUhEXPgwxgcPHjx9+jTmaeGLFy86duy4evVqfpNK7mNEDLC1tVVTU6tVqxa004uKiry9vWl5oImIiICGKrl83rx5ZHwMY1xQULB7924LCwsdHR3yNGOMU1NTfX19+Y1rjHFycrKBgUFRUVF4eDiM6hw5csTJyYn4qkCt8eHDB1KP37lzByE0depU+DcvL09fXx/8U2Sf+kBmZmb87dkePHiwfv16iDAqKqpdu3Y1a9akvXAFAsHSpUs/fPiQmJgIE4T8YuQXi0wmu3nz5ogRI4YOHVq1atWqVauSFYT0vZbJZHSVmpSURH/YwcTEhIy3w5ENGzbcu3ePHImNjTUzMyPFAtHSI6t5eXkLFy6Eh61Zs2Zyl3PAWgv4Df3CatWqzZkzB5r/pLVx8eLF/Px8EvODBw8QQkOHDiXjXcQRSSQSLV++HB5+siCSXCiVSk1MTIYPHw4PLRAUFLRy5UryoNLhBQIBRLtw4UIDAwMIEBgYyNk5LD4+vlGjRlWrVj1x4gTGePTo0fBtZA8Pj7Zt2yKEateuHR8fn5WVBUJy7ty5AQMG0PLj5OQ0efJkdXV1aATQNXtBQYG2tvaIESPIECVHb8hLZ2dnRy8hFYvFXl5eIFq5ubk9e/Zs2bIlmRpMTEy0srKi3SyVuwJgjFetWqWmpla/fn0fH5+srKzLly8rCUwjEAgcHBxmzpyZl5d369athg0bQnefjPbzNWbHjh2kdYIpgffz8/vnn39gezaMsY2NTZ8+fTglRuJ59OgRPH6DBg3q1auXnZ0dMYkuwz///FNNTa1OnTo7d+6U61eIMd6+fTtnNKKUea9EqJQcyj6N1Lm4uBgYGHCG17Kzs5csWUK0EOY2ONf6+vrq6+sXFhbm5ubSDugYYx8fn27duoHvOCElJWX06NERERH480oEY7xw4UK6O0Kc9OQ+SfRZqVQaEhIybtw4SCsmJgZqAbkXhoaGwvtPTi1YsKB9+/ak5b5gwQJoHnIWk3h4eJC6nmNYbGws9C/d3d179Oihq6u7ePFiUpXTNTJpHU+ePBkhNG3aNPg3IyNjx44d9FYAa9eu5Qw8ArNnz4Z3Hl48gUBgYWFx6NAhkqNz584ZGhrCrRk2bNjTp0/pTgmH/Pz8V69e2dnZaWpqqqmpHTx4sLCwMCAgADZRw59XAfyuwI4dO3777TeQEA8PD2tra3AWhauCgoL09PToCUIHBwfigkiKkVSpQUFBgwcPhoetYcOGfMd6Drm5uU2bNp02bRospsRU7ezn50fWXMOpPXv2QMybN28mBsDZ69evE93CvMfS1dXVzMyMNP+BvXv30s8YCS8QCK5duwad8i1btkCPUyqVXrt2LfbTxnUQ0t/fv1q1amTE5ffff1dXV4c7df/+fejx79u3b+3atWFhYRjjK1euVKlShYyynDp1CiHUt29fenE6sfnPP//U1dUlbSn6JtIFnpiYOGjQIIgfPNESExPJfIejoyNCqEqVKmRGNiIiAjqLuNT1u1Qq9fX1nTRpkkgkevHiRffu3Q8ePBgXF8cpTwA+KHb9+vUtW7bAFjn79+/HGF+8eLFevXoHDx5csmTJhQsXyJgH3ZB69+7dmDFjYNSEUxp//fUXGf6B+wLLo2XyxkvPnDlDaryFCxeS4yQw/CgsLLx37x5ZG03XRRDg+fPngwYNgjEqep6oxBKrXKiUHJIfixcvht4AqfXS09NHjhwJ7p0bNmyATaHw508P7PIMjxqJigTYvn37qlWr6PBisdjAwIB2VSCntm3bRlZwyz5HbguU88Tv3r17/fr18NvNzY1ev8XBzc2NdILhyObNmxFCI0aMsLe3nzdvnrW1NX/Dl5ycHBcXF/qVoA24ePEitCLpnjSZjyTleffuXZKXq1evamlpkdV7AQEBVlZWpNNz+/btnj17xsXFcYp0165dI0eOpEd6Q0JCJk+eTFxSQ0NDe/XqBTOO0M6tU6eOhobG0KFDR40aNZZizJgxw4YNU1dXr1mzJkKoUaNGFy5c4JQw/6+MEkVPT89mzZohhAYMGKCnp9ehQwcvLy9yeXZ29qhRo6DHAzx8+NDU1JQe/aZvroeHR926dRs1ajR8+PAjR44o+rwiXRrW1tZkrz76lEwmO3PmDJQeMT4oKKhr167VqlWj5xcxxpGRkX369IH1fPyHzd/ff+rUqWQBABw8dOiQgYEB/elNEn7Xrl0gGB4eHjDIiTF+//79f//9xym9wsLCQ4cONWvWDNy4jIyMEEL9+vUbN26choZG48aNO3XqpK2tTbpTT548qVGjRseOHW1tbVeuXGlqanrnzh26L0tsuHPnTteuXUknmPOs0n1imNH4448/7OzswN2UyDbGOCQkRE9Pz9zcnCyVcXNz44/+lVjFW1lZgVf5oUOH4NXo0KGDpqbmyJEjR48eDY/iiBEjhg0bNmDAANjDD2R469at0PFyc3NDCNWoUQMh1L9/f3o/Utmn3vCECRNIBULfxFOnTg0YMIAecLK2tiZOEnAkLy/v4cOHfn5+jx8/NjU1vXTp0syZM83MzMgIE/38k4fKzMyMM1ROzqakpICcm5iYHDlyhP4UgfKyqnSojhwCQUFBo0ePnjJlCv16FxYWGhoazp4929fXl9MnIHcUquxBgwbxP24gk8msra379OlDr7UICAj4/fffmzdvznH/CwwMHDduXI8ePaB9ihXImCKSkpIWL16soaEB05aFhYWLFy/mL5Qmr82JEyfI1AgcdHd3Jxq2ZMkSTkYgTHBwsNxtyQBbW1sYEvTy8oLJvPr165OdG4EbN27AS0jSJRs3Y4yvXbvm6OgIBzdu3Fi3bl16d1PIpqGhYbVq1WCBAcZYIpFcu3atc+fOUL9LpdLTp0+3bt165syZEH9ISAh4MSinZcuWGzdupAcwlZQ2vWKhe/fuBgYGJJ7Zs2eTYI8ePQJ3R6iGiouL161bV69ePfLJJ87dCQ0NHTJkyPr160v8nAK55MmTJ8bGxnL1ICQkZNu2bfSKN9mn4e4//viD7t+cP3++Xbt2EydO5C9RyMvLs7a2bty4Me2KmZqaCsOz9+/f54RPTExcs2aNpaWlRCIJCAgYP348aRz4+vpeunSJlB4RJIyxu7v74cOHMcaurq6kJKtVq3b37l3OSoDk5GTwzEIIaWhocCYUiNB++PBh4MCB9Ef45N7Q7OzscePG9evXDxpDgwcPFgqFIpFo165d0JWB2IqKiojLklAoPH/+PGdHIX7k9MF3794ZGBgMHToUOkm3b98GSVNO/fr1jYyMaG+D4ODgOnXqwFnocNOp+Pj4DBo0qG3btnTXEGOclZW1fPnyWrVqwXA0AKtT6G0x7O3tYVcKgPN1Vbla6O/vP3HixLlz5/KFGWMcExMzaNCggQMHQoSw2SRmcljBgeELCwuLAwcOcL6x4OjouH37dkUX5uTk2Nvb6+jowBwV52x6evrx48dNTExgRBQScnJymjhx4uTJk2EIERLKy8vbsGHDgAEDhg0bxhGPEoH3PzIycuvWrevXrye1eWRk5PTp04m0c1aVnTx5kqyLJ01IoVC4ePHiqlWrTpkyhb9ACn7b29sTvwzy9EOVERkZOWfOHKi8hEKhnp5e/fr1ics1qfg2b94MjiF0bQiAs2VaWlpMTIy5ubmOjs7Ro0dpz0kfHx9jY2NtbW2ihZmZmU5OTkuWLAEZTkpKsrGx0dXVtba2JiMzUqnU1tYWdmcmNG/eXF1d/ffff9fX11+9evWjR4/oJkuJr6tMJvv48ePatWvbtm174sSJrKysfv36IYRmzpwJPfL8/HxHR8fRo0f/+eefMLYcHBw8efLk3r17Ez8UTiqFhYV///039GhLiVAoNDY2holYuraC3xcuXICOgowCLjQwMIBuU3p6uq2tra6u7tq1a4nvDAn5+PHj6dOnDxgwwMbGhiTq5eU1duzY33//nb9fbnR0tJWV1alTp0QiUXJy8qlTp+i1fWfPnoWWEJl1I/ZcunQJxirz8vJmzJiBEOrduze5y6S44EJHR8d69ep17doVumjkAZZ9cp/BGBsaGpLdEDnlTMT45s2bmpqaenp6iYmJ8+bNa9WqFUxSJCcn79y5kwxFcsr86tWrMHSpHOhKSiSSkJCQ9evX29jYkAXBGOPjx4/DsDz9TKqpqamrqw8bNmzGjBmnT58m9QYdp6GhYfXq1RcsWECvnCksLDx37tzYsWMNDAzINvfA69evFy9erK2tfe7cOZJxjHFRURE9JQGftgaR/vXXX7du3arcOUggEHh6epqZmR07dgyas/QzJhQKjx8/3rNnT1NT08TExOHDh3fq1Il+EpgcVlxEItH79+/pQXzSiD58+LBcBy0gLy/v9evXimakMjIyiBMXGZYJDAwkjuAkzry8vBs3bnCWl5USqCM+fvxIzAAjAwMDtbS0OM7WgLOzs7a2NvE7oEdUxGLx8+fP5e48BxgZGcFHGOi0gOnTp8PUHZCQkEBWrNMhzc3N5W4yKZVKTU1NYVDxw4cPUDmSU2Dn27dvOSv0s7Oz6Y5UampqYGAguZV01j58+PDgwYMHDx64u7s/ePDg7du3GRkZHI9wfrtBETk5OW5ubvb29sTO+Pj4hw8fElepgoKC169f0/3vyMjI+/fvcxx96bS8vLyuXr3KKS65kLOHDh3irP+jI1y5ciVn4B1+79ixY/78+dJPW94EBgbSyzDo8G/evHnx4gV9ViaTvX79OiAggJ+iTCZLSEigv3rB6Wvq6elNnz6dn51r166ZmZmRoW+hUOjt7c1pnQDkhgYGBtILLjnP6rZt20xMTGi3NT5hYWFHjhy5desWPC1ZWVlktMbPz699+/ZyO+hubm46OjocPwC5gD0SieTDhw/0h6uIPQUFBQEBAfA0AgEBAenp6XI3ESVkZWW9ePGCk+vCwsI3b97I3QI3OjqazCjTxUWHzM7Ovnz58r1790JDQ93d3Uk5yH0R4KBAIIiJieFsiAM/RCLRy5cvDx48CAtJMcbJyclyZ3ZVCdWRQwJ9UzHGFy9edHJywtRckaIbqfwe0y8z/wjnwi96Vjgx0E9bZGTkL7/8YmBgEBUVJfvUhHz79q21tXXDhg3JnAHdTqeTpqfE6RQnTJgwfPhwzsZOiYmJs2fPJh4QnFxwBMbCwqJWrVrnz58nr31BQcGDBw90dXXV1dX5Y19KCoq2jWM/+ZefBeXlWZo3Vm6xcwLQ/3K2Z1OUBGylrSQAh/v372/YsIHjYUj/0NLS6tevH2ef2CNHjiB5a0D5l/NzLTenMl4bQu6jjjGeP39+gwYN/vvvv8zMTIFAUFBQ8Pbt202bNtWpUwcGVBXFT0PfTXqIglxob29vYGAAXTGpVKpobzy6+ct5Qvz9/evWrQt7Ass+DZxER0fDmne5W5LKLS7O80l7vZXmcuUhyZvLMZ4eiOYYgHklxi9SOh7pp48hc45zHhvaEYxftjJe7VT697ESoTpyyHmjyHNz+fJljgul3Av5vzlhOJIj94nknC295XJjwxhnZma2b98eIdS4cWNdXd2FCxd26tSpbt26CCHiwsOvs2RUhSj3hZkzZw5CaNiwYbt27Tpx4oSDg4OVlRU4ktBfv5NrGMSwf/9+GBrq27evmZmZkZER2Fm7dm2yuJDjTMs3DPPqCzpdRUkriZCTWVnpboFywzhDgvSYnox60uBvenr6mTNnoB4pjQHu7u7W1taxn9w9+PnCGMNSDQ0NjYMHD544ceLQoUPgx0vcR0ssEEW1J12qWF7jiX8txvjmzZtw61u0aNG2bds2bdrUq1cPIQRrVOiQnBKmbwrHVJJZjHFhYeGGDRsWL15Mu37wK19OQvxCKCwshKHvJk2ajBkzZuXKlT179qxfvz5nAkz5DeLYzCkNetmu3KKWm4SiOyLjVTJyY5b7nJDy4ZSw7NNDqyh3/Aj5l3BO0ReqGKojh3LJyspycHBQtJc0pl42zv1WBP1AfEc7lcQGG+JwMDMzU7Sxp9zXD3/+etAuozT0MKns89qKPoIxjoqK6tKlC+fydu3acbxYv7qUyvJ9oxMq5QuvqHZ49uwZbAdTYnJ5eXnHjx+3sbEp8aPHHh4e/DulpaVFfLVKAz+PiupBRTeOHMzIyBg0aBDHHiMjIzJOoCgvnONyn6vw8PDt27fb2dnRWy6QSzg6Tf8rN0WyHIXG0NBQ0Q6fpbeZ/Esbr8SSL0rr61RHrm0c85Rcy3+e+anLfVNUCZWVQ7hbERERcj9MCHea06TifOS2glBcXLxr165OnTrVqFGjVq1aU6ZMoT9A+EWQ3AmFQtiJFKhSpYqGhgb5InyJwyAQyatXr/T09GrWrFmjRo3OnTuvWbMm9tNaNNLI/To7KwWc3hVk2cHBgbibKhGV4uJiHx8fDw8P5bUVHC8qKqLXy1atWnXy5Mnf5SOFWEGPUFbSOMrbt2/19PRq1apVs2bNUaNGkS0OFOVaCXTNW1xc/PjxY+J7wunocHpUZHBPbpxwtri4eOfOnU2bNoV3Z+zYsZzGigo/onL1DMu7QXJ1VCXHQktExeUwLCyM9hmhTwE5OTlkrp7vJFlxSE5ODgoKiomJkX1VyxHzenuFhYX37983MzNbtmyZh4cH2dq7xDjpurK4uDg0NDQoKIj+gkwFL8nvAl0v08ddXFzIkn+suMEulUrpvpSiYifHYfvK+fPnW1pa+vj40BNOX/oYyI2fjkHJ7aNvvVgsjoiICA8P53jofKkxtA0ymYy4t/CnsuSmUppKPz4+PigoKCoqiowS0Sl+delVcIikPXnyJDY2NiwsbO7cuUo+usJvwhYWFvJXnak2Ki6HgYGB9IofQnJy8uHDh3NyciZNmmRsbEwvUqxor4eiV/0r5JDUAsorO+XRkupSrmGcWqz05lVGZJ8GSO3t7aFIHzx4AH40Si6RW5XLdTLihOfH8+1PLB0D/TkF5U8d31Tl1io3gPyNjo5u3779unXraF9HuZZgjEUiEeezJ3JNVZKiaj+ikLWsrKyuXbuqq6vDF4lhpY2iIoUCKSgoWL9+fWpq6oYNG3R1dZV/h07FUHE59PHxofdHJqc2bdoEviQw016Rd2rn1DVfXQ/KeG1tzm/O3xJNUm6YClc0mCqi4uLi8ePHI4RgtzYXFxf+sj8OSu4CPxUZ7xu8Mkquvr2oyeWZmZkjRoyws7NTFCc5SDeG6JDkwi8dGCCRwPdM+vfvTwZs4HhoaKhYLH758uWaNWtycnLg4KpVq+bNmweVtdzGBB9FPU4V5sqVKwghdXX18ePHd+jQQdG3IWXUi3/27Fm4pF69enXq1ImLiytLg8sXlZVD4MmTJ/R6HaCoqGjIkCHVqlU7dOiQtrb2vn374LgKvCGyT+7RAoHgzp07ZDub8rZL1SCPSnx8fP369atWrQq7w7i5uXEWQX7fRH9EnBDtwYMHEUI9e/b8QR9A5yglX0cxxq6urv379ydbhUHg5OTkDh06TJs2bfjw4Qgh2OYtJCSkdu3aCCHy/YofYbMKkJqaumzZMh8fH7FYrHxJNCnDefPmIYSsra0nT578zz///Ax+AASVlUO4f05OTmQXdpqXL1/C/H9RURHfW7qsbf2ubNq0aeHChXZ2drVq1SIfEK7smapokH6GVCp1dXU9efIkxrigoODMmTNyvyRcYSEPxsmTJ3/99VfYqOW7vwj8Ppnc3hvGOCsri7OyZffu3QihAQMG9OrVq1OnTvA6h4aGdunSRUdHB/ZDUIHX9rsjUzwnIvcgKcOAgIA9e/bIZLL8/Hz6Iyrnzp3jb+anYqi4HFpaWnK2O+KHwV87G1cBeffuXaNGjerWrdu8eXMygqcC+apoyC3S5OTk5cuXl4s9Xw2dkZiYGPpzV983CTohzhI6jv5hSj4xxj4+Pnp6en5+fmlpaTDWB+ETExPJjjZftMz354EuFrnlTKDbKHJPJSYmtmrViv5etEqi4nJoZ2fH2WIb856M0ixgqiwEBwc3bdrUxMTkwYMHGzZsgDkY1XbyLHs4fRpS6WRlZZGB98oCvxX4I6bWSGx5eXng3MhpiSpZY44x5myPQhtGLvlepqoScotUyZ2lz3K84VxcXBBCvXv3pj9wpnqouBz6+vrSW9xyzso9UtlfrXfv3l28eNHAwIDe96t8TVJVOPVLQUGBku+EVFj4FeX31UI6wk2bNrVp04bsW0QnpEgOOZZwjn93U1UGfhmWUgvlXgW7Rnz1iufKgsrKIcZYKpV6e3tX9vmbzAAAIABJREFUZK/RHwR84UXJKDHj+wIVR0ZGBvlcO4NPWloa7JEGU61shJNR0VBlOSwuLn79+jX9HT7VBtpxEonE3Nz8jz/+UL6nPuM7AtV6Wlra133PRLWhe8+Wlpba2trM/4VRMVFxOQwJCfmp5BCmCSUSyY9z92dwINV6eno6/+N2DPy58pHFP2xKm1HRUFk5hH5Sfn4+/9MwqoryuQHGjwPKvKio6Odpe5UeGW+XInqjVPa4MioOqiyHnB8/Dz9hlssX1gpRDt+VQ66bDINRvqisHDIYDAaDUXqYHDIYDAaDweSQwWAwGAwmhwwGg8FgYCaHDAaDwWBgJocMBoPBYGAmhwwGg8FgYCaHDAaDwWBgJocMBoPBYGAmhwwGg8FgYCaHDAaDwWBgJocMBoPBYGAmhwwGg8FgYCaHDAaDwWBgJocMBoPBYGAmhwwGg8FgYCaHDAaDwWBgJocMBoPBYGAmhwwGg8FgYCaHDAYDkMlk5W3C1yOVSqVSqUwmo3ORk5MTGxtbjlYxKhdMDhmMn5fU1NQ5c+Z4eHjgSi6HYDyRw7i4uDdv3tjY2ISFhZW3aYxKA5NDBuPnZevWrQghTU1NiURS3rZ8E3S/MDw8/M6dO+bm5mZmZmKxuHwNY1QimBwyGD8vGzZsqFq16r59+8rbkG9F9okPHz54e3uvXr164sSJOTk55W0XozLB5JDBUHGUjIIKBIL79+9nZWWVpT3fEZI1IodpaWlr1qzp16/fx48fcSUfAWaUMUwOGaoMGUPj/P15akniY8I5LpPJpFJpuZj0XYCbSNxnSF42btzYtGnTV69eYUojy9XSb4Kv91+XKVJQlb1AfihMDhk/BbTbIf33Z4DIBud4ZZdDovQkI1ZWVk2aNAkICMCf2gG4kt9oWsbgCP9HaaCvqtT3/YfC5JChykgkkvDw8PT0dPx59VGpq8gvhWTW1dU1MzPz+PHjixYtys/Pr9SFQHcN4ci6deuaN2/+8uVLrGDdRWVEbrf+6zp50dHRbABZOUwOGaqMk5OTg4NDTEwMVAFXrlw5duyYCnQaSg+pN319fatVqzZ27NhffvkFIRQcHFzepn0TtB5IJJL169e3bt0atJAzJF7Zb3RiYmJwcLBQKDQ2NnZ1dcVfNQgsFotHjBihq6tbVFT0wyyt9DA5ZKgsUqn02rVrt2/fhn/T09M7d+6MEIJK8wchtwouxzkbkqihoSFCaNasWT179hw7dmxhYSHHvLI0RlFPXYkZIpFI7nGpVLpgwQItLa13797hz/PC70h9XTbL9/YtWbLk119/nTZtGkJo3Lhx9KnSXA4/vLy8EEK9e/cuLi7+ChtUoJ9dGpgcMlSW4uLi/Px88q+Pjw9CqHHjxlBv/jhI9UFP0pTXnA2pyJ48ebJkyZKcnJzk5OQPHz5wAsidXPyONpCZPE6i+NP0GFZcv2dmZjo5Ocm9ayKRaNGiRbq6uomJiXRUAEmUJP3VeSwvPYiNja1du7aampqpqWmjRo327t2LS6dPnHuan59vZGS0bds2+o0oJZzetgrD5JCh4pCaVywWX7lyxcnJqQwSlUgkZIjS1ta2uLi4HLsXipKOiooqKCjAP7im42ihnZ3dvXv3MFVf05N8Eonk4sWLo0aN0tfX3759u62traWl5aBBg+zt7fkL6qVSqYWFxcyZM3Nzc+lcpKenb968+f3797TK3rx508HB4UuzKZFIQkNDy7F7VFBQsGXLFnt7e4zx8+fPi4qKSmkMJ5ijo2NsbKydnZ2GhkZoaGjpDYAYSLpfm4/KAZNDhipD17n0wR/3btOtcrFYPGvWLITQmTNncDlNYskoD0z6+MePH1u2bDlr1iyiiD/OBiKHUVFRDRs2bNasWUpKCuYNZubn569atapatWqIolu3brdu3ZIb7ZkzZxYuXJiXl4c/73zv27cPITR37lySr7y8vEGDBiGEnj59+kWWx8bG+vj4VCglkPF8iJSEhDDe3t4IoV69erVp0wYhdOPGjS9KEQrh6y2uPDA5ZKgynFqD/P6hnSGSaEpKClRAUJuUV5VK+sfEh0gmk61evRr0hmxY+uPGckmBXLlyBSGkpaUFQ3Z0700gELi4uFy6dMnX13fDhg1z5syZP3/+mTNnQO34FBcX37t3D7Qcfz5MOnfuXISQra0tiTwoKKhatWpNmzb90h29r127FhgYiHnDsGUPRwK/SKH//fdfhNDChQvNzc319fUFAsEXJf3vv/9evXoVV36npBJhcshQQTjdjsLCwoyMDPwl77NMJissLAwPDy99cvzjUqnU29vb0dHxiyqvUobkB5ObitwKVCKR9OjRA+QQnBXpAPx4vlc9mJ+ff+rUKW9vb05yGGOBQJCdna38cjr8o0ePiOUcEhMT7ezsSAcUYywSiVxdXa9cuaI8Zvx5TjMzM21sbODJ4cx9fimcNpncJprcs/n5+XL3DKKLQtHTlZWVBc2OpKSkQ4cOwSoLGFhWZCQdLVBcXKyjo+Ps7Iw/f61UUhqZHDJUCrkvalBQ0IULF740Knt7+/r168+fPz8tLa3EROHHmTNnYD+UUhrGD1PKRSAQAGrqhISEs2fPguOlog1oSOpCoXDNmjW3b9/29fXdv39/zZo1yeIEGjDDy8vr2LFjSoyXfd6nvHfv3s2bN5VbTtvPr1vl1sj8jMhkMm1t7X79+pGRUkWplLJXx8lgYGDg2bNnfXx8DA0N5Qb4Usi1xLFTua6Qg48fPyZjxfyQdHeff/bSpUuldKLm33qMcWho6KlTp1JSUmrUqAHTvfjzG6d6y/mZHDJUloKCgsjISIzxrVu3yHKLUpKdnd23b1+EkJqaWkxMTInhY2NjJ02a1LFjx/j4eM4pfkWjCNmXTAtJJJIxY8ZYWlpaWloihM6fP48V1Nr0QUdHx169eo0ePRpjHB4ePnnyZKFQiOVVc5mZmVpaWrVq1VI0wEjrVn5+vpmZWf369e/fv1+i5bT44c9refgdGxsbHR2t/FojIyOE0LNnz7BS7eTsRqREe8ipw4cPN2nS5NixY7du3erVq9f79++V56j07Ny5U0tLCzbNUSKH9JFz5855eXnJNV6JmmKMRSLR4cOH4dGlM6ikrOizR48ebdq06d69e1NSUmrWrHn37l1OeOIspkowOWSoGj4+PpMmTdLQ0Jg3bx440S1ZsuTFixdfFElSUlKDBg0QQg4ODiUGfv36dZs2bapWrbpjxw7OqdJrIQlfSgudnJwQQs2aNWvdunW1atWgwuJ3hmRUD+nx48d3797Nz88/duzYy5cvFyxYQCY1ORVrYWHhhAkTEELVq1eHBftK6uKsrKwRI0YghGbOnFkayecLFSfA2rVr5Xo/0okGBAQ0aNAgJCREbqGRJGhHKpm8ziIdp1QqXbZsGUJo4MCBQqHQzMwMIdS+ffudO3cqz1RpuH///qVLl86dOxcXF4cxJl/UUnLHi4qKjI2NoWWgKJii41FRUSYmJqStQxc1/x5xAqxcuRIh1Ldv38zMzOTk5Bo1anh6enIC4wown/rdYXLIUCkuXLigo6Mze/bsLl26LFq0CGNcXFw8a9asiIiIL4rnzJkzenp6pRn6i4yMHDdu3ObNmwMCAuQ6KfCb3oogATIzM0tM98mTJ126dDlz5syLFy9OnjzJiUGuATt27IA1+P369Rs6dKibmxtW0Mk4fvw4Qqhhw4bGxsZQpSoyNS8vz8TEZOHChT4+PrAZHkGRNNJaSKcLv+Pj40eOHCl3lSGtZ4GBgfPnz1c0tkyvZSRn5eaUPrt169ZJkybdvXsXuvjgFYwQ+qIPYMn9cqS3t3ffvn2fPHmSlpbm6urq4uICg9tyLSe/k5OTp0+fzilVuezcufPRo0f0ER8fHyMjI/6DJxAIODImo4CoJkyY4ObmBnONqampdevWhdYkXXqZmZlBQUFMDhmMCgT9Jj98+PDIkSPgbUh2XUlJSZkyZQo4VsDxoKAgTE2/EV8J8m5fuXJl3bp1xIWBTiItLQ00D44IhcJNmzaBrhBot4vs7GxSJVlYWBgYGEC0Mt44HrncwcEBhnbh7D///DN79mwikPRVCQkJnAUkHINpy2NjY6dPnz5gwIDJkydbWFjAMDIdWCgUmpubOzg4vH792tjY+OXLl/Hx8WQvGIgkNzeX2A/HT548efDgQf59cXR01NfXh4/RExuKior4LQaJRJKbm0vC3L59u27duklJSeRCkUgUHx8PMkNu07Vr13x9fTGvNid/OcsZc3Jy6JBCoZD2tcEYe3p6rl27ll7d+Pz5819++UVTU5Osa+RYTmIjd8HX13fkyJEWFhbEWjgOnU6EUJMmTUxMTGDXQP7MHzwk06dPh60Ew8PDZ86cSW5BVlYWXZ7EnoiIiPr167do0QLuKRy/fv36kiVLOAYHBgbevn2bXF5cXJycnEzH9vTp01WrVtEbuYWHh48ePRq8nEiK0dHRa9asqez7/PFhcshQHaZOnbp06VKifEBERMS0adNAHWNiYgwMDK5fvw6nZDKZm5sb1O9kLuTVq1cXL14kAehKUCqVenh4pKam4k9Vw/Pnzx88eEAHln0aR8IYf/z4kbTZ7927BxUiva8mn/Pnz48dO5Y479y9exeuIs1zvlX8qPijWC9fvtTQ0GjVqhU0+fkGY4zPnDkDo6NdunR5/fo1fTkJ9urVK/C2hX/fv38PLvgc4uLiYKj56NGj9OXe3t78bnpISAgRNozxxYsX1dTUiBxijIODg589e0Zbm56e7uzsTG9qI/vkKEQvVCfHw8LCyLAwXPXu3Tt69UteXt6FCxfohg4Ec3JyIgtR5N4v+nhERMTx48etrKwmTpxI1odAADc3N4RQx44dr1y5Qg+Tcm7B06dPYdnlL7/8IhKJfH19YfUkxtjf33/69OnkcaIvPH36NDwkx48fJymeOHFi9erVENjPz2/v3r1Hjx5dsmQJmbmE2weFAJnNz8+/cOEC8U6CMM+ePfvnn3/oIydOnKhdu7axsbHcAqnUMDlkVG5IA9zZ2blOnToIoX79+tHNeWdnZ3h1/f39V65cSe9KExYWBvULedXfvXtnampKd00gJPyIi4u7f/8+ERupVGpubh4VFYU/r9rIhUePHoVJF5FINHToUKiz/Pz8MFWNpqWlXb9+fdGiRdu2bfP391+6dCnx3KGvIh9qkH0+E8YfFaQr6GfPnsXFxZ04cWL8+PHXr18nXUzaSPibk5PTp08fSOvvv//mB8MY5+fn37p1i9700tbW1sXFhX9TYNYNIUQcU+HyzZs38zcJ27FjB3RrIF8XL16sV68edNkxxmKx+ObNm5zFBlu3boWdDTgZ9/PzIytq6HLYsWMHWTMDqbi6ukKbA4LduHFjz549JEIIk52dffv2bboPx88pOVhcXAwLSAIDA3fu3AmlRMS7X79+K1asIJminxPyAMfExIwcORLKbd26dRjjY8eOWVpaYowfP368cuVKcFMizx5JOjExcc6cOQih/fv3E8MsLCyOHz8uk8kePnw4ZcqU3r17T506lTSGZJ864snJyaRM3NzcYLEmnV8/Pz9/f39yiYeHR6NGjczNzaFRqGIwOWSoAu/evevatStUJbq6urS35LFjx9q1ayeRSFxcXHJyciA8VENubm7g10Be/sOHD1etWnXEiBEwYcPZz8XZ2Tk2NpbE/PHjxxYtWvz222+kNqcruKSkpOXLl0MNWFBQ0K9fP4RQq1atYHdN4NKlSx06dCA7sMycOTMhIYHELxAIBg4ciBBq3bp1eHj4mzdv6Cwr90GNjY01MTHp1atXjRo1OnXqRHp7UqkU+sHEVLA2OTm5Y8eOCKHOnTuTmo4jh15eXrQnJ6xIa9GiBekfE+bPn48Qql27Nt21unnz5q5duzgh37x5Y2lpSW9id+vWrXr16pGx7qioKI6Xf2pqaqtWrerWrQudeHJhZGSknZ0d3YKRfZpltLCwgAFAOBIVFUU+DQGBV69eXaNGjSNHjtClevLkSbKLjZKifvbs2fbt21euXLljx47t27cPGzYM7hR5EmxtbelxS7rwSZynT5+GL438+uuva9euBTVds2aNrq6uVCq9du0af4UG/AvH09PT27VrRwoEY6yrq7t8+XKhUOjm5lZUVEQmgIlVMTExt27dovNrbW1do0aNgwcPwogxHMzKyiJ2FhUVLV++HDxdVRImh4xKQ0ZGBtmFhMbJyQk2f4GuIUfhDh061KxZs/Pnz2/atMnLy0ssFpNa9cKFC5zKZfny5RCPu7s7HCHVR0JCgpOTE119kPHPmTNn0iGBPXv2QNMeSE1N3bdv34IFC8iR3bt3w2TSb7/91rJly65duxL3V1IBpaWl7d+/f9GiRXl5eVOnTr13757yLxKkpqa6u7uvWLGiSZMmkyZNio2N9fDwiP18pQRdrcuoObbY2FgbG5tNmzbJDSkQCC5evEhPHCYkJDRs2BAh1KFDB45VOTk5zs7O+vr6ZMxQJBJNmjTp8ePHHIPNzMxgQ05SepcvX540aRJJ+vr162RKDA6+fv0aRhSnTp1KR7Vq1SowXiAQkLsMSRw+fJiOwdnZGdw1yf0aMmQI3EoylpucnDxgwAAXF5ebN28WFhbSRVFcXOzv7w+PYnBw8K+//krvKsfZAi0jI0NXV5e0omhISdrZ2TVo0GDWrFlubm60D5G5uXnXrl3Pnj27bds2uaPlUVFRIE5FRUVGRkb0QPSoUaMmTZp05MiRw4cPk54xnbSLiwt9HGNM+qZkcp0kBwX16NEjuFmcZ0NlYHLIqOjAi1dQUDB48OC//vqLbubjTxtQ1apVa8KECfv374dvNdCv/dmzZxFC9erVQwgNHz6cOCa4ubnBknn6xX758uXo0aMRQnQTWPZpZAkOEq2Kj4+HAcbFixfTITHGAQEBvXv35ixRWLp0KezugTHev39/ixYtyMowV1dXa2trjuXkqrt37+bl5bVs2VJNTW3UqFHTp0+fM2eO6f+1d6dhVVx3GMBHYwVErTER61LUms1UTYvW5DFqW7VuWDGidYnVIMS4gQsa0ahRtMZorLgkRlAEJCCLGlQwFYOIKCAYiOyauLEoAqIXgQt3+ffDeTzPYeYCF1zCvXl/H3xwmOXMXJiXc+acM87OLi4uc+bMcXZ2dnZ2nj59+rhx415//XV2R/vggw+MmYtLPFZlZaWzszOrR+oVnT9TU1Nls7qo1WpHR8cWLVr85S9/EesTzPbt2zdt2sT/++mnn44fP17WETQ8PNzOzo43ITJ79+7lTXaFhYWBgYG8ewvbXKVSrV692sLCgj9XI6KTJ0+2bNmyR48e/v7+J06c4J/y0aNH7ezsxPc/l5SUsFZE8aDr169v1aqV+LaT4OBgSZJeeeWVP/3pT7xRgYjS0tJGjx7dokWLd95551//+td77723Y8eOcePGvfTSS87Ozqz2zBUUFLzzzjtubm71fATx8fGvvPIK//OLYcXz9PRs1aqVlZWVJEnTp08Xv0VEZWVlnp6erBPsli1bPv74Y3EPkydPbtOmTcuWLSVJUo7/KS0tVb74c/Pmza1atWrfvn12djYpfhTT09PHjBljcDyo2UAcgmnw9vZmqcZ+/9lvqY+Pzx//+Mddu3YVFBTw31vZX9AxMTHspsDHqhORTqfbs2eP+LvN19dqtYsXL+YD2vSPn3tt2LCBPXtjC9mtpKCgYOrUqeITMiI6cOBA586dFy5cyHeek5MzZswYfvu+dOnSoEGD2CMZpqysjD2wFHulXr16ddy4cew+WF5e/uabb0pGsLe352PhjZ83JCEhYdiwYatWrVJeQ/b1F198wZOSb8WSaf78+eLye/fuzZs3z87Ojl2uu3fvOjk5tW7dWqwaqtVqT09PS0tL3iuVH/HYsWN80GFSUpKvr6/4QfAzOnz4MB8OeOTIkV69erFaWosWLdgQTLVavXnz5jZt2sg6vgYFBYl9qdi/Go3G399/3LhxvFFx165d7HpOmzaNn1pSUlKHDh3Eq80enep0Oj7DnP5xu6KXl1e3bt1ee+012axG4s8PG93Ir4ys7hgQEMCO0rJlS9bszE8/Nzd35syZfn5+RBQSEjJ+/Hg+/wNbh03OIElSp06dlKNWwsLCwsLCxAITkUajCQoKGj16tPI9J8ePH7exsXF1dSWzhjgE0xAQEGBtbc36nesfN5o5OTmxFGH4jUa8p5SXl7NB4tOnT+c9yFUq1fbt28V4E9c/dOiQ2MmQHWv27NniyvzGtHTpUj7Q4vbt22vWrLGzs/Pw8GB9RqqqquLj411cXDZu3MjumCqVavLkyeKYP74rvnO1Wn3hwgUXF5dNmzbx+6yPj0/Xrl1l4delS5c//OEP/fr1s7e3Z4PrxYsmqwMZ9ODBg9DQ0BkzZvj4+MgGw4nXZOrUqbxLrXi5vL29WeVDr9drtdqsrKwVK1YsWrSI3YJjY2MdHR379+8vdkDNyMhYsGBB3759t2/fLhvIodVqxWl9wsPDed7wI7J/T548mZmZmZeXt3r16m7duh0+fDggIMDCwmLChAk6nS47O3v+/Pl9+/bdunVrdXW1+EfGvHnzWLRQ7by/dOmSi4sL3z9rCbexsWEth0R0+fLlIUOGDBgwgDUJvPzyy+zPINlYF71eHxcX16dPn44dO44YMYINjZB9KPygCQkJPO9JkfqFhYXsifjSpUv5Uaqrq6OjoxcsWMCuTEpKipeXF3sgLf44paSkWFlZWVlZsc69MosWLWJPfGWf5o8//ujk5CQuycvLW7RoUd++ffmgfvHzMjOIQzAB7HcvPj6ed+PU6XT79u0TW3XElcVcJKJr165FRESIraw//fRTx44d+Z1OtG7dOj7BKd+zv7//5MmTlcc6d+7cP/7xDx6rV69ejY2N5Z0n9Xr9o0ePMjMzxUeenp6e/H1P4i1evJdVVFRkZmaK+2FfXLt2zc/Pb//+/fv37/fx8YmMjMzOzs7Ly5O1i8qSo/4LW1RUlJ6eruypwYcxEFF6evq7776rbH0tLCwcPnw4Hzqi0WhycnJ4ZUij0cTHxysv8g8//JCQkGBw9k5ZKkyaNElsEeUuXLjg6upaWFgYHBy8YcMGVs/WaDTnzp1jn0VqaurFixdlhyCiysrKv/71r2JXSX6Jpk2bduDAARIaw0+dOsWfrlVVVfn7+7McffToUXh4OG8Jl5WciM6dO7d79+6srKy6Zgng62/cuFE2lFC2w9TUVD5BGlvIZpZnD2V1Oh3rGkpCFvLDnT17ls+WLh69urp65MiRCQkJVPuHkIhmzpzJx8awJVlZWcHBwbz/l5E/VyYKcQjNnSyBmCtXrvC2MoO5YnBDvuT69esWFhZTpkzhM1Jqtdq0tLRJkyYNGjSIBydf383NrXPnzrLO5cnJyR07dly/fn1dxVYu9PLycnd3l1UHxZWf5EYjbqvcszGby1bm99ajR4+2bNlS9tK7/Pz8t99+e/To0QZLLmunNfgh1lVUMaJefPHFqKgoPlavqKjIx8enQ4cOJ0+e1Gq1yncCKw8tHuLWrVuSJLGeNVxVVdWcOXO6d+8uGywoft3ghGT1/MgZXJOIKioqvvzyS/EVE+J1MPijW881FH+o6joic+fOnRYtWsim2qmpqZk3b16XLl3qmRGpwVMzdYhDMDHsl/D77783OCu38u9rgzQaDesyY2trO3DgwIEDB/bv379du3aSJMlGoDNbtmyRJKl///6+vr6nTp2KiIjw8PCwtra2tbWtfw4tXgytVvv5558vXbqUDSF4nreSBq9G/dsS0dmzZ1u1avW73/3uiy++OHXq1MmTJ7du3dq7d2/p8YDIZyQkJESSJEtLy/79+7OPiY1LEZ/LGoOfe1FR0YsvvtiuXbvly5dHRUVFRkbu27eP9aj09vZ+BmdQX2F+/vnnxs4s/1Tcv3/fxsambdu2S5YsiYqKioqK8vHxYb8OvOPorxPiEEzSN998w8YUK/9iZX/I1zXjPg+GAwcOKPuh8Mk/ZbKysqytrWUrt2vXjjVGNSg/P3/btm1btmwxOKXyc9DkI+ofPzu0s7NTXi4WIcZ32GmsoqIi/l5GbsSIEfW8tM8g8fTnzJmjPBFXV9f6x3E+XewoUVFR7Onj88fnSRDNnTvXYG371wNxCKaHvXlA7D0v+y77oq44ZCtoNJpNmzaxARitW7e2t7ePjo6uZ0MPDw/x3tG7d28240yDN9DCwsLQ0FD2qIYUT5uegyepGvJtAwMD2bVi2rdvz6tTz+hE2G4TExPZXASSJL355ptbtmxp7MvcSfgjiYhSUlJ+//vfix/lihUr+Nsin/pZ1FUeIgoODvb3938+R5RJS0uztbUVL4Kbmxt7RvDcLkIzhDgE0/PgwYN9+/aJU8/IlJWVsYlOxIX8zi7GQ1ZW1pkzZ9hEjkxdT4nY5CDvvffe2LFjDx48yKaPqasA4kEfPHgg9p/U155c7bnhZTCeLLnPnj3r4uIydOjQHTt28A4yzyHX79+/HxcXFxMTI06X06g9yE4kMzPTw8NjyJAhn3zyiTHzzjxd/EAXLlwQB9s8Zzk5OWvXrh0yZMjKlStjY2NlZft1QhyC6blz505kZCTrXiH+9mZlZW3btq2kpGT06NELFix4kqdlz4LBdl2Dqz1hGcRbf2lp6TfffFNdXf3RRx8tWbKkCaH4yzLju3NiYmKzGtVuxpfaSIhDMD3Xrl2TjZpiXzs5OUmSxN5i379//2b+IEQsORH5+fl9+OGHKpXqqfyFznfi4+MjSdKoUaMkSbK0tBRnTIVfBP9wz58/b4ofh4eHx5o1a5StL2YAcQimJzMzk7VxiXFYVFTUqVMnCwsLLy+vESNG8DceNGe88MXFxWx+teDgYHri4RZ8tzU1Nazb5Lp16+zs7Nzd3c3vFmaKWMNAcnKybIK65i8tLc3CwkKSJDbk18wgDsH0XLx4kc1rpa897vgKH6GQAAARC0lEQVTkyZNsJLX4OtlmS8yt/Pz8V1991dbWls8z8CR7FjtJRkdHs1cXFRcXK1+uBL8UrVZ75coVg1PSN2dxcXHW1tajRo3irxwxJ4hDMD0+Pj5s9lFxPjZlhDTzOKTaJbx161b9bwY2fp96Yaa6Jy0iPAPsA0pKSmrsiJHmID09nc07b359UBGHYHpCQkLYhI3iHV/5BcjgyjQH7FPQaDTfffddEwaNwLODOATTk56ezl7thvs7mBz2Q1tdXR0TE4Mf4GYFcQim5/Lly+yFCc9/9B7AU6FWq6Ojo82vvdGkIQ7B9GRkZNy9e5dQOwSTVVNTk5ycjL/nmhXEIZienJycumZoA2jmZA+88TPcfCAOwcTo9foHDx6Y5ShgAPgFIQ7BVCEOAeApQhyCialnoCEAQJMhDsHEIA4B4FlAHILpQRACwFOHOAQAAEAcAgAAIA4BAAAIcQgAAECIQwAAAEIcAgAAEOIQAACAEIcAAACEOAQAACDEIQAAACEOAQAACHEIAABAiEMAAABCHAIAABDiEAAAgBCHAAAAhDgEAAAgxCEAAAAhDgEAAAhxCAAAQIhDAAAAQhwCAAAQ4hAAAIAQhwAAAIQ4BAAAIMQhAAAAIQ4BAAAIcQgAAECIQwAAAEIcAgAAEOIQAACAEIcAAACEOAQAACCzjEO9Xm/kOsas+YtgBauurn6SzZu2lXJbvqTZXi4AgCdnVnGof0yn0zW4Glun/jV/ESx1EhMTR4wYcenSpSZs3uAVqOug/AIqV9DpdE3YLQCAqTCfOBSzsMG7djOv6Gi12ilTpkiSNG/evEZtyMOsrlSrf0OD102sNTZ2twAApsKs4tD4NfV6/cOHD/fs2VNSUvJMS9U0Wq127ty5bdu2jYmJacLmT7ex9Al3CwBgEswnDokoPT09MzOTjLtx79y5U5KkDz744NmXqymqq6uTk5Mb+/jw/Pnz9+7do8ZX41QqlUajYV/LNtRqtd9//71arW7CbgEATIX5xKFarR42bFjPnj1//vlntkR8Eia7j+v1+okTJ0qStGrVqqYdrq4uJ0YGhtg/paKiIjMzkzVUKp/e1fM8T1yBiO7evdu1a9eRI0dWVlYaUwC+z6KiotjYWNnjQ75CUlKShYWFm5ubwW0BAMyD+cRhXFycJElWVlZZWVl8oXjjlt3l09LSVq1adfPmTeMPwTbUarW3b99WPmbT6/VardaYqJCVJCgo6NChQ+J3eb8V/jyvrlAUH/jt3btXkqQ33nhDpVIZcy58h6dPnz569CgpspB9d8GCBZIkTZw4kW3Iy8OK1+CBAABMgvnEYXFxsaur67p166h2jxIi0mg0rK3P4B3c+Hs6W/PUqVOhoaEGv8tDwsgKIlvtww8/3L9/Pymqs/zfqqoqrVarLL+4sl6vT09Pnzp1alBQkDEnxVfQ6XRLly5NSkriC/kpsP/GxMQ4OjomJCSQoT8vGjxNAACTYD5xyCgrN0SUmZkZHR1Nihs91RGQ9Vu8eDFPL9mhxTI0WE72RUVFhY2NTWBgIAlVLubRo0c1NTVEFBERkZeXpzyi7JRlSxo8EbZOSUmJg4PDnTt3iIh1yhWTWKwEy04QcQgA5sR84lCZRnzJiRMnwsPD+XfFO35jb+iVlZU9e/asqwYmtqPWv2d+6Hv37llaWrI4pNrZFhsbe//+fZ1O99///vfWrVsG9ylLQeX+mfLycoNlIKLr16+vXbuW/1d5TcT/6nS627dvazQaJCIAmBnTjkNZXVBW+eNfrFq16ocfflBuRYopV5R7YN/llaSSkpLWrVsHBweLu+Jr8nZU40OisLDQysqKt77yDTMyMpYsWVJTU5Odne3h4cGqiY3NHrZ+Xl7eoEGD5s2bx3YiPvwjotTU1F27drm7u48bN443mcqijn99+vTp4OBgRCAAmB8TjkMxBXkfFmWVpaKiYtKkSay7qcFbPAkJoUxT2cK8vDwrK6vDhw+ToXBasmSJwXZUg4UvLi5Wq9UVFRVt2rQ5ceKEbCt/f/9Ro0YRUWRk5Ny5c6lJ7bqMm5ubJEmSJEVGRpIQhGxvfn5+r776qp2dnSRJe/bsUV4HXmAicnd337t3bxPKAADQzJlDHBoMwqysrM2bN9+7d+/evXuOjo5suL3s/q5MF2VYynb+8OHD1q1bR0REsNXS0tI2bdqkf9zhpVevXqwdlRpKxIcPH/r7+2s0mtLS0oEDB/LesPn5+ayX6cGDB2fPnk1Evr6+bDSIwQLXf32IqKCgoG3btiwOt2zZItuPWq3evXt3YmKiRqNZtGiRrL+M7K+E6urq3r17Hzx4kG2LCdsAwJyYcBxS3c8Lq6urhw8fLkmSl5dXQUGBg4MDayfkK4jrP3r0SNxVYmIiz04xOX788ceioiKVSvXGG29cvnyZLdy0aZMkSZs3byai0tLS3/zmN6wdtcHcSklJmTVrFhFlZ2fPmDGDF+/999+XJGnlypULFixgLaj/+c9/Nm7cSI2vHfIM8/PzW7duXY8ePb777jvZeaWlpeXm5qpUqo8//vjTTz/VC8M2+LGSkpJKS0uJ6OHDh5aWlv7+/rJrDgBgBkw4Dvm9uLy8nE9GwxaGhoay+tC2bdvi4uIWLVqk3IqItFrtzp07s7Oz+fKysrJevXoNGzbs4cOH4g5VKtX27dsrKiquXr06efJkNmyjuLi4Z8+ebKhfeXl5SUmJpaVlSEiIeCw+1UtZWVlRURH/VkhIiKOjIxGlpqbyB4cxMTFWVlas5KxqSERubm5hYWFUO/uNvDh8zRs3bvzzn/+8e/eu+N2qqqqtW7cuW7asY8eOgwcPLi4u5qfMo/f69eu7du1ik+MUFRW1adOG9/oxsiQAACbBtOOQ3Y4jIiJ4dY0tcXJyYqESHBzs5+c3ePBgccP79++znpbr1693cnLiNTMi+uqrr9iGFy9eFHcYHR392WefEVFKSspXX31FRDqd7tGjR0uWLJEkqUePHkVFRTU1NZaWlsePH+d7y8jI4Am0YcOGfv365eTksP8GBgZOmjSJiG7fvs27fW7YsIEd/W9/+xvbUK/XDxgwgB2RiLRa7Y0bNyoqKqh25UwWfqLy8vL9+/d37dpVHNrIvsUKL0nStGnTeBZS7c63Bw4c8PPz47uysrJi2QwAYGZMOw6JSKVSTZ06lU3UyTrUEFFGRsbq1avfeuutgoKC7du3d+jQgdfSUlJSunfvbm9vHxYWtnDhQtkzxWXLlrVs2VKSpPj4ePFYM2bMiIuLI6Lc3FyWHPz9ULNmzerbt69arVapVH369OFdWFUqVWxsbFVVFRHdvn27a9eukiSxTCKiiIgIT09P2blkZmauX79+wIABiYmJbLlOp+vVq9eyZcuIqLS0tKCgYMeOHWfOnGnw4pSXlycmJq5atapfv36SJHl4eFDtZtK1a9d26dJl8eLFAQEBvAyy9s/y8vIJEybcuHGD/besrKxPnz7snVN4cAgAZsaE45BZs2aNpaVleHh4Tk7OtWvX+PL09PQ5c+YQkZeXlyRJy5YtO3bs2M6dO8eOHTty5EhJksaPH8+npWY39+Li4tzc3MjIyNdee41PfEpEvr6+9vb2fGWqnRw7d+5kPT9zcnImT57M8o+Irly5wjM1Nze3c+fOtra2bDQ9EQUEBLDepFS7xnbjxg13d3dxmOCf//znXr167dmzZ82aNQ8ePBg5cmT79u2XLVvm7e19+PDhY8eOHTt27Ntvvz169GhwcLCPj8+2bdumTJnSrVu3F154QZIkGxsbPgMcz7Djx4+//vrrsbGx/CjiGfHCrFy5kp0ac/XqVUdHRzYhKppJAcDMmHYc7tixgz1sa9269YQJE/hcnadPn+7bty+73f/vf/9jFT5Jkn77299euXKFiIYOHfrtt99S7Rj4/PPPL1y4EB8fv2PHDjYpGhEdPHiwffv2bBS/8uldaGjogAEDrl69SkQpKSlffvkl/25wcHB+fj49DqHLly+LE6EtX76cVQHFXLlw4cLbb7/Npg/lzZVjx45lhZ85c6Zer+/evbtkHBsbm9WrV7Px+ySk/p07dyZOnMiuA9UeaskPqtFoNm7c+NJLL4njNdPS0thIDL5DhCIAmA0Ti0N+/y0sLHR1dbW1tQ0MDOzUqZMkSbt37yYi9hbDYcOGff3112xN1hdUkiQLCwv+3GvDhg2ss4zY6MfeuDt8+PCffvqJiHJycpydna2trVesWKGcq0ytVh85cuStt946duwYW56bm8vabImovLx8zpw5vD+O7CwyMzPXrVvHp1Fl63t5eY0ZM4Y/JuRbeXt7S5LUrl071krp7e3Nu9soWVhYvPvuu/Pnzw8NDWWNuqTokrp8+XLeQCqeFA/CS5cu2dvbd+7cmaU7L8y1a9fE3kAAAObE9OJQr9dnZ2d/9NFHU6dOTU1NJaKwsLCdO3dWV1fr9frLly+fOXOGJwG7j1+8eHH27Nl8nrbKysrU1FRlVe/cuXMuLi6sk2pFRcWhQ4d8fX155xexmZSIrly5EhAQwKcSZbN98tXy8/MdHBzu379PQvWL7aempmbu3LlidVOv1yckJERFRRkMm/Lycnd39yNHjvAlycnJgYGBbm5uDg4ODg4Os2bN2rNnT1hY2Llz53JyclhHG9kV4wU4ffq0+Kom2WpEVFxc/PXXXwcFBbGqrXh9eI0ZAMD8mFIcslu2VqvNz88vKyszuIJyfVmnD71eX1NTU1NTY/BpWV27Eneo3Kc4Vo99ffPmzW7dun3yySeyPdy8efP999+fOHEi69ojlqH+QzeBmN/6x1PquLq6ii/AUp6aweXKrwEAzIzpxaHy7ize98WqmGyJ7jG2ufKZmXJD2XFlNS0S+pfKClNWVsaGJP773/8OCgpKSEjw8/NbuHBhp06dXn75ZRZIsnMR3yYhO2txuXJ+cNnXskLybYuLi5cvX3769Ol6rq14ONl+lOcOAGBOTCkOSQgq2XuIZMtFdcWevqHslN36ZbsVZ0kV1+FxxZ5EMtbW1rwvz/nz58lQ1a2upFEWTNy8rlnLZWVLTk728PDgXUmVuVvX1ZOdO8ZXAIC5MrE45JR38yfcQ2N3Uld6cXl5eYMHD+aJ+MILL4wfP55NFyDGjDEF0Nddd2yQVqtNTEzctm0bm3ynfk3YPwCAeTDVODQJJSUl+/btW758uYeHR3R0tPi80PidyOqyjd2wqqrqxo0b6AUDAFA/xOGzUn9Vz/j9yB5hNqoMSEEAACMhDp+Vuh6/UeNbZYmIz1Pa2AI0uZUVAOBXBXH4rOjr1ahdFRQUDB06VBx62NgCIBEBAOqHODQBnp6ekiT9/e9//6ULAgBgthCHJuCzzz7r2LEjf+8uAAA8dYhD05CRkVFZWdmEhlYAADAG4rC5E/MPo+ABAJ4RxGFzh+4wAADPAeIQAAAAcQgAAIA4BAAAIMQhAAAAIQ4BAAAIcQgAAECIQwAAAEIcAgAAEOIQAACAEIcAAACEOAQAACDEIQAAACEOAQAACHEIAABAiEMAAABCHAIAABDiEAAAgBCHAAAAhDgEAAAgxCEAAAAhDgEAAAhxCAAAQIhDAAAAQhwCAAAQ4hAAAIAQhwAAAIQ4BAAAIMQhAAAAIQ4BAAAIcQgAAECIQwAAAEIcAgAAEOIQAACAEIcAAACEOAQAACDEIQAAACEOAQAACHEIAABAiEMAAABCHAIAABDiEAAAgBCHAAAAhDgEAAAgxCEAAAAhDgEAAAhxCAAAQIhDAAAAQhwCAAAQ4hAAAIAQhwAAAIQ4BAAAIMQhAAAAIQ4BAAAIcQgAAECIQwAAAEIcAgAAEOIQAACAEIcAAABE9H/YYwaMItSZvwAAAABJRU5ErkJggg==" style="height:295px; width:838px" /></p>
', NULL, N'1397/12/09', N'10:50     ', 1)
INSERT [dbo].[tblnews] ([id_news], [cat_id_news], [user_id_news], [title_news], [content_news], [att_news], [dtesabt_news], [timsabt_news], [isact_news]) VALUES (CAST(266 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'ارسال عکس در درب اداره ', N'<p><span style="font-size:14px"><span style="font-family:b yekan"><span style="color:#FF0000">قابل توجه مسئولین محترم فناوری اطلاعات ادارات </span></span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">با سلام و وقت بخیر</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">لطفا تا آخر وقت روز شنبه یک <span style="color:#008000">عکس </span>از <span style="color:#008000">سر درب اداره </span>با زاویه و کیفیت نمونه پیوست، به FTPبنده (پناهی) ارسال گردد.</span></span></p>

<p><span style="font-size:14px"><span style="font-family:b yekan">با سپاس</span></span></p>

<p>&nbsp;</p>
', N'Sardarb.JPG', N'1397/12/09', N'12:33     ', 1)
SET IDENTITY_INSERT [dbo].[tblnews] OFF
/****** Object:  Table [dbo].[tblacc]    Script Date: 06/22/2019 10:47:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblacc](
	[id_acc] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[news_id_acc] [numeric](18, 0) NULL,
	[user_id_acc] [numeric](18, 0) NULL,
	[dte_acc] [nchar](10) NULL,
	[tim_acc] [nchar](10) NULL,
 CONSTRAINT [PK_tblacc] PRIMARY KEY CLUSTERED 
(
	[id_acc] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblacc] ON
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1394/11/27', N'13:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(2 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1394/11/27', N'13:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(3 AS Numeric(18, 0)), CAST(46 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1394/11/27', N'13:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(4 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1394/11/27', N'13:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(5 AS Numeric(18, 0)), CAST(44 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1394/11/27', N'13:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(7 AS Numeric(18, 0)), CAST(44 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(8 AS Numeric(18, 0)), CAST(39 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(9 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(10 AS Numeric(18, 0)), CAST(46 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(11 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(12 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(13 AS Numeric(18, 0)), CAST(23 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(14 AS Numeric(18, 0)), CAST(22 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(15 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(16 AS Numeric(18, 0)), CAST(28 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(17 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(18 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(19 AS Numeric(18, 0)), CAST(29 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(20 AS Numeric(18, 0)), CAST(32 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(21 AS Numeric(18, 0)), CAST(33 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(22 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(23 AS Numeric(18, 0)), CAST(43 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(24 AS Numeric(18, 0)), CAST(42 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(25 AS Numeric(18, 0)), CAST(41 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(26 AS Numeric(18, 0)), CAST(40 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/11/28', N'14:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(27 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1394/12/03', N'08:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(28 AS Numeric(18, 0)), CAST(46 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1394/12/03', N'08:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(29 AS Numeric(18, 0)), CAST(44 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1394/12/03', N'08:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(30 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1394/12/03', N'08:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(31 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1394/12/03', N'08:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(32 AS Numeric(18, 0)), CAST(46 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1394/12/03', N'13:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(33 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1394/12/03', N'13:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(34 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1394/12/03', N'13:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(35 AS Numeric(18, 0)), CAST(44 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1394/12/03', N'13:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(36 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1394/12/04', N'07:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(37 AS Numeric(18, 0)), CAST(46 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1394/12/04', N'07:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(38 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1394/12/04', N'07:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(39 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1394/12/04', N'11:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(40 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1394/12/04', N'11:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(41 AS Numeric(18, 0)), CAST(46 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1394/12/04', N'11:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(42 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1394/12/04', N'11:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(43 AS Numeric(18, 0)), CAST(44 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1394/12/04', N'11:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(44 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1394/12/04', N'12:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(45 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(39 AS Numeric(18, 0)), N'1394/12/07', N'13:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(46 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(18 AS Numeric(18, 0)), N'1394/12/10', N'12:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(47 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/12/12', N'14:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(48 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1394/12/13', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(49 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1394/12/13', N'08:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(50 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1394/12/13', N'08:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(51 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1394/12/13', N'08:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(52 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1394/12/13', N'08:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(53 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1394/12/13', N'09:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(54 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1394/12/13', N'09:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(55 AS Numeric(18, 0)), CAST(46 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1394/12/13', N'09:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(56 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), N'1394/12/13', N'10:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(57 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1394/12/13', N'12:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(58 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1394/12/15', N'07:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(59 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1394/12/16', N'07:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(60 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1394/12/16', N'09:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(61 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1394/12/16', N'09:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(62 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1394/12/16', N'12:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(63 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1394/12/16', N'14:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(64 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1394/12/16', N'14:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(65 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1394/12/16', N'14:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(66 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1394/12/16', N'14:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(67 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(29 AS Numeric(18, 0)), N'1394/12/17', N'14:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(68 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), N'1394/12/18', N'07:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(69 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1394/12/18', N'09:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(70 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1394/12/18', N'09:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(71 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), N'1394/12/18', N'13:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(72 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1394/12/20', N'10:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(73 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1394/12/20', N'10:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(74 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1394/12/20', N'12:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(75 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1394/12/20', N'12:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(76 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1394/12/20', N'12:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(77 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1394/12/20', N'13:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(78 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1394/12/26', N'13:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(79 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1394/12/26', N'13:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(80 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1394/12/29', N'10:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(81 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/07', N'07:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(82 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/01/07', N'07:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(83 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/01/07', N'08:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(84 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/01/07', N'14:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(85 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/01/09', N'11:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(86 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/01/09', N'11:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(87 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/01/09', N'11:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(88 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/01/09', N'11:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(89 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/09', N'12:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(90 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/09', N'12:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(91 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/09', N'12:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(92 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/09', N'12:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(93 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/09', N'12:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(94 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/09', N'12:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(95 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/01/09', N'12:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(96 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/01/09', N'12:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(97 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/01/09', N'13:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(98 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/09', N'13:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(99 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/09', N'13:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(100 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/09', N'13:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(101 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/01/10', N'07:37     ')
GO
print 'Processed 100 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(102 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/01/10', N'07:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(103 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/01/10', N'07:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(104 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/01/10', N'07:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(105 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/01/10', N'07:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(106 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/01/10', N'07:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(107 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/01/10', N'07:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(108 AS Numeric(18, 0)), CAST(44 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/10', N'09:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(109 AS Numeric(18, 0)), CAST(43 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/10', N'09:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(110 AS Numeric(18, 0)), CAST(42 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/10', N'09:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(111 AS Numeric(18, 0)), CAST(40 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/10', N'09:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(112 AS Numeric(18, 0)), CAST(41 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/10', N'09:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(113 AS Numeric(18, 0)), CAST(39 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/10', N'09:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(114 AS Numeric(18, 0)), CAST(38 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(115 AS Numeric(18, 0)), CAST(37 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(116 AS Numeric(18, 0)), CAST(32 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(117 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(118 AS Numeric(18, 0)), CAST(22 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(119 AS Numeric(18, 0)), CAST(23 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(120 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(121 AS Numeric(18, 0)), CAST(28 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(122 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(123 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(124 AS Numeric(18, 0)), CAST(29 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(125 AS Numeric(18, 0)), CAST(33 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(126 AS Numeric(18, 0)), CAST(43 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(127 AS Numeric(18, 0)), CAST(42 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(128 AS Numeric(18, 0)), CAST(41 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(129 AS Numeric(18, 0)), CAST(40 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(130 AS Numeric(18, 0)), CAST(39 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/10', N'11:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(131 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/01/11', N'09:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(132 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/01/15', N'09:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(133 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/01/16', N'14:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(134 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/01/17', N'10:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(135 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/01/17', N'10:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(136 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/01/18', N'14:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(137 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/22', N'08:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(138 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/01/22', N'10:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(139 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1395/01/22', N'11:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(140 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/22', N'12:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(141 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/01/22', N'12:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(142 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/01/22', N'13:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(143 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/01/22', N'14:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(144 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/01/23', N'07:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(145 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(146 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(147 AS Numeric(18, 0)), CAST(46 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(148 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(149 AS Numeric(18, 0)), CAST(44 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(150 AS Numeric(18, 0)), CAST(43 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(151 AS Numeric(18, 0)), CAST(42 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(152 AS Numeric(18, 0)), CAST(40 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(153 AS Numeric(18, 0)), CAST(39 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(154 AS Numeric(18, 0)), CAST(38 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(155 AS Numeric(18, 0)), CAST(37 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(156 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(157 AS Numeric(18, 0)), CAST(33 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(158 AS Numeric(18, 0)), CAST(32 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(159 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(160 AS Numeric(18, 0)), CAST(28 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(161 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(162 AS Numeric(18, 0)), CAST(29 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(163 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(164 AS Numeric(18, 0)), CAST(22 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(165 AS Numeric(18, 0)), CAST(23 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/23', N'07:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(166 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/01/23', N'08:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(167 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/01/23', N'08:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(168 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/01/23', N'08:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(169 AS Numeric(18, 0)), CAST(77 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/01/23', N'08:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(170 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/01/23', N'08:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(171 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1395/01/23', N'09:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(172 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/01/23', N'14:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(173 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(18 AS Numeric(18, 0)), N'1395/01/24', N'10:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(174 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/01/24', N'12:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(175 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/01/24', N'12:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(176 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/01/24', N'12:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(177 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/01/24', N'12:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(178 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/01/24', N'12:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(179 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/24', N'13:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(180 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/01/25', N'11:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(181 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/01/25', N'12:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(182 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/25', N'12:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(183 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/01/25', N'12:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(184 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), N'1395/01/25', N'12:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(185 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), N'1395/01/25', N'12:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(186 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/01/25', N'13:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(187 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/01/26', N'10:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(188 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/01/26', N'10:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(189 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/01/26', N'10:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(190 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/01/26', N'11:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(191 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(66 AS Numeric(18, 0)), N'1395/01/26', N'12:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(192 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(66 AS Numeric(18, 0)), N'1395/01/26', N'12:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(193 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/01/26', N'12:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(194 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/01/28', N'08:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(195 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/01/29', N'07:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(196 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(100 AS Numeric(18, 0)), N'1395/01/31', N'12:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(197 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/01/31', N'14:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(198 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/02/01', N'07:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(199 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/02/01', N'08:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(200 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/02/01', N'09:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(201 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/02/01', N'09:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(202 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/02/01', N'10:24     ')
GO
print 'Processed 200 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(203 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/02/01', N'11:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(204 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/02/01', N'11:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(205 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/02/01', N'13:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(206 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/02/04', N'07:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(207 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/02/04', N'08:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(208 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1395/02/04', N'09:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(209 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1395/02/04', N'09:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(210 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/02/04', N'10:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(211 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/02/04', N'10:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(212 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(101 AS Numeric(18, 0)), N'1395/02/04', N'13:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(213 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/02/04', N'14:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(214 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/02/04', N'14:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(215 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/02/05', N'08:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(216 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/02/05', N'08:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(217 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/02/05', N'08:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(218 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/02/05', N'09:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(219 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/02/05', N'09:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(220 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/02/05', N'12:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(221 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/02/06', N'08:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(222 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/02/06', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(223 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/02/06', N'08:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(224 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(97 AS Numeric(18, 0)), N'1395/02/06', N'10:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(225 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(97 AS Numeric(18, 0)), N'1395/02/06', N'10:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(226 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/02/09', N'11:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(227 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/02/09', N'11:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(228 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/02/09', N'11:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(229 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/02/12', N'10:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(230 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/02/12', N'10:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(231 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/02/12', N'10:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(232 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/02/14', N'13:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(233 AS Numeric(18, 0)), CAST(76 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/02/25', N'07:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(234 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/02/25', N'08:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(235 AS Numeric(18, 0)), CAST(56 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/02/25', N'08:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(236 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/02/25', N'08:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(237 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/02/26', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(238 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), N'1395/02/26', N'08:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(239 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/02/26', N'11:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(240 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/02/26', N'12:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(241 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/02/26', N'12:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(242 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/02/26', N'12:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(243 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/02/26', N'12:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(244 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(101 AS Numeric(18, 0)), N'1395/02/26', N'12:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(245 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/02/26', N'12:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(246 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/02/26', N'12:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(247 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/02/26', N'13:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(248 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/02/27', N'07:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(249 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(62 AS Numeric(18, 0)), N'1395/02/27', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(250 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(62 AS Numeric(18, 0)), N'1395/02/27', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(251 AS Numeric(18, 0)), CAST(128 AS Numeric(18, 0)), CAST(62 AS Numeric(18, 0)), N'1395/02/27', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(252 AS Numeric(18, 0)), CAST(110 AS Numeric(18, 0)), CAST(62 AS Numeric(18, 0)), N'1395/02/27', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(253 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/02/27', N'07:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(254 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/02/27', N'09:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(255 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/02/27', N'09:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(256 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/02/28', N'08:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(257 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/02/29', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(258 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/02/30', N'10:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(259 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/03/10', N'09:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(260 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/03/10', N'10:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(261 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/03/10', N'10:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(262 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/03/10', N'11:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(263 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/03/10', N'12:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(264 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/03/10', N'12:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(265 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/03/10', N'13:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(266 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/03/10', N'13:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(267 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/03/10', N'14:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(268 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/03/11', N'07:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(269 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/03/11', N'08:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(270 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/03/11', N'09:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(271 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/03/12', N'08:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(272 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/03/12', N'09:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(273 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/03/13', N'07:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(274 AS Numeric(18, 0)), CAST(116 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/03/17', N'09:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(275 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(18 AS Numeric(18, 0)), N'1395/03/19', N'08:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(276 AS Numeric(18, 0)), CAST(146 AS Numeric(18, 0)), CAST(18 AS Numeric(18, 0)), N'1395/03/19', N'08:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(277 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/03/20', N'10:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(278 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/03/27', N'11:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(279 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/03/30', N'07:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(280 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/03/30', N'07:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(281 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/03/30', N'07:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(282 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/03/30', N'07:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(283 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/03/30', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(284 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/03/30', N'08:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(285 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/03/30', N'08:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(286 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/03/30', N'08:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(287 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/03/30', N'13:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(288 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/03/30', N'13:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(289 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/03/30', N'14:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(290 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/03/30', N'17:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(291 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/03/30', N'17:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(292 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/03/31', N'07:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(293 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/03/31', N'09:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(294 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/03/31', N'11:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(295 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/04/01', N'08:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(296 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/04/01', N'11:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(297 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/04/01', N'12:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(298 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/04/05', N'11:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(299 AS Numeric(18, 0)), CAST(148 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/04/06', N'11:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(300 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/04/08', N'07:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(301 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(18 AS Numeric(18, 0)), N'1395/04/19', N'10:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(302 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/04/22', N'10:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(303 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1395/04/26', N'09:34     ')
GO
print 'Processed 300 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(304 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/04/26', N'13:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(305 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/04/26', N'13:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(306 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/04/26', N'13:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(307 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/04/26', N'13:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(308 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/04/26', N'13:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(309 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/04/26', N'13:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(310 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/04/26', N'14:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(311 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/04/26', N'15:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(312 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/04/27', N'07:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(313 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/04/27', N'07:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(314 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/04/27', N'07:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(315 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/04/27', N'08:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(316 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/04/27', N'08:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(317 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/04/27', N'11:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(318 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/04/27', N'12:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(319 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/04/28', N'11:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(320 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/04/28', N'14:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(321 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/04/31', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(322 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/04/31', N'08:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(323 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/04/31', N'09:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(324 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/04/31', N'09:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(325 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/04/31', N'09:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(326 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/04/31', N'09:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(327 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/04/31', N'10:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(328 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/04/31', N'13:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(329 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/05/02', N'08:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(330 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/05/02', N'08:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(331 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/05/02', N'10:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(332 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/05/02', N'10:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(333 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/05/02', N'12:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(334 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/05/02', N'13:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(335 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/05/02', N'13:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(336 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/05/02', N'19:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(337 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/05/03', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(338 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/05/03', N'08:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(339 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/05/03', N'08:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(340 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/05/03', N'08:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(341 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/05/03', N'08:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(342 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/05/03', N'10:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(343 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/05/03', N'10:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(344 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/05/03', N'10:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(345 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/05/03', N'10:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(346 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/05/03', N'11:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(347 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/05/03', N'11:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(348 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/05/03', N'12:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(349 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/05/03', N'13:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(350 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/05/03', N'13:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(351 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/05/03', N'20:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(352 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/05/03', N'20:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(353 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/05/05', N'13:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(354 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/05/05', N'13:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(355 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/05/05', N'13:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(356 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/05/06', N'08:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(357 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/05/07', N'08:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(358 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/05/07', N'08:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(359 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/05/18', N'11:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(360 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/05/18', N'13:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(361 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/05/18', N'13:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(362 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/05/18', N'14:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(363 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/05/19', N'07:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(364 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/05/19', N'07:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(365 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/05/19', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(366 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/05/19', N'07:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(367 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1395/05/19', N'07:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(368 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/05/19', N'09:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(369 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/05/19', N'09:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(370 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/05/19', N'09:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(371 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/05/19', N'09:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(372 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/05/19', N'09:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(373 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/05/20', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(374 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/05/20', N'08:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(375 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1395/05/20', N'09:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(376 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/05/21', N'08:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(377 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/05/21', N'10:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(378 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(101 AS Numeric(18, 0)), N'1395/05/21', N'17:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(379 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(101 AS Numeric(18, 0)), N'1395/05/21', N'17:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(380 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/05/23', N'07:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(381 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(18 AS Numeric(18, 0)), N'1395/05/25', N'13:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(382 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/05/30', N'12:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(383 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/05/30', N'12:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(384 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/05/30', N'12:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(385 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/05/30', N'12:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(386 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1395/05/30', N'12:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(387 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/05/30', N'13:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(388 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/05/30', N'13:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(389 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/05/31', N'07:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(390 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/05/31', N'07:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(391 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/05/31', N'07:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(392 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/05/31', N'07:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(393 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/05/31', N'07:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(394 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/05/31', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(395 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/05/31', N'07:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(396 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/05/31', N'08:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(397 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/05/31', N'08:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(398 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/05/31', N'08:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(399 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'1395/05/31', N'08:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(400 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/05/31', N'08:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(401 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/05/31', N'08:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(402 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/05/31', N'10:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(403 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/05/31', N'10:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(404 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/05/31', N'10:42     ')
GO
print 'Processed 400 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(405 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/05/31', N'13:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(406 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/05/31', N'14:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(407 AS Numeric(18, 0)), CAST(169 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/05/31', N'14:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(408 AS Numeric(18, 0)), CAST(165 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/05/31', N'14:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(409 AS Numeric(18, 0)), CAST(166 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/06/01', N'08:06     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(410 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/06/01', N'12:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(411 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/06/01', N'13:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(412 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/06/02', N'08:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(413 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/06/02', N'10:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(414 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/06/02', N'11:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(415 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/06/02', N'11:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(416 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/06/02', N'11:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(417 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/06/02', N'12:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(418 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/06/03', N'07:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(419 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/06/03', N'07:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(420 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/06/03', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(421 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/06/03', N'08:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(422 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/06/03', N'11:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(423 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/06/03', N'11:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(424 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/06/03', N'11:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(425 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/06/03', N'11:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(426 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/06/03', N'12:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(427 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/06/03', N'12:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(428 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/06/03', N'12:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(429 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/06/03', N'12:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(430 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/06/03', N'13:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(431 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/06/04', N'08:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(432 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1395/06/04', N'08:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(433 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/06/04', N'08:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(434 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/06/04', N'08:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(435 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/06/04', N'08:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(436 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/06/04', N'09:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(437 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/06/04', N'09:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(438 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1395/06/04', N'09:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(439 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/06/04', N'09:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(440 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/06/04', N'09:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(441 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/06/04', N'10:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(442 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/06/04', N'10:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(443 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/06/06', N'13:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(444 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/06/06', N'13:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(445 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/06/07', N'20:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(446 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/06/09', N'07:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(447 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'1395/06/09', N'09:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(448 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/06/15', N'07:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(449 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/06/15', N'07:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(450 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/06/15', N'08:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(451 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/06/15', N'08:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(452 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/06/15', N'09:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(453 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1395/06/15', N'11:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(454 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1395/06/15', N'12:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(455 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/06/15', N'12:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(456 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/06/16', N'13:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(457 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/06/17', N'07:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(458 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/06/17', N'07:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(459 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/06/17', N'10:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(460 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/06/17', N'10:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(461 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/06/17', N'11:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(462 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/06/18', N'08:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(463 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(28 AS Numeric(18, 0)), N'1395/06/18', N'08:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(464 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(18 AS Numeric(18, 0)), N'1395/06/20', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(465 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(28 AS Numeric(18, 0)), N'1395/06/21', N'08:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(466 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/06/25', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(467 AS Numeric(18, 0)), CAST(174 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/06/25', N'09:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(468 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/06/25', N'09:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(469 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/06/25', N'09:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(470 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/06/25', N'09:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(471 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/06/25', N'10:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(472 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/06/27', N'08:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(473 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/06/27', N'08:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(474 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/07/24', N'08:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(475 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/07/24', N'08:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(476 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/07/24', N'09:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(477 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/07/24', N'10:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(478 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/07/24', N'12:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(479 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/07/24', N'12:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(480 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/07/24', N'13:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(481 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/07/25', N'07:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(482 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/07/25', N'07:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(483 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/07/25', N'07:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(484 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/07/25', N'08:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(485 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1395/07/25', N'13:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(486 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/07/26', N'08:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(487 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/07/26', N'11:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(488 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1395/07/27', N'08:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(489 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/07/29', N'09:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(490 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/08/01', N'11:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(491 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/08/01', N'11:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(492 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/08/01', N'11:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(493 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/08/01', N'12:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(494 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/08/01', N'12:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(495 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/08/01', N'13:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(496 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1395/08/01', N'13:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(497 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/08/01', N'14:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(498 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/08/02', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(499 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/08/02', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(500 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/08/02', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(501 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/08/02', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(502 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/08/02', N'07:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(503 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/08/02', N'08:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(504 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/08/03', N'07:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(505 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/08/04', N'07:39     ')
GO
print 'Processed 500 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(506 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1395/08/04', N'07:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(507 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/08/04', N'07:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(508 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/08/04', N'13:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(509 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/08/05', N'12:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(510 AS Numeric(18, 0)), CAST(188 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1395/08/10', N'09:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(511 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(75 AS Numeric(18, 0)), N'1395/08/12', N'13:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(512 AS Numeric(18, 0)), CAST(41 AS Numeric(18, 0)), CAST(75 AS Numeric(18, 0)), N'1395/08/12', N'13:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(513 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), CAST(75 AS Numeric(18, 0)), N'1395/08/12', N'13:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(514 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(23 AS Numeric(18, 0)), N'1395/09/03', N'11:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(515 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(23 AS Numeric(18, 0)), N'1395/09/03', N'11:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(516 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/09/16', N'09:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(517 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/09/16', N'09:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(518 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/09/16', N'11:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(519 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/09/16', N'13:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(520 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/09/16', N'13:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(521 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/09/16', N'13:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(522 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/09/17', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(523 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/09/17', N'10:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(524 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/09/17', N'11:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(525 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/09/20', N'13:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(526 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/09/20', N'16:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(527 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/09/21', N'08:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(528 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/09/24', N'07:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(529 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/10/02', N'07:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(530 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/11/07', N'12:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(531 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/11/07', N'12:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(532 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/11/07', N'13:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(533 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/11/09', N'08:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(534 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/11/09', N'08:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(535 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/11/09', N'09:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(536 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/11/09', N'09:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(537 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/11/09', N'12:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(538 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/11/10', N'08:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(539 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/11/11', N'07:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(540 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/11/11', N'08:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(541 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'1395/11/11', N'13:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(542 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1395/11/11', N'13:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(543 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/11/12', N'10:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(544 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1395/11/13', N'09:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(545 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/11/17', N'08:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(546 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/11/17', N'12:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(547 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/11/17', N'14:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(548 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/11/18', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(549 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/11/18', N'08:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(550 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1395/11/18', N'13:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(551 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/11/20', N'08:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(552 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/11/20', N'08:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(553 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1395/11/20', N'09:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(554 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/11/20', N'12:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(555 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/11/20', N'13:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(556 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/11/23', N'07:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(557 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/11/23', N'08:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(558 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/11/26', N'07:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(559 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/11/26', N'08:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(560 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/11/26', N'08:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(561 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/11/26', N'08:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(562 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/11/26', N'08:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(563 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/11/26', N'09:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(564 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1395/11/26', N'09:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(565 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/11/26', N'09:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(566 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/11/26', N'12:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(567 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/11/26', N'13:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(568 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/11/26', N'13:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(569 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/11/27', N'09:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(570 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/11/29', N'08:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(571 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/02', N'11:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(572 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/12/02', N'12:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(573 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/12/02', N'13:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(574 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/12/02', N'13:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(575 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/12/02', N'17:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(576 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/12/03', N'07:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(577 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1395/12/03', N'07:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(578 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), N'1395/12/03', N'08:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(579 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), N'1395/12/03', N'08:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(580 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), N'1395/12/03', N'08:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(581 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), N'1395/12/03', N'08:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(582 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/12/03', N'10:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(583 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/12/03', N'12:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(584 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/12/03', N'12:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(585 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1395/12/04', N'08:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(586 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/12/04', N'09:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(587 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/12/05', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(588 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/12/05', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(589 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/12/05', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(590 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/12/05', N'08:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(591 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/12/05', N'12:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(592 AS Numeric(18, 0)), CAST(191 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), N'1395/12/07', N'09:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(593 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/11', N'07:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(594 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/12/11', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(595 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/12/11', N'08:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(596 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1395/12/11', N'08:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(597 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1395/12/11', N'08:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(598 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1395/12/11', N'08:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(599 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/12/15', N'13:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(600 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/12/15', N'13:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(601 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/12/15', N'13:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(602 AS Numeric(18, 0)), CAST(196 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/16', N'13:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(603 AS Numeric(18, 0)), CAST(195 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/16', N'13:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(604 AS Numeric(18, 0)), CAST(182 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/16', N'13:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(605 AS Numeric(18, 0)), CAST(177 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/16', N'13:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(606 AS Numeric(18, 0)), CAST(176 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/16', N'13:48     ')
GO
print 'Processed 600 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(607 AS Numeric(18, 0)), CAST(175 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/16', N'13:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(608 AS Numeric(18, 0)), CAST(187 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/16', N'13:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(609 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/16', N'13:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(610 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/16', N'13:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(611 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1395/12/18', N'08:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(612 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/12/18', N'10:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(613 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/12/18', N'12:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(614 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/12/18', N'13:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(615 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/12/18', N'14:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(616 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/12/18', N'14:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(617 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/12/19', N'07:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(618 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1395/12/19', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(619 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/12/19', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(620 AS Numeric(18, 0)), CAST(199 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/12/19', N'09:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(621 AS Numeric(18, 0)), CAST(23 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/12/19', N'09:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(622 AS Numeric(18, 0)), CAST(29 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/12/19', N'09:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(623 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1395/12/19', N'09:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(624 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/12/21', N'08:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(625 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1395/12/21', N'12:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(626 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1395/12/21', N'18:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(627 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1395/12/23', N'07:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(628 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/12/23', N'12:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(629 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/12/23', N'13:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(630 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/12/23', N'14:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(631 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1395/12/23', N'16:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(632 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/12/24', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(633 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1395/12/24', N'07:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(634 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1395/12/24', N'10:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(635 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1395/12/25', N'13:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(636 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1395/12/26', N'08:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(637 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1395/12/26', N'09:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(638 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1395/12/28', N'08:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(639 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/12/28', N'11:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(640 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/12/28', N'11:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(641 AS Numeric(18, 0)), CAST(201 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1395/12/28', N'11:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(642 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1395/12/28', N'11:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(643 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1395/12/28', N'11:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(644 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1395/12/28', N'13:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(645 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1395/12/30', N'11:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(646 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1395/12/30', N'11:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(647 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/01/01', N'08:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(648 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/01/02', N'11:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(649 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/01/02', N'11:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(650 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/01/04', N'16:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(651 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/01/05', N'07:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(652 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/01/05', N'07:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(653 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/01/05', N'08:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(654 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/01/06', N'08:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(655 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/01/07', N'07:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(656 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/01/07', N'10:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(657 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/01/07', N'11:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(658 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/01/08', N'08:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(659 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/01/08', N'08:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(660 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/01/15', N'14:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(661 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/01/15', N'14:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(662 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/01/16', N'07:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(663 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/01/16', N'07:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(664 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/01/16', N'07:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(665 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/01/16', N'08:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(666 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/01/16', N'08:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(667 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/01/16', N'08:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(668 AS Numeric(18, 0)), CAST(203 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/01/16', N'09:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(669 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/01/16', N'09:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(670 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/01/16', N'10:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(671 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/01/16', N'11:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(672 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/01/16', N'11:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(673 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/01/16', N'12:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(674 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/01/16', N'13:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(675 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/01/17', N'09:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(676 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/01/19', N'09:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(677 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/01/19', N'09:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(678 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/01/20', N'07:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(679 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/01/20', N'09:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(680 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/01/21', N'08:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(681 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/01/24', N'11:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(682 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/01/26', N'08:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(683 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1396/01/27', N'07:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(684 AS Numeric(18, 0)), CAST(202 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1396/01/29', N'08:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(685 AS Numeric(18, 0)), CAST(200 AS Numeric(18, 0)), CAST(75 AS Numeric(18, 0)), N'1396/02/20', N'09:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(686 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/02/24', N'07:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(687 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/02/24', N'07:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(688 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/02/24', N'07:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(689 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/02/24', N'07:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(690 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/02/24', N'07:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(691 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/02/24', N'08:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(692 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/02/24', N'08:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(693 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/02/24', N'09:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(694 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/02/24', N'09:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(695 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/02/24', N'09:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(696 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/02/24', N'09:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(697 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/02/24', N'10:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(698 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/02/24', N'12:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(699 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/02/24', N'13:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(700 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/02/24', N'13:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(701 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/02/25', N'08:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(702 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/02/25', N'08:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(703 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/02/31', N'09:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(704 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/03/06', N'08:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(705 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/03/06', N'08:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(706 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/03/06', N'10:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(707 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/03/06', N'11:04     ')
GO
print 'Processed 700 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(708 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/03/06', N'11:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(709 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/03/06', N'11:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(710 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/03/06', N'11:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(711 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/03/06', N'13:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(712 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/03/06', N'13:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(713 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/03/06', N'13:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(714 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/03/06', N'13:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(715 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/03/06', N'13:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(716 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/03/06', N'13:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(717 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/03/07', N'07:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(718 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/03/07', N'07:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(719 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/03/07', N'07:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(720 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/03/07', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(721 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/03/07', N'07:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(722 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/03/07', N'13:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(723 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/03/07', N'14:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(724 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/03/07', N'14:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(725 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/03/09', N'07:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(726 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/03/09', N'07:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(727 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/03/09', N'08:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(728 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/03/09', N'08:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(729 AS Numeric(18, 0)), CAST(208 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/03/09', N'10:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(730 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/03/09', N'10:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(731 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/03/09', N'10:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(732 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/03/16', N'09:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(733 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1396/03/16', N'09:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(734 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/03/16', N'10:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(735 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/03/16', N'10:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(736 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/03/16', N'12:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(737 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/03/16', N'12:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(738 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/03/16', N'12:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(739 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/03/16', N'13:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(740 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/03/16', N'13:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(741 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/03/16', N'13:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(742 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/03/16', N'13:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(743 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/03/16', N'13:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(744 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/03/17', N'07:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(745 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/03/17', N'07:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(746 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/03/17', N'07:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(747 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/03/17', N'07:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(748 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/03/17', N'07:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(749 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/03/17', N'08:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(750 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/03/17', N'08:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(751 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/03/17', N'08:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(752 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/03/17', N'08:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(753 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/03/18', N'07:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(754 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/03/18', N'07:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(755 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/03/18', N'08:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(756 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/03/18', N'08:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(757 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/03/18', N'08:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(758 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/03/21', N'12:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(759 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1396/03/24', N'10:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(760 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1396/03/24', N'10:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(761 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1396/03/24', N'10:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(762 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1396/03/24', N'10:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(763 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1396/03/25', N'08:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(764 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1396/03/25', N'08:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(765 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1396/03/25', N'09:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(766 AS Numeric(18, 0)), CAST(205 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1396/03/25', N'09:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(767 AS Numeric(18, 0)), CAST(204 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1396/03/25', N'09:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(768 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/03/29', N'13:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(769 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/04/03', N'13:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(770 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/04/04', N'08:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(771 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(99 AS Numeric(18, 0)), N'1396/04/10', N'11:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(772 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(99 AS Numeric(18, 0)), N'1396/04/10', N'11:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(773 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(99 AS Numeric(18, 0)), N'1396/04/10', N'11:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(774 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/04/26', N'13:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(775 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/04/27', N'07:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(776 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/04/27', N'07:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(777 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/04/27', N'07:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(778 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/04/27', N'07:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(779 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/04/27', N'07:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(780 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/04/27', N'07:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(781 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/04/27', N'07:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(782 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/04/27', N'08:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(783 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/04/27', N'08:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(784 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/04/27', N'09:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(785 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/04/27', N'09:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(786 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/04/27', N'12:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(787 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/04/27', N'12:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(788 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/04/27', N'13:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(789 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/04/28', N'07:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(790 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/04/28', N'07:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(791 AS Numeric(18, 0)), CAST(209 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/04/28', N'07:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(792 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/04/28', N'07:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(793 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/04/28', N'08:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(794 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/05/04', N'10:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(795 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/05/04', N'11:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(796 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1396/05/04', N'12:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(797 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1396/05/04', N'12:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(798 AS Numeric(18, 0)), CAST(207 AS Numeric(18, 0)), CAST(154 AS Numeric(18, 0)), N'1396/05/04', N'12:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(799 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/05/04', N'13:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(800 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/05/04', N'13:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(801 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/05/04', N'13:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(802 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1396/05/04', N'13:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(803 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1396/05/04', N'13:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(804 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/05/04', N'13:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(805 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/05/05', N'07:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(806 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/05/05', N'07:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(807 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/05/05', N'07:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(808 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/05/05', N'08:57     ')
GO
print 'Processed 800 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(809 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/05/05', N'09:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(810 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/05/05', N'09:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(811 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/05/05', N'10:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(812 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/05/05', N'10:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(813 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/05/07', N'07:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(814 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/05/07', N'07:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(815 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/05/07', N'10:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(816 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/05/07', N'10:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(817 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/05/09', N'11:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(818 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/05/09', N'13:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(819 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/05/09', N'13:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(820 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/05/09', N'13:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(821 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/05/10', N'07:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(822 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/05/10', N'07:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(823 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/05/10', N'07:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(824 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/05/10', N'07:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(825 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/05/10', N'07:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(826 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/05/10', N'07:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(827 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/05/10', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(828 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/05/10', N'07:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(829 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/05/10', N'07:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(830 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/05/10', N'11:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(831 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/05/10', N'11:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(832 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/05/10', N'11:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(833 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/05/10', N'12:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(834 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/05/10', N'12:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(835 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/05/10', N'12:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(836 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/05/11', N'07:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(837 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/05/11', N'07:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(838 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/05/11', N'20:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(839 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/05/11', N'20:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(840 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/05/12', N'08:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(841 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/05/12', N'08:06     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(842 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/05/12', N'11:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(843 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/05/12', N'11:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(844 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/05/14', N'08:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(845 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/05/14', N'08:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(846 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/05/14', N'10:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(847 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/05/14', N'10:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(848 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/05/15', N'08:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(849 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/05/15', N'08:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(850 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'1396/05/15', N'12:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(851 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/05/15', N'13:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(852 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/05/15', N'13:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(853 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/05/15', N'13:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(854 AS Numeric(18, 0)), CAST(210 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/05/15', N'13:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(855 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(152 AS Numeric(18, 0)), N'1396/05/15', N'13:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(856 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/05/17', N'10:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(857 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/05/17', N'10:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(858 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/05/19', N'12:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(859 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/05/21', N'07:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(860 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/05/21', N'07:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(861 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/05/21', N'07:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(862 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/05/21', N'08:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(863 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/05/21', N'08:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(864 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/05/21', N'08:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(865 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/05/21', N'08:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(866 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/05/21', N'12:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(867 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/05/22', N'07:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(868 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/05/22', N'07:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(869 AS Numeric(18, 0)), CAST(212 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/05/22', N'07:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(870 AS Numeric(18, 0)), CAST(211 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/05/22', N'08:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(871 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/05/22', N'08:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(872 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/05/22', N'10:06     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(873 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/05/24', N'12:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(874 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/05/25', N'07:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(875 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/05/25', N'10:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(876 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/05/26', N'12:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(877 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/05/28', N'08:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(878 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/06/20', N'11:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(879 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/06/20', N'12:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(880 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/06/20', N'13:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(881 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/06/20', N'13:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(882 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/06/20', N'13:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(883 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/06/21', N'07:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(884 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/06/21', N'07:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(885 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/06/21', N'07:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(886 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/06/21', N'11:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(887 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/06/21', N'12:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(888 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/06/21', N'13:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(889 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/06/21', N'13:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(890 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/06/22', N'10:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(891 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/06/25', N'09:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(892 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/06/25', N'09:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(893 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/06/25', N'11:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(894 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/06/25', N'13:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(895 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/06/25', N'15:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(896 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/06/25', N'16:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(897 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/06/26', N'08:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(898 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/06/26', N'09:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(899 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/06/26', N'09:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(900 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/06/27', N'07:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(901 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/06/28', N'06:06     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(902 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/06/28', N'07:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(903 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/06/28', N'10:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(904 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/06/28', N'10:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(905 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/06/29', N'06:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(906 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/06/29', N'06:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(907 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/06/30', N'06:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(908 AS Numeric(18, 0)), CAST(216 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/06/30', N'06:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(909 AS Numeric(18, 0)), CAST(215 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/06/30', N'06:38     ')
GO
print 'Processed 900 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(910 AS Numeric(18, 0)), CAST(214 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/06/30', N'06:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(911 AS Numeric(18, 0)), CAST(213 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'1396/06/30', N'06:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(912 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/07/01', N'12:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(913 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/07/01', N'12:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(914 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/07/01', N'13:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(915 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/07/01', N'14:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(916 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/07/02', N'07:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(917 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/07/02', N'07:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(918 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/07/02', N'07:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(919 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/07/02', N'07:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(920 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/07/02', N'09:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(921 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/07/02', N'11:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(922 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/07/02', N'13:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(923 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1396/07/02', N'13:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(924 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/07/04', N'10:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(925 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/07/06', N'08:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(926 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/07/06', N'12:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(927 AS Numeric(18, 0)), CAST(217 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/07/11', N'09:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(928 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/07/12', N'10:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(929 AS Numeric(18, 0)), CAST(219 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/07/12', N'10:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(930 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/07/12', N'10:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(931 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/07/12', N'12:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(932 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/07/12', N'13:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(933 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/07/12', N'13:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(934 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/07/12', N'13:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(935 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/07/13', N'07:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(936 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/07/13', N'07:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(937 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(147 AS Numeric(18, 0)), N'1396/07/13', N'08:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(938 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/07/13', N'12:06     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(939 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/07/17', N'08:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(940 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/07/18', N'07:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(941 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/07/18', N'09:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(942 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/07/22', N'09:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(943 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/08/06', N'08:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(944 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/08/06', N'08:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(945 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/08/06', N'08:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(946 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/08/06', N'12:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(947 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/08/06', N'12:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(948 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/08/06', N'13:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(949 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/08/06', N'13:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(950 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/08/06', N'13:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(951 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/08/07', N'07:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(952 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/08/07', N'08:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(953 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/08/07', N'11:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(954 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/08/08', N'07:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(955 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/08/08', N'10:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(956 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/08/09', N'08:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(957 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1396/08/09', N'09:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(958 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/08/09', N'10:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(959 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/08/14', N'08:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(960 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/08/21', N'13:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(961 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/08/21', N'13:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(962 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/08/21', N'13:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(963 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/08/21', N'13:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(964 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/08/22', N'07:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(965 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/08/22', N'07:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(966 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/08/22', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(967 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/08/22', N'09:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(968 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/08/22', N'10:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(969 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/08/23', N'07:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(970 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(105 AS Numeric(18, 0)), N'1396/08/23', N'09:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(971 AS Numeric(18, 0)), CAST(221 AS Numeric(18, 0)), CAST(105 AS Numeric(18, 0)), N'1396/08/23', N'09:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(972 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/08/24', N'10:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(973 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/08/27', N'07:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(974 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/08/29', N'12:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(975 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/09/04', N'10:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(976 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/09/04', N'11:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(977 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/09/04', N'11:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(978 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/09/04', N'12:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(979 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/09/04', N'13:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(980 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/09/04', N'13:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(981 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/09/05', N'07:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(982 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/09/05', N'07:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(983 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/09/05', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(984 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1396/09/05', N'11:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(985 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/09/05', N'12:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(986 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/09/05', N'13:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(987 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/09/08', N'11:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(988 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/09/09', N'11:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(989 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/09/09', N'11:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(990 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/09/09', N'11:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(991 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/09/09', N'11:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(992 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/09/09', N'11:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(993 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/09/09', N'12:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(994 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/09/09', N'12:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(995 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/09/09', N'12:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(996 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/09/11', N'07:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(997 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/09/11', N'08:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(998 AS Numeric(18, 0)), CAST(220 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), N'1396/09/12', N'08:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(999 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/09/18', N'12:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1000 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/09/25', N'10:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1001 AS Numeric(18, 0)), CAST(223 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/09/25', N'10:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1002 AS Numeric(18, 0)), CAST(222 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/09/25', N'10:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1003 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/09/25', N'12:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1004 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/09/25', N'13:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1005 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/09/25', N'13:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1006 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/09/25', N'13:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1007 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/09/25', N'14:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1008 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/09/25', N'23:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1009 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/09/26', N'07:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1010 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/09/26', N'08:42     ')
GO
print 'Processed 1000 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1011 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/09/26', N'08:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1012 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(54 AS Numeric(18, 0)), N'1396/09/26', N'08:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1013 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/09/26', N'09:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1014 AS Numeric(18, 0)), CAST(224 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/09/26', N'09:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1015 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(55 AS Numeric(18, 0)), N'1396/09/26', N'11:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1016 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/09/26', N'17:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1017 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/09/29', N'10:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1018 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/10/06', N'08:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1019 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/10/16', N'09:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1020 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/10/16', N'10:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1021 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/10/16', N'11:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1022 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/10/16', N'11:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1023 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/10/16', N'12:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1024 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1396/10/16', N'13:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1025 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/10/16', N'18:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1026 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/10/17', N'09:06     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1027 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/10/17', N'09:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1028 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/10/17', N'13:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1029 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/10/17', N'16:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1030 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1396/10/19', N'09:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1031 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/10/19', N'10:06     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1032 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/11/01', N'11:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1033 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/11/01', N'11:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1034 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/11/01', N'13:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1035 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(156 AS Numeric(18, 0)), N'1396/11/01', N'14:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1036 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/11/01', N'18:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1037 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/11/01', N'18:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1038 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/11/02', N'07:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1039 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/11/02', N'08:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1040 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/11/02', N'08:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1041 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/11/04', N'13:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1042 AS Numeric(18, 0)), CAST(227 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/11/04', N'13:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1043 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1396/11/10', N'12:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1044 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/11/10', N'12:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1045 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/11/10', N'12:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1046 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/11/10', N'13:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1047 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/11/10', N'17:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1048 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/11/10', N'18:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1049 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/11/11', N'08:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1050 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/11/14', N'12:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1051 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/11/16', N'13:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1052 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1396/11/25', N'08:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1053 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1396/11/25', N'10:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1054 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1396/11/25', N'13:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1055 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1396/11/25', N'14:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1056 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'1396/11/26', N'07:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1057 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/11/26', N'08:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1058 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1396/11/26', N'08:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1059 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1396/11/26', N'09:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1060 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1396/11/28', N'07:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1061 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1396/11/28', N'10:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1062 AS Numeric(18, 0)), CAST(230 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/12/07', N'10:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1063 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/12/07', N'10:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1064 AS Numeric(18, 0)), CAST(228 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/12/07', N'10:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1065 AS Numeric(18, 0)), CAST(225 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1396/12/07', N'10:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1066 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'1396/12/26', N'11:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1067 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1396/12/26', N'18:53     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1068 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1396/12/27', N'10:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1069 AS Numeric(18, 0)), CAST(229 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1396/12/27', N'13:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1070 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/02/11', N'11:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1071 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/02/13', N'07:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1072 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/02/13', N'07:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1073 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(51 AS Numeric(18, 0)), N'1397/02/13', N'08:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1074 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/02/13', N'08:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1075 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/02/13', N'08:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1076 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/02/15', N'07:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1077 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/02/15', N'08:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1078 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/02/15', N'13:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1079 AS Numeric(18, 0)), CAST(231 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/02/22', N'12:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1080 AS Numeric(18, 0)), CAST(232 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/03/09', N'12:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1081 AS Numeric(18, 0)), CAST(232 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/03/09', N'12:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1082 AS Numeric(18, 0)), CAST(232 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/03/09', N'13:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1083 AS Numeric(18, 0)), CAST(232 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/03/10', N'07:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1084 AS Numeric(18, 0)), CAST(232 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/03/10', N'13:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1085 AS Numeric(18, 0)), CAST(232 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/03/12', N'08:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1086 AS Numeric(18, 0)), CAST(232 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/03/12', N'12:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1087 AS Numeric(18, 0)), CAST(232 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/03/29', N'22:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1088 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/04/07', N'21:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1089 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/04/09', N'16:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1090 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/04/09', N'16:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1091 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/04/09', N'17:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1092 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/04/09', N'19:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1093 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/04/10', N'19:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1094 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/04/12', N'07:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1095 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/04/21', N'19:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1096 AS Numeric(18, 0)), CAST(232 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/04/21', N'19:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1097 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/04/30', N'19:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1098 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/04/30', N'19:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1099 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/04/30', N'21:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1100 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/04/30', N'21:13     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1101 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/04/31', N'16:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1102 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/05/01', N'17:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1103 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/07', N'02:48     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1104 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(159 AS Numeric(18, 0)), N'1397/05/09', N'20:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1105 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(159 AS Numeric(18, 0)), N'1397/05/09', N'20:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1106 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/09', N'22:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1107 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/09', N'22:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1108 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/05/10', N'17:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1109 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/10', N'17:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1110 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/10', N'22:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1111 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(155 AS Numeric(18, 0)), N'1397/05/11', N'20:53     ')
GO
print 'Processed 1100 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1112 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/05/13', N'17:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1113 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/05/13', N'18:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1114 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/05/13', N'18:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1115 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/13', N'19:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1116 AS Numeric(18, 0)), CAST(236 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/05/13', N'19:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1117 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/05/14', N'20:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1118 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/14', N'22:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1119 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/15', N'16:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1120 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/15', N'16:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1121 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(160 AS Numeric(18, 0)), N'1397/05/15', N'17:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1122 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/15', N'21:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1123 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/16', N'04:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1124 AS Numeric(18, 0)), CAST(238 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/16', N'19:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1125 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/16', N'19:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1126 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/16', N'19:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1127 AS Numeric(18, 0)), CAST(234 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/16', N'19:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1128 AS Numeric(18, 0)), CAST(238 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/16', N'19:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1129 AS Numeric(18, 0)), CAST(238 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/05/16', N'19:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1130 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/05/16', N'19:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1131 AS Numeric(18, 0)), CAST(235 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/05/16', N'19:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1132 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'1397/05/16', N'20:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1133 AS Numeric(18, 0)), CAST(238 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/05/16', N'22:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1134 AS Numeric(18, 0)), CAST(238 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/16', N'22:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1135 AS Numeric(18, 0)), CAST(238 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/17', N'16:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1136 AS Numeric(18, 0)), CAST(238 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/17', N'16:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1137 AS Numeric(18, 0)), CAST(239 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/05/18', N'19:00     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1138 AS Numeric(18, 0)), CAST(239 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/20', N'16:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1139 AS Numeric(18, 0)), CAST(239 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/20', N'16:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1140 AS Numeric(18, 0)), CAST(242 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/05/20', N'17:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1141 AS Numeric(18, 0)), CAST(242 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/20', N'18:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1142 AS Numeric(18, 0)), CAST(242 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/20', N'19:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1143 AS Numeric(18, 0)), CAST(242 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/20', N'19:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1144 AS Numeric(18, 0)), CAST(239 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/20', N'19:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1145 AS Numeric(18, 0)), CAST(242 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/20', N'22:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1146 AS Numeric(18, 0)), CAST(242 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/21', N'16:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1147 AS Numeric(18, 0)), CAST(239 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/21', N'16:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1148 AS Numeric(18, 0)), CAST(246 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/21', N'16:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1149 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/21', N'16:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1150 AS Numeric(18, 0)), CAST(246 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/05/21', N'18:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1151 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/05/21', N'18:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1152 AS Numeric(18, 0)), CAST(242 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/05/21', N'18:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1153 AS Numeric(18, 0)), CAST(238 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/05/21', N'18:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1154 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/21', N'19:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1155 AS Numeric(18, 0)), CAST(246 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/21', N'19:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1156 AS Numeric(18, 0)), CAST(246 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/21', N'20:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1157 AS Numeric(18, 0)), CAST(242 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/05/21', N'20:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1158 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/21', N'22:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1159 AS Numeric(18, 0)), CAST(246 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/21', N'22:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1160 AS Numeric(18, 0)), CAST(247 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/05/22', N'17:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1161 AS Numeric(18, 0)), CAST(247 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/22', N'17:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1162 AS Numeric(18, 0)), CAST(247 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/22', N'18:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1163 AS Numeric(18, 0)), CAST(247 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/22', N'18:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1164 AS Numeric(18, 0)), CAST(247 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/22', N'19:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1165 AS Numeric(18, 0)), CAST(246 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/22', N'19:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1166 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/22', N'19:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1167 AS Numeric(18, 0)), CAST(247 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/22', N'22:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1168 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/22', N'22:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1169 AS Numeric(18, 0)), CAST(239 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/22', N'22:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1170 AS Numeric(18, 0)), CAST(249 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/23', N'18:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1171 AS Numeric(18, 0)), CAST(249 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/23', N'18:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1172 AS Numeric(18, 0)), CAST(249 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/23', N'19:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1173 AS Numeric(18, 0)), CAST(249 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/23', N'19:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1174 AS Numeric(18, 0)), CAST(249 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/23', N'19:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1175 AS Numeric(18, 0)), CAST(247 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/23', N'19:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1176 AS Numeric(18, 0)), CAST(246 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/23', N'19:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1177 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/23', N'19:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1178 AS Numeric(18, 0)), CAST(242 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/23', N'19:59     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1179 AS Numeric(18, 0)), CAST(249 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/23', N'22:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1180 AS Numeric(18, 0)), CAST(249 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/05/24', N'16:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1181 AS Numeric(18, 0)), CAST(247 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/05/24', N'16:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1182 AS Numeric(18, 0)), CAST(246 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/05/24', N'16:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1183 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/05/24', N'16:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1184 AS Numeric(18, 0)), CAST(250 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/24', N'18:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1185 AS Numeric(18, 0)), CAST(250 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/24', N'19:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1186 AS Numeric(18, 0)), CAST(250 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/24', N'19:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1187 AS Numeric(18, 0)), CAST(250 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/24', N'20:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1188 AS Numeric(18, 0)), CAST(250 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/24', N'21:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1189 AS Numeric(18, 0)), CAST(250 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1397/05/25', N'16:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1190 AS Numeric(18, 0)), CAST(249 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1397/05/25', N'16:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1191 AS Numeric(18, 0)), CAST(247 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1397/05/25', N'16:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1192 AS Numeric(18, 0)), CAST(246 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1397/05/25', N'16:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1193 AS Numeric(18, 0)), CAST(251 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/25', N'17:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1194 AS Numeric(18, 0)), CAST(251 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/25', N'17:58     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1195 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/05/25', N'19:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1196 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/25', N'19:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1197 AS Numeric(18, 0)), CAST(251 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/05/25', N'19:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1198 AS Numeric(18, 0)), CAST(251 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/05/25', N'20:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1199 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/05/25', N'20:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1200 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/05/25', N'20:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1201 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/05/25', N'21:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1202 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/05/27', N'16:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1203 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1397/05/27', N'18:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1204 AS Numeric(18, 0)), CAST(251 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1397/05/27', N'18:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1205 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/27', N'22:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1206 AS Numeric(18, 0)), CAST(251 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/05/27', N'22:10     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1207 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/05/30', N'20:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1208 AS Numeric(18, 0)), CAST(251 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/05/30', N'20:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1209 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/05/30', N'20:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1210 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/06/03', N'09:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1211 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/06/03', N'14:03     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1212 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/06/03', N'16:37     ')
GO
print 'Processed 1200 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1213 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/06/04', N'07:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1214 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/06/04', N'08:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1215 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/06/04', N'08:27     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1216 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/06/04', N'08:28     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1217 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/06/04', N'08:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1218 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/06/04', N'09:18     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1219 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/06/04', N'13:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1220 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1397/06/13', N'11:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1221 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/06/17', N'14:06     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1222 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/06/18', N'09:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1223 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'1397/07/02', N'10:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1224 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'1397/07/02', N'10:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1225 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/07/09', N'11:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1226 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1397/07/11', N'11:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1227 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1397/07/11', N'11:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1228 AS Numeric(18, 0)), CAST(251 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1397/07/11', N'11:20     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1229 AS Numeric(18, 0)), CAST(244 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'1397/07/11', N'11:21     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1230 AS Numeric(18, 0)), CAST(252 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), N'1397/07/12', N'09:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1231 AS Numeric(18, 0)), CAST(251 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), N'1397/07/12', N'09:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1232 AS Numeric(18, 0)), CAST(237 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), N'1397/07/12', N'09:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1233 AS Numeric(18, 0)), CAST(254 AS Numeric(18, 0)), CAST(47 AS Numeric(18, 0)), N'1397/07/12', N'09:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1234 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/08/28', N'09:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1235 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(160 AS Numeric(18, 0)), N'1397/08/28', N'09:54     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1236 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/08/28', N'10:26     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1237 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), N'1397/08/28', N'12:05     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1238 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/08/28', N'12:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1239 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/08/28', N'13:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1240 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/08/28', N'13:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1241 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/08/29', N'08:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1242 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/08/29', N'08:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1243 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/08/29', N'08:56     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1244 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/08/29', N'11:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1245 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/08/29', N'11:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1246 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/08/30', N'07:55     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1247 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/09/01', N'09:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1248 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/09/01', N'09:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1249 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/09/01', N'16:07     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1250 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/09/01', N'16:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1251 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/09/06', N'11:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1252 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/09/06', N'11:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1253 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/09/11', N'13:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1254 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/09/11', N'13:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1255 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/09/11', N'13:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1256 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/09/11', N'13:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1257 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/09/12', N'07:34     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1258 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/09/12', N'07:46     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1259 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1397/09/12', N'08:12     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1260 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/09/13', N'11:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1261 AS Numeric(18, 0)), CAST(257 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/09/13', N'11:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1262 AS Numeric(18, 0)), CAST(256 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'1397/09/13', N'11:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1263 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/09/14', N'07:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1264 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/09/17', N'08:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1265 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/09/17', N'09:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1266 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/09/17', N'11:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1267 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/09/17', N'13:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1268 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/09/18', N'09:24     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1269 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/09/18', N'09:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1270 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1397/09/18', N'11:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1271 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(87 AS Numeric(18, 0)), N'1397/09/18', N'11:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1272 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/09/18', N'13:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1273 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(160 AS Numeric(18, 0)), N'1397/09/21', N'11:15     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1274 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/09/25', N'12:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1275 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/09/25', N'13:04     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1276 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/09/26', N'07:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1277 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/09/26', N'07:37     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1278 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'1397/09/26', N'10:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1279 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'1397/09/26', N'10:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1280 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'1397/09/26', N'10:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1281 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), N'1397/09/26', N'10:38     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1282 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(155 AS Numeric(18, 0)), N'1397/09/26', N'10:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1283 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(161 AS Numeric(18, 0)), N'1397/09/26', N'10:41     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1284 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/09/27', N'07:43     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1285 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/09/27', N'13:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1286 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/09/28', N'08:25     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1287 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/09/28', N'12:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1288 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/09/28', N'12:06     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1289 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/09/28', N'12:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1290 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/09/29', N'07:35     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1291 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/09/29', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1292 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/09/29', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1293 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/09/29', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1294 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1397/09/29', N'08:19     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1295 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/09/29', N'10:44     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1296 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/09/29', N'10:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1297 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(84 AS Numeric(18, 0)), N'1397/09/29', N'10:45     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1298 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/09/29', N'13:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1299 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'1397/10/01', N'09:36     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1300 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(73 AS Numeric(18, 0)), N'1397/10/03', N'08:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1301 AS Numeric(18, 0)), CAST(261 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/10/05', N'12:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1302 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/10/08', N'13:40     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1303 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/10/09', N'07:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1304 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/10/09', N'08:01     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1305 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'1397/10/09', N'08:49     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1306 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/10/09', N'08:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1307 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/10/09', N'09:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1308 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(158 AS Numeric(18, 0)), N'1397/10/09', N'12:57     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1309 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/10/10', N'07:32     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1310 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/10/10', N'07:51     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1311 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/10/10', N'07:52     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1312 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'1397/10/10', N'08:02     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1313 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/10/10', N'09:33     ')
GO
print 'Processed 1300 total records'
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1314 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/10/11', N'07:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1315 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/10/11', N'07:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1316 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'1397/10/11', N'07:42     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1317 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/10/11', N'11:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1318 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/10/11', N'14:16     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1319 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1397/10/15', N'13:17     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1320 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/11/03', N'09:30     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1321 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1397/11/03', N'09:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1322 AS Numeric(18, 0)), CAST(266 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'1397/12/09', N'13:14     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1323 AS Numeric(18, 0)), CAST(266 AS Numeric(18, 0)), CAST(153 AS Numeric(18, 0)), N'1397/12/11', N'07:33     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1324 AS Numeric(18, 0)), CAST(266 AS Numeric(18, 0)), CAST(71 AS Numeric(18, 0)), N'1397/12/11', N'07:39     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1325 AS Numeric(18, 0)), CAST(266 AS Numeric(18, 0)), CAST(81 AS Numeric(18, 0)), N'1397/12/12', N'08:22     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1326 AS Numeric(18, 0)), CAST(266 AS Numeric(18, 0)), CAST(150 AS Numeric(18, 0)), N'1397/12/12', N'10:11     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1327 AS Numeric(18, 0)), CAST(266 AS Numeric(18, 0)), CAST(151 AS Numeric(18, 0)), N'1398/01/26', N'11:47     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1328 AS Numeric(18, 0)), CAST(266 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1398/01/28', N'08:31     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1329 AS Numeric(18, 0)), CAST(263 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1398/01/28', N'08:50     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1330 AS Numeric(18, 0)), CAST(266 AS Numeric(18, 0)), CAST(92 AS Numeric(18, 0)), N'1398/01/29', N'09:08     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1331 AS Numeric(18, 0)), CAST(259 AS Numeric(18, 0)), CAST(92 AS Numeric(18, 0)), N'1398/01/29', N'09:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1332 AS Numeric(18, 0)), CAST(258 AS Numeric(18, 0)), CAST(92 AS Numeric(18, 0)), N'1398/01/29', N'09:09     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1333 AS Numeric(18, 0)), CAST(262 AS Numeric(18, 0)), CAST(135 AS Numeric(18, 0)), N'1398/01/31', N'09:29     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1334 AS Numeric(18, 0)), CAST(266 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'1398/02/30', N'09:23     ')
INSERT [dbo].[tblacc] ([id_acc], [news_id_acc], [user_id_acc], [dte_acc], [tim_acc]) VALUES (CAST(1335 AS Numeric(18, 0)), CAST(260 AS Numeric(18, 0)), CAST(105 AS Numeric(18, 0)), N'1398/03/23', N'08:12     ')
SET IDENTITY_INSERT [dbo].[tblacc] OFF
/****** Object:  View [dbo].[گزارش پیام همکار]    Script Date: 06/22/2019 10:47:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[گزارش پیام همکار]
AS
SELECT        TOP (100) PERCENT dbo.tbluser.lname_user, dbo.tbluser.fname_user, dbo.tblcategory.name_cat, COUNT(*) AS tedad
FROM            dbo.tblnews INNER JOIN
                         dbo.tblcategory ON dbo.tblnews.cat_id_news = dbo.tblcategory.id_cat INNER JOIN
                         dbo.tbluser ON dbo.tblnews.user_id_news = dbo.tbluser.id_user
GROUP BY dbo.tblcategory.group_cat, dbo.tbluser.lname_user, dbo.tbluser.fname_user, dbo.tblcategory.name_cat
HAVING        (dbo.tblcategory.group_cat = 7)
ORDER BY tedad DESC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[21] 4[31] 2[19] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tblnews"
            Begin Extent = 
               Top = 0
               Left = 253
               Bottom = 200
               Right = 423
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "tblcategory"
            Begin Extent = 
               Top = 3
               Left = 450
               Bottom = 135
               Right = 620
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbluser"
            Begin Extent = 
               Top = 20
               Left = 17
               Bottom = 150
               Right = 187
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 26
         Width = 284
         Width = 2610
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrd' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'گزارش پیام همکار'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'er = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'گزارش پیام همکار'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'گزارش پیام همکار'
GO
/****** Object:  View [dbo].[گزارش پیامهای رویت شده]    Script Date: 06/22/2019 10:47:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[گزارش پیامهای رویت شده]
AS
SELECT        TOP (100) PERCENT dbo.tbluser.lname_user, dbo.tbluser.fname_user, dbo.tblcategory.name_cat, COUNT(*) AS tedad
FROM            dbo.tblnews INNER JOIN
                         dbo.tblcategory ON dbo.tblnews.cat_id_news = dbo.tblcategory.id_cat INNER JOIN
                         dbo.tblacc ON dbo.tblnews.id_news = dbo.tblacc.news_id_acc INNER JOIN
                         dbo.tbluser ON dbo.tblacc.user_id_acc = dbo.tbluser.id_user
GROUP BY dbo.tblcategory.group_cat, dbo.tbluser.lname_user, dbo.tbluser.fname_user, dbo.tblcategory.name_cat
ORDER BY tedad DESC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tblnews"
            Begin Extent = 
               Top = 19
               Left = 202
               Bottom = 149
               Right = 388
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblcategory"
            Begin Extent = 
               Top = 19
               Left = 0
               Bottom = 149
               Right = 186
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbluser"
            Begin Extent = 
               Top = 6
               Left = 620
               Bottom = 136
               Right = 806
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblacc"
            Begin Extent = 
               Top = 20
               Left = 396
               Bottom = 171
               Right = 582
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'گزارش پیامهای رویت شده'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'گزارش پیامهای رویت شده'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'گزارش پیامهای رویت شده'
GO
/****** Object:  ForeignKey [FK_tblacc_tblnews]    Script Date: 06/22/2019 10:47:13 ******/
ALTER TABLE [dbo].[tblacc]  WITH CHECK ADD  CONSTRAINT [FK_tblacc_tblnews] FOREIGN KEY([news_id_acc])
REFERENCES [dbo].[tblnews] ([id_news])
GO
ALTER TABLE [dbo].[tblacc] CHECK CONSTRAINT [FK_tblacc_tblnews]
GO
/****** Object:  ForeignKey [FK_tblacc_tbluser]    Script Date: 06/22/2019 10:47:13 ******/
ALTER TABLE [dbo].[tblacc]  WITH CHECK ADD  CONSTRAINT [FK_tblacc_tbluser] FOREIGN KEY([user_id_acc])
REFERENCES [dbo].[tbluser] ([id_user])
GO
ALTER TABLE [dbo].[tblacc] CHECK CONSTRAINT [FK_tblacc_tbluser]
GO
/****** Object:  ForeignKey [FK_tblimmnews_tbluser]    Script Date: 06/22/2019 10:47:13 ******/
ALTER TABLE [dbo].[tblnews]  WITH CHECK ADD  CONSTRAINT [FK_tblimmnews_tbluser] FOREIGN KEY([user_id_news])
REFERENCES [dbo].[tbluser] ([id_user])
GO
ALTER TABLE [dbo].[tblnews] CHECK CONSTRAINT [FK_tblimmnews_tbluser]
GO
/****** Object:  ForeignKey [FK_tblnews_tblcategory]    Script Date: 06/22/2019 10:47:13 ******/
ALTER TABLE [dbo].[tblnews]  WITH CHECK ADD  CONSTRAINT [FK_tblnews_tblcategory] FOREIGN KEY([cat_id_news])
REFERENCES [dbo].[tblcategory] ([id_cat])
GO
ALTER TABLE [dbo].[tblnews] CHECK CONSTRAINT [FK_tblnews_tblcategory]
GO
/****** Object:  ForeignKey [FK_tblslide_tblcategory]    Script Date: 06/22/2019 10:47:13 ******/
ALTER TABLE [dbo].[tblslide]  WITH CHECK ADD  CONSTRAINT [FK_tblslide_tblcategory] FOREIGN KEY([cat_id_slide])
REFERENCES [dbo].[tblcategory] ([id_cat])
GO
ALTER TABLE [dbo].[tblslide] CHECK CONSTRAINT [FK_tblslide_tblcategory]
GO
/****** Object:  ForeignKey [FK_tblslide_tbluser]    Script Date: 06/22/2019 10:47:13 ******/
ALTER TABLE [dbo].[tblslide]  WITH CHECK ADD  CONSTRAINT [FK_tblslide_tbluser] FOREIGN KEY([user_id_slide])
REFERENCES [dbo].[tbluser] ([id_user])
GO
ALTER TABLE [dbo].[tblslide] CHECK CONSTRAINT [FK_tblslide_tbluser]
GO
/****** Object:  ForeignKey [FK_tblsoft_tblcategory]    Script Date: 06/22/2019 10:47:13 ******/
ALTER TABLE [dbo].[tblsoft]  WITH CHECK ADD  CONSTRAINT [FK_tblsoft_tblcategory] FOREIGN KEY([cat_id_soft])
REFERENCES [dbo].[tblcategory] ([id_cat])
GO
ALTER TABLE [dbo].[tblsoft] CHECK CONSTRAINT [FK_tblsoft_tblcategory]
GO
/****** Object:  ForeignKey [FK_tblsoft_tbluser]    Script Date: 06/22/2019 10:47:13 ******/
ALTER TABLE [dbo].[tblsoft]  WITH CHECK ADD  CONSTRAINT [FK_tblsoft_tbluser] FOREIGN KEY([user_id_soft])
REFERENCES [dbo].[tbluser] ([id_user])
GO
ALTER TABLE [dbo].[tblsoft] CHECK CONSTRAINT [FK_tblsoft_tbluser]
GO
