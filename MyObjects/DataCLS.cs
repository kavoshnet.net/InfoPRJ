﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfoModel;
namespace MyObjects
{

    public static class DataCLS
    {
        public static string MemberVisibleNews(long news_id)
        {
            InfoModel.InfoDBEntities _edbcontext = new InfoModel.InfoDBEntities();
            var _news = (from n in _edbcontext.tblacc
                        where n.news_id_acc == news_id
                        select new { nameuser = n.tbluser.lname_user + " " + n.tbluser.fname_user }).ToList();
            string retvalue = "";
            foreach (var item in _news)
            {
                retvalue = retvalue+item.nameuser + ",";
            }
            if (retvalue != "")
                retvalue = retvalue.Substring(0, retvalue.Length - 1);
            return retvalue;

        }
    }

}

