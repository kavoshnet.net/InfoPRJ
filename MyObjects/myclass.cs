﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfoModel;
using System.Globalization;
namespace MyObjects
{
    //*********************actlevel*****************************************************
    public static class myclass
    {
        public static Boolean validshmeli(string shmeli)
        {
            int temp;
            int i;
            Boolean answer;
            i = 0;
            temp = 0;
            if (shmeli.Length == 9)
                shmeli = "0" + shmeli;
            if (shmeli.Length == 8)
                shmeli = "00" + shmeli;
            if (shmeli.Equals("0000000000") || shmeli.Equals("1111111111") || shmeli.Equals("2222222222") ||
                shmeli.Equals("3333333333")
                || shmeli.Equals("4444444444") || shmeli.Equals("5555555555") || shmeli.Equals("6666666666") ||
                shmeli.Equals("7777777777")
                || shmeli.Equals("8888888888") || shmeli.Equals("9999999999") || shmeli.Length < 10)
                return false;
            while (i < shmeli.Length - 1)
            {
                temp = temp + int.Parse((shmeli.Substring(i, 1))) * (10 - i);
                i = i + 1;
            }
            temp = temp % 11;
            if ((temp < 2 && int.Parse(shmeli.Substring(9, 1)) == temp) ||
                (temp >= 2 && int.Parse(shmeli.Substring(9, 1)) == 11 - temp))
                answer = true;
            else
                answer = false;
            return answer;
        }
        public static string longshamsidate(DateTime now)
        {
            string[] persiandayname = { "شنبه", "یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنج شنبه", "جمعه" };
            string[] persianmonthname = { "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند" };
            PersianCalendar pc = new PersianCalendar();
            string pccalender = "امروز: " + persiandayname[((int)pc.GetDayOfWeek(now) + 1) % 7] + " " + pc.GetDayOfMonth(now).ToString() + " " + persianmonthname[pc.GetMonth(now) - 1] + " " + pc.GetYear(now);
            return pccalender;
        }
        public static string SubString(object Text, object Length)
        {
            string StringText = Text.ToString();
            int StringLength = int.Parse(Length.ToString());
            if (StringText.Length > StringLength)
            {
                return StringText.Substring(0, StringLength) + "...";
            }
            else
            {
                return StringText;
            }
        }

    }
    //********************************************************************************************
    public enum grouplevel
    {
        payam = 1,
        narmafzar = 2,
        etela = 3,
        eslaid = 4,
        dastor = 5,
        payamfori = 6,
        payamhamkar = 7,
        narmafzaromomi = 8,
        driver = 9,
        narmafzaredari = 10,
        karthoshmand = 11,
        archive = 12,
        dcu = 13,
        abc = 14,
        estelam = 15,
        payameslaider = 16,
    }

    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
    //********************************************************************************************
}